#include "hdx/hdx9.h"
#include "hdx/hdx9app.h"
#include "hdx/hdx9font.h"
#include "hdx/hdx9texture.h"
#include "hdx/hdx9inputmanager.h"

#include "hdx/hdx9vertex.h"

#include <fstream>
#include <vector>
#include "hdx/entity.h"
#include "hdx/sound/hdx9sound.h"
#include "hdx/spacialpartition.h"
#include "hdx/physics/physics.h"

LRESULT WINAPI HDX9::_WndProc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam) {
	HDX_MAIN->mfunc_onwndproc.doIterate([&](std::function<void(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam)> &difunc)->bool {
		difunc(hwnd,msg,wparam,lparam);
		return false;
	});

	HDX_INPUT->_procMessage(hwnd,msg,wparam,lparam);

	switch(msg) {
		case WM_PAINT: {
			InvalidateRect(hwnd,0,true);
			break;
		}
		case WM_CLOSE:
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}
		case WM_EXITSIZEMOVE: {
			RECT rect;
			GetClientRect(hwnd,&rect);
			HDX_MAIN->md3dpp.BackBufferWidth = rect.right;
			HDX_MAIN->md3dpp.BackBufferHeight = rect.bottom;
			HDX_MAIN->_onLostDevice();
			HDX_MAIN->_resetDevice();
			HDX_MAIN->_onResetDevice();
			return 0;
		}
	}

	return DefWindowProc(hwnd,msg,wparam,lparam);
}

HDX9::HDX9() {
	rand_generator.seed((unsigned int)time(0));
	srand((unsigned int)time(0));

	mcom_d3d = 0;
	mcom_d3ddevice = 0;
	mcom_d3dsprite = 0;
	mshader_cur = 0;
	mwnd_clearcolor = 0xff000055; // dark blue
	mwnd_fullscreen = false;
	mwnd_borderless = false;
	mwnd_vsync = true;
	mframes_cur = mframes_prev = -1; //mframes_cur being -1 indicates the first frame
	mframes_prevtime = mupdate_prevtime = std::chrono::high_resolution_clock::now();
	mwnd_windowedwidth = 640;
	mwnd_windowedheight = 480;
	mwnd_windowedx = mwnd_windowedy = 0;
	mtimescale = 1.0f;

	setCameraDefault();
	setShader(0);
}

HDX9::~HDX9() {
}

void HDX9::initializeNewWindow(HINSTANCE hinstance,int x,int y,int w,int h) {
	mwnd_mode = WNM_NEWWINDOW;

	mhinstance = hinstance;

	WNDCLASSEX wndclass;
	memset(&wndclass,0,sizeof(wndclass));

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.lpfnWndProc = (WNDPROC)HDX9::_WndProc;
	wndclass.lpszClassName = WNDCLASSNAME;
	wndclass.hInstance = mhinstance;
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)(COLOR_WINDOWFRAME);

	RegisterClassEx(&wndclass);

	DWORD windowstyle = _getWindowStyle(mwnd_fullscreen,!mwnd_borderless);

	RECT rect_window,rect_client;

	SetRect(&rect_client,x,y,x+w,y+h);
	rect_window = rect_client;
	AdjustWindowRect(&rect_window,windowstyle,false);

	mwnd_hwnd = CreateWindowEx(0,
							   WNDCLASSNAME,
							   mwnd_title.c_str(),
							   windowstyle,
							   rect_window.left,
							   rect_window.top,
							   rect_window.right-rect_window.left,
							   rect_window.bottom-rect_window.top,
							   0,
							   0,
							   mhinstance,
							   0);

	if(mwnd_hwnd==0) {
		DXERR_FATAL("Failed to create application window.");
		return;
	}

	ShowWindow(mwnd_hwnd,SW_SHOW);
	UpdateWindow(mwnd_hwnd);

	_attachDXToWindow();
}

void HDX9::initializeEmbeddedWindow(HWND wnd) {
	mwnd_mode = WNM_EMBEDDEDWINDOW;

	mwnd_hwnd = wnd;
	mhinstance = (HINSTANCE)GetWindowLong(mwnd_hwnd,GWL_HINSTANCE);

	_attachDXToWindow();
}

void HDX9::_attachDXToWindow() {
	RECT rect_client;
	GetClientRect(mwnd_hwnd,&rect_client);

	mcom_d3d = Direct3DCreate9(D3D_SDK_VERSION);
	if(mcom_d3d == 0) {
		DXERR_FATAL("Could not retrieve D3D COM.");
		return;
	}

	mdevicebehaviorflags = 0;
	mcom_d3d->GetDeviceCaps(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,&md3dcaps);
	if(md3dcaps.TextureCaps & D3DPTEXTURECAPS_CUBEMAP_POW2) {
		DXERR_FATAL("Device does not have required capabilities.");
		return;
	}
	if(md3dcaps.DevCaps & D3DCREATE_HARDWARE_VERTEXPROCESSING) {
		mdevicebehaviorflags |= D3DCREATE_HARDWARE_VERTEXPROCESSING;
	} else {
		mdevicebehaviorflags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}
	if((md3dcaps.DevCaps&D3DDEVCAPS_PUREDEVICE) && (mdevicebehaviorflags&D3DCREATE_HARDWARE_VERTEXPROCESSING)) {
		mdevicebehaviorflags |= D3DCREATE_PUREDEVICE;
	}

	memset(&md3dpp,0,sizeof(D3DPRESENT_PARAMETERS));

	md3dpp.hDeviceWindow = mwnd_hwnd;
	md3dpp.Windowed = true;
	md3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;
	md3dpp.EnableAutoDepthStencil = true;
	md3dpp.BackBufferCount = 1;
	md3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
	md3dpp.BackBufferWidth = rect_client.right-rect_client.left;
	md3dpp.BackBufferHeight = rect_client.bottom-rect_client.top;
	md3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	md3dpp.PresentationInterval = mwnd_vsync ? D3DPRESENT_INTERVAL_DEFAULT : D3DPRESENT_INTERVAL_IMMEDIATE;
	md3dpp.Flags = D3DPRESENTFLAG_DISCARD_DEPTHSTENCIL;
	md3dpp.FullScreen_RefreshRateInHz = !mwnd_fullscreen ? 0 : D3DPRESENT_RATE_DEFAULT;
	md3dpp.MultiSampleQuality = 2;//0
	md3dpp.MultiSampleType = D3DMULTISAMPLE_NONMASKABLE;//D3DMULTISAMPLE_NONE

	mcom_d3d->CreateDevice(D3DADAPTER_DEFAULT,
						   D3DDEVTYPE_HAL,
						   mwnd_hwnd,
						   HDX_MAIN->instance()->mdevicebehaviorflags,
						   &md3dpp,
						   &mcom_d3ddevice);

	if(mcom_d3ddevice==0) {
		DXERR_FATAL("Failed to create D3D device.");
		return;
	}

	D3DXCreateSprite(mcom_d3ddevice,&mcom_d3dsprite);

	if(mcom_d3dsprite==0) {
		DXERR_FATAL("Failed to create D3D Sprite.");
		return;
	}

	if(!HDXVERTP::init(mcom_d3ddevice)) {
		DXERR_FATAL("Failed to create D3D vertex decleration P.");
		return;
	}

	if(!HDXVERTPN::init(mcom_d3ddevice)) {
		DXERR_FATAL("Failed to create D3D vertex decleration PN.");
		return;
	}

	if(!HDXVERTPTN::init(mcom_d3ddevice)) {
		DXERR_FATAL("Failed to create D3D vertex decleration PTN.");
		return;
	}

	if(!HDXVERTPCTN::init(mcom_d3ddevice)) {
		DXERR_FATAL("Failed to create D3D vertex decleration PCTN.");
		return;
	}

	mcom_d3ddevice->SetVertexDeclaration(HDXVERTPTN::decl);

	//call once to ensure everything is initialized
	_onLostDevice();
	_resetDevice();
	_onResetDevice();

	mcom_d3ddevice->SetRenderState(D3DRS_LIGHTING,false);

	// initialize the various managers
	HDX_PHYMAN;
	HDX_ENTMAN;
	HDX_INPUT;
	HDX_SPMAN;
	HDX_SOUNDMAN;

	/*std::ofstream os("resolution.txt");
	D3DDISPLAYMODE mode;
	for(int i = 0; i < mcom_d3d->GetAdapterCount(); i++) {
	UINT num = mcom_d3d->GetAdapterModeCount(i,D3DFMT_X8R8G8B8);
	for(int j = 0; j < num; j++) {
	mcom_d3d->EnumAdapterModes(i,D3DFMT_X8R8G8B8,j,&mode);
	os << "Width = "  << mode.Width << " ";
	os << "Height = " << mode.Height << " ";
	os << "Format = " << mode.Format << " ";
	os << "Refresh = " << mode.RefreshRate << '\n';
	}
	}
	os.close();*/
}

void HDX9::deinitialize() {
	//release COMs
	if(mcom_d3dsprite) mcom_d3dsprite->Release();

	if(mcom_d3ddevice) mcom_d3ddevice->Release();

	if(mcom_d3d) mcom_d3d->Release();

	switch(mwnd_mode) {
		case WNM_NEWWINDOW: {
			if(mwnd_hwnd) DestroyWindow(mwnd_hwnd);
			UnregisterClass(WNDCLASSNAME,mhinstance);
			break;
		}
		case WNM_EMBEDDEDWINDOW: {
			break;
		}
	}
}

std::string HDX9::getWindowTitle() {
	return mwnd_title;
}

void HDX9::setWindowTitle(const std::string &title) {
	mwnd_title = title;
	SetWindowText(mwnd_hwnd,title.c_str());
}

bool HDX9::getWindowBorderless() {
	return mwnd_borderless;
}

void HDX9::setWindowBorderless(bool borderless) {
	if(mwnd_borderless != borderless) {
		if(!mwnd_fullscreen) {
			mwnd_borderless = borderless;

			DWORD newstyle = _getWindowStyle(mwnd_fullscreen,!mwnd_borderless);

			RECT rect_window,rect_client;
			GetClientRect(mwnd_hwnd,&rect_client);
			ClientToScreen(mwnd_hwnd,reinterpret_cast<POINT*>(&rect_client.left));
			ClientToScreen(mwnd_hwnd,reinterpret_cast<POINT*>(&rect_client.right));
			rect_window = rect_client;
			AdjustWindowRect(&rect_window,newstyle,false);

			SetWindowLong(mwnd_hwnd,GWL_STYLE,newstyle);
			SetWindowPos(mwnd_hwnd,borderless ? HWND_TOPMOST : HWND_TOP,
						 rect_window.left,
						 rect_window.top,
						 rect_window.right-rect_window.left,
						 rect_window.bottom-rect_window.top,
						 SWP_NOZORDER|SWP_SHOWWINDOW);
		}
	}
}

void HDX9::getWindowPosition(int *x,int *y) {
	RECT rect;
	GetClientRect(mwnd_hwnd,&rect);
	ClientToScreen(mwnd_hwnd,reinterpret_cast<POINT*>(&rect.left));
	ClientToScreen(mwnd_hwnd,reinterpret_cast<POINT*>(&rect.right));
	*x = rect.left;
	*y = rect.top;
}

void HDX9::setWindowPosition(int x,int y) {
	int w,h;
	getWindowSize(&w,&h);

	RECT rect_window,rect_client;

	SetRect(&rect_client,x,y,x+w,y+h);
	rect_window = rect_client;
	AdjustWindowRect(&rect_window,_getWindowStyle(mwnd_fullscreen,!mwnd_borderless),false);

	SetWindowPos(mwnd_hwnd,HWND_TOP,rect_window.left,rect_window.top,0,0,SWP_NOSIZE);
}

void HDX9::getWindowSize(int *w,int *h) {
	RECT rect;
	GetClientRect(mwnd_hwnd,&rect);
	if(w) *w = rect.right - rect.left;
	if(h) *h = rect.bottom - rect.top;
}

int HDX9::getWindowWidth() {
	int ret;
	getWindowSize(&ret,0);
	return ret;
}

int HDX9::getWindowHeight() {
	int ret;
	getWindowSize(0,&ret);
	return ret;
}

void HDX9::setWindowSize(int w,int h) {
	int x,y;
	getWindowPosition(&x,&y);

	RECT rect_window,rect_client;

	SetRect(&rect_client,x,y,x+w,y+h);
	rect_window = rect_client;
	AdjustWindowRect(&rect_window,_getWindowStyle(mwnd_fullscreen,!mwnd_borderless),false);

	SetWindowPos(mwnd_hwnd,
				 HWND_TOP,
				 0,
				 0,
				 rect_window.right-rect_window.left,
				 rect_window.bottom-rect_window.top,
				 SWP_NOMOVE);

	md3dpp.BackBufferWidth = RECTWIDTH(rect_client);
	md3dpp.BackBufferHeight = RECTHEIGHT(rect_client);
	_onLostDevice();
	_resetDevice();
	_onResetDevice();
}

HWND HDX9::getWindowHWND() {
	return mwnd_hwnd;
}

HINSTANCE HDX9::getHINSTANCE() {
	return mhinstance;
}

DWORD HDX9::getWindowCurrentStyle() {
	return _getWindowStyle(mwnd_fullscreen,!mwnd_borderless);
}

D3DCOLOR HDX9::getWindowClearColor() {
	return mwnd_clearcolor;
}

void HDX9::setWindowClearColor(D3DCOLOR color) {
	mwnd_clearcolor = color;
}

bool HDX9::getWindowFullscreen() {
	return mwnd_fullscreen;
}

void HDX9::setWindowFullscreen(bool fullscreen) {
	if(mwnd_fullscreen != fullscreen) {
		mwnd_fullscreen = fullscreen;

		int width,height;
		int x,y;
		if(fullscreen) {
			getWindowSize(&mwnd_windowedwidth,&mwnd_windowedheight);
			getWindowPosition(&mwnd_windowedx,&mwnd_windowedy);

			width = GetSystemMetrics(SM_CXSCREEN);
			height = GetSystemMetrics(SM_CYSCREEN);
			x = 0;
			y = 0;
		} else {
			width = mwnd_windowedwidth;
			height = mwnd_windowedheight;
			x = mwnd_windowedx;
			y = mwnd_windowedy;
		}

		md3dpp.BackBufferWidth = width;
		md3dpp.BackBufferHeight = height;
		// 		md3dpp.Windowed = !fullscreen;
		// 		md3dpp.FullScreen_RefreshRateInHz = !fullscreen ? 0 : D3DPRESENT_RATE_DEFAULT;

		DWORD windowstyle = _getWindowStyle(mwnd_fullscreen,!mwnd_borderless);

		RECT rect_window,rect_client;

		SetRect(&rect_client,x,y,x+width,y+height);
		rect_window = rect_client;
		AdjustWindowRect(&rect_window,windowstyle,false);

		SetWindowLong(mwnd_hwnd,GWL_STYLE,windowstyle);
		SetWindowPos(mwnd_hwnd,
					 HWND_TOP,
					 rect_window.left,
					 rect_window.top,
					 rect_window.right-rect_window.left,
					 rect_window.bottom-rect_window.top,
					 SWP_NOZORDER|SWP_SHOWWINDOW);

		_onLostDevice();
		_resetDevice();
		_onResetDevice();
	}
}

bool HDX9::getWindowVsync() {
	return mwnd_vsync;
}

void HDX9::setWindowVsync(bool vsync) {
	if(mwnd_vsync != vsync) {
		mwnd_vsync = vsync;

		md3dpp.PresentationInterval = vsync ? D3DPRESENT_INTERVAL_DEFAULT : D3DPRESENT_INTERVAL_IMMEDIATE;

		_onLostDevice();
		_resetDevice();
		_onResetDevice();
	}
}

IDirect3DDevice9* HDX9::getD3DDevice() {
	return mcom_d3ddevice;
}

ID3DXSprite* HDX9::getD3DSprite() {
	return mcom_d3dsprite;
}

D3DPRESENT_PARAMETERS* HDX9::getD3DPresentationParameters() {
	return &md3dpp;
}

HDXCamera* HDX9::getCameraCurrent() {
	return mcamera_cur;
}

void HDX9::setCamera(HDXCamera *camera) {
	mcamera_cur = camera;
}

void HDX9::setCameraDefault() {
	mcamera_cur = &mcamera_default;
}

HDXShader_Base* HDX9::getShaderCurrent() {
	return mshader_cur;
}

void HDX9::setShader(HDXShader_Base *shader) {
	mshader_cur = shader;
}

unsigned int HDX9::getFPS() {
	return mframes_prev != -1 ? mframes_prev : mframes_cur;
}

float HDX9::getTimeScale() {
	return mtimescale;
}

void HDX9::setTimeScale(float scale) {
	mtimescale = scale;
}

void HDX9::step() {
	if(mcom_d3ddevice) do {
		//check for and deal with lost devices
			{
				HRESULT hr = mcom_d3ddevice->TestCooperativeLevel();
				if(hr == D3DERR_DEVICELOST) {
					Sleep(20);
					break;
				} else if(hr == D3DERR_DRIVERINTERNALERROR) {
					DXERR_FATAL("Internal Driver Error.");
					break;
				} else if(hr == D3DERR_DEVICENOTRESET) {
					_onLostDevice();
					if(_resetDevice()) break; //error messages are produced inside function
					_onResetDevice();
				}
			}

		std::chrono::high_resolution_clock::time_point curtime = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> updatetime = mframes_cur!=-1 ? (curtime-mupdate_prevtime) : std::chrono::duration<double>(0.0f);

		if(std::chrono::duration_cast<std::chrono::milliseconds>(curtime-mframes_prevtime).count() >= 1000) {
			mframes_prevtime = curtime;
			mframes_prev = mframes_cur;
			mframes_cur = 0;
		}

		mframes_cur++;
		mupdate_prevtime = curtime;

		float updatedt = (float)updatetime.count();

		//for breakpoint continuity
#ifdef _DEBUG
		updatedt = 1.0f/60.0f;
#endif

		updatedt *= mtimescale;
		updatedt = (std::min)(updatedt,TIME_MAXDT*mtimescale);

		mfunc_update_pre.doIterate([&](std::function<void(float dt)> &difunc)->bool {
			difunc(updatedt);
			return false;
		});

		mfunc_update.doIterate([&](std::function<void(float dt)> &difunc)->bool {
			difunc(updatedt);
			return false;
		});

		mfunc_update_post0.doIterate([&](std::function<void(float dt)> &difunc)->bool {
			difunc(updatedt);
			return false;
		});

		mfunc_update_post1.doIterate([&](std::function<void(float dt)> &difunc)->bool {
			difunc(updatedt);
			return false;
		});

		mcom_d3ddevice->Clear(0,0,D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,mwnd_clearcolor,1.0f,0);
		mcom_d3ddevice->BeginScene();

		if(mcamera_cur) mcom_d3ddevice->SetTransform(D3DTS_VIEW,mcamera_cur->getViewMatrix().getD3DX());

		mfunc_render_device.doIterate([&](std::function<void(IDirect3DDevice9 *com)> &difunc)->bool {
			difunc(mcom_d3ddevice);
			return false;
		});

		if(mshader_cur) {
			mshader_cur->doDraw([&](HDXShader_Base *hdxfx)->void {
				hdxfx->setWVPFromDevice();
				mfunc_render_shader.doIterate([&](std::function<void(HDXShader_Base *shader)> &difunc)->bool {
					difunc(hdxfx);
					return false;
				});
			});
		}

		mcom_d3dsprite->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_DEPTH_BACKTOFRONT);
		mfunc_render_sprite.doIterate([&](std::function<void(ID3DXSprite *com)> &difunc)->bool {
			difunc(mcom_d3dsprite);
			return false;
		});
		mcom_d3dsprite->End();

		mcom_d3ddevice->EndScene();
		mcom_d3ddevice->Present(0,0,0,0);
	} while(0);
}

void HDX9::_onLostDevice() {
	mcom_d3dsprite->OnLostDevice();
	HDX_FONTMAN->_onLostDevice();

	mfunc_ondevice_lost.doIterate([&](std::function<void(void)> &difunc)->bool {
		difunc();
		return false;
	});
}

HRESULT HDX9::_resetDevice() {
	HRESULT hr = mcom_d3ddevice->Reset(&md3dpp);
	DXERR_FATAL_IF(hr,"Failed to reinitialize the D3D Device.");
	return hr;
}

void HDX9::_onResetDevice() {
	mcom_d3dsprite->OnResetDevice();
	HDX_FONTMAN->_onResetDevice();

	mfunc_ondevice_reset.doIterate([&](std::function<void(void)> &difunc)->bool {
		difunc();
		return false;
	});

	{
		HDXMatrix4x4 proj;

		D3DXMatrixPerspectiveFovLH(proj.getD3DX(),
								   45.0f,
								   (float)md3dpp.BackBufferWidth/(float)md3dpp.BackBufferHeight,
								   1.0f,
								   7500.0f);

		mcom_d3ddevice->SetTransform(D3DTS_PROJECTION,proj.getD3DX());
	}
}

DWORD HDX9::_getWindowStyle(bool fullscreen,bool border) {
	return (!fullscreen && border ? WS_OVERLAPPEDWINDOW : WS_POPUP) | WS_VISIBLE;
}

int HDX9::registerFuncOnDeviceLost(std::function<void(void)> &&func) {
	return mfunc_ondevice_lost.addItem(func);
}

void HDX9::unregisterFuncOnDeviceLost(int id) {
	mfunc_ondevice_lost.removeItem(id);
}

int HDX9::registerFuncOnDeviceReset(std::function<void(void)> &&func) {
	return mfunc_ondevice_reset.addItem(func);
}

void HDX9::unregisterFuncOnDeviceReset(int id) {
	mfunc_ondevice_reset.removeItem(id);
}

int HDX9::registerFuncOnWndProc(std::function<void(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam)> &&func) {
	return mfunc_onwndproc.addItem(func);
}

void HDX9::unregisterFuncOnWndProc(int id) {
	mfunc_onwndproc.removeItem(id);
}

int HDX9::registerFuncUpdatePre(std::function<void(float dt)> &&func) {
	return mfunc_update_pre.addItem(func);
}

void HDX9::unregisterFuncUpdatePre(int id) {
	mfunc_update_pre.removeItem(id);
}

int HDX9::registerFuncUpdate(std::function<void(float dt)> &&func) {
	return mfunc_update.addItem(func);
}

void HDX9::unregisterFuncUpdate(int id) {
	mfunc_update.removeItem(id);
}

int HDX9::registerFuncUpdatePost0(std::function<void(float dt)> &&func) {
	return mfunc_update_post0.addItem(func);
}

void HDX9::unregisterFuncUpdatePost0(int id) {
	mfunc_update_post0.removeItem(id);
}

int HDX9::registerFuncUpdatePost1(std::function<void(float dt)> &&func) {
	return mfunc_update_post1.addItem(func);
}

void HDX9::unregisterFuncUpdatePost1(int id) {
	mfunc_update_post1.removeItem(id);
}

int HDX9::registerFuncRenderDevice(std::function<void(IDirect3DDevice9 *com)> &&func) {
	return mfunc_render_device.addItem(func);
}

void HDX9::unregisterFuncRenderDevice(int id) {
	mfunc_render_device.removeItem(id);
}

int HDX9::registerFuncRenderSprite(std::function<void(ID3DXSprite *com)> &&func) {
	return mfunc_render_sprite.addItem(func);
}

void HDX9::unregisterFuncRenderSprite(int id) {
	mfunc_render_sprite.removeItem(id);
}

int HDX9::registerFuncRenderShader(std::function<void(HDXShader_Base *shader)> &&func) {
	return mfunc_render_shader.addItem(func);
}

void HDX9::unregisterFuncRenderShader(int id) {
	mfunc_render_shader.removeItem(id);
}
