#include "hdx/hdx9intersection.h"
#include "hdx/util.h"
#include "hdx/hdx9math.h"

bool HDX9IntersectionTests::box_point(const HDXVector3 *boxext,const HDXVector3 *boxpos,const HDXVector3 *point) {
	HDXVector3 pt = *point - *boxpos;
	for(int i = 0; i < 3; i++) {
		if(abs(pt[i]) >(*boxext)[i]) return false;
	}
	return true;
}

bool HDX9IntersectionTests::box_point(const RECT *box,const POINT *point) {
	if(point->x < box->left ||
	   point->x > box->right ||
	   point->y < box->top ||
	   point->y > box->bottom) {
		return false;
	}
	return true;
}

bool HDX9IntersectionTests::obb_point(const HDXVector3 *boxext,const HDXMatrix4x4 *trans,const HDXVector3 *point) {
	HDXVector3 relcenter = *point * trans->getInvert();

	for(int i = 0; i < 3; i++) {
		if(abs(relcenter[i]) >(*boxext)[i]) return false;
	}

	return true;
}

bool HDX9IntersectionTests::obb_obb(const HDXVector3 *boxext0,const HDXMatrix4x4 *trans0,const HDXVector3 *boxext1,const HDXMatrix4x4 *trans1) {
	struct OBB {
		const HDXVector3 *ext;
		const HDXMatrix4x4 *trans;
	};

	OBB obb0,obb1;
	obb0.ext = boxext0;
	obb0.trans = trans0;
	obb1.ext = boxext1;
	obb1.trans = trans1;

	auto transformToAxis = [&](OBB &obb,const HDXVector3 &axis)->float {
		return obb.ext->x * abs(axis*obb.trans->getRow(0)) +
			obb.ext->y * abs(axis*obb.trans->getRow(1)) +
			obb.ext->z * abs(axis*obb.trans->getRow(2));
	};

	auto overlapOnAxis = [&](OBB &obb0,OBB &obb1,const HDXVector3 &axis,const HDXVector3 &tocenter)->float {
		float proj[2];

		proj[0] = transformToAxis(obb0,axis);
		proj[1] = transformToAxis(obb1,axis);
		float distance = abs(tocenter*axis);

		return proj[0] + proj[1] - distance;
	};

	struct TEST {
		HDXVector3 axis;
		float overlap;
	};

	TEST tests[15] = {
		{obb0.trans->getRow(0),0},
		{obb0.trans->getRow(1),0},
		{obb0.trans->getRow(2),0},

		{obb1.trans->getRow(0),0},
		{obb1.trans->getRow(1),0},
		{obb1.trans->getRow(2),0},

		{(HDXVector3)obb0.trans->getRow(0)%obb1.trans->getRow(0),0},
		{(HDXVector3)obb0.trans->getRow(0)%obb1.trans->getRow(1),0},
		{(HDXVector3)obb0.trans->getRow(0)%obb1.trans->getRow(2),0},
		{(HDXVector3)obb0.trans->getRow(1)%obb1.trans->getRow(0),0},
		{(HDXVector3)obb0.trans->getRow(1)%obb1.trans->getRow(1),0},
		{(HDXVector3)obb0.trans->getRow(1)%obb1.trans->getRow(2),0},
		{(HDXVector3)obb0.trans->getRow(2)%obb1.trans->getRow(0),0},
		{(HDXVector3)obb0.trans->getRow(2)%obb1.trans->getRow(1),0},
		{(HDXVector3)obb0.trans->getRow(2)%obb1.trans->getRow(2),0},
	};

	HDXVector3 tocenter = obb1.trans->getRow(3) - obb0.trans->getRow(3);

	int best = 0,bestsingle = 0;
	for(int i = 0; i < 15; i++) {
		if(tests[i].axis.magnitudeSq() < 0.0001f) continue;
		tests[i].axis.setNormal();
		tests[i].overlap = overlapOnAxis(obb0,obb1,tests[i].axis,tocenter);
		if(tests[i].overlap < 0) return false; // no collision if there is a separating axis
		if(tests[i].overlap < tests[best].overlap) {
			if(i < 6) bestsingle = i;
			best = i;
		}
	}

	return true;
}