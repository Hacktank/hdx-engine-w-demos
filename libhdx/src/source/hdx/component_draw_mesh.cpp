#include "hdx/component_draw_mesh.h"
#include "hdx/hdx9.h"
#include "hdx/hdx9mesh.h"
#include "hdx/hdx9material.h"
#include "hdx/entity.h"
#include "hdx/component_transform.h"

#include <algorithm>

component_Draw_Mesh::Item::Item() {
	mtrans.setIdentity();
	mrenderfunc = nullptr;
	mrenderphase = RP_NONE;
	mcbid_render = -1;
	mmesh = 0;
	mflag_wireframe = false;
	mrenderfunc = [&]()->void {
		if(mmesh) {
			//device->SetRenderState(D3DRS_FILLMODE,D3DFILL_WIREFRAME);

			HDXShader_Base *shader = HDX_MAIN->getShaderCurrent();

			DWORD tmpflag_fillmode;
			DWORD tmpflag_cullmode;
			shader->getD3DDevice()->GetRenderState(D3DRS_FILLMODE,&tmpflag_fillmode);

			if(isWireframe()) {
				shader->getD3DDevice()->GetRenderState(D3DRS_CULLMODE,&tmpflag_cullmode);
				shader->getD3DDevice()->SetRenderState(D3DRS_CULLMODE,D3DCULL_NONE);
			}

			shader->getD3DDevice()->SetRenderState(D3DRS_FILLMODE,isWireframe() ? D3DFILL_WIREFRAME : D3DFILL_SOLID);

			shader->pushMatrixWorld();
			shader->multMatrixWorld(mcomp->getEntity()->getComponent<component_Transform>()->getTransform() * mtrans);
			for(unsigned int i = 0; i < mmesh->getNumSubsets(); i++) {
				shader->setMaterial(mmesh->mmaterials[(std::min)(i,mmesh->mmaterials.size())]);
				shader->setTexture(mmesh->mtextures[(std::min)(i,mmesh->mtextures.size())]);
				mmesh->md3dmesh->DrawSubset(i);
			}
			shader->popMatrixWorld();

			if(isWireframe()) {
				shader->getD3DDevice()->SetRenderState(D3DRS_CULLMODE,tmpflag_cullmode);
			}

			shader->getD3DDevice()->SetRenderState(D3DRS_FILLMODE,tmpflag_fillmode);
		}
	};
	setRenderPhase(RP_SHADER);
}

component_Draw_Mesh::Item::~Item() {
	setRenderPhase(RP_NONE);
	if(mmesh) {
		mmesh->release();
	}
}

component_Draw_Mesh::Item::RENDERPHASE component_Draw_Mesh::Item::getRenderPhase() {
	return mrenderphase;
}

void component_Draw_Mesh::Item::setRenderPhase(RENDERPHASE phase) {
	if(mrenderphase != RP_NONE) {
		switch(mrenderphase) {
			case RP_DEVICE: {
				HDX_MAIN->unregisterFuncRenderDevice(mcbid_render);
				break;
			}
			case RP_SPRITE: {
				HDX_MAIN->unregisterFuncRenderSprite(mcbid_render);
				break;
			}
			case RP_SHADER: {
				HDX_MAIN->unregisterFuncRenderShader(mcbid_render);
				break;
			}
		}
	}

	mrenderphase = phase;
	switch(mrenderphase) {
		case RP_DEVICE: {
			mcbid_render = HDX_MAIN->registerFuncRenderDevice([&](IDirect3DDevice9 *com)->void {
				if(mrenderfunc != nullptr) mrenderfunc();
			});
			break;
		}
		case RP_SPRITE: {
			mcbid_render = HDX_MAIN->registerFuncRenderSprite([&](ID3DXSprite *com)->void {
				if(mrenderfunc != nullptr) mrenderfunc();
			});
			break;
		}
		case RP_SHADER: {
			mcbid_render = HDX_MAIN->registerFuncRenderShader([&](HDXShader_Base *shader)->void {
				if(mrenderfunc != nullptr) mrenderfunc();
			});
			break;
		}
	}
}

const HDXMatrix4x4& component_Draw_Mesh::Item::getTransform() const {
	return mtrans;
}

void component_Draw_Mesh::Item::setTransform(const HDXMatrix4x4 &trans) {
	mtrans = trans;
}

bool component_Draw_Mesh::Item::isWireframe() const {
	return mflag_wireframe;
}

void component_Draw_Mesh::Item::setWireframe(bool wireframe) {
	mflag_wireframe = wireframe;
}

HDXMesh* component_Draw_Mesh::Item::getMesh() {
	return mmesh;
}

const HDXMesh* component_Draw_Mesh::Item::getMesh() const {
	return mmesh;
}

component_Draw_Mesh::component_Draw_Mesh(Entity *entity) : component_Base(entity) {
}

component_Draw_Mesh::~component_Draw_Mesh() {
	for(auto itm : mitems) {
		delete itm;
	}
}

int component_Draw_Mesh::addMesh(const HDXMatrix4x4 &trans,HDXMesh *mesh,bool give_mesh) {
	Item *newitem = new Item();

	newitem->mcomp = this;
	newitem->mtype = Item::PT_MESH;
	newitem->mtrans = trans;
	newitem->mmesh = mesh;
	if(!give_mesh) mesh->addUser();

	return mitems.addItem(newitem);
}

int component_Draw_Mesh::addSphere(const HDXMatrix4x4 &trans,const HDXMaterial &mat,float rad) {
	Item *newitem = new Item();

	newitem->mcomp = this;
	newitem->mtype = Item::PT_SPHERE;
	newitem->mtrans = trans;
	newitem->mmesh = HDXMeshCreateSphere(rad,mat);

	return mitems.addItem(newitem);
}

int component_Draw_Mesh::addBox(const HDXMatrix4x4 &trans,const HDXMaterial &mat,const HDXVector3 &ext) {
	Item *newitem = new Item();

	newitem->mcomp = this;
	newitem->mtype = Item::PT_BOX;
	newitem->mtrans = trans;
	newitem->mmesh = HDXMeshCreateBox(ext,mat);

	return mitems.addItem(newitem);
}

int component_Draw_Mesh::addRod(const HDXMatrix4x4 &trans,const HDXMaterial &mat,float halflength) {
	Item *newitem = new Item();

	newitem->mcomp = this;
	newitem->mtype = Item::PT_LINE;
	newitem->mtrans = trans;
	newitem->mmesh = HDXMeshCreateRod(halflength,mat);

	return mitems.addItem(newitem);
}

component_Draw_Mesh::Item* component_Draw_Mesh::getItem(int id) const {
	return mitems.getItem(id);
}

void component_Draw_Mesh::removeItem(int id) {
	if(mitems.hasItem(id)) {
		delete mitems.getItem(id);
		mitems.removeItem(id);
	}
}

void component_Draw_Mesh::clear() {
	for(auto itm : mitems) {
		delete itm;
	}
}

BaseManagerID<component_Draw_Mesh::Item*>& component_Draw_Mesh::getItems() {
	return mitems;
}

const BaseManagerID<component_Draw_Mesh::Item*>& component_Draw_Mesh::getItems() const {
	return mitems;
}
