#include "hdx/component_transform.h"

component_Transform::component_Transform(Entity *entity) : component_Base(entity) {
	setPosition(0,0,0);
	setScale(1,1,1);
	setOrientation(1,0,0,0);
}

component_Transform::~component_Transform() {
}

HDXQuaternion* component_Transform::getOrientationPtr() {
	mchanged = true;
	return &mori;
}

void component_Transform::addOrientation(const HDXVector3 &localeulardelta) {
	mori += localeulardelta;
}

const HDXQuaternion& component_Transform::getOrientation() const {
	return mori;
}

void component_Transform::getOrientation(HDXQuaternion *orientation) const {
	*orientation = mori;
}

void component_Transform::setOrientation(const float w,const float x,const float y,const float z) {
	mori.x = x; mori.y = y; mori.z = z; mori.w = w; mchanged = true;
	mori.setNormal();
}

void component_Transform::setOrientation(const HDXQuaternion &orientation) {
	mori = orientation; mchanged = true;
	mori.setNormal();
}


HDXVector3* component_Transform::getScalePtr() {
	mchanged = true;
	return &mscale;
}

const HDXVector3& component_Transform::getScale() const {
	return mscale;
}

void component_Transform::getScale(HDXVector3 *scale) const {
	*scale = mscale;
}

void component_Transform::addScale(const HDXVector3 &scale) {
	mscale += scale; mchanged = true;
}

void component_Transform::setScale(const float x,const float y,const float z) {
	mscale.x = x; mscale.y = y; mscale.z = z; mchanged = true;
}

void component_Transform::setScale(const HDXVector3 &scale) {
	mscale = scale; mchanged = true;
}

HDXVector3* component_Transform::getPositionPtr() {
	mchanged = true;
	return &mpos;
}

const HDXVector3& component_Transform::getPosition() const {
	return mpos;
}

void component_Transform::getPosition(HDXVector3 *position) const {
	*position = mpos;
}

void component_Transform::addPosition(const HDXVector3 &position) {
	mpos += position; mchanged = true;
}

void component_Transform::setPosition(const float x,const float y,const float z) {
	mpos.x = x; mpos.y = y; mpos.z = z; mchanged = true;
}

void component_Transform::setPosition(const HDXVector3 &position) {
	mpos = position; mchanged = true;
}

const HDXMatrix4x4& component_Transform::getTransform() const {
	if(mchanged) {
		*(bool*)&mchanged = false;

		((HDXQuaternion*)&mori)->setNormal();

		HDXMatrix4x4 mat_rot = mori.toRotationMatrix();
		HDXMatrix4x4 mat_trans = HDXMatrix4x4::gen::translation3d(mpos);
		HDXMatrix4x4 mat_scale = HDXMatrix4x4::gen::scale3d(mscale);

		//get around the const flag
		*(HDXMatrix4x4*)&mtrans = mat_scale * mat_rot * mat_trans;
	}
	return mtrans;
}