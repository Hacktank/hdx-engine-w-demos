#include "hdx/entity.h"

#include "hdx/hdx9.h"

#include <algorithm>

Entity::Entity(int id) {
	mid = id;
	mflag_destroy = false;
	mflag_active = true;
}

Entity::~Entity() {
	for(auto comp : mcomponents) {
		auto &tvec = HDX_ENTMAN->mcomponents[comp->getName()];
		tvec.erase(std::remove(tvec.begin(),tvec.end(),comp),tvec.end());
		delete comp;
	}
	mcomponents.clear();
}

int Entity::getID() const {
	return mid;
}

bool Entity::isActive() const {
	return mflag_active && !mflag_destroy;
}

void Entity::setActive(bool active) {
	mflag_active = active;
}

void Entity::destroy() {
	mflag_destroy = true;
}

EntityManager::EntityManager() {
	mcbid_onupdate = HDX_MAIN->registerFuncUpdate([&](float dt)->void {
		std::vector<int> todestroy;
		mentities.doIterate([&](Entity *ent)->bool {
			if(ent->mflag_destroy) todestroy.push_back(ent->getID());
			return false;
		});
		for(auto it = todestroy.begin(); it != todestroy.end(); it++) {
			Entity *ent = getEntity(*it);
			if(ent) {
				delete ent;
				mentities.removeItem(*it);
			}
		}
	});
}

EntityManager::~EntityManager() {
	HDX_MAIN->unregisterFuncUpdate(mcbid_onupdate);
	clear();
}

Entity* EntityManager::createEntity() {
	Entity *comp = new Entity(mentities.getNVID());
	mentities.addItem(comp);
	return comp;
}

Entity* EntityManager::getEntity(int id) {
	if(mentities.hasItem(id)) {
		return mentities.getItem(id);
	}
	return 0;
}

void EntityManager::clear() {
	mentities.doIterate([&](Entity *ent)->bool {
		delete ent;
		return false;
	});
	mentities.clear();
}
