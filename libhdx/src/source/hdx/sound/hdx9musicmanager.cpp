
#include "hdx/sound/hdx9musicmanager.h"


HDX9MusicManager::HDX9MusicManager() {
	mchannel_cur = 0;
	msound_cur = 0;
}

HDX9MusicManager::~HDX9MusicManager() {

}

FMOD::Channel* HDX9MusicManager::getCurrentChannel() {
	return mchannel_cur;
}

HDX9Sound* HDX9MusicManager::getCurrentSound() {
	return msound_cur;
}

void HDX9MusicManager::controlPlay() {

}

void HDX9MusicManager::controlPause() {

}

void HDX9MusicManager::setTrackCrossfade(HDX9Sound *track,float fadetime) {
	struct MusicEffectCrossfade : public HDX9SoundChannelEffectBase {
		float mtimeleft;

		MusicEffectCrossfade() {

		}

		virtual bool isComplete() {
			return mtimeleft <= 0.0f;
		}
	};

	if(track != msound_cur) {
		if(mchannel_cur) {
			float curvolume;
			mchannel_cur->getVolume(&curvolume);
			HDX_SOUNDMAN->channelEffectFadeVolume(mchannel_cur,fadetime,curvolume,0.0f);
		}

		auto channel_new = HDX_SOUNDMAN->playSoundChannelGroup(track,"music",true,-1);
		channel_new->setPaused(false);
		channel_new->setVolume(0.0f);
		HDX_SOUNDMAN->channelEffectFadeVolume(channel_new,fadetime,0.0f,1.0f);

		MusicEffectCrossfade *effect = new MusicEffectCrossfade();
		effect->mtimeleft = fadetime;
		effect->setFunctionOnUpdate([this,effect,channel_new,track](float dt)->void {
			effect->mtimeleft -= dt;
			if(effect->isComplete()) {
				if(mchannel_cur) mchannel_cur->stop();
				mchannel_cur = channel_new;
				msound_cur = track;
			}
		});
	}
}
