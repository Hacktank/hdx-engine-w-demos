#include "hdx/hdx9vertex.h"

IDirect3DVertexDeclaration9 * HDXVERTP::decl;

HDXVERTP::HDXVERTP(const D3DXVECTOR3 &pp) :
p(pp) {
}

bool HDXVERTP::init(IDirect3DDevice9 *device) {
	D3DVERTEXELEMENT9 decl_ele[] = {
			{0,0,D3DDECLTYPE_FLOAT3,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_POSITION,0},
			D3DDECL_END()
	};

	device->CreateVertexDeclaration(decl_ele,&decl);
	return decl != 0;
}

IDirect3DVertexDeclaration9 * HDXVERTPN::decl;

HDXVERTPN::HDXVERTPN(const D3DXVECTOR3 &pp,const D3DXVECTOR3 &pn) :
p(pp),n(pn) {
}

bool HDXVERTPN::init(IDirect3DDevice9 *device) {
	D3DVERTEXELEMENT9 decl_ele[] = {
			{0,0,D3DDECLTYPE_FLOAT3,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_POSITION,0},
			{0,12,D3DDECLTYPE_FLOAT3,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_NORMAL,0},
			D3DDECL_END()
	};

	device->CreateVertexDeclaration(decl_ele,&decl);
	return decl != 0;
}

IDirect3DVertexDeclaration9 * HDXVERTPTN::decl;

HDXVERTPTN::HDXVERTPTN(const D3DXVECTOR3 &pp,const D3DXVECTOR2 &pt,const D3DXVECTOR3 &pn) :
p(pp),t(pt),n(pn) {
}

bool HDXVERTPTN::init(IDirect3DDevice9 *device) {
	D3DVERTEXELEMENT9 decl_ele[] = {
			{0,0,D3DDECLTYPE_FLOAT3,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_POSITION,0},
			{0,12,D3DDECLTYPE_FLOAT2,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_TEXCOORD,0},
			{0,20,D3DDECLTYPE_FLOAT3,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_NORMAL,0},
			D3DDECL_END()
	};

	device->CreateVertexDeclaration(decl_ele,&decl);
	return decl != 0;
}

IDirect3DVertexDeclaration9 *HDXVERTPCTN::decl = 0;

HDXVERTPCTN::HDXVERTPCTN(const D3DXVECTOR3 &pp,const D3DCOLOR &pc,const D3DXVECTOR2 &pt,const D3DXVECTOR3 &pn) :
p(pp),c(pc),t(pt),n(pn) {
}

bool HDXVERTPCTN::init(IDirect3DDevice9 *device) {
	D3DVERTEXELEMENT9 decl_ele[] = {
			{0,0,D3DDECLTYPE_FLOAT3,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_POSITION,0},
			{0,12,D3DDECLTYPE_D3DCOLOR,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_COLOR,0},
			{0,16,D3DDECLTYPE_FLOAT2,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_TEXCOORD,0},
			{0,24,D3DDECLTYPE_FLOAT3,D3DDECLMETHOD_DEFAULT,D3DDECLUSAGE_NORMAL,0},
			D3DDECL_END()
	};

	device->CreateVertexDeclaration(decl_ele,&decl);
	return decl != 0;
}