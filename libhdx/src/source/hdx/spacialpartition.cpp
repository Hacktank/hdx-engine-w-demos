#include <cassert>
#include <algorithm>
#include <math.h>
#include "hdx/spacialpartition.h"
#include "hdx/component_transform.h"
#include "hdx/entity.h"

#include "hdx/hdx9.h"
#include "hdx/util.h"
#include "hdx/hdx9math.h"

component_SpacialPartition::component_SpacialPartition(Entity *entity) : component_Base(entity) {
	mcbid_onupdate_post0 = HDX_MAIN->registerFuncUpdatePost0([&](float dt)->void {
		auto ctrans = getEntity()->getComponent<component_Transform>(true);
		for(auto &reg : mregistrations) {
			switch(reg->mtype) {
				case Registration::SPT_POINT: {
					HDX_SPMAN->p_reportLocationPoint(reg,ctrans->getPosition());
					break;
				}
				case Registration::SPT_AABB: {
					HDX_SPMAN->p_reportLocationAABB(reg,ctrans->getPosition(),reg->mbbext);
					break;
				}
				case Registration::SPT_OBB: {
					HDX_SPMAN->p_reportLocationOBB(reg,ctrans->getPosition(),reg->mbbext,ctrans->getOrientation());
					break;
				}
			}
		}
	});
}

component_SpacialPartition::~component_SpacialPartition() {
	HDX_MAIN->unregisterFuncUpdatePost0(mcbid_onupdate_post0);
	for(auto &reg : mregistrations) {
		for(auto &buc : reg->mbuckets) {
			HDX_SPMAN->p_cellRemoveComp(HDX_SPMAN->mmultiverse[reg->muniverse],reg,buc->mpos);
		}
		delete reg;
	}
	mregistrations.clear();
}

int component_SpacialPartition::addRegistrationPoint(const std::string &universe,void *userdata) {
	Registration *newitem = new Registration;

	newitem->mcomp = this;
	newitem->muniverse = universe;
	newitem->mtype = Registration::SPT_POINT;
	newitem->muserdata = userdata;

	newitem->mid = mregistrations.getNVID();
	return mregistrations.addItem(newitem);
}

int component_SpacialPartition::addRegistrationAABB(const std::string &universe,const HDXVector3 &ext,void *userdata) {
	Registration *newitem = new Registration;

	newitem->mcomp = this;
	newitem->muniverse = universe;
	newitem->mbbext = ext;
	newitem->mtype = Registration::SPT_AABB;
	newitem->muserdata = userdata;

	newitem->mid = mregistrations.getNVID();
	return mregistrations.addItem(newitem);
}

int component_SpacialPartition::addRegistrationOBB(const std::string &universe,const HDXVector3 &ext,void *userdata) {
	Registration *newitem = new Registration;

	newitem->mcomp = this;
	newitem->muniverse = universe;
	newitem->mbbext = ext;
	newitem->mtype = Registration::SPT_OBB;
	newitem->muserdata = userdata;

	newitem->mid = mregistrations.getNVID();
	return mregistrations.addItem(newitem);
}

void component_SpacialPartition::removeRegistration(int id) {
	if(mregistrations.hasItem(id)) {
		delete mregistrations.getItem(id);
		mregistrations.removeItem(id);
	}
}

SpacialPartitionManager::Universe::Universe() {
	mcellgridsize = POSMANDEFAULTGRIDSIZE;
}

SpacialPartitionManager::SpacialPartitionManager() {
}

SpacialPartitionManager::~SpacialPartitionManager() {
}

void SpacialPartitionManager::createUniverse(const std::string &universe,double gridsize) {
	auto newuniverse = mmultiverse.emplace(universe,Universe());
	newuniverse.first->second.mcellgridsize = gridsize; // even if a new one isnt created adjust its gridsize
}

void SpacialPartitionManager::destroyUniverse(const std::string &universe) {
	mmultiverse.erase(universe);
}

double SpacialPartitionManager::getUniverseGridsize(const std::string &universe) {
	assert(mmultiverse.count(universe)>0);
	Universe &curuniverse = mmultiverse[universe];
	return curuniverse.mcellgridsize;
}

void SpacialPartitionManager::getUniverseGridsize(const std::string &universe,double gridsize) {
	assert(mmultiverse.count(universe)>0);
	Universe &curuniverse = mmultiverse[universe];
	curuniverse.mcellgridsize = gridsize;
}

void SpacialPartitionManager::doIterateUniverseBuckets(std::function<bool(spdata::bucket &bucket)> func,const std::string &universe) {
	assert(mmultiverse.count(universe)>0);
	Universe &curuniverse = mmultiverse[universe];

	curuniverse.mbuckets.doIterate([&](spdata::bucket &bucket)->bool {
		return func(bucket);
	});
}

void SpacialPartitionManager::doIterateUniverseIntersectionAABBBuckets(std::function<bool(spdata::bucket &bucket)> func,const std::string &universe,const HDXVector3 &pos,const HDXVector3 &ext) {
	assert(mmultiverse.count(universe)>0);
	Universe &curuniverse = mmultiverse[universe];

	p_doIterateIntersectionAABB([&](const spdata::position &pos)->bool {
		if(curuniverse.mbuckets.hasItem(pos)) {
			func(curuniverse.mbuckets[pos]);
		}
		return false;
	},curuniverse,pos,ext);
}

void SpacialPartitionManager::doIterateUniverseIntersectionOBBBuckets(std::function<bool(spdata::bucket &bucket)> func,const std::string &universe,const HDXVector3 &pos,const HDXVector3 &ext,const HDXQuaternion &ori) {
	doIterateUniverseIntersectionAABBBuckets(func,universe,pos,p_calculateAABBExtentsFromOBB(ext,ori));
}

void SpacialPartitionManager::doIterateUniverseCellElements(std::function<bool(component_SpacialPartition::Registration *reg)> func,const std::string &universe,const HDXVector3 &pos) {
	assert(mmultiverse.count(universe)>0);
	Universe &curuniverse = mmultiverse[universe];

	spdata::position cell = p_calculateUniverseCell(pos,curuniverse.mcellgridsize);

	if(curuniverse.mbuckets.hasItem(cell)) {
		spdata::bucket &curbucket = curuniverse.mbuckets[cell];
		for(auto it = curbucket.mregs.begin(); it != curbucket.mregs.end(); it++) {
			if(func(*it)) break;
		}
	}
}

void SpacialPartitionManager::doIterateUniverseIntersectionAABBElements(std::function<bool(component_SpacialPartition::Registration *reg)> func,const std::string &universe,const HDXVector3 &pos,const HDXVector3 &ext) {
	assert(mmultiverse.count(universe)>0);
	Universe &curuniverse = mmultiverse[universe];

	p_doIterateIntersectionAABB([&](const spdata::position &pos)->bool {
		if(curuniverse.mbuckets.hasItem(pos)) {
			spdata::bucket &curbucket = curuniverse.mbuckets[pos];
			for(auto it = curbucket.mregs.begin(); it != curbucket.mregs.end(); it++) {
				if(func(*it)) return true;
			}
		}
		return false;
	},curuniverse,pos,ext);
}

void SpacialPartitionManager::doIterateUniverseIntersectionOBBElements(std::function<bool(component_SpacialPartition::Registration *reg)> func,const std::string &universe,const HDXVector3 &pos,const HDXVector3 &ext,const HDXQuaternion &ori) {
	doIterateUniverseIntersectionAABBElements(func,universe,pos,p_calculateAABBExtentsFromOBB(ext,ori));
}

void SpacialPartitionManager::p_reportLocationPoint(component_SpacialPartition::Registration *reg,const HDXVector3 &pos) {
	Universe &curuniverse = mmultiverse[reg->muniverse];

	p_doIterateIntersectionPoint([&](const spdata::position &pos)->bool {
		int curcellsnum = reg->mbuckets.size();
		if(curcellsnum!=1 || (curcellsnum==1 && reg->mbuckets[0]->mpos != pos)) {
			for(int i = 0; i < curcellsnum; i++) p_cellRemoveComp(curuniverse,reg,reg->mbuckets[i]->mpos);
			reg->mbuckets.clear();
			reg->mbuckets.push_back(&p_cellAddComp(curuniverse,reg,pos));
		}
		return false;
	},curuniverse,pos);
}

void SpacialPartitionManager::p_reportLocationAABB(component_SpacialPartition::Registration *reg,const HDXVector3 &pos,const HDXVector3 &ext) {
	Universe &curuniverse = mmultiverse[reg->muniverse];
	std::vector<spdata::position> newcells;

	p_doIterateIntersectionAABB([&](const spdata::position &pos)->bool {
		newcells.emplace_back(pos);
		return false;
	},curuniverse,pos,ext);

	bool match = reg->mbuckets.size() == newcells.size();
	if(match) {
		for(unsigned int i = 0; i < newcells.size(); i++) {
			if(reg->mbuckets[i]->mpos != newcells[i]) {
				match = false;
				break;
			}
		}
	}

	if(!match) {
		for(unsigned int i = 0; i < reg->mbuckets.size(); i++) p_cellRemoveComp(curuniverse,reg,reg->mbuckets[i]->mpos);
		reg->mbuckets.clear();
		for(unsigned int i = 0; i < newcells.size(); i++) {
			reg->mbuckets.push_back(&p_cellAddComp(curuniverse,reg,newcells[i]));
		}
	}
}

void SpacialPartitionManager::p_reportLocationOBB(component_SpacialPartition::Registration *data,const HDXVector3 &pos,const HDXVector3 &ext,const HDXQuaternion &ori) {
	p_reportLocationAABB(data,pos,p_calculateAABBExtentsFromOBB(ext,ori));
}

void SpacialPartitionManager::p_doIterateIntersectionPoint(std::function<bool(const spdata::position &pos)> func,Universe &universe,const HDXVector3 &pos) {
	spdata::position newcell = p_calculateUniverseCell(pos,universe.mcellgridsize);
	if(func(newcell)) return;
}

void SpacialPartitionManager::p_doIterateIntersectionAABB(std::function<bool(const spdata::position &pos)> func,Universe &universe,const HDXVector3 &pos,const HDXVector3 &ext) {
	spdata::position nzepicenter = p_calculateUniverseCell(pos,universe.mcellgridsize);

	int cellnid[2][3];

	for(int sign = 0; sign < 2; sign++) {
		for(int i = 0; i < 3; i++) {
			cellnid[sign][i] = (int)std::floor(((double)pos[i]+(sign==0 ? -1 : 1)*ext[i]) / universe.mcellgridsize) - nzepicenter[i];
		}
	}

	for(int ix = cellnid[0][0]; ix <= cellnid[1][0]; ix++) {
		for(int iy = cellnid[0][1]; iy <= cellnid[1][1]; iy++) {
			for(int iz = cellnid[0][2]; iz <= cellnid[1][2]; iz++) {
				spdata::position newcell(nzepicenter.x+ix,nzepicenter.y+iy,nzepicenter.z+iz);
				if(func(newcell)) return;
			}
		}
	}
}

HDXVector3 SpacialPartitionManager::p_calculateAABBExtentsFromOBB(const HDXVector3 &ext,const HDXQuaternion &ori) {
	HDXVector3 corners[8];
	for(int i = 0; i < 2; i++) {
		for(int j = 0; j < 2; j++) {
			for(int k = 0; k < 2; k++) {
				corners[i*4+j*2+k] = HDXVector3(ext.x*(i==0 ? -1 : 1),
												 ext.y*(j==0 ? -1 : 1),
												 ext.z*(k==0 ? -1 : 1)
												 );
			}
		}
	}

	HDXVector3 newext(0,0,0);
	for(int i = 0; i < 8; i++) {

		corners[i] = HDXVector3::gen::transformQuaternion(corners[i],ori);
		for(int j = 0; j < 3; j++) newext[j] = (std::max)(abs(newext[j]),abs(corners[i][j]));
	}

	return newext;
}

spdata::bucket& SpacialPartitionManager::p_cellAddComp(Universe &universe,component_SpacialPartition::Registration *reg,const spdata::position &key) {
	spdata::bucket &bucket = universe.mbuckets[key]; // auto add if it doesnt exist
	bucket.mpos = key;
	bucket.mregs.push_back(reg);
	return bucket;
}

int SpacialPartitionManager::p_cellRemoveComp(Universe &universe,component_SpacialPartition::Registration *reg,const spdata::position &key) {
	if(universe.mbuckets.hasItem(key)) {
		spdata::bucket &bucket = universe.mbuckets[key];
		int numcomps = bucket.mregs.size();
		if(numcomps) {
			auto findit = std::find(bucket.mregs.begin(),bucket.mregs.end(),reg);
			if(findit != bucket.mregs.end()) {
				if(numcomps>1) {
					bucket.mregs.erase(findit);
				} else {
					universe.mbuckets.removeItem(key);
				}
			}
			return numcomps -1;
		}
	}
	return 0;
}

spdata::position SpacialPartitionManager::p_calculateUniverseCell(const HDXVector3 &pos,double gridsize) {
	spdata::position ret;
	ret.x = (spdata::pointdt)std::floor((double)pos.x / gridsize);
	ret.y = (spdata::pointdt)std::floor((double)pos.y / gridsize);
	ret.z = (spdata::pointdt)std::floor((double)pos.z / gridsize);
	return ret;
}