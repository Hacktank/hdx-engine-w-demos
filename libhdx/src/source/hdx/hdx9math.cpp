#include "hdx/hdx9math.h"

float HDXVector2::operator*(const HDXVector2 &r) const {
	return dot(r);
}

float HDXVector2::dot(const HDXVector2 &r) const {
	return D3DXVec2Dot(&_d3dx,&r._d3dx);
}

float HDXVector2::magnitude() const {
	return sqrt(magnitudeSq());
}

float HDXVector2::magnitudeSq() const {
	return D3DXVec2LengthSq(&_d3dx);
}

HDXVector2& HDXVector2::setNormal() {
	D3DXVec2Normalize(&_d3dx,&_d3dx);
	return *this;
}

HDXVector2 HDXVector2::getNormal() const {
	HDXVector2 ret = *this;
	ret.setNormal();
	return ret;
}

float* HDXVector2::data() {
	return (float*)_d3dx;
}

const float* HDXVector2::data() const {
	return (const float*)_d3dx;
}

const D3DXVECTOR2* HDXVector2::getD3DX() const {
	return &_d3dx;
}

HDXVector2 HDXVector2::operator-() const {
	HDXVector2 ret = *this; ret._d3dx = -ret._d3dx; return ret;
}

HDXVector2 HDXVector2::operator-(const HDXVector2 &r) const {
	HDXVector2 ret = *this; ret -= r; return ret;
}

HDXVector2 HDXVector2::operator+(const HDXVector2 &r) const {
	HDXVector2 ret = *this; ret += r; return ret;
}

HDXVector2& HDXVector2::operator/=(float r) {
	_d3dx /= r; return *this;
}

HDXVector2& HDXVector2::operator*=(float r) {
	_d3dx *= r; return *this;
}

HDXVector2& HDXVector2::operator-=(const HDXVector2 &r) {
	_d3dx -= r._d3dx; return *this;
}

HDXVector2& HDXVector2::operator+=(const HDXVector2 &r) {
	_d3dx += r._d3dx; return *this;
}

const float& HDXVector2::operator[](unsigned r) const {
	assert(r>=0 && r<2);
	return _d3dx[r];
}

float& HDXVector2::operator[](unsigned r) {
	assert(r>=0 && r<2);
	return _d3dx[r];
}

void HDXVector2::clear() {
	for(unsigned i = 0; i < 2; i++) (*this)[i] = 0.0f;
}

BOOL HDXVector2::operator!=(const HDXVector2 &r) const {
	return !(*this == r);
}

BOOL HDXVector2::operator==(const HDXVector2 &r) const {
	return _d3dx == r._d3dx;
}

HDXVector2& HDXVector2::operator=(const HDXVector2 &r) {
	_d3dx = r._d3dx; return *this;
}

HDXVector2::~HDXVector2() {
}

HDXVector2::HDXVector2(const D3DXVECTOR2 &r) : _d3dx(r),x(_d3dx.x),y(_d3dx.y) {
}

HDXVector2::HDXVector2(const HDXVector3 &r) : _d3dx(r.x,r.y),x(_d3dx.x),y(_d3dx.y) {
}

HDXVector2::HDXVector2(const HDXVector2 &r) : _d3dx(r._d3dx),x(_d3dx.x),y(_d3dx.y) {
}

HDXVector2::HDXVector2(float x,float y) : _d3dx(x,y),x(_d3dx.x),y(_d3dx.y) {
}

HDXVector2::HDXVector2() : _d3dx(),x(_d3dx.x),y(_d3dx.y) {
}

HDXVector3 HDXVector3::operator%(const HDXVector3 &r) const {
	return cross(r);
}

float HDXVector3::operator*(const HDXVector3 &r) const {
	return dot(r);
}

HDXVector3 HDXVector3::mirror(const HDXVector3 &n) const {
	return 2*(n * *this)*n-*this;
}

HDXMatrix3x3 HDXVector3::outer(const HDXVector3 &v) const {
	return HDXMatrix3x3((*this)[0]*v[0],(*this)[0]*v[1],(*this)[0]*v[2],
						(*this)[1]*v[0],(*this)[1]*v[1],(*this)[1]*v[2],
						(*this)[2]*v[0],(*this)[2]*v[1],(*this)[2]*v[2]);
}

HDXVector3 HDXVector3::cross(const HDXVector3 &r) const {
	HDXVector3 ret; D3DXVec3Cross(&ret._d3dx,&_d3dx,&r._d3dx); return ret;
}

float HDXVector3::dot(const HDXVector3 &r) const {
	return D3DXVec3Dot(&_d3dx,&r._d3dx);
}

float HDXVector3::magnitude() const {
	return sqrt(magnitudeSq());
}

float HDXVector3::magnitudeSq() const {
	return D3DXVec3LengthSq(&_d3dx);
}

HDXVector3& HDXVector3::setNormal() {
	D3DXVec3Normalize(&_d3dx,&_d3dx);
	return *this;
}

HDXVector3 HDXVector3::getNormal() const {
	HDXVector3 ret = *this;
	ret.setNormal();
	return ret;
}

float* HDXVector3::data() {
	return (float*)_d3dx;
}

const float* HDXVector3::data() const {
	return (const float*)_d3dx;
}

D3DXVECTOR3* HDXVector3::getD3DX() {
	return &_d3dx;
}

const D3DXVECTOR3* HDXVector3::getD3DX() const {
	return &_d3dx;
}

HDXVector3 HDXVector3::operator-() const {
	HDXVector3 ret = *this; ret._d3dx = -ret._d3dx; return ret;
}

HDXVector3 HDXVector3::operator-(const HDXVector3 &r) const {
	HDXVector3 ret = *this; ret -= r; return ret;
}

HDXVector3 HDXVector3::operator+(const HDXVector3 &r) const {
	HDXVector3 ret = *this; ret += r; return ret;
}

HDXVector3& HDXVector3::operator/=(float r) {
	_d3dx /= r; return *this;
}

HDXVector3& HDXVector3::operator*=(float r) {
	_d3dx *= r; return *this;
}

HDXVector3& HDXVector3::operator-=(const HDXVector3 &r) {
	_d3dx -= r._d3dx; return *this;
}

HDXVector3& HDXVector3::operator+=(const HDXVector3 &r) {
	_d3dx += r._d3dx; return *this;
}

const float& HDXVector3::operator[](unsigned r) const {
	assert(r>=0 && r<3);
	return _d3dx[r];
}

float& HDXVector3::operator[](unsigned r) {
	assert(r>=0 && r<3);
	return _d3dx[r];
}

void HDXVector3::clear() {
	for(unsigned i = 0; i < 3; i++) (*this)[i] = 0.0f;
}

BOOL HDXVector3::operator!=(const HDXVector3 &r) const {
	return !(*this == r);
}

BOOL HDXVector3::operator==(const HDXVector3 &r) const {
	return _d3dx == r._d3dx;
}

HDXVector3& HDXVector3::operator=(const HDXVector3 &r) {
	_d3dx = r._d3dx; return *this;
}

HDXVector3::~HDXVector3() {
}

HDXVector3::HDXVector3(const HDXVector2 &r) : _d3dx(r.x,r.y,0),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z) {
}

HDXVector3::HDXVector3(const D3DXVECTOR3 &r) : _d3dx(r),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z) {
}

HDXVector3::HDXVector3(const HDXQuaternion &r) : _d3dx(r.x,r.y,r.z),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z) {
}

HDXVector3::HDXVector3(const HDXVector4 &r) : _d3dx(r.x,r.y,r.z),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z) {
}

HDXVector3::HDXVector3(const HDXVector3 &r) : _d3dx(r._d3dx),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z) {
}

HDXVector3::HDXVector3(float x,float y,float z) : _d3dx(x,y,z),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z) {
}

HDXVector3::HDXVector3() : _d3dx(),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z) {
}

HDXVector3 HDXVector3::gen::transformQuaternion(const HDXVector3 &v,const HDXQuaternion &q) {
	return q.getInverse() * (HDXQuaternion)v * q;
}

void HDXVector3::gen::makeOrthoNormalBasis(HDXVector3 *x,HDXVector3 *y,HDXVector3 *z) {
	x->setNormal();
	(*z) = (*x) % (abs(x->x) > abs(x->y) ? YUNIT3 : XUNIT3);
	if(z->magnitudeSq() == 0.0) return; // Or generate an error.
	z->setNormal();
	(*y) = (*z) % (*x);
}

float HDXVector4::operator*(const HDXVector4 &r) const {
	return dot(r);
}

float HDXVector4::dot(const HDXVector4 &r) const {
	return D3DXVec4Dot(&_d3dx,&r._d3dx);
}

float HDXVector4::magnitude() const {
	return sqrt(magnitudeSq());
}

float HDXVector4::magnitudeSq() const {
	return D3DXVec4LengthSq(&_d3dx);
}

HDXVector4& HDXVector4::setNormal() {
	D3DXVec4Normalize(&_d3dx,&_d3dx);
	return *this;
}

HDXVector4 HDXVector4::getNormal() const {
	HDXVector4 ret = *this;
	ret.setNormal();
	return ret;
}

float* HDXVector4::data() {
	return (float*)_d3dx;
}

const float* HDXVector4::data() const {
	return (const float*)_d3dx;
}

D3DXVECTOR4* HDXVector4::getD3DX() {
	return &_d3dx;
}

const D3DXVECTOR4* HDXVector4::getD3DX() const {
	return &_d3dx;
}

HDXVector4 HDXVector4::operator-() const {
	HDXVector4 ret = *this; ret._d3dx = -ret._d3dx; return ret;
}

HDXVector4 HDXVector4::operator-(const HDXVector4 &r) const {
	HDXVector4 ret = *this; ret -= r; return ret;
}

HDXVector4 HDXVector4::operator+(const HDXVector4 &r) const {
	HDXVector4 ret = *this; ret += r; return ret;
}

HDXVector4& HDXVector4::operator/=(float r) {
	_d3dx /= r; return *this;
}

HDXVector4& HDXVector4::operator*=(float r) {
	_d3dx *= r; return *this;
}

HDXVector4& HDXVector4::operator-=(const HDXVector4 &r) {
	_d3dx -= r._d3dx; return *this;
}

HDXVector4& HDXVector4::operator+=(const HDXVector4 &r) {
	_d3dx += r._d3dx; return *this;
}

const float& HDXVector4::operator[](unsigned r) const {
	assert(r>=0 && r<4);
	return _d3dx[r];
}

float& HDXVector4::operator[](unsigned r) {
	assert(r>=0 && r<4);
	return _d3dx[r];
}

void HDXVector4::clear() {
	for(unsigned i = 0; i < 4; i++) (*this)[i] = 0.0f;
}

BOOL HDXVector4::operator!=(const HDXVector4 &r) const {
	return !(*this == r);
}

BOOL HDXVector4::operator==(const HDXVector4 &r) const {
	return _d3dx == r._d3dx;
}

HDXVector4& HDXVector4::operator=(const HDXVector4 &r) {
	_d3dx = r._d3dx;
	return *this;
}

HDXVector4::~HDXVector4() {
}

HDXVector4::HDXVector4(const D3DXVECTOR4 &r) : _d3dx(r),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXVector4::HDXVector4(const HDXVector3 &r) : _d3dx(r.x,r.y,r.z,0),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXVector4::HDXVector4(const HDXVector4 &r) : _d3dx(r._d3dx),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXVector4::HDXVector4(float x,float y,float z,float w) : _d3dx(x,y,z,w),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXVector4::HDXVector4() : _d3dx(),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXQuaternion HDXQuaternion::operator+(const HDXVector3 &r) const {
	HDXQuaternion ret = *this;
	ret += r;
	return ret;
}

HDXQuaternion HDXQuaternion::operator+(const HDXQuaternion &r) const {
	HDXQuaternion ret = *this;
	ret += r;
	return ret;
}

HDXVector3 HDXQuaternion::operator*(const HDXVector3 &r) const {
	float num = x * 2;
	float num2 = y * 2;
	float num3 = z * 2;
	float num4 = x * num;
	float num5 = y * num2;
	float num6 = z * num3;
	float num7 = x * num2;
	float num8 = x * num3;
	float num9 = y * num3;
	float num10 = w * num;
	float num11 = w * num2;
	float num12 = w * num3;

	return HDXVector3((1 - (num5 + num6)) * r.x + (num7 - num12) * r.y + (num8 + num11) * r.z,
					  (num7 + num12) * r.x + (1 - (num4 + num6)) * r.y + (num9 - num10) * r.z,
					  (num8 - num11) * r.x + (num9 + num10) * r.y + (1 - (num4 + num5)) * r.z);
}

HDXQuaternion HDXQuaternion::operator*(const HDXQuaternion &r) const {
	HDXQuaternion ret = *this; ret *= r; return ret;
}

HDXQuaternion& HDXQuaternion::operator+=(const HDXVector3 &r) {
	HDXQuaternion q = r;
	q = *this * q;
	q *= 0.5f;
	*this += q;
	return *this;
}

HDXQuaternion& HDXQuaternion::operator+=(const HDXQuaternion &r) {
	_d3dx += r._d3dx;
	return *this;
}

HDXQuaternion& HDXQuaternion::operator*=(const HDXQuaternion &r) {
	_d3dx *= r._d3dx;
	return *this;
}

HDXQuaternion& HDXQuaternion::operator*=(float r) {
	_d3dx *= r;
	return *this;
}

HDXQuaternion HDXQuaternion::operator~() const {
	return getConjugate();
}

HDXQuaternion& HDXQuaternion::setInverse() {
	return *this = getInverse();
}

HDXQuaternion HDXQuaternion::getInverse() const {
	HDXQuaternion ret; D3DXQuaternionInverse(&ret._d3dx,&_d3dx); return ret;
}

HDXQuaternion& HDXQuaternion::setConjugate() {
	return *this = getConjugate();
}

HDXQuaternion HDXQuaternion::getConjugate() const {
	HDXQuaternion ret; D3DXQuaternionConjugate(&ret._d3dx,&_d3dx); return ret;
}

void HDXQuaternion::setEular(float x,float y,float z) {
	D3DXQuaternionRotationYawPitchRoll(&_d3dx,y,x,z);
}

void HDXQuaternion::setEular(const HDXVector3 &eular) {
	setEular(eular.x,eular.y,eular.z);
}

HDXVector3 HDXQuaternion::getEular() const {
	HDXVector3 ret;

	float r11,r21,r31,r32,r33;
	float q00,q11,q22,q33;

	q00 = w*w;
	q11 = x*x;
	q22 = y*y;
	q33 = z*z;

	r11 = q00 + q11 - q22 - q33;
	r21 = 2 * (x*y + w*z);
	r31 = 2 * (x*z - w*y);
	r32 = 2 * (y*z + w*x);
	r33 = q00 - q11 - q22 + q33;

	float absr31 = abs(r31);
	if(absr31 > 0.9999) { // gimbal lock has occured, try to compensate
		float r12 = 2 * (x*y - w*z);
		float r13 = 2 * (x*z + w*y);

		ret.x = 0;
		ret.y = -((float)(M_PI/2))*r31/absr31;
		ret.z = atan2(-r12,-r31*r31);
	} else {
		ret.x = atan2(r32,r33);
		ret.y = asin(-r31);
		ret.z = atan2(r21,r11);
	}

	return ret;
}

HDXQuaternion& HDXQuaternion::setAxisAngle(const HDXVector3 &axis,float angle) {
	D3DXQuaternionRotationAxis(&_d3dx,&axis._d3dx,angle);
	return *this;
}

void HDXQuaternion::setAxis(const HDXVector3 &axis) {
	auto angle = getAngle(); D3DXQuaternionRotationAxis(&_d3dx,&axis._d3dx,angle);
}

HDXVector3 HDXQuaternion::getAxis() const {
	D3DXVECTOR3 axis; float angle; D3DXQuaternionToAxisAngle(&_d3dx,&axis,&angle); return axis;
}

void HDXQuaternion::setAngle(float angle) {
	auto axis = getAxis(); D3DXQuaternionRotationAxis(&_d3dx,&axis._d3dx,angle);
}

float HDXQuaternion::getAngle() const {
	D3DXVECTOR3 axis; float angle; D3DXQuaternionToAxisAngle(&_d3dx,&axis,&angle); return angle;
}

HDXMatrix3x3 HDXQuaternion::toRotationMatrix() const {
	return HDXMatrix3x3(*this);
}

float HDXQuaternion::magnitude() const {
	return sqrt(magnitudeSq());
}

float HDXQuaternion::magnitudeSq() const {
	return D3DXQuaternionLengthSq(&_d3dx);
}

HDXQuaternion& HDXQuaternion::setIdentity() {
	x = y = z = 0; w = 1; return *this;
}

HDXQuaternion& HDXQuaternion::setNormal() {
	D3DXQuaternionNormalize(&_d3dx,&_d3dx);
	return *this;
}

HDXQuaternion HDXQuaternion::getNormal() const {
	HDXQuaternion ret = *this;
	ret.setNormal();
	return ret;
}

float* HDXQuaternion::data() {
	return (float*)_d3dx;
}

const float* HDXQuaternion::data() const {
	return (const float*)_d3dx;
}

D3DXQUATERNION* HDXQuaternion::getD3DX() {
	return &_d3dx;
}

const D3DXQUATERNION* HDXQuaternion::getD3DX() const {
	return &_d3dx;
}

HDXQuaternion HDXQuaternion::operator-() const {
	HDXQuaternion ret = *this; ret._d3dx = -ret._d3dx; return ret;
}

HDXQuaternion HDXQuaternion::operator-(const HDXQuaternion &r) const {
	HDXQuaternion ret = *this; ret -= r; return ret;
}

HDXQuaternion& HDXQuaternion::operator/=(float r) {
	_d3dx /= r; return *this;
}

HDXQuaternion& HDXQuaternion::operator-=(const HDXQuaternion &r) {
	_d3dx -= r._d3dx; return *this;
}

const float& HDXQuaternion::operator[](unsigned r) const {
	assert(r>=0 && r<4);
	return _d3dx[r];
}

float& HDXQuaternion::operator[](unsigned r) {
	assert(r>=0 && r<4);
	return _d3dx[r];
}

void HDXQuaternion::clear() {
	for(unsigned i = 0; i < 4; i++) (*this)[i] = 0.0f;
}

BOOL HDXQuaternion::operator!=(const HDXQuaternion &r) const {
	return !(*this == r);
}

BOOL HDXQuaternion::operator==(const HDXQuaternion &r) const {
	return _d3dx == r._d3dx;
}

HDXQuaternion& HDXQuaternion::operator=(const HDXQuaternion &r) {
	_d3dx = r._d3dx; return *this;
}

HDXQuaternion::~HDXQuaternion() {
}

HDXQuaternion::HDXQuaternion(const D3DXQUATERNION &r) : _d3dx(r),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXQuaternion::HDXQuaternion(const HDXMatrix3x3 &r) : _d3dx(),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
	D3DXQuaternionRotationMatrix(&_d3dx,&r._d3dx);
}

HDXQuaternion::HDXQuaternion(const HDXQuaternion &r) : _d3dx(r._d3dx),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXQuaternion::HDXQuaternion(const HDXVector3 &vec) : _d3dx(vec.x,vec.y,vec.z,0),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXQuaternion::HDXQuaternion(float eularx,float eulary,float eularz) : _d3dx(),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
	setEular(eularx,eulary,eularz);
}

HDXQuaternion::HDXQuaternion(const HDXVector3 &axis,float angle) : _d3dx(),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
	setAxisAngle(axis,angle);
}

HDXQuaternion::HDXQuaternion(float x,float y,float z,float w) : _d3dx(x,y,z,w),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXQuaternion::HDXQuaternion() : _d3dx(),x(_d3dx.x),y(_d3dx.y),z(_d3dx.z),w(_d3dx.w) {
}

HDXQuaternion HDXQuaternion::gen::rotateTo(const HDXVector3 &q,const HDXVector3 &p) {
	HDXQuaternion ret;

	float dot = p * q;
	if(dot <= -0.9995f) {
		ret.setAxisAngle(YUNIT3,-(float)M_PI);//left handed, remove negation for right handed
	} else if(dot >= 0.9995f) {
		ret = HDXQuaternion(0,0,0,1);
	} else {
		HDXVector3 tmp = q % p; //left handed, swap q and p for right handed
		ret = HDXQuaternion(tmp.x,tmp.y,tmp.z,1+dot).getNormal();
	}

	return ret;
}

HDXQuaternion HDXQuaternion::gen::identity() {
	return HDXQuaternion(0,0,0,1);
}

void HDXMatrix3x3::_contrain() {
	_d3dx[3] = _d3dx[7] = _d3dx[11] = _d3dx[12] = _d3dx[13] = _d3dx[14] = 0.0f; _d3dx[15] = 1.0f;
}

unsigned HDXMatrix3x3::ij(unsigned i,unsigned j) {
	return 3*j+i;
}

HDXMatrix3x3 HDXMatrix3x3::operator*(const HDXMatrix3x3 &r) const {
	HDXMatrix3x3 ret = *this; ret *= r; return ret;
}

HDXMatrix3x3& HDXMatrix3x3::operator*=(const HDXMatrix3x3 &r) {
	_d3dx *= r._d3dx; _contrain(); return *this;
}

HDXMatrix3x3& HDXMatrix3x3::operator*=(float r) {
	_d3dx *= r; return *this;
}

HDXMatrix3x3& HDXMatrix3x3::setTranspose() {
	return *this = getTranspose();
}

HDXMatrix3x3 HDXMatrix3x3::getTranspose() const {
	HDXMatrix3x3 ret; ((HDXMatrix3x3*)this)->_contrain(); D3DXMatrixTranspose(&ret._d3dx,&_d3dx); ret._contrain(); return ret;
}

HDXMatrix3x3& HDXMatrix3x3::setInvert() {
	return *this = getInvert();
}

HDXMatrix3x3 HDXMatrix3x3::getInvert() const {
	HDXMatrix3x3 ret; ((HDXMatrix3x3*)this)->_contrain(); D3DXMatrixInverse(&ret._d3dx,0,&_d3dx); ret._contrain(); return ret;
}

HDXMatrix3x3& HDXMatrix3x3::setIdentity() {
	D3DXMatrixIdentity(&_d3dx); return *this;
}

HDXMatrix3x3& HDXMatrix3x3::orthonormalize() {
	HDXVector3 rows[3] = {
		getRow(0),
		getRow(1),
		getRow(2),
	};
	HDXVector3::gen::makeOrthoNormalBasis(&rows[0],&rows[1],&rows[2]);
	for(unsigned i = 0; i < 3; i++) setRow(i,rows[i]);
	return *this;
}

HDXVector3 HDXMatrix3x3::getScale() const {
	return HDXVector3(getRow(0).magnitude(),
					  getRow(1).magnitude(),
					  getRow(2).magnitude());
}

void HDXMatrix3x3::setColumn(unsigned i,const HDXVector3 &vec) {
	for(unsigned ind = 0; ind < 3; ind++) (*this)[ij(i,ind)] = vec[ind];
}

void HDXMatrix3x3::setColumn(unsigned i,const HDXVector2 &vec) {
	for(unsigned ind = 0; ind < 2; ind++) (*this)[ij(i,ind)] = vec[ind];
}

HDXVector3 HDXMatrix3x3::getColumn(unsigned i) const {
	return HDXVector3((*this)[ij(i,0)],(*this)[ij(i,1)],(*this)[ij(i,2)]);
}

void HDXMatrix3x3::setRow(unsigned i,const HDXVector3 &vec) {
	for(unsigned ind = 0; ind < 3; ind++) (*this)[ij(ind,i)] = vec[ind];
}

void HDXMatrix3x3::setRow(unsigned i,const HDXVector2 &vec) {
	for(unsigned ind = 0; ind < 2; ind++) (*this)[ij(ind,i)] = vec[ind];
}

HDXVector3 HDXMatrix3x3::getRow(unsigned i) const {
	return HDXVector3((*this)[ij(0,i)],(*this)[ij(1,i)],(*this)[ij(2,i)]);
}

HDXQuaternion HDXMatrix3x3::toRotationQuaternion() const {
	return HDXQuaternion(*this);
}

HDXMatrix4x4 HDXMatrix3x3::promote() const {
	return HDXMatrix4x4(*this);
}

float* HDXMatrix3x3::data() {
	return (float*)_d3dx;
}

const float* HDXMatrix3x3::data() const {
	return (const float*)_d3dx;
}

D3DXMATRIX* HDXMatrix3x3::getD3DX() {
	return &_d3dx;
}

const D3DXMATRIX* HDXMatrix3x3::getD3DX() const {
	return &_d3dx;
}

HDXMatrix3x3 HDXMatrix3x3::operator-() const {
	HDXMatrix3x3 ret = *this; ret._d3dx = -ret._d3dx; return ret;
}

HDXMatrix3x3 HDXMatrix3x3::operator-(const HDXMatrix3x3 &r) const {
	HDXMatrix3x3 ret = *this; ret -= r; return ret;
}

HDXMatrix3x3 HDXMatrix3x3::operator+(const HDXMatrix3x3 &r) const {
	HDXMatrix3x3 ret = *this; ret += r; return ret;
}

HDXMatrix3x3& HDXMatrix3x3::operator/=(float r) {
	_d3dx /= r; return *this;
}

HDXMatrix3x3& HDXMatrix3x3::operator-=(const HDXMatrix3x3 &r) {
	_d3dx -= r._d3dx; _contrain(); return *this;
}

HDXMatrix3x3& HDXMatrix3x3::operator+=(const HDXMatrix3x3 &r) {
	_d3dx += r._d3dx; _contrain(); return *this;
}

const float& HDXMatrix3x3::operator[](unsigned r) const {
	assert(r>=0 && r<9);
	r += r/3; return _d3dx[r];
}

float& HDXMatrix3x3::operator[](unsigned r) {
	assert(r>=0 && r<9);
	r += r/3; return _d3dx[r];
}

void HDXMatrix3x3::clear() {
	for(unsigned i = 0; i < 9; i++) (*this)[i] = 0.0f; _contrain();
}

BOOL HDXMatrix3x3::operator!=(const HDXMatrix3x3 &r) const {
	return !(*this == r);
}

BOOL HDXMatrix3x3::operator==(const HDXMatrix3x3 &r) const {
	return _d3dx == r._d3dx;
}

HDXMatrix3x3& HDXMatrix3x3::operator=(const HDXMatrix3x3 &r) {
	_d3dx = r._d3dx; return *this;
}

HDXMatrix3x3::~HDXMatrix3x3() {
}

HDXMatrix3x3::HDXMatrix3x3(const D3DXMATRIX &r) : _d3dx(r) {
	_contrain();
}

HDXMatrix3x3::HDXMatrix3x3(const HDXQuaternion &r) : _d3dx() {
	D3DXMatrixRotationQuaternion(&_d3dx,&r._d3dx); _contrain();
}

HDXMatrix3x3::HDXMatrix3x3(const HDXMatrix4x4 &r) : _d3dx(r._d3dx) {
	_contrain();
}

HDXMatrix3x3::HDXMatrix3x3(const HDXMatrix3x3 &r) : _d3dx(r._d3dx) {
}

HDXMatrix3x3::HDXMatrix3x3(FLOAT _11,FLOAT _12,FLOAT _13,FLOAT _21,FLOAT _22,FLOAT _23,FLOAT _31,FLOAT _32,FLOAT _33) : _d3dx(_11,_12,_13,0,_21,_22,_23,0,_31,_32,_33,0,0,0,0,1) {
}

HDXMatrix3x3::HDXMatrix3x3() {
	_contrain();
}

float HDXMatrix3x3::determinant() const {
	const float
		a00 = (*this)[0],a01 = (*this)[1],a02 = (*this)[2],
		a10 = (*this)[3],a11 = (*this)[4],a12 = (*this)[5],
		a20 = (*this)[6],a21 = (*this)[7],a22 = (*this)[8],

		b01 = a22 * a11 - a12 * a21,
		b11 = -a22 * a10 + a12 * a20,
		b21 = a21 * a10 - a11 * a20;

	return 1.0f/(a00 * b01 + a01 * b11 + a02 * b21);
}

HDXMatrix3x3 HDXMatrix3x3::gen::skewSymmetric(const HDXVector3 &vec) {
	HDXMatrix3x3 ret;
	ret.clear();
	ret[1] = -vec.z;
	ret[2] = vec.y;
	ret[3] = vec.z;
	ret[5] = -vec.x;
	ret[6] = -vec.y;
	ret[7] = vec.x;
// 	ret[1] = vec.z;
// 	ret[2] = -vec.y;
// 	ret[3] = -vec.z;
// 	ret[5] = vec.x;
// 	ret[6] = vec.y;
// 	ret[7] = -vec.x;
	return ret;
}

HDXMatrix3x3 HDXMatrix3x3::gen::identity() {
	return HDXMatrix3x3().setIdentity();
}

HDXMatrix3x3 HDXMatrix3x3::gen::transformation2d(const HDXVector2 &scale,const HDXVector2 &rotcenter,float rot,const HDXVector2 &pos) {
	HDXMatrix3x3 ret;
	D3DXMatrixTransformation2D(&ret._d3dx,0,0,scale.getD3DX(),rotcenter.getD3DX(),rot,pos.getD3DX());
	return ret;
}

HDXMatrix3x3 HDXMatrix3x3::gen::rotationAxis(const HDXVector3 &axis,float ang) {
	HDXMatrix3x3 ret;
	D3DXMatrixRotationAxis(&ret._d3dx,axis.getD3DX(),ang);
	return ret;
}

HDXMatrix3x3 HDXMatrix3x3::gen::rotationAxisX(float ang) {
	HDXMatrix3x3 ret;
	D3DXMatrixRotationX(&ret._d3dx,ang);
	return ret;
}

HDXMatrix3x3 HDXMatrix3x3::gen::rotationAxisY(float ang) {
	HDXMatrix3x3 ret;
	D3DXMatrixRotationY(&ret._d3dx,ang);
	return ret;
}

HDXMatrix3x3 HDXMatrix3x3::gen::rotationAxisZ(float ang) {
	HDXMatrix3x3 ret;
	D3DXMatrixRotationZ(&ret._d3dx,ang);
	return ret;
}

HDXMatrix3x3 HDXMatrix3x3::gen::lookDirection(const HDXVector3 &localforward,const HDXVector3 &targetdir,const HDXVector3 &localup,const HDXVector3 &worldup) {
	const HDXVector3 tdir = targetdir.getNormal();
	const HDXVector3 localright = (localup % localforward).getNormal();
	const HDXVector3 worldright = (fabs(worldup*tdir)<0.99f ? (worldup % tdir) : (XUNIT3)).getNormal();
	const HDXVector3 perpworldup = (tdir % worldright).getNormal();

	HDXMatrix3x3 m1;
	m1.setColumn(0,worldright);
	m1.setColumn(1,perpworldup);
	m1.setColumn(2,tdir);
	HDXMatrix3x3 m2;
	m2.setColumn(0,localright);
	m2.setColumn(1,localup);
	m2.setColumn(2,localforward);
	m2 = m2 * m1;

// 	m2[HDXMatrix4x4::ij(0,3)] = -(m2.getColumn(0)*eye);
// 	m2[HDXMatrix4x4::ij(1,3)] = -(m2.getColumn(1)*eye);
// 	m2[HDXMatrix4x4::ij(2,3)] = -(m2.getColumn(2)*eye);

	return m2;
}

HDXMatrix3x3 HDXMatrix3x3::gen::view(const HDXVector3 &right,const HDXVector3 &up,const HDXVector3 &forward) {
	HDXMatrix3x3 ret;
	ret.setRow(0,right);
	ret.setRow(1,up);
	ret.setRow(2,forward);
	return ret;
}

unsigned HDXMatrix4x4::ij(unsigned i,unsigned j) {
	return 4*j+i;
}

HDXMatrix4x4 HDXMatrix4x4::operator*(const HDXMatrix4x4 &r) const {
	HDXMatrix4x4 ret = *this; ret *= r; return ret;
}

HDXMatrix4x4& HDXMatrix4x4::operator*=(const HDXMatrix4x4 &r) {
	_d3dx *= r._d3dx; return *this;
}

HDXMatrix4x4& HDXMatrix4x4::operator*=(float r) {
	_d3dx *= r; return *this;
}

HDXMatrix4x4& HDXMatrix4x4::setTranspose() {
	return *this = getTranspose();
}

HDXMatrix4x4 HDXMatrix4x4::getTranspose() const {
	HDXMatrix4x4 ret; D3DXMatrixTranspose(&ret._d3dx,&_d3dx); return ret;
}

HDXMatrix4x4& HDXMatrix4x4::setInvert() {
	return *this = getInvert();
}

HDXMatrix4x4 HDXMatrix4x4::getInvert() const {
	HDXMatrix4x4 ret; D3DXMatrixInverse(&ret._d3dx,0,&_d3dx); return ret;
}

HDXMatrix4x4& HDXMatrix4x4::setIdentity() {
	D3DXMatrixIdentity(&_d3dx); return *this;
}

HDXVector4 HDXMatrix4x4::getScale() const {
	return HDXVector4(getRow(0).magnitude(),
					  getRow(1).magnitude(),
					  getRow(2).magnitude(),
					  getRow(3).magnitude());
}

void HDXMatrix4x4::setColumn(unsigned i,const HDXVector4 &vec) {
	for(unsigned ind = 0; ind < 4; ind++) (*this)[ij(i,ind)] = vec[ind];
}

void HDXMatrix4x4::setColumn(unsigned i,const HDXVector3 &vec) {
	for(unsigned ind = 0; ind < 3; ind++) (*this)[ij(i,ind)] = vec[ind];
}

HDXVector4 HDXMatrix4x4::getColumn(unsigned i) const {
	return HDXVector4((*this)[ij(i,0)],(*this)[ij(i,1)],(*this)[ij(i,2)],(*this)[ij(i,3)]);
}

void HDXMatrix4x4::setRow(unsigned i,const HDXVector4 &vec) {
	for(unsigned ind = 0; ind < 4; ind++) (*this)[ij(ind,i)] = vec[ind];
}

void HDXMatrix4x4::setRow(unsigned i,const HDXVector3 &vec) {
	for(unsigned ind = 0; ind < 3; ind++) (*this)[ij(ind,i)] = vec[ind];
}

HDXVector4 HDXMatrix4x4::getRow(unsigned i) const {
	return HDXVector4((*this)[ij(0,i)],(*this)[ij(1,i)],(*this)[ij(2,i)],(*this)[ij(3,i)]);
}

HDXMatrix3x3 HDXMatrix4x4::demote() const {
	return HDXMatrix3x3(*this);
}

float* HDXMatrix4x4::data() {
	return (float*)_d3dx;
}

const float* HDXMatrix4x4::data() const {
	return (const float*)_d3dx;
}

D3DXMATRIX* HDXMatrix4x4::getD3DX() {
	return &_d3dx;
}

const D3DXMATRIX* HDXMatrix4x4::getD3DX() const {
	return &_d3dx;
}

HDXMatrix4x4 HDXMatrix4x4::operator-() const {
	HDXMatrix4x4 ret = *this; ret._d3dx = -ret._d3dx; return ret;
}

HDXMatrix4x4 HDXMatrix4x4::operator-(const HDXMatrix4x4 &r) const {
	HDXMatrix4x4 ret = *this; ret -= r; return ret;
}

HDXMatrix4x4 HDXMatrix4x4::operator+(const HDXMatrix4x4 &r) const {
	HDXMatrix4x4 ret = *this; ret += r; return ret;
}

HDXMatrix4x4& HDXMatrix4x4::operator/=(float r) {
	_d3dx /= r; return *this;
}

HDXMatrix4x4& HDXMatrix4x4::operator-=(const HDXMatrix4x4 &r) {
	_d3dx -= r._d3dx; return *this;
}

HDXMatrix4x4& HDXMatrix4x4::operator+=(const HDXMatrix4x4 &r) {
	_d3dx += r._d3dx; return *this;
}

const float& HDXMatrix4x4::operator[](unsigned r) const {
	assert(r>=0 && r<16);
	return _d3dx[r];
}

float& HDXMatrix4x4::operator[](unsigned r) {
	assert(r>=0 && r<16);
	return _d3dx[r];
}

void HDXMatrix4x4::clear() {
	for(unsigned i = 0; i < 16; i++) (*this)[i] = 0.0f;
}

BOOL HDXMatrix4x4::operator!=(const HDXMatrix4x4 &r) const {
	return !(*this == r);
}

BOOL HDXMatrix4x4::operator==(const HDXMatrix4x4 &r) const {
	return _d3dx == r._d3dx;
}

HDXMatrix4x4& HDXMatrix4x4::operator=(const HDXMatrix4x4 &r) {
	_d3dx = r._d3dx; return *this;
}

HDXMatrix4x4::~HDXMatrix4x4() {
}

HDXMatrix4x4::HDXMatrix4x4(const D3DXMATRIX &r) : _d3dx(r) {
}

HDXMatrix4x4::HDXMatrix4x4(const HDXMatrix3x3 &r) : _d3dx(r._d3dx) {
	(*this)[15] = 1;
}

HDXMatrix4x4::HDXMatrix4x4(const HDXMatrix4x4 &r) : _d3dx(r._d3dx) {
}

HDXMatrix4x4::HDXMatrix4x4(FLOAT _11,FLOAT _12,FLOAT _13,FLOAT _14,FLOAT _21,FLOAT _22,FLOAT _23,FLOAT _24,FLOAT _31,FLOAT _32,FLOAT _33,FLOAT _34,FLOAT _41,FLOAT _42,FLOAT _43,FLOAT _44) : _d3dx(_11,_12,_13,_14,_21,_22,_23,_24,_31,_32,_33,_34,_41,_42,_43,_44) {
}

HDXMatrix4x4::HDXMatrix4x4() : _d3dx() {
}

float HDXMatrix4x4::determinant() const {
	return D3DXMatrixDeterminant(&_d3dx);
}

HDXMatrix3x3 HDXMatrix4x4::getRotation() const {
	HDXMatrix3x3 ret;
	for(int i = 0; i < 3; i++) ret.setRow(i,getRow(i).getNormal());
	return ret;
}

HDXMatrix4x4 HDXMatrix4x4::gen::identity() {
	return HDXMatrix4x4().setIdentity();
}

HDXMatrix4x4 HDXMatrix4x4::gen::translation3d(const HDXVector3 &v) {
	HDXMatrix4x4 ret;
	D3DXMatrixTranslation(&ret._d3dx,v.x,v.y,v.z);
	return ret;
}

HDXMatrix4x4 HDXMatrix4x4::gen::transformation3d(const HDXVector3 &scale,const HDXVector3 &rotcenter,const HDXQuaternion &rot,const HDXVector3 &pos) {
	HDXMatrix4x4 ret;
	D3DXMatrixTransformation(&ret._d3dx,0,0,scale.getD3DX(),rotcenter.getD3DX(),rot.getD3DX(),pos.getD3DX());
	return ret;
}

HDXMatrix4x4 HDXMatrix4x4::gen::scale3d(const HDXVector3 &scale) {
	HDXMatrix4x4 ret;
	D3DXMatrixScaling(&ret._d3dx,scale.x,scale.y,scale.z);
	return ret;
}