
#include "hdx/shader/hdx9shader_phonglighting.h"

#include "hdx/util.h"
#include "hdx/hdx9math.h"


HDXShader_PhongLighting::HDXShader_PhongLighting(IDirect3DDevice9 *device,const char *fname,const char *techname) :
HDXShader_Base(device,fname,techname) {
	ID3DXEffect *mfx = getD3DEffect();
	if(mfx) {
		mhandle_flag_lightingenabled = mfx->GetParameterByName(0,"g_flag_lightingenabled");

		mhandle_mat_world = mfx->GetParameterByName(0,"g_w");
		mhandle_mat_world_inv_trans = mfx->GetParameterByName(0,"g_w_inv_trans");
		mhandle_eye_pos = mfx->GetParameterByName(0,"g_eye_pos");

		//not working in release mode for some reason
		//mfx->GetValue(mfx->GetParameterByName(0,"g_light_max"),&mlight_maxnum,sizeof(unsigned int));
		mlight_maxnum = 20;

		mhandle_light_num = mfx->GetParameterByName(0,"g_light_num");
		mhandle_light_type = mfx->GetParameterByName(0,"g_light_type");
		mhandle_light_ambient = mfx->GetParameterByName(0,"g_light_ambient");
		mhandle_light_diffuse = mfx->GetParameterByName(0,"g_light_diffuse");
		mhandle_light_specular = mfx->GetParameterByName(0,"g_light_specular");
		mhandle_light_pos_w = mfx->GetParameterByName(0,"g_light_pos_w");
		mhandle_light_attenuation_012 = mfx->GetParameterByName(0,"g_light_attenuation_012");
		mhandle_light_direction = mfx->GetParameterByName(0,"g_light_direction");
		mhandle_light_falloff = mfx->GetParameterByName(0,"g_light_falloff");
		mhandle_light_theta = mfx->GetParameterByName(0,"g_light_theta");
		mhandle_light_phi = mfx->GetParameterByName(0,"g_light_phi");

		// ensures that the shader has the correct data
		mflag_lightingenabled = false;
		setLightingEnabled(true);
	}
}

HDXShader_PhongLighting::~HDXShader_PhongLighting() {

}

void HDXShader_PhongLighting::setMatrixWorld(const HDXMatrix4x4 &mat) {
	HDXShader_Base::setMatrixWorld(mat);

	ID3DXEffect *mfx = getD3DEffect();
	if(mfx) {
		const HDXMatrix4x4 wld_inv_trans = mat.getInvert().getTranspose();

		mfx->SetMatrix(mhandle_mat_world,mat.getD3DX());
		mfx->SetMatrix(mhandle_mat_world_inv_trans,wld_inv_trans.getD3DX());
		mfx->CommitChanges();
	}
}

void HDXShader_PhongLighting::setMatrixView(const HDXMatrix4x4 &mat) {
	HDXShader_Base::setMatrixView(mat);

	ID3DXEffect *mfx = getD3DEffect();
	if(mfx) {
		const HDXMatrix4x4 view_inv = mat.getInvert();

		const HDXVector3 eye = view_inv.getRow(3);

		mfx->SetFloatArray(mhandle_eye_pos,eye.data(),3);
		mfx->CommitChanges();
	}
}

bool HDXShader_PhongLighting::isLightingEnabled() const {
	return mflag_lightingenabled;
}

void HDXShader_PhongLighting::setLightingEnabled(bool enabled) {
	ID3DXEffect *mfx = getD3DEffect();

	if(mfx) {
		if(mflag_lightingenabled != enabled) {
			mflag_lightingenabled = enabled;
			mfx->SetBool(mhandle_flag_lightingenabled,enabled);
			mfx->CommitChanges();
		}
	}
}

void HDXShader_PhongLighting::doDraw(std::function<void(HDXShader_Base *hdxfx)> func) {
	HDXShader_Base::doDraw([&](HDXShader_Base *hdxfx)->void {
		ID3DXEffect *mfx = getD3DEffect();
		if(mfx) {
			unsigned totallightsnum = mlights.size();
			std::vector<int> lgt_type; lgt_type.reserve(totallightsnum);
			std::vector<D3DXCOLOR> lgt_col_ambient; lgt_col_ambient.reserve(totallightsnum);
			std::vector<D3DXCOLOR> lgt_col_diffuse; lgt_col_diffuse.reserve(totallightsnum);
			std::vector<D3DXCOLOR> lgt_col_specular; lgt_col_specular.reserve(totallightsnum);
			std::vector<D3DXVECTOR3> lgt_pos; lgt_pos.reserve(totallightsnum);
			std::vector<D3DXVECTOR3> lgt_attenuation; lgt_attenuation.reserve(totallightsnum);
			std::vector<D3DXVECTOR3> lgt_direction; lgt_direction.reserve(totallightsnum);
			std::vector<float> lgt_range; lgt_range.reserve(totallightsnum);
			std::vector<float> lgt_falloff; lgt_falloff.reserve(totallightsnum);
			std::vector<float> lgt_theta; lgt_theta.reserve(totallightsnum);
			std::vector<float> lgt_phi; lgt_phi.reserve(totallightsnum);

			mlights.doIterate([&](const HDXLight *light)->bool {
				if(lgt_type.size() < mlight_maxnum) {
					if(light->getType() >= 0 && light->isEnabled()) {
						lgt_type.push_back(light->getType());
						lgt_col_ambient.push_back(light->getColorAmbient());
						lgt_col_diffuse.push_back(light->getColorDiffuse());
						lgt_col_specular.push_back(light->getColorSpecular());
						lgt_pos.push_back(*light->getPosition().getD3DX());
						lgt_attenuation.push_back(*light->getAttenuation().getD3DX());
						lgt_direction.push_back(*light->getDirection().getD3DX());
						lgt_falloff.push_back(light->getFalloff());
						lgt_theta.push_back(light->getTheta());
						lgt_phi.push_back(light->getPhi());
					}
					return false;
				} else {
					return true; //break
				}
			});

			int numlights = lgt_type.size();
			mfx->SetInt(mhandle_light_num,numlights);
			mfx->SetIntArray(mhandle_light_type,lgt_type.data(),numlights);
			mfx->SetFloatArray(mhandle_light_ambient,(float*)lgt_col_ambient.data(),numlights*4);
			mfx->SetFloatArray(mhandle_light_diffuse,(float*)lgt_col_diffuse.data(),numlights*4);
			mfx->SetFloatArray(mhandle_light_specular,(float*)lgt_col_specular.data(),numlights*4);
			mfx->SetFloatArray(mhandle_light_pos_w,(float*)lgt_pos.data(),numlights*3);
			mfx->SetFloatArray(mhandle_light_attenuation_012,(float*)lgt_attenuation.data(),numlights*3);
			mfx->SetFloatArray(mhandle_light_direction,(float*)lgt_direction.data(),numlights*3);
			mfx->SetFloatArray(mhandle_light_falloff,(float*)lgt_falloff.data(),numlights);
			mfx->SetFloatArray(mhandle_light_theta,(float*)lgt_theta.data(),numlights);
			mfx->SetFloatArray(mhandle_light_phi,(float*)lgt_phi.data(),numlights);
			mfx->CommitChanges();

			func(hdxfx);
		}
	});
}

HDXLight* HDXShader_PhongLighting::createLight() {
	int newid = mlights.addItem(0);
	HDXLight *light = new HDXLight(newid);
	mlights.setItem(newid,light);
	return light;
}

HDXLight* HDXShader_PhongLighting::getLight(int id) {
	return mlights.getItem(id);
}

void HDXShader_PhongLighting::destroyLight(int id) {
	HDXLight *light = getLight(id);
	if(light) {
		delete light;
		mlights.removeItem(id);
	}
}
