#include "hdx/hdx9geometry.h"

#include "hdx/util.h"
#include "hdx/hdx9math.h"

#include <list>
#include <set>
#include <stack>
#include <unordered_map>
#include <map>
#include <numeric>

HDXConvexHull::HDXConvexHull() {
	mverts.clear();
	mtriangles.clear();
	madjacency.clear();
	maabbhs.clear();
	mcentroid.clear();
}

HDXConvexHull::HDXConvexHull(const HDXConvexHull &r) :
mverts(r.mverts),
mtriangles(r.mtriangles),
madjacency(r.madjacency),
maabbhs(r.maabbhs),
mcentroid(r.mcentroid) {
}

HDXConvexHull::HDXConvexHull(HDXConvexHull &&r) :
mverts(std::forward<std::vector<HDXVector3>>(r.mverts)),
mtriangles(std::forward<std::vector<std::array<unsigned,3>>>(r.mtriangles)),
madjacency(std::forward<std::vector<std::vector<unsigned>>>(r.madjacency)),
maabbhs(std::forward<HDXVector3>(r.maabbhs)),
mcentroid(std::forward<HDXVector3>(r.mcentroid)) {
}

HDXConvexHull::~HDXConvexHull() {
}

HDXConvexHull& HDXConvexHull::operator=(const HDXConvexHull &r) {
	mverts = r.mverts;
	mtriangles = r.mtriangles;
	madjacency = r.madjacency;
	maabbhs = r.maabbhs;
	mcentroid = r.mcentroid;
	return *this;
}

HDXConvexHull& HDXConvexHull::operator=(HDXConvexHull &&r) {
	mverts = std::forward<std::vector<HDXVector3>>(r.mverts);
	mtriangles = std::forward<std::vector<std::array<unsigned,3>>>(r.mtriangles);
	madjacency = std::forward<std::vector<std::vector<unsigned>>>(r.madjacency);
	maabbhs = std::forward<HDXVector3>(r.maabbhs);
	mcentroid = std::forward<HDXVector3>(r.mcentroid);
	return *this;
}

void HDXConvexHull::offset(const HDXVector3 &vec) {
	for(auto &v : mverts) {
		v += vec;
	}
	mcentroid += vec;
}

void barycentric(const HDXVector3 &p,const HDXVector3 &a,const HDXVector3 &b,const HDXVector3 &c,float *u,float *v,float *w) {
	// code from Crister Erickson's Real-Time Collision Detection
	HDXVector3 v0 = b - a,v1 = c - a,v2 = p - a;
	float d00 = v0*v0;
	float d01 = v0*v1;
	float d11 = v1*v1;
	float d20 = v2*v0;
	float d21 = v2*v1;
	float denom = d00 * d11 - d01 * d01;
	*v = (d11 * d20 - d01 * d21) / denom;
	*w = (d00 * d21 - d01 * d20) / denom;
	*u = 1.0f - *v - *w;
}

float pointDistanceFromLine(const HDXVector3 &p,const HDXVector3 &a,const HDXVector3 &b) {
	return ((b-a)%(a-p)).magnitude() / (b-a).magnitude();
}

float pointDistanceFromTriangle(const HDXVector3 &p,const HDXVector3 &a,const HDXVector3 &b,const HDXVector3 &c) {
	const HDXVector3 plane_n = ((b-a)%(c-a)).getNormal();
	const float plane_d = plane_n * a;

	HDXVector3 p_ontrianle = p - (plane_n * (plane_n*p - plane_d));

	float tri_u,tri_v,tri_w;
	barycentric(p_ontrianle,a,b,c,&tri_u,&tri_v,&tri_w);
	tri_u = CLAMP(tri_u,0,1);
	tri_v = CLAMP(tri_v,0,1);
	tri_w = CLAMP(tri_w,0,1);

	p_ontrianle = p_ontrianle*tri_u + p_ontrianle*tri_v + p_ontrianle*tri_w;

	return (p_ontrianle-p).magnitude() * SIGN(plane_n * (p+plane_n*-plane_d));
}

float pointDistanceFromPlane(const HDXVector3 &p,const HDXVector3 &a,const HDXVector3 &b,const HDXVector3 &c) {
	const HDXVector3 n = ((b-a)%(c-a)).getNormal();
	return pointDistanceFromPlane(p,n,a);
}

float pointDistanceFromPlane(const HDXVector3 &p,const HDXVector3 &n,const HDXVector3 &a) {
	const float d = n * a;
	return pointDistanceFromPlane(p,n,d);
}

float pointDistanceFromPlane(const HDXVector3 &p,const HDXVector3 &n,float d) {
	return n*p-d;
}

HDXConvexHull HDXQuickHull(HDXVector3 *pointcloud,unsigned n) {
	assert(n>=4); //must have at least 4 points or the algorithm will not work

	//adapted from the EPA algorithm implemented in collisiondetectionbv.cpp::col_convex_gjkepa()
	struct Triangle {
		HDXVector3 *points[3];
		HDXVector3 n;
		std::vector<int> pointstocheck;

		Triangle(HDXVector3 *a,HDXVector3 *b,HDXVector3 *c) {
			points[0] = a;
			points[1] = b;
			points[2] = c;
			n = ((*b-*a) % (*c-*a)).getNormal();
		}
	};
	struct Edge {
		HDXVector3 *points[2];

		Edge(HDXVector3 *a,HDXVector3 *b) {
			points[0] = a;
			points[1] = b;
		}
	};

	std::list<Triangle> lst_triangles;
	std::list<Edge> lst_edges;
	std::list<Triangle*> lst_facestocheck;
	//point list, second is true if point is available for use
	std::vector<std::pair<HDXVector3*,bool>> lst_points;

	auto lam_addEdge = [&](HDXVector3 *a,HDXVector3 *b)->void {
		for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
			if(it->points[0]==b && it->points[1]==a) {
				//opposite edge found, remove it and do not add new one
				lst_edges.erase(it);
				return;
			}
		}
		lst_edges.emplace_back(a,b);
	};

	auto lam_processPoints = [&]()->void {
		for(auto it = lst_facestocheck.begin(); it != lst_facestocheck.end(); it++) {
			(*it)->pointstocheck.clear();
		}

		for(unsigned i = 0; i < n; i++) {
			if(!lst_points[i].second) continue;

			bool assigned = false;
			for(auto it = lst_facestocheck.begin(); it != lst_facestocheck.end(); it++) {
				if((*it)->n * (*lst_points[i].first - *(*it)->points[0]) > 0) {
					assigned = true;
					(*it)->pointstocheck.push_back(i);
					break;
				}
			}

			if(!assigned) lst_points[i].second = false;
		}
	};

	lst_points.reserve(n);
	for(unsigned i = 0; i < n; i++) lst_points.emplace_back(&pointcloud[i],true);

	//build the initial polytope
	; {
		std::set<int> ep;

		//find up to 6 extreme points (no duplicates)
		for(int e = 0; e < 3; e++) {
			for(int c = 0; c < 2; c++) {
				unsigned best = -1;
				for(unsigned i = 0; i < n; i++) {
					if(!lst_points[i].second) continue;

					if(c==0) {
						if(best==-1 || (*lst_points[i].first)[e] < (*lst_points[best].first)[e]) best = i;
					} else {
						if(best==-1 || (*lst_points[i].first)[e] > (*lst_points[best].first)[e]) best = i;
					}
				}
				if(best==-1) continue;

				lst_points[best].second = false;
				ep.insert(best);
			}
		}

		assert(ep.size() >= 3);

		//find the most distant pair of points in ep
		int baseline[2];
		; {
			auto it = ep.begin();
			baseline[0] = *it++;
			baseline[1] = *it++;
			float baseline_dst = (*lst_points[baseline[0]].first-*lst_points[baseline[1]].first).magnitudeSq();
			for(auto ita = it; ita != ep.end(); ita++) {
				for(auto itb = std::next(ita); itb != ep.end(); itb++) {
					const float dst = (*lst_points[*itb].first-*lst_points[*ita].first).magnitudeSq();
					if(dst > baseline_dst) {
						baseline[0] = *ita;
						baseline[1] = *itb;
						baseline_dst = dst;
					}
				}
			}
		}

		//find the most distant point from the baseline in ep
		int basetip;
		; {
			basetip = 0;
			float basetip_dst = FLT_MIN;
			for(auto it = ep.begin(); it != ep.end(); it++) {
				if(*it == baseline[0] || *it == baseline[1]) continue;
				float dst = pointDistanceFromLine(*lst_points[*it].first,*lst_points[baseline[0]].first,*lst_points[baseline[1]].first);
				if(dst > basetip_dst) {
					basetip = *it;
					basetip_dst = dst;
				}
			}
		}

		//add the unused points from ep back into lst_points
		ep.erase(baseline[0]);
		ep.erase(baseline[1]);
		ep.erase(basetip);
		for(auto it = ep.begin(); it != ep.end(); it++) {
			lst_points[*it].second = true;
		}

		//find the most distant point from the triangle in the pointcloud
		int apex;
		; {
			unsigned best = -1;
			float apex_dst = FLT_MIN;
			for(unsigned i = 0; i < n; i++) {
				if(!lst_points[i].second) continue;
				float dst = fabs(pointDistanceFromPlane(*lst_points[i].first,*lst_points[baseline[0]].first,*lst_points[baseline[1]].first,*lst_points[basetip].first));
				if(dst > apex_dst) {
					best = i;
					apex_dst = dst;
				}
			}
			assert(best!=-1);

			lst_points[best].second = false;
			apex = best;
		}

		//build the tetrahedron with proper winding
		; {
			HDXVector3 *a,*b,*c,*d;

			a = lst_points[apex].first;
			b = lst_points[baseline[0]].first;
			c = lst_points[basetip].first;
			d = lst_points[baseline[1]].first;

			if(pointDistanceFromPlane(*a,*b,*d,*c) > 0) {
				//apex is in front of the base with current winding, rewind
				std::swap(c,d);
			}

			lst_triangles.emplace_back(a,b,c);
			lst_triangles.emplace_back(a,c,d);
			lst_triangles.emplace_back(a,d,b);
			lst_triangles.emplace_back(b,d,c);
		}
	}

	for(auto it = lst_triangles.begin(); it != lst_triangles.end(); it++) {
		lst_facestocheck.emplace_back(&*it);
	}

	lam_processPoints();

	while(true) {
		if(lst_facestocheck.size()==0) break;

		auto cur_triangle = lst_facestocheck.back(); lst_facestocheck.pop_back();

		HDXVector3 *newpoint = 0;

		; {
			unsigned best = -1;
			float dst_best = FLT_MIN;
			for(unsigned i = 0; i < cur_triangle->pointstocheck.size(); i++) {
				if(!lst_points[cur_triangle->pointstocheck[i]].second) continue;
				float dst = pointDistanceFromPlane(*lst_points[cur_triangle->pointstocheck[i]].first,*cur_triangle->points[0],*cur_triangle->points[1],*cur_triangle->points[2]);
				if(dst > dst_best) {
					best = cur_triangle->pointstocheck[i];
					dst_best = dst;
				}
			}
			if(best != -1) {
				lst_points[best].second = false;
				newpoint = lst_points[best].first;
			}
		}

		if(newpoint == 0) continue;

		for(auto it = lst_triangles.begin(); it != lst_triangles.end();) {
			//can this face be 'seen' by newpoint?
			if(it->n * (*newpoint - *it->points[0]) > 0) {
				lam_addEdge(it->points[0],it->points[1]);
				lam_addEdge(it->points[1],it->points[2]);
				lam_addEdge(it->points[2],it->points[0]);
				lst_facestocheck.remove(&*it);
				it = lst_triangles.erase(it);
				continue;
			}
			it++;
		}

		//create new triangles from the edges in the edge list
		for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
			lst_triangles.emplace_back(newpoint,it->points[0],it->points[1]);
			lst_facestocheck.emplace_back(&lst_triangles.back());
		}

		lst_edges.clear();

		lam_processPoints();
	}

	//build and return the convex hull structure
	HDXConvexHull ret;

	//map all of the hull's verts to a unique id while adding the triangles to the hull container
	//and generate adjacency information
	std::unordered_map<HDXVector3*,std::pair<unsigned,std::set<unsigned>>> map_vertinfo;
	unsigned curid = 0;
	for(auto it = lst_triangles.begin(); it != lst_triangles.end(); it++) {
		ret.mtriangles.emplace_back();
		for(unsigned i = 0; i < 3; i++) {
			auto ins = map_vertinfo.insert(std::make_pair(it->points[i],std::make_pair(curid,std::set<unsigned>())));
			ret.mtriangles.back()[i] = ins.first->second.first;
			if(ins.second) curid++;
		}
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(i==j) continue;
				map_vertinfo[it->points[i]].second.insert(map_vertinfo[it->points[j]].first);
			}
		}
	}

	//copy all of the vert data into the hull while calculating the centroid
	ret.mverts.resize(map_vertinfo.size());
	ret.madjacency.resize(map_vertinfo.size());
	ret.mcentroid.clear();

	HDXVector3 vec_aabbhs_min(FLT_MAX,FLT_MAX,FLT_MAX);
	HDXVector3 vec_aabbhs_max(FLT_MIN,FLT_MIN,FLT_MIN);
	for(auto it = map_vertinfo.begin(); it != map_vertinfo.end(); it++) {
		for(int i = 0; i < 3; i++) {
			vec_aabbhs_min[i] = (std::min)(vec_aabbhs_min[i],(*it->first)[i]);
			vec_aabbhs_max[i] = (std::max)(vec_aabbhs_max[i],(*it->first)[i]);
		}
		ret.mcentroid += *it->first;
		ret.mverts[it->second.first] = *it->first;
		std::copy(it->second.second.begin(),it->second.second.end(),std::back_inserter(ret.madjacency[it->second.first]));
	}
	ret.mcentroid /= (float)map_vertinfo.size();
	ret.maabbhs = (vec_aabbhs_max - vec_aabbhs_min)/2.0f;

	return std::move(ret);
}


std::vector<HDXConvexHull> HDXMeshDecomposeToConvexSet(HDXVector3 *points,unsigned numpoints,unsigned *triangles,unsigned numtriangles,float targetconcavity) {
	// algorithm based on Khaled Marmau's excellent paper: http://www.researchgate.net/publication/221129055_A_simple_and_efficient_approach_for_3D_mesh_approximate_convex_decomposition

	assert(numtriangles>0);

	// need to generate the convex hull of the mesh to measure concavity of features later
	HDXConvexHull convexhull = HDXQuickHull(points,numpoints);

	targetconcavity = 0.3f * convexhull.maabbhs.magnitude();

	assert(convexhull.mtriangles.size()>0);
	// need to pre-calculate the face normals for the hull's faces, as we will need them later
	std::vector<HDXVector3> convexhull_normals(convexhull.mtriangles.size());
	for(unsigned i = 0; i < convexhull.mtriangles.size(); i++) {
		const HDXVector3 &a = convexhull.mverts[convexhull.mtriangles[i][0]];
		const HDXVector3 &b = convexhull.mverts[convexhull.mtriangles[i][1]];
		const HDXVector3 &c = convexhull.mverts[convexhull.mtriangles[i][2]];
		convexhull_normals[i] = ((b-a)%(c-a)).getNormal();
	}

	// dual graph, implemented such that iteration and derefrencing is as fast as possible
	struct _dgraphvert {
		struct _edge {
			unsigned target; // id of the _dgraphvert this is pointing to
			unsigned a,b; // the ids of the points that connect the two shapes (from the current vert's point of view), used to determine perimiter

			_edge(unsigned target,unsigned a,unsigned b) : target(target), a(a), b(b) {}
		};

		bool active;
		HDXVector3 p;
		float perimiter;
		float area; // combinied area of this vert and all ancestor's areas
		std::list<unsigned> ancestors;
		std::list<_edge> edges;

		_dgraphvert() { active = true; }
	};

	std::vector<_dgraphvert> dgraph(numtriangles);

	// generate adjacency information for the points and use it to fill the dual graph
	; {
		// calculate triangle adjacency data and use it to initialize the dual graph
		// this is done by mapping a list of triangle by the two point ids of the edge
		std::map<std::pair<unsigned,unsigned>,std::vector<unsigned>> map_edge_tri;
		for(unsigned tri = 0; tri < numtriangles; tri++) {
			const unsigned &a = triangles[tri+0];
			const unsigned &b = triangles[tri+1];
			const unsigned &c = triangles[tri+2];
			map_edge_tri[{a,b}].push_back(tri);
			map_edge_tri[{b,c}].push_back(tri);
			map_edge_tri[{c,a}].push_back(tri);
		}

		// initialize the dual graph
		// query the edge map with reverse winding to find neighbors
		// the p(tri) is the triangle's centroid
		for(unsigned tri = 0; tri < numtriangles; tri++) {
			const unsigned &a = triangles[tri+0];
			const unsigned &b = triangles[tri+1];
			const unsigned &c = triangles[tri+2];
			std::vector<unsigned> *neighbors;

			neighbors = &map_edge_tri[{b,a}];
			for(auto &n : *neighbors) {
				dgraph[tri].edges.emplace_back(n,a,b);
			}

			neighbors = &map_edge_tri[{c,b}];
			for(auto &n : *neighbors) {
				dgraph[tri].edges.emplace_back(n,b,c);
			}

			neighbors = &map_edge_tri[{a,c}];
			for(auto &n : *neighbors) {
				dgraph[tri].edges.emplace_back(n,c,a);
			}

			dgraph[tri].p = (points[a]+points[b]+points[c])/3.0f;
			dgraph[tri].area = ((points[b]-points[a])%(points[c]-points[a])).magnitude() * 0.5f;
			dgraph[tri].perimiter = (points[b]-points[a]).magnitude() + (points[c]-points[b]).magnitude() + (points[a]-points[c]).magnitude();
		}
	}

	// calculates what the perimiter and area would be if w as to be merged into v
	auto lam_decimation_getAreaPerimiter = [&](unsigned v,unsigned w,float *area,float *perimiter)->void {
		_dgraphvert &_v = dgraph[v];
		_dgraphvert &_w = dgraph[w];

		*area = _v.area + _w.area;

		float _perimiter = _v.perimiter + _w.perimiter;

		// the perimiter is not exactly a straight addition, as the edge(s)
		// that were connecting the two verts are now gone, and therefore must be
		// subtracted from the final perimiter
		for(auto &edge : _v.edges) {
			if(edge.target == w) {
				_perimiter -= (points[edge.b] - points[edge.a]).magnitude();
			}
		}

		*perimiter = _perimiter;
	};

	// applies a half-edge collapse of dgraph point w into v
	// w is deactivated, essentially destroying it
	// v's position is now the average of both v's and w's positions
	// v assumes w's:
	//   ancestors, plus w its self
	//   edges
	//   area
	//   perimiter (minus the edge that was seperating them
	auto lam_decimation_helcol = [&](unsigned v,unsigned w)->void {
		_dgraphvert &_v = dgraph[v];
		_dgraphvert &_w = dgraph[w];
		_w.active = false;

		_v.p = _w.p;

		lam_decimation_getAreaPerimiter(v,w,&_v.area,&_v.perimiter);

		// remove all edges of v that point to w
		_v.edges.erase(std::remove_if(_v.edges.begin(),_v.edges.end(),[&](_dgraphvert::_edge &edge) { return edge.target == w; }),_v.edges.end());

		for(auto &wedge : _w.edges) {
			// change all edges pointing towards w to point towards v
			for(auto &oedge : dgraph[wedge.target].edges) {
				if(oedge.target == w) oedge.target = v;
			}
			// copy all of w's edges to v except for the one(s) that point to v
			if(wedge.target != v) {
				_v.edges.push_back(wedge);
			}
		}

		std::copy(_w.ancestors.begin(),_w.ancestors.end(),std::back_inserter(_v.ancestors));
		_v.ancestors.push_back(w);
	};

	// calculates the concavity of the resulting decimation, part of the cost
	// function as well as the exit condition
	auto lam_decimation_getConcavity = [&](unsigned v,unsigned w)->float {
		// get concavity of the the new v point by getting the distance
		// between the new point and its projection onto the mesh's convex hull
		// this projection is in the direction of the hull's centroid to thew new point
		// find the triangle on the hull who's normal most closely matches this direction
		// and use a barycentric projection to get the actual point
		
		HDXVector3 proj_dir = (dgraph[v].p - convexhull.mcentroid).getNormal();

		unsigned proj_tri_best = -1;
		float proj_tri_best_dot = FLT_MIN;
		for(unsigned i = 0; i < convexhull_normals.size(); i++) {
			float dot = convexhull_normals[i] * proj_dir;
			if(dot > proj_tri_best_dot) {
				proj_tri_best = i;
				proj_tri_best_dot = dot;
			}
		}
		assert(proj_tri_best!=-1);

		const HDXVector3 &proj_tri_a = convexhull.mverts[convexhull.mtriangles[proj_tri_best][0]];
		const HDXVector3 &proj_tri_b = convexhull.mverts[convexhull.mtriangles[proj_tri_best][1]];
		const HDXVector3 &proj_tri_c = convexhull.mverts[convexhull.mtriangles[proj_tri_best][2]];

		return (std::max)(0.0f,-pointDistanceFromTriangle(dgraph[v].p,proj_tri_a,proj_tri_b,proj_tri_c));
	};

	// the cost function guiding the decimation
	// calculates the cost of merging w and v
	auto lam_decimation_getCost = [&](unsigned v,unsigned w,float concavity)->float {
		float perimiter,area;
		lam_decimation_getAreaPerimiter(v,w,&area,&perimiter);

		// the aspect ratio of the shape
		const float E = (perimiter*perimiter)/(4*(float)M_PI*area);

		const float D = convexhull.maabbhs.magnitude();

		// allows the cost fuction to be dominated by the aspect ratio (E) during
		// its early stages, and the concavity during its later stages
		const float A = targetconcavity/(10*D);

		return concavity/D + A*E;
	};

	// run the iterative protion of the algorithm
	while(true) {
		// find the decimation with the lowest cost
		unsigned best_v = -1;
		unsigned best_w = -1;
		float min_concavity = FLT_MAX;
		float best_cost = FLT_MAX;
		for(unsigned i = 0; i < dgraph.size(); i++) {
			if(!dgraph[i].active) continue;
			const unsigned v = i;

			// prevent checking the same edge multiple times
			std::set<unsigned> checks;
			for(auto &edge : dgraph[i].edges) {
				const unsigned w = edge.target;
				if(w == v) continue;
				checks.insert(w);
			}

			for(auto &w : checks) {
				float concavity = lam_decimation_getConcavity(v,w);
				if(concavity < min_concavity) min_concavity = concavity;

				float cost = lam_decimation_getCost(v,w,concavity);
				if(cost < best_cost) {
					best_v = v;
					best_w = w;
					best_cost = cost;
				}
			}
		}

		// exit condition
		if(best_v==-1 || best_w==-1 || min_concavity >= targetconcavity) break;

		// perform decimation and repeat
		lam_decimation_helcol(best_v,best_w);
	}

	std::vector<HDXConvexHull> ret;

	for(unsigned i = 0; i < dgraph.size(); i++) {
		if(!dgraph[i].active) continue;
		//if(dgraph[i].ancestors.size()==0) continue;

		std::vector<std::array<unsigned,3>> lst_triangles(dgraph[i].ancestors.size()+1);
		int lst_tri_i = 0;
		for(auto &ans : dgraph[i].ancestors) {
			lst_triangles[lst_tri_i][0] = triangles[ans+0];
			lst_triangles[lst_tri_i][1] = triangles[ans+1];
			lst_triangles[lst_tri_i][2] = triangles[ans+2];
			lst_tri_i++;
		}
		lst_triangles[lst_tri_i][0] = triangles[i+0];
		lst_triangles[lst_tri_i][1] = triangles[i+1];
		lst_triangles[lst_tri_i][2] = triangles[i+2];

		HDXConvexHull subret;

		//map all of the hull's verts to a unique id while adding the triangles to the hull container
		//and generate adjacency information
		std::unordered_map<HDXVector3*,std::pair<unsigned,std::set<unsigned>>> map_vertinfo;
		unsigned curid = 0;
		for(auto it = lst_triangles.begin(); it != lst_triangles.end(); it++) {
			subret.mtriangles.emplace_back();
			for(unsigned i = 0; i < 3; i++) {
				auto ins = map_vertinfo.insert(std::make_pair(&points[(*it)[i]],std::make_pair(curid,std::set<unsigned>())));
				subret.mtriangles.back()[i] = ins.first->second.first;
				if(ins.second) curid++;
			}
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					if(i==j) continue;
					map_vertinfo[&points[(*it)[i]]].second.insert(map_vertinfo[&points[(*it)[j]]].first);
				}
			}
		}

		//copy all of the vert data into the hull while calculating the centroid
		subret.mverts.resize(map_vertinfo.size());
		subret.madjacency.resize(map_vertinfo.size());
		subret.mcentroid.clear();

		HDXVector3 vec_aabbhs_min(FLT_MAX,FLT_MAX,FLT_MAX);
		HDXVector3 vec_aabbhs_max(FLT_MIN,FLT_MIN,FLT_MIN);
		for(auto it = map_vertinfo.begin(); it != map_vertinfo.end(); it++) {
			for(int i = 0; i < 3; i++) {
				vec_aabbhs_min[i] = (std::min)(vec_aabbhs_min[i],(*it->first)[i]);
				vec_aabbhs_max[i] = (std::max)(vec_aabbhs_max[i],(*it->first)[i]);
			}
			subret.mcentroid += *it->first;
			subret.mverts[it->second.first] = *it->first;
			std::copy(it->second.second.begin(),it->second.second.end(),std::back_inserter(subret.madjacency[it->second.first]));
		}

		ret.push_back(subret);

/*
		std::set<unsigned> pointids;
		pointids.insert(triangles[i+0]);
		pointids.insert(triangles[i+1]);
		pointids.insert(triangles[i+2]);
		for(auto &ancestor : dgraph[i].ancestors) {
			pointids.insert(triangles[ancestor+0]);
			pointids.insert(triangles[ancestor+1]);
			pointids.insert(triangles[ancestor+2]);
		}

		if(pointids.size() < 4) continue;

		std::vector<HDXVector3> clusterpoints;
		for(auto &pid : pointids) {
			clusterpoints.push_back(points[pid]);
		}

		ret.push_back(HDXQuickHull(clusterpoints.data(),clusterpoints.size()));*/
	}

	return std::move(ret);
}

HDXMatrix3x3 inertialTensor_cylinder(float rad,float height,float mass) {
	HDXMatrix3x3 ret;
	if(mass > 0) {
		ret.clear();
		ret[0] = ret[4] = 1.0f/12.0f*mass*(3*SQ(rad)+SQ(height));
		ret[8] = 1.0f/2.0f*mass*rad*rad;
	} else {
		ret.clear();
	}
	return ret;
}

HDXMatrix3x3 inertialTensor_rod(float halflength,float mass) {
	HDXMatrix3x3 ret;
	if(mass > 0) {
		ret.clear();
		ret[0] = ret[4] = 1.0f/12.0f*mass*SQ(halflength*2.0f);
		ret[8] = 0.5f*mass*halflength*halflength;
	} else {
		ret.clear();
	}
	return ret;
}

HDXMatrix3x3 inertialTensor_box(const HDXVector3 &halfsize,float mass) {
	HDXMatrix3x3 ret;
	if(mass > 0) {
		ret.setIdentity();
		HDXVector3 squares;
		for(int i = 0; i < 3; i++) squares[i] = halfsize[i] * halfsize[i];
		ret[0] = 0.3f*mass*(squares.y + squares.z);
		ret[4] = 0.3f*mass*(squares.x + squares.z);
		ret[8] = 0.3f*mass*(squares.x + squares.y);
	} else {
		ret.clear();
	}
	return ret;
}

HDXMatrix3x3 inertialTensor_sphere(float r,float mass) {
	HDXMatrix3x3 ret;
	if(mass > 0) {
		ret.setIdentity();
		ret[0] = ret[4] = ret[8] = (float)0.4*mass*r*r;
	} else {
		ret.clear();
	}
	return ret;
}

HDXMatrix3x3 inertialTensor_convexhull(const HDXConvexHull &hull,float mass) {
	//implementation of Johnatan Blow's algorithm: http://number-none.com/blow/inertia/bb_inertia.doc
	//heavly influenced by: http://sourceforge.net/p/niftools/niflib/ci/master/tree/src/Inertia.cpp

	HDXMatrix3x3 cannocial_covariance(2.0f,1.0f,1.0f,
									  1.0f,2.0f,1.0f,
									  1.0f,1.0f,2.0f);

	std::vector<HDXMatrix4x4> accum_covariance; accum_covariance.reserve(hull.mtriangles.size());
	std::vector<float> accum_mass; accum_mass.reserve(hull.mtriangles.size());
	std::vector<HDXVector3> accum_center; accum_center.reserve(hull.mtriangles.size());

	//fill accumulators by forming a tetrahedron using each triangle
	//and the origin, calculate the transform required to change the
	//cannocial shape into this shape
	for(auto tri : hull.mtriangles) {
		const HDXVector3 &a = hull.mverts[tri[0]];
		const HDXVector3 &b = hull.mverts[tri[1]];
		const HDXVector3 &c = hull.mverts[tri[2]];

		HDXMatrix4x4 fromcanoc_transpose(a[0],a[1],a[2],0,
										 b[0],b[1],b[2],0,
										 c[0],c[1],c[2],0,
										 0,0,0,1);
		fromcanoc_transpose.setRow(0,a);
		fromcanoc_transpose.setRow(0,a);// ?
		HDXMatrix4x4 fromcanoc = fromcanoc_transpose.getTranspose();

		float fromcanoc_det = fromcanoc.determinant();

		//C' = (A det) * A * C * A^T
		accum_covariance.push_back(fromcanoc_transpose * cannocial_covariance * fromcanoc * fromcanoc_det);

		//m = (A det) / 6
		accum_mass.push_back(fromcanoc_det / 6.0f);

		accum_center.emplace_back(0.25f * (a[0] + b[0] + c[0]),
								  0.25f * (a[0] + b[0] + c[0]),
								  0.25f * (a[0] + b[0] + c[0]));
	}

	float total_mass = std::accumulate(accum_mass.begin(),accum_mass.end(),0.0f);
	if(total_mass < 0.0001f) {
		total_mass = 0;
	}

	//allows the mass to be specified rather than be dependant upon volume or density
	const float density = mass / total_mass;

	//weighted average of centers with masses
	HDXVector3 center(0,0,0);
	for(unsigned i = 0; i < accum_mass.size(); i++) {
		center += accum_center[i] * (accum_mass[i]/total_mass);
	}

	HDXMatrix4x4 total_covariance;
	total_covariance.setIdentity();
	for(auto &cov : accum_covariance) {
		total_covariance = total_covariance * cov;
	}
	
	//translate covariance to center of gravity:
	//C' = C - m * (x dt^T + dx x^T + dx dx^T)
	//x is the translation vector, dx is the center of gravity
	HDXMatrix4x4 translate_correction(center[0]*center[0],center[0]*center[1],center[0]*center[2],0,
									  center[1]*center[0],center[1]*center[1],center[1]*center[2],0,
									  center[2]*center[0],center[2]*center[1],center[2]*center[2],0,
									  0,0,0,1);
	translate_correction *= total_mass;

	//convert covariance matrix into interia tensor
	HDXMatrix4x4 trace_matrix;
	trace_matrix[0] = trace_matrix[5] = trace_matrix[10] = total_covariance[0] + total_covariance[5] + total_covariance[10];

	//correct for the calculated density
	HDXMatrix3x3 ret = (trace_matrix - total_covariance).getRotation() * density;
	return (trace_matrix - total_covariance).getRotation() * density;

	/*const unsigned _PARTICLESPERAXIS = 50;

	const float cube_size = 2*MAX3(hull.maabbhs.x,hull.maabbhs.y,hull.maabbhs.z);
	const unsigned particle_dirnum[3] = {
		(unsigned)((float)_PARTICLESPERAXIS*(hull.maabbhs[0]*2.0f/cube_size)),
		(unsigned)((float)_PARTICLESPERAXIS*(hull.maabbhs[1]*2.0f/cube_size)),
		(unsigned)((float)_PARTICLESPERAXIS*(hull.maabbhs[2]*2.0f/cube_size)),
	};
	const unsigned particle_num = particle_dirnum[0]*particle_dirnum[1]*particle_dirnum[2];
	const float particle_spacing = (float)_PARTICLESPERAXIS / cube_size;

	unsigned particle_active_num = particle_num;
	std::vector<bool> particle_list(particle_num,true);

	auto lam_calculatePoint = [&](const unsigned &x,const unsigned &y,const unsigned &z,unsigned *ind,HDXVector3 *out)->bool {
		*ind = z*particle_dirnum[0]*particle_dirnum[1] + y*particle_dirnum[0] + x;
		if(!particle_list[*ind]) return false;
		out->x = (float)((int)x-(int)(particle_dirnum[0]/2)) * particle_spacing;
		out->y = (float)((int)y-(int)(particle_dirnum[1]/2)) * particle_spacing;
		out->z = (float)((int)z-(int)(particle_dirnum[2]/2)) * particle_spacing;
		return true;
	};

	//pre-calculate all triangle planes
	std::vector<std::pair<HDXVector3,float>> tri_plane(hull.mtriangles.size());
	for(unsigned i = 0; i < hull.mtriangles.size(); i++) {
		const HDXVector3 &tri_a = hull.mverts[hull.mtriangles[i][0]];
		const HDXVector3 &tri_b = hull.mverts[hull.mtriangles[i][1]];
		const HDXVector3 &tri_c = hull.mverts[hull.mtriangles[i][2]];
		tri_plane[i].first = ((tri_b-tri_a) % (tri_c-tri_a)).getNormal();
		tri_plane[i].second = tri_plane[i].first * tri_a;
	}

	//eliminate all points not within the hull
	; {
		HDXVector3 pt_point;
		unsigned pt_id;
		for(unsigned x = 0; x < particle_dirnum[0]; x++) {
			for(unsigned y = 0; y < particle_dirnum[1]; y++) {
				for(unsigned z = 0; z < particle_dirnum[2]; z++) {
					if(!lam_calculatePoint(x,y,z,&pt_id,&pt_point)) continue;

					for(unsigned i = 0; i < tri_plane.size(); i++) {
						float dst = pointDistanceFromPlane(pt_point,tri_plane[i].first,tri_plane[i].second);
						if(dst > 0) {
							particle_list[pt_id] = false; //disable this point
							particle_active_num--;
							break;
						}
					}
				}
			}
		}
	}

	//calculate inertial tensor via the formula explained here: http://en.wikipedia.org/wiki/Moment_of_inertia
	HDXMatrix3x3 ret;
	ret.clear();
	; {
		const HDXMatrix3x3 ident = HDXMatrix3x3::gen::identity();

		const float particle_mass = mass / (float)particle_active_num;
		HDXVector3 pt_point;
		unsigned pt_id;
		for(unsigned x = 0; x < particle_dirnum[0]; x++) {
			for(unsigned y = 0; y < particle_dirnum[1]; y++) {
				for(unsigned z = 0; z < particle_dirnum[2]; z++) {
					if(!lam_calculatePoint(x,y,z,&pt_id,&pt_point)) continue;
					ret += particle_mass * ((pt_point*pt_point) * ident - pt_point.outer(pt_point));
				}
			}
		}
	}

	return ret;*/
}