#include "hdx/physics/physics.h"
#include "hdx/physics/contact.h"
#include "hdx/physics/rigidbody.h"
#include <algorithm>

PhysicsContact::PhysicsContact(const HDXVector3 &contactpoint,const HDXVector3 &normal,float penetration,RigidBody *b0,RigidBody *b1,float friction,float restitution) {
	mflag_remove = false;
	mtime_seperated = 0;
	mcolpoint = contactpoint;
	mnormal = normal;
	mpenetration = penetration;
	mbody[0] = b0;
	mbody[1] = b1;
	mfriction = friction;
	mrestitution = restitution;
	mtimesresolved = 0;

	mlife_body_change_linear[0].clear();
	mlife_body_change_linear[1].clear();

	// add the contact to each body's list
	for(int i = 0; i < 2; i++) {
		if(mbody[i]) {
			mbody[i]->mcontactinvolvement.push_back(this);
		}
	}
}

PhysicsContact::~PhysicsContact() {
	// remove the contact from each body's list
	for(int i = 0; i < 2; i++) {
		if(mbody[i]) {
			mbody[i]->mcontactinvolvement.erase(std::remove(mbody[i]->mcontactinvolvement.begin(),mbody[i]->mcontactinvolvement.end(),this),mbody[i]->mcontactinvolvement.end());
		}
	}
}

void PhysicsContact::swapData() {
	mnormal *= -1;
	std::swap(mbody[0],mbody[1]);
}

void PhysicsContact::updateInternals(float d) {
	if(mflag_remove) return;

	if(mbody[0]==0) {
		if(mbody[1]==0) {
			mflag_remove = true;
			return;
		} else {
			swapData(); //ensure the body in position 0 is valid
		}
	} else {
		if(mbody[1] && mbody[1]->hasFiniteMass() && !mbody[0]->hasFiniteMass()) swapData();
	}

	calculateContactBasis();

	mrelativecontactposition[0] = mcolpoint - mbody[0]->getComp()->getTrans()->getPosition();
	if(mbody[1]) mrelativecontactposition[1] = mcolpoint - mbody[1]->getComp()->getTrans()->getPosition();

	//find the relative velocity of the bodies at the contact point
	mcontactvelocity = calculateLocalVelocity(0,d);
	if(mbody[1]) mcontactvelocity -= calculateLocalVelocity(1,d);

	calculateDesiredDeltaVelocity(d);

	matchAwakeState(d);

	if((mtimesresolved > 0 && (!mbody[0]->mcancachecontacts || !mbody[1]->mcancachecontacts)) ||
	   ((!mbody[0]->isAwake() || !mbody[0]->hasFiniteMass()) &&
	   (!mbody[1]->isAwake() || !mbody[1]->hasFiniteMass()))) {
		mflag_remove = true;
	} else {
		float dst = 0;
		for(int i = 0; i < 2; i++) {
			dst += (mlife_body_change_linear[i]).magnitude();
		}
		if(dst >= HDX_PHYSICS_CONTACT_COLLISION_LIFETIME_MAXDST) {
			mtime_seperated += d;
			if(mtime_seperated >= HDX_PHYSICS_CONTACT_COLLISION_LIFETIME_SEPERATION || mpenetration < -HDX_PHYSICS_CONTACT_COLLISION_LIFETIME_MAXDST) {
				mflag_remove = true;
			}
		} else {
			mtime_seperated = 0;
		}
	}
}

void PhysicsContact::matchAwakeState(float dt) {
	if((!mbody[0]||mbody[0]->getInverseMass()==0) || (!mbody[1]||mbody[1]->getInverseMass()==0)) return; //its a static object, which doesnt wake the other object up

	for(int i = 0; i < 2; i++) {
		if(!mbody[i]->isAwake()) {
			if((mdesireddeltavelocity > HDX_PHYSICS_CONTACT_COLLISION_WAKEUP_VELOCITY_THRESHOLD)) {
				mbody[i]->setAwake(true);
			}
		}
	}
}

void PhysicsContact::calculateContactBasis() {
	mcttw.setRow(0,mnormal);
	mcttw.orthonormalize();
	mcttw_trans = mcttw.getTranspose();
}

HDXVector3 PhysicsContact::calculateLocalVelocity(int bodyind,float d) {
	RigidBody *thisbody = mbody[bodyind];

	const HDXMatrix3x3 ctw_trans = mcttw_trans;

	//velocity of contact point
	HDXVector3 contactvelocity = ((thisbody->getAngularVelocity()%mrelativecontactposition[bodyind])+thisbody->getVelocity()) * ctw_trans;

	//amount of velocity that is due to forces w/o reactions
	HDXVector3 accvelocity = (thisbody->getLastFrameAcceleration() * d) * ctw_trans;

	//vector is in the contact's coordinate system, x axis is the contact normal. ignore accelleration in contact normal's direction
	accvelocity.x = 0;

	contactvelocity += accvelocity;
	return contactvelocity;
}

void PhysicsContact::calculateDesiredDeltaVelocity(float d) {
	//acceleration induced velocity accumulated this frame
	float velocityfromacc = 0;

	if(mbody[0] && mbody[0]->isAwake()) {
		velocityfromacc += mbody[0]->getLastFrameAcceleration() * d * mnormal;
	}

	if(mbody[1] && mbody[1]->isAwake()) {
		velocityfromacc -= mbody[1]->getLastFrameAcceleration() * d * mnormal;
	}

	//if velocity is slow limit the restitution (quell bounciness)
	float thisrestitution = mrestitution;
	if(abs(mcontactvelocity.x) < HDX_PHYSICS_CONTACT_COLLISION_VELOCITY_RESTLIMIT) thisrestitution = 0;

	mdesireddeltavelocity = -mcontactvelocity.x - thisrestitution * (mcontactvelocity.x - velocityfromacc);
}

void PhysicsContact::applyVelocityChange(HDXVector3 velocitychange[2],HDXVector3 angularvelocitychange[2]) {
	//tensors in world coords
	HDXMatrix3x3 iitw[2] = {
		mbody[0]->getInverseInertiaTensorWorld(),
		(mbody[1] ? mbody[1]->getInverseInertiaTensorWorld() : HDXMatrix3x3::gen::identity())
	};

	HDXVector3 impulsecontact;

	if(mfriction == 0) { // no friction
		impulsecontact = calculateFrictionlessImpulse(iitw);
	} else { // friction
		impulsecontact = calculateFrictionImpulse(iitw);
	}

	//in world coords
	HDXVector3 impulse = impulsecontact * mcttw;

	for(int i = 0; i < 2; i++) {
		if(mbody[i]) {
			//split impulse into linear and rotational components
			HDXVector3 impulseivetorque = mrelativecontactposition[i] % impulse;
			angularvelocitychange[i] = impulseivetorque * iitw[i];
			velocitychange[i] = impulse * mbody[i]->getInverseMass();
			//apply changes
			if(mbody[i]->getInverseMass()>0) {
				mbody[i]->addVelocity(velocitychange[i]);
				mbody[i]->addAngularVelocity(angularvelocitychange[i]);
			}
		} else {
			angularvelocitychange[i].clear();
			velocitychange[i].clear();
		}
		impulse *= -1;
	}
}

HDXVector3 PhysicsContact::calculateFrictionlessImpulse(const HDXMatrix3x3 inverseinertiatensor[2]) {
	HDXVector3 impulsecontact;

	//change in velocity in contact coords
	float deltavelocity = 0;

	for(int i = 0; i < 2; i++) {
		if(!mbody[i]) continue;

		//build vector that shows the change in velocity in world space for a unit impulse in the direction of normal
		HDXVector3 deltavelworld = mrelativecontactposition[i] % mnormal;
		deltavelworld = deltavelworld * inverseinertiatensor[i];
		deltavelworld = deltavelworld % mrelativecontactposition[i];
		deltavelocity += deltavelworld * mnormal;

		deltavelocity += mbody[i]->getInverseMass();
	}

	impulsecontact.x = mdesireddeltavelocity / deltavelocity;
	impulsecontact.y = impulsecontact.z = 0;

	return impulsecontact;
}

HDXVector3 PhysicsContact::calculateFrictionImpulse(const HDXMatrix3x3 inverseinertiatensor[2]) {
	HDXVector3 impulsecontact;
	HDXMatrix3x3 deltavelworld;
	float inversemass = 0;
	deltavelworld.clear();

	for(int i = 0; i < 2; i++) {
		if(!mbody[i]) continue;

		HDXMatrix3x3 impulsetotorque = HDXMatrix3x3::gen::skewSymmetric(mrelativecontactposition[i]);
		//build matrix to convert contact impulse to change in velocity in world coords
		HDXMatrix3x3 tmpdeltavelworld = -1.0f * impulsetotorque * inverseinertiatensor[i] * impulsetotorque;

		deltavelworld += tmpdeltavelworld;
		inversemass += mbody[i]->getInverseMass();
	}

	//convert into contact coords
	HDXMatrix3x3 deltavelocity = mcttw * deltavelworld * mcttw_trans;
	//add linear velocity change
	deltavelocity[0] += inversemass;
	deltavelocity[4] += inversemass;
	deltavelocity[8] += inversemass;

	//invert to get the impulse needed per unit velocity
	HDXMatrix3x3 impulsematrix = deltavelocity.getInvert();

	//find the target velocies to kill
	HDXVector3 velkill(mdesireddeltavelocity,
					   -mcontactvelocity.y,
					   -mcontactvelocity.z);

	//find the impule to kill target velocities
	impulsecontact = velkill * impulsematrix;

	//check for exceeding friction
	float planarimpulse = sqrt(impulsecontact.y*impulsecontact.y +
							   impulsecontact.z*impulsecontact.z);
	if(planarimpulse>impulsecontact.x*mfriction) {
		//dynamic friction
		impulsecontact.y /= planarimpulse;
		impulsecontact.z /= planarimpulse;

		impulsecontact.x = deltavelocity[0] +
			deltavelocity[1]*mfriction*impulsecontact.y +
			deltavelocity[2]*mfriction*impulsecontact.z;

		impulsecontact.x = mdesireddeltavelocity / impulsecontact.x;
		impulsecontact.y *= mfriction * impulsecontact.x;
		impulsecontact.z *= mfriction * impulsecontact.x;
	}
	return impulsecontact;
}

void PhysicsContact::applyPositionChange(HDXVector3 linearchange[2],HDXVector3 angularchange[2],float penetration) {
	float angularmove[2];
	float linearmove[2];
	float totalinertia = 0;
	float linearinertia[2];
	float angularinertia[2];

	//calculate inertia of objects in normal direction
	for(int i = 0; i < 2; i++) {
		if(!mbody[i]) continue;

		HDXVector3 angularinertiaworld = mrelativecontactposition[i] % mnormal;
		angularinertiaworld = angularinertiaworld * mbody[i]->getInverseInertiaTensorWorld();
		angularinertiaworld = angularinertiaworld % mrelativecontactposition[i];
		angularinertia[i] = angularinertiaworld * mnormal;

		linearinertia[i] = mbody[i]->getInverseMass();

		totalinertia += linearinertia[i] + angularinertia[i];
	}

	//apply the changes
	for(int i = 0; i < 2; i++) {
		if(!mbody[i]) continue;

		float sign = (float)(i==0 ? 1 : -1);
		angularmove[i] = sign*penetration*(angularinertia[i]/totalinertia);
		linearmove[i] = sign*penetration*(linearinertia[i]/totalinertia);

		//limit the angular move to avoid large mass/low inertial tensor objects from overrotating
		HDXVector3 projection = mrelativecontactposition[i] +
			(mnormal * -(mrelativecontactposition[i]*mnormal));

		float maxmagnitude = HDX_PHYSICS_CONTACT_COLLISION_ANGULARCHANGELIMIT*projection.magnitude();

		//if the angular move is too high transfer burdon to linear movement
		if(angularmove[i] < -maxmagnitude) {
			float totalmove = angularmove[i] + linearmove[i];
			angularmove[i] = -maxmagnitude;
			linearmove[i] = totalmove-angularmove[i];
		} else if(angularmove[i] > maxmagnitude) {
			float totalmove = angularmove[i] + linearmove[i];
			angularmove[i] = maxmagnitude;
			linearmove[i] = totalmove-angularmove[i];
		}

		if(angularmove[i]==0) {
			angularchange[i].clear(); // no angular movement
		} else {
			//determine direction to rotate in
			HDXVector3 targetangulardirection = mrelativecontactposition[i] % mnormal;

			angularchange[i] = (targetangulardirection*mbody[i]->getInverseInertiaTensorWorld())*(angularmove[i]/angularinertia[i]);
		}

		linearchange[i] = mnormal * linearmove[i];

		float mag = linearchange[i].magnitude();

		if(mbody[i]->getInverseMass()>0) {
			//apply linear movement
			mbody[i]->getComp()->getTrans()->addPosition(linearchange[i]);

			//apply orientation change
			mbody[i]->getComp()->getTrans()->setOrientation(mbody[i]->getComp()->getTrans()->getOrientation()+angularchange[i]);
		}

		//if an object is asleep its derived data will not be automatically updated, so update it manually
		if(!mbody[i]->isAwake()) mbody[i]->_syncInternals();
	}
}