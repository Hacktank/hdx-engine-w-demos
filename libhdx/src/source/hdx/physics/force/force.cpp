#include "hdx/physics/force/force.h"
#include "hdx/physics/rigidbody.h"
#include "hdx/component_transform.h"
#include "hdx/physics/physics.h"

#include "hdx/hdx9.h"

#include <algorithm>
#include <set>

BaseForce::BaseForce() {
	mregistered = 0;
	mawakensbody = true;
}

BaseForce::~BaseForce() {
	// ensure that if the force is destroyed that it is removed from the registry
	if(mregistered) mregistered->remove(this);
}

ForceRegistry::ForceRegistry() {
	clear();
	mcbid_onupdate_pre = HDX_MAIN->registerFuncUpdatePre([&](float dt)->void {
		updateForces(dt);
	});
}

ForceRegistry::~ForceRegistry() {
	clear();
	HDX_MAIN->unregisterFuncUpdatePre(mcbid_onupdate_pre);
}

void ForceRegistry::add(RigidBody *body,BaseForce *force) {
	ForceRegistration newreg;
	newreg.body = body;
	newreg.force = force;
	force->mregistered = this;
	mregistrations.push_back(newreg);
}

ForceRegistry::REGISTRYCONT::iterator ForceRegistry::remove(REGISTRYCONT::iterator it) {
	return mregistrations.erase(it);
}

void ForceRegistry::remove(RigidBody *body,BaseForce *force) {
	for(REGISTRYCONT::iterator it = mregistrations.begin(); it!=mregistrations.end(); it++) {
		if((*it).body == body && (*it).force == force) {
			it = remove(it);
			break;
		}
	}
}

void ForceRegistry::remove(RigidBody *body) {
	for(REGISTRYCONT::iterator it = mregistrations.begin(); it!=mregistrations.end();) {
		if((*it).body == body) {
			it = remove(it);
			continue;
		}
		it++;
	}
}

void ForceRegistry::remove(BaseForce *force) {
	for(REGISTRYCONT::iterator it = mregistrations.begin(); it!=mregistrations.end();) {
		if((*it).force == force) {
			it = remove(it);
			continue;
		}
		it++;
	}
}

void ForceRegistry::clear() {
	mregistrations.clear();
}

void ForceRegistry::updateForces(float dt) {
	std::set<BaseForce*> forceremovalqueue;

	for(REGISTRYCONT::iterator it = mregistrations.begin(); it!=mregistrations.end(); it++) {
		(*it).force->updateForce((*it).body,dt);
		if((*it).force->p_shouldDelete()) forceremovalqueue.insert((*it).force);
	}

	for(std::set<BaseForce*>::iterator it = forceremovalqueue.begin(); it!=forceremovalqueue.end(); it++) {
		remove(*it);
		//delete *it;
	}
}

ForceSpringPoint::ForceSpringPoint(const HDXVector3 &point,float springconstant,float restlength) {
	mpoint = point;
	mspringconstant = springconstant;
	mrestlength = restlength;
	mselfoffset.clear();
}

void ForceSpringPoint::updateForce(RigidBody *body,float dt) {
	HDXVector3 kx = body->getPointInWorldSpace(mselfoffset)-mpoint;

	float magnitude = kx.magnitude();
	magnitude -= mrestlength;
	magnitude *= mspringconstant;

	kx.setNormal();
	kx *= -magnitude;

	bool tmp = body->isAwake();
	body->addForceAtBodyPoint(kx,mselfoffset);
	if(!mawakensbody && !tmp) body->setAwake(false);
}

ForceSpringObject::ForceSpringObject(RigidBody *other,float springconstant,float restlength) {
	mother = other;
	mspringconstant = springconstant;
	mrestlength = restlength;
	mselfoffset.clear();
	motheroffset.clear();
}

void ForceSpringObject::updateForce(RigidBody *body,float dt) {
	HDXVector3 kx = body->getPointInWorldSpace(mselfoffset)-
		mother->getPointInWorldSpace(motheroffset);

	float magnitude = kx.magnitude();
	magnitude -= mrestlength;
	magnitude *= mspringconstant;

	kx.setNormal();
	kx *= -magnitude;

	bool tmp = body->isAwake();
	body->addForceAtBodyPoint(kx,mselfoffset);
	if(!mawakensbody && !tmp) body->setAwake(false);
}

ForceBungeePoint::ForceBungeePoint(HDXVector3 point,float springconstant,float restlength) {
	mpoint = point;
	mspringconstant = springconstant;
	mrestlength = restlength;
	mselfoffset.clear();
}

void ForceBungeePoint::updateForce(RigidBody *body,float dt) {
	HDXVector3 kx = body->getPointInWorldSpace(mselfoffset)-mpoint;

	float magnitude = kx.magnitude();
	magnitude -= mrestlength;
	magnitude *= mspringconstant;

	if(magnitude<0) magnitude = 0;

	kx.setNormal();
	kx *= -magnitude;

	bool tmp = body->isAwake();
	body->addForceAtBodyPoint(kx,mselfoffset);
	if(!mawakensbody && !tmp) body->setAwake(false);
}

ForceBungeeObject::ForceBungeeObject(RigidBody *other,float springconstant,float restlength) {
	mother = other;
	mspringconstant = springconstant;
	mrestlength = restlength;
	mselfoffset.clear();
	motheroffset.clear();
	mapplytoboth = false;
}

void ForceBungeeObject::updateForce(RigidBody *body,float dt) {
	HDXVector3 kx = body->getPointInWorldSpace(mselfoffset)-
		mother->getPointInWorldSpace(motheroffset);

	float magnitude = kx.magnitude();
	magnitude -= mrestlength;
	magnitude *= mspringconstant;

	if(magnitude<0) magnitude = 0;

	kx.setNormal();
	kx *= -magnitude;

	bool tmp = body->isAwake();
	body->addForceAtBodyPoint(kx,mselfoffset);
	if(!mawakensbody && !tmp) body->setAwake(false);

	if(mapplytoboth) {
		bool tmp = mother->isAwake();
		mother->addForceAtBodyPoint(-kx,motheroffset);
		if(!mawakensbody && !tmp) mother->setAwake(false);
	}
}

ForceGravityAxis::ForceGravityAxis(const HDXVector3 &gravity) {
	mgravity = gravity;
	mawakensbody = false;
}

void ForceGravityAxis::updateForce(RigidBody *body,float dt) {
	bool tmp = body->isAwake();
	body->addForce(mgravity*body->getMass());
	if(!mawakensbody && !tmp) body->setAwake(false);
}

ForceDrag::ForceDrag(float dragcoef,float area,float airdensity) {
	mdragcoef = dragcoef;
	marea = area;
	mairdensity = airdensity;
}

void ForceDrag::updateForce(RigidBody *body,float dt) {
	float velocity = body->getVelocity().magnitude();
	float dragforce = 0.5f*mdragcoef*mairdensity*marea*velocity*velocity;
	bool tmp = body->isAwake();
	const HDXVector3 velnormal = body->getVelocity().getNormal();
	body->addForce(velnormal*-dragforce);
	if(!mawakensbody && !tmp) body->setAwake(false);
}

ForceAirDrag::ForceAirDrag(float k1,float k2) {
	mk1 = k1;
	mk2 = k2;
}

void ForceAirDrag::updateForce(RigidBody *body,float dt) {
	const HDXVector3 &vel = body->getVelocity();
	static float oldvelm = FLT_MAX;
	float velm = vel.magnitude();

	float velmchange = abs(velm-oldvelm);
	oldvelm = velm;

	HDXVector3 velnormal = vel.getNormal();
	HDXVector3 dragforce = -(mk1*velm+mk2*velm*velm)*velnormal;

	bool tmp = body->isAwake();
	body->addForce(dragforce);
	if(!mawakensbody && !tmp) body->setAwake(false);
}

ForceBuoyancy::ForceBuoyancy(float fluiddensity,float displacedvolume,const HDXVector3 &gravity) {
	mfluiddensity = fluiddensity;
	mdisplacedvolume = displacedvolume;
	mgravity = gravity;
}

void ForceBuoyancy::updateForce(RigidBody *body,float dt) {
	HDXVector3 buoyantforce = mfluiddensity * mdisplacedvolume * -mgravity;
	bool tmp = body->isAwake();
	body->addForce(buoyantforce);
	if(!mawakensbody && !tmp) body->setAwake(false);
}

const bool ForceBlast::p_shouldDelete() const {
	return true;
}

ForceBlast::ForceBlast(const HDXVector3 &point,float maxdst,float strength) {
	mpoint = point;
	mstrength = strength;
	mmaxdst = maxdst;
}

void ForceBlast::updateForce(RigidBody *body,float dt) {
	HDXVector3 kx = body->getComp()->getTrans()->getPosition()-mpoint;
	float kxm = kx.magnitude();
	if(kxm <= mmaxdst) {
		kx = (kx/kxm) * ((float)1.0-kxm/mmaxdst) * ((float)1.0/dt) * mstrength;
		bool tmp = body->isAwake();
		body->addForce(kx);
		if(!mawakensbody && !tmp) body->setAwake(false);
	}
}