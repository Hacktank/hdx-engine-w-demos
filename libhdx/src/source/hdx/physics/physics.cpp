#include "hdx/physics/physics.h"
#include "hdx/entity.h"
#include "hdx/spacialpartition.h"
#include "hdx/physics/collision/detection/collisiondetectionbv.h"
#include "hdx/hdx9.h"
#include "hdx/hdx9geometry.h"
#include <algorithm>

component_Physics::component_Physics(Entity *entity) : component_Base(entity) {
	mbody = 0;
	mspreg = -1;
	mtrans = getEntity()->getComponent<component_Transform>(true);
	mspart = getEntity()->getComponent<component_SpacialPartition>(true);
	mcbid_onupdate = HDX_MAIN->registerFuncUpdate([&](float dt)->void {
		if(mbody) {
			mbody->update(dt);
		}
	});
}

component_Physics::~component_Physics() {
	mspreg = -1; //the destructor for component_SpacialPartition will take care of it, and it is likely already deconstructed at this point anyway
	deinitialize();
	HDX_MAIN->unregisterFuncUpdate(mcbid_onupdate);
}

void component_Physics::initializeSphere(float rad,float mass) {
	deinitialize();
	mbody = new RigidBody(this);
	mbody->initializeSphere(rad,mass);
	mspreg = mspart->addRegistrationAABB("physics",HDXVector3(rad,rad,rad),this);
}

void component_Physics::initializeBox(const HDXVector3 &ext,float mass) {
	deinitialize();
	mbody = new RigidBody(this);
	mbody->initializeBox(ext,mass);
	mspreg = mspart->addRegistrationOBB("physics",ext,this);
}

void component_Physics::initializeHalfspace(const HDXVector3 &pos,const HDXVector3 &normal) {
	deinitialize();
	mbody = new RigidBody(this);
	mbody->initializeHalfspace(pos,normal);
}

void component_Physics::initializePlane(const HDXVector3 &pos,const HDXVector3 &normal) {
	deinitialize();
	mbody = new RigidBody(this);
	mbody->initializePlane(pos,normal);
}

void component_Physics::initializeRod(float halflength,float mass) {
	deinitialize();
	mbody = new RigidBody(this);
	mbody->initializeRod(halflength,mass);
	mspreg = mspart->addRegistrationOBB("physics",HDXVector3(halflength,halflength,halflength),this);
}

void component_Physics::initializeConvexHull(HDXConvexHull hull,float mass) {
	deinitialize();
	mbody = new RigidBody(this);
	//have to access hull.maabbhs before it is forwarded
	mspreg = mspart->addRegistrationOBB("physics",hull.maabbhs,this);
	mbody->initializeConvexHull(std::forward<HDXConvexHull>(hull),mass);
}

void component_Physics::deinitialize() {
	if(mbody) {
		mbody->deinitialize();
		delete mbody;
		mbody = 0;
	}
	if(mspreg>=0) {
		mspart->removeRegistration(mspreg);
		mspreg = -1;
	}
}

PhysicsManager::PhysicsManager() {
	miterations_velocity_max = 2000;
	miterations_position_max = 2000;

	HDX_SPMAN->createUniverse("physics",20);

	mcbid_onupdate_post1 = HDX_MAIN->registerFuncUpdatePost1([&](float dt)->void {
		CollisionTestCont collisionchecklist;

		auto lam_checkCollision = [&](component_Physics *cad,component_Physics *cbd)->void {
			if(cad == cbd) return; // dont check against itself

			// COARSE COLLISION (sphere-sphere)
			; {
				const float a_rad = cad->getBody()->getBV()->getRadius() * VEC3MAXABSSCALAR(cad->getTrans()->getScale());
				const float b_rad = cbd->getBody()->getBV()->getRadius() * VEC3MAXABSSCALAR(cbd->getTrans()->getScale());
				const HDXVector3 a_cen = cad->getBody()->getBV()->getCenter();
				const HDXVector3 b_cen = cad->getBody()->getBV()->getCenter();

				if((b_cen-a_cen).magnitude() > a_rad+b_rad+HDX_PHYSICS_COLLISION_SKIN*2) return;
			}

			col_bv_bv(cad->getBody()->getBV(),cbd->getBody()->getBV(),[&](BaseBV *bv0,BaseBV *bv1,const HDXVector3 &contactpoint,const HDXVector3 &normal,float penetration)->void {
				penetration -= HDX_PHYSICS_COLLISION_SKIN*2;

				RigidBody *b[2] = {bv0->body,bv1->body};

				// check to ensure that this contact doesnt already exist, if it does remove the old one
				for(unsigned i = 0; i < 2; i++) {
					for(auto &contact : b[i]->mcontactinvolvement) {
						if(!(contact->mbody[0] == b[(i+1)%2] || contact->mbody[1] == b[(i+1)%2])) continue;
						if((contactpoint - contact->mcolpoint).magnitude() < HDX_PHYSICS_CONTACT_COLLISION_EQUALITY_EPSILON) contact->mflag_remove = true;
					}
				}

				float friction = (b[0] ? b[0]->getFriction() : 1) * (b[1] ? b[1]->getFriction() : 1);
				float restitution(b[0]&&b[1] ? (std::max)(b[0]->getRestitution(),b[1]->getRestitution()) : (b[0] ? b[0]->getRestitution() : b[1]->getRestitution()));
				mcontacts.emplace_back(contactpoint,normal,penetration,b[0],b[1],friction,restitution);
			});
		};

		auto lam_isActive = [](component_Physics *cd)->bool {
			return cd->getBody() && cd->getBody()->getBV() && cd->getBody()->hasFiniteMass() && cd->getBody()->isAwake();
		};

		auto lam_addToCheckList = [&](component_Physics *cad,component_Physics *cbd)->bool {
			if(cad == cbd) return false;

			if(!lam_isActive(cad) && !lam_isActive(cbd)) return false;

			auto placement = collisionchecklist.emplace(cad,cbd);
			return placement.second;
		};

		HDX_SPMAN->doIterateUniverseBuckets([&](spdata::bucket &bucket)->bool {
			unsigned int bucketsize = bucket.mregs.size();
			for(unsigned int i = 0; i < bucketsize; i++) {
				component_Physics *cad = (component_Physics*)bucket.mregs[i]->muserdata;
				for(unsigned int j = i+1; j < bucketsize; j++) {
					component_Physics *cbd = (component_Physics*)bucket.mregs[j]->muserdata;
					lam_addToCheckList(cad,cbd);
				}
				for(auto opc = momnipresentcandidates.begin(); opc != momnipresentcandidates.end(); opc++) {
					lam_addToCheckList(cad,*opc);
				}
			}

			return false;
		},"physics");

		for(auto it = collisionchecklist.begin(); it != collisionchecklist.end(); it++) {
			lam_checkCollision(it->candidates[0],it->candidates[1]);
		}

		// resolve contacts in order of severity

		_processContacts(dt);		
	});
}

PhysicsManager::~PhysicsManager() {
	HDX_MAIN->unregisterFuncUpdatePost1(mcbid_onupdate_post1);
}

const PhysicsManager::ContactCont& PhysicsManager::getContacts() {
	return mcontacts;
}

void PhysicsManager::_processContacts(float d) {
	if(mcontacts.size()==0) return;

	// update all contacts internals (closing velocity, local basis, etc)
	for(auto &contact : mcontacts) {
		contact.updateInternals(d);
	}

	// remove any contacts that have become invalid
	for(auto it = mcontacts.begin(); it != mcontacts.end();) {
		if(it->mflag_remove) {
			it = mcontacts.erase(it);
			continue;
		} else {
			it->mtimesresolved++;
		}
		it++;
	}

	//resolve interpenetration problems
	_resolveContactPositions(d);

	//resolve velocity problems
	_resolveContactVelocities(d);
}

void PhysicsManager::_resolveContactVelocities(float d) {
	//handle impacts in order of severity
	for(unsigned iteration = 0; iteration < miterations_velocity_max; iteration++) {
		//find contact with highest desired velocity change
		float best_value = 0;
		PhysicsContact *best_contact = 0;
		for(auto &contact : mcontacts) {
			float value = contact.mdesireddeltavelocity;
			if((contact.mbody[0]->isAwake() || contact.mbody[1]->isAwake()) && contact.mpenetration > 0 && value > best_value) {
				best_contact = &contact;
				best_value = value;
			}
		}
		
		if(best_value <= HDX_PHYSICS_CONTACT_COLLISION_VELOCITY_EPSILON || best_contact == 0) break;

		HDXVector3 velocitychange[2];
		HDXVector3 angularvelocitychange[2];
		best_contact->applyVelocityChange(velocitychange,angularvelocitychange);
		float tot = velocitychange[0].magnitude() + velocitychange[1].magnitude() + angularvelocitychange[0].magnitude() + angularvelocitychange[1].magnitude();
		// update other effected contacts
		for(unsigned i = 0; i < 2; i++) {
			RigidBody *body = best_contact->mbody[i];
			if(body == 0) continue;
			body->_syncInternals_onKineticChange(d,velocitychange[i],angularvelocitychange[i]);
		}
	}
}

void PhysicsManager::_resolveContactPositions(float d) {
	//handle impacts in order of severity
	for(unsigned iteration = 0; iteration < miterations_velocity_max; iteration++) {
		//find contact with greatest pentration
		float best_value = 0;
		PhysicsContact *best_contact = 0;
		for(auto &contact : mcontacts) {
			float value = contact.mpenetration;
			if((contact.mbody[0]->isAwake() || contact.mbody[1]->isAwake()) && value > best_value) {
				best_contact = &contact;
				best_value = value;
			}
		}

		if(best_value <= HDX_PHYSICS_CONTACT_COLLISION_POSITION_EPSILON || best_contact == 0) break;

		HDXVector3 linearchange[2];
		HDXVector3 angularchange[2];
		best_contact->applyPositionChange(linearchange,angularchange,best_value*0.75f);

		// update other effected contacts
		for(unsigned i = 0; i < 2; i++) {
			RigidBody *body = best_contact->mbody[i];
			if(body == 0) continue;
			body->_syncInternals_onTransformChange(linearchange[i],angularchange[i]);
		}
	}
}