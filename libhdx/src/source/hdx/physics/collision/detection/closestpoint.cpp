#include "hdx/physics/collision/detection/closestpoint.h"
#include "hdx/util.h"
#include "hdx/hdx9math.h"
#include <algorithm>

HDXVector3 closestPoint_sphere_point(const HDXVector3 &p0,float r0,const HDXVector3 &p) {
	HDXVector3 d = p - p0;
	float dst = d.magnitudeSq();
	dst = sqrt((std::min)(dst,r0*r0));
	return d.getNormal()*dst;
}

HDXVector3 closestPoint_aabb_point(const HDXVector3 &p0,const HDXVector3 &hs0,const HDXVector3 &p) {
	HDXVector3 low = p0-hs0;
	HDXVector3 high = p0+hs0;
	return CLAMPVEC3(p,low,high);
}

HDXVector3 closestPoint_obb_point(const HDXMatrix4x4 &trans,const HDXVector3 &hs0,const HDXVector3 &p) {
	HDXVector3 q = trans.getColumn(3);
	HDXVector3 d = p - q;

	for(int i = 0; i < 3; i++) {
		float dst = d*trans.getColumn(i);
		float hs = hs0[i]; // x, y, z
		if(dst > hs)
			dst = hs;
		else if(dst < -hs)
			dst = -hs;
		q += trans.getColumn(i) * dst;
	}

	return q;
}

HDXVector3 closestPoint_obb_point(const HDXVector3 &p0,const HDXVector3 &hs0,const HDXQuaternion &o,const HDXVector3 &p) {
	HDXMatrix4x4 trans = o.toRotationMatrix();
	trans.setColumn(3,p0);
	return closestPoint_obb_point(trans,hs0,p);
}

HDXVector3 closestPoint_plane_point(const HDXVector3 &n0,float d0,const HDXVector3 &p) {
	float t = n0*p-d0;
	return p - (n0 * t);
}

HDXVector3 closestPoint_segment_point(const HDXVector3 &p00,const HDXVector3 &p10,const HDXVector3 &p) {
	const HDXVector3 &a = p00;
	const HDXVector3 &b = p10;
	HDXVector3 ab = b-a;
	float t = CLAMP(((p-a)*ab) / (ab*ab),0,1);

	return a + ab*t;
}

HDXVector3 closestPoint_segment_segment(const HDXVector3 &p0,const HDXVector3 &d0,float l0,const HDXVector3 &p1,const HDXVector3 &d1,float l1) {
	HDXVector3 toSt,cOne,cTwo;
	float dpStaOne,dpStaTwo,dp0Two,smOne,smTwo;
	float denom,mua,mub;

	smOne = d0.magnitudeSq();
	smTwo = d1.magnitudeSq();
	dp0Two = d1*d0;

	toSt = p0-p1;
	dpStaOne = d0*toSt;
	dpStaTwo = d1*toSt;

	denom = smOne*smTwo-dp0Two*dp0Two;

	//Zero denominator indicates parrallel lines
	if(abs(denom)<0.0001f) return p0;

	mua = (dp0Two*dpStaTwo-smTwo*dpStaOne)/denom;
	mub = (smOne*dpStaTwo-dp0Two*dpStaOne)/denom;

	if(mua>l0||
	   mua<-l0||
	   mub>l1||
	   mub<-l1) {
		return p0;
	} else {
		cOne = p0+d0*mua;
		cTwo = p1+d1*mub;
		return cOne*0.5+cTwo*0.5;
	}
}