#include "hdx/physics/collision/detection/collisiondetectionbv.h"
#include "hdx/physics/collision/detection/closestpoint.h"
#include "hdx/physics/collision/volume/bvsphere.h"
#include "hdx/physics/collision/volume/bvaabb.h"
#include "hdx/physics/collision/volume/bvobb.h"
#include "hdx/physics/collision/volume/bvrod.h"
#include "hdx/physics/collision/volume/bvplane.h"
#include "hdx/component_transform.h"
#include "hdx/physics/rigidbody.h"
#include "hdx/physics/physics.h"
#include "hdx/util.h"
#include "hdx/hdx9math.h"

#include "hdx/hdx9geometry.h"

#include <algorithm>
#include <list>

bool col_convex_plane(BaseBV *bv0,BaseBV *bv1,ContactCallback contactcallback /*= nullptr*/) {
	BaseBV *convex;
	BVPlane *plane;
	if(bv0->type == BaseBV::BV_PLANE) {
		convex = bv1;
		plane = (BVPlane*)bv0;
	} else if(bv1->type == BaseBV::BV_PLANE) {
		convex = bv0;
		plane = (BVPlane*)bv1;
	} else {
		assert(false);
	}

	HDXVector3 plane_normal = plane->getAxis(0);
	float plane_d = plane_normal * plane->getAxis(3);

	HDXVector3 p_behind = convex->getFurthestPointInDirection(-plane_normal);
	float p_behind_dst = pointDistanceFromPlane(p_behind,plane_normal,plane_d);
	HDXVector3 p_infront = convex->getFurthestPointInDirection(plane_normal);
	float p_intfront_dst = pointDistanceFromPlane(p_infront,plane_normal,plane_d);

	if((p_behind_dst>=0 && p_intfront_dst>=0) || (p_behind_dst<=0 && p_intfront_dst<=0)) return false;

	HDXVector3 contact_target;
	HDXVector3 contact_normal;
	float contact_pen;
	if(fabs(p_behind_dst) > fabs(p_intfront_dst)) {
		contact_target = p_infront;
		contact_normal = -plane_normal;
		contact_pen = fabs(p_intfront_dst);
	} else {
		contact_target = p_behind;
		contact_normal = plane_normal;
		contact_pen = fabs(p_behind_dst);
	}

	if(contactcallback != nullptr) {
		contactcallback(convex,plane,contact_target + contact_normal*contact_pen,contact_normal,contact_pen+HDX_PHYSICS_COLLISION_SKIN*2);
	}

	return true;
}

bool col_convex_halfspace(BaseBV *bv0,BaseBV *bv1,ContactCallback contactcallback /*= nullptr*/) {
	BaseBV *convex;
	BVPlane *halfspace;
	if(bv0->type == BaseBV::BV_HALFSPACE) {
		convex = bv1;
		halfspace = (BVPlane*)bv0;
	} else if(bv1->type == BaseBV::BV_HALFSPACE) {
		convex = bv0;
		halfspace = (BVPlane*)bv1;
	} else {
		assert(false);
	}

	HDXVector3 halfspace_normal = halfspace->getAxis(0);
	float halfspace_d = halfspace_normal * halfspace->getAxis(3);

	HDXVector3 p_behind = convex->getFurthestPointInDirection(-halfspace_normal);
	float p_behind_dst = pointDistanceFromPlane(p_behind,halfspace_normal,halfspace_d);

	if(p_behind_dst >= 0) return false;

	if(contactcallback != nullptr) {
		contactcallback(convex,halfspace,p_behind + halfspace_normal*-p_behind_dst,halfspace_normal,-p_behind_dst+HDX_PHYSICS_COLLISION_SKIN*2);
	}

	return true;
}

#define gjk_simtest(v) ((v)*(ao) > 0)
bool col_convex_convex_gjkepa(BaseBV *bv0,BaseBV *bv1,ContactCallback contactcallback /*= nullptr*/) {
	try {
		struct SupportPoint {
			HDXVector3 v;
			HDXVector3 sup_a;
			HDXVector3 sup_b;

			BOOL operator==(const SupportPoint &r) const { return v == r.v; }
		};

		struct Simplex {
		public:
			SupportPoint _simplex[4];
			int num;
			SupportPoint &a;
			SupportPoint &b;
			SupportPoint &c;
			SupportPoint &d;

			Simplex() : a(_simplex[0]),b(_simplex[1]),c(_simplex[2]),d(_simplex[3]) { clear(); }

			void clear() { num = 0; }

			void set(SupportPoint a,SupportPoint b,SupportPoint c,SupportPoint d) { num = 4; this->a = a; this->b = b; this->c = c; this->d = d; }
			void set(SupportPoint a,SupportPoint b,SupportPoint c) { num = 3; this->a = a; this->b = b; this->c = c; }
			void set(SupportPoint a,SupportPoint b) { num = 2; this->a = a; this->b = b; }
			void set(SupportPoint a) { num = 1; this->a = a; }

			void push(SupportPoint p) { num = (std::min)(num+1,4); for(int i = num-1; i > 0; i--) _simplex[i] = _simplex[i-1]; _simplex[0] = p; }
		};

		Simplex sim;

		auto lam_support = [&](HDXVector3 d)->SupportPoint {
			d.setNormal(); // to quell numerical instability
			SupportPoint ret;
			ret.sup_a = bv0->getFurthestPointInDirection(d);
			ret.sup_b = bv1->getFurthestPointInDirection(-d);
			ret.v = ret.sup_a - ret.sup_b;
			return ret;
		};

		//GJK algorithm, as explained by Casey here: http://mollyrocket.com/849, and here: http://vec3.ca/gjk/implementation/
		; {
			const unsigned _EXIT_ITERATION_LIMIT = 75;
			unsigned _EXIT_ITERATION_NUM = 0;

			// build the initial simplex, a single support point in an arbetrary position
			sim.clear();
			HDXVector3 dir = XUNIT3;
			SupportPoint s = lam_support(dir);
			if(fabs(dir*s.v)>=s.v.magnitude()*0.8f) {
				// the chosen direction is invalid, will produce (0,0,0) for a subsequent direction later
				dir = YUNIT3;
				s = lam_support(dir);
			}
			sim.push(s);
			dir = -s.v;

			// iteratively attempt to build a simplex that encloses the origin
			while(true) {
				if(_EXIT_ITERATION_NUM++ >= _EXIT_ITERATION_LIMIT) return false;

				// error, for some reason the direction vector is broken
				if(dir.magnitudeSq()<=0.0001f) return false;

				// get the next point in the direction of the origin
				SupportPoint a = lam_support(dir);

				// early out: if a.v * d is less than zero that means that the new point did not go past the origin
				// and thus there is no intersection
				if(a.v * dir < 0) return false;
				sim.push(a);

				const HDXVector3 ao = -sim.a.v;

				// simplex tests
				if(sim.num == 2) {
					// simplex is a line, being here means that the early out was passed, and thus
					// the origin must be between point a and b
					// search direction is perpendicular to ab and coplaner with ao
					const HDXVector3 ab = (sim.b.v-sim.a.v);
					dir = ab % ao % ab;
					continue;

				} else if(sim.num == 3) {
					// simplex is a triangle, meaning that the origin must be
					const HDXVector3 ab = (sim.b.v-sim.a.v);
					const HDXVector3 ac = (sim.c.v-sim.a.v);
					const HDXVector3 ad = (sim.d.v-sim.a.v);
					const HDXVector3 abc = ab % ac;

					if(gjk_simtest(ab % abc)) {
						// origin is outside the triangle, near the edge ab
						// reset the simplex to the line ab and continue
						// search direction is perpendicular to ab and coplaner with ao
						sim.set(sim.a,sim.b);
						dir = ab % ao % ab;
						continue;
					}

					if(gjk_simtest(abc % ac)) {
						// origin is outside the triangle, near the edge ac
						// reset the simplex to the line ac and continue
						// search direction is perpendicular to ac and coplaner with ao
						sim.set(sim.a,sim.c);
						dir = ac % ao % ac;
						continue;
					}

					// origin is within the triangular prism defined by the triangle
					// determine if it is above or below
					if(gjk_simtest(abc)) {
						// origin is above the triangle, so the simplex is not modified,
						// the search direction is the triangle's face normal
						dir = abc;
						continue;
					}

					// origin is below the triangle, so the simplex is rewound the oposite direction
					// the search direction is the new triangle's face normal
					sim.set(sim.a,sim.c,sim.b);
					dir = -abc;
					continue;

				} else { // == 4
					// the simplex is a tetrahedron, must check if it is outside any of the side triangles, (abc, acd, adb)
					// if it is then set the simplex equal to that triangle and continue, otherwise we know
					// there is an intersection and exit
					
					// check the triangles (abc,acd,adb), scoped as the temporary variables used here
					// will no longer be valid afterward
					; {
						const HDXVector3 ab = (sim.b.v-sim.a.v);
						const HDXVector3 ac = (sim.c.v-sim.a.v);

						if(gjk_simtest(ab % ac)) {
							// origin is in front of triangle abc, simplex is already what it needs to be
							// go to jmp_face
							goto jmp_face;
						}

						const HDXVector3 ad = (sim.d.v-sim.a.v);

						if(gjk_simtest(ac % ad)) {
							// origin is in front of triangle acd, simplex is set to this triangle
							// go to jmp_face
							sim.set(sim.a,sim.c,sim.d);
							goto jmp_face;
						}

						if(gjk_simtest(ad % ab)) {
							// origin is in front of triangle adb, simplex is set to this triangle
							// go to jmp_face
							sim.set(sim.a,sim.d,sim.b);
							goto jmp_face;
						}

						// intersction confirmed, break from the loop
						break;
					}

					jmp_face:
					// the simplex is equal to the triangle that the origin is infront of
					// this is exactly the same as the triangular simplex test except that we know
					// that the origin is not behind the triangle
					const HDXVector3 ab = (sim.b.v-sim.a.v);
					const HDXVector3 ac = (sim.c.v-sim.a.v);
					const HDXVector3 abc = ab % ac;

					if(gjk_simtest(ab % abc)) {
						sim.set(sim.a,sim.b);
						dir = ab % ao % ab;
						continue;
					}

					if(gjk_simtest(abc % ac)) {
						sim.set(sim.a,sim.c);
						dir = ac % ao % ac;
						continue;
					}

					sim.set(sim.a,sim.b,sim.c);
					dir = abc;
					continue;
				}
			}

			// collision was detected
			if(contactcallback != nullptr) {
				//Expanding Polytope Algorithm (EPA) as described here: http://allenchou.net/2013/12/game-physics-contact-generation-epa/
				struct Triangle {
					SupportPoint points[3];
					HDXVector3 n;

					Triangle(const SupportPoint &a,const SupportPoint &b,const SupportPoint &c) {
						points[0] = a;
						points[1] = b;
						points[2] = c;
						n = ((b.v-a.v) % (c.v-a.v)).getNormal();
					}
				};
				struct Edge {
					SupportPoint points[2];

					Edge(const SupportPoint &a,const SupportPoint &b) {
						points[0] = a;
						points[1] = b;
					}
				};

				const float _EXIT_THRESHOLD = 0.0001f;
				const unsigned _EXIT_ITERATION_LIMIT = 50;
				unsigned _EXIT_ITERATION_CUR = 0;

				std::list<Triangle> lst_triangles;
				std::list<Edge> lst_edges;

				// process the specified edge, if another edge with the same points in the
				// opposite order exists then it is removed and the new point is also not added
				// this ensures only the outermost ring edges of a cluster of traingles remain
				// in the list
				auto lam_addEdge = [&](const SupportPoint &a,const SupportPoint &b)->void {
					for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
						if(it->points[0]==b && it->points[1]==a) {
							//opposite edge found, remove it and do not add new one
							lst_edges.erase(it);
							return;
						}
					}
					lst_edges.emplace_back(a,b);
				};

				// add the GJK simplex triangles to the list
				lst_triangles.emplace_back(sim.a,sim.b,sim.c);
				lst_triangles.emplace_back(sim.a,sim.c,sim.d);
				lst_triangles.emplace_back(sim.a,sim.d,sim.b);
				lst_triangles.emplace_back(sim.b,sim.d,sim.c);

				while(true) {
					if(_EXIT_ITERATION_NUM++ >= _EXIT_ITERATION_LIMIT) return false;

					// find closest triangle to origin
					std::list<Triangle>::iterator entry_cur_triangle_it = lst_triangles.begin();
					float entry_cur_dst = FLT_MAX;
					for(auto it = lst_triangles.begin(); it != lst_triangles.end(); it++) {
						float dst = fabs(it->n * it->points[0].v);
						if(dst < entry_cur_dst) {
							entry_cur_dst = dst;
							entry_cur_triangle_it = it;
						}
					}

					SupportPoint entry_cur_support = lam_support(entry_cur_triangle_it->n);
					if((entry_cur_triangle_it->n*entry_cur_support.v - entry_cur_dst < _EXIT_THRESHOLD)) {
						// calculate the barycentric coordinates of the closest triangle with respect to
						// the projection of the origin onto the triangle
						float bary_u,bary_v,bary_w;
						barycentric(entry_cur_triangle_it->n * entry_cur_dst,
									entry_cur_triangle_it->points[0].v,
									entry_cur_triangle_it->points[1].v,
									entry_cur_triangle_it->points[2].v,
									&bary_u,
									&bary_v,
									&bary_w);

						// collision point on object a in world space
						HDXVector3 wcolpoint((bary_u*entry_cur_triangle_it->points[0].sup_a)+
											 (bary_v*entry_cur_triangle_it->points[1].sup_a)+
											 (bary_w*entry_cur_triangle_it->points[2].sup_a));

						// collision normal
						HDXVector3 wcolnormal = -entry_cur_triangle_it->n;

						// penetration depth
						float wpendepth = entry_cur_dst;

						contactcallback(bv0,bv1,wcolpoint,-entry_cur_triangle_it->n,entry_cur_dst);
						break;
					}

					for(auto it = lst_triangles.begin(); it != lst_triangles.end();) {
						// can this face be 'seen' by entry_cur_support?
						if(it->n * (entry_cur_support.v - it->points[0].v) > 0) {
							lam_addEdge(it->points[0],it->points[1]);
							lam_addEdge(it->points[1],it->points[2]);
							lam_addEdge(it->points[2],it->points[0]);
							it = lst_triangles.erase(it);
							continue;
						}
						it++;
					}

					// create new triangles from the edges in the edge list
					for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
						lst_triangles.emplace_back(entry_cur_support,it->points[0],it->points[1]);
					}

					lst_edges.clear();
				}
			}

			return true;
		}
	} catch(...) {
		return false;
	}
}
#undef gjk_simtest

bool col_bv_bv(BaseBV *v0,BaseBV *v1,ContactCallback contactcallback) {
	// create and initialize the function matrix
	typedef bool(*COLFUNC)(BaseBV*,BaseBV*,ContactCallback);
	static COLFUNC functable[BaseBV::BV_NUM][BaseBV::BV_NUM] = {
			{
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_plane,
				&col_convex_halfspace,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
			},

			{
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_plane,
				&col_convex_halfspace,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
			},

			{
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_plane,
				&col_convex_halfspace,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
			},

			{
				&col_convex_plane,
				&col_convex_plane,
				&col_convex_plane,
				0,// error
				0,// error
				&col_convex_plane,
				&col_convex_plane,
			},

			{
				&col_convex_halfspace,
				&col_convex_halfspace,
				&col_convex_halfspace,
				0,// error
				0,// error
				&col_convex_halfspace,
				&col_convex_halfspace,
			},

			{
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_plane,
				&col_convex_halfspace,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
			},

			{
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
				&col_convex_plane,
				&col_convex_halfspace,
				&col_convex_convex_gjkepa,
				&col_convex_convex_gjkepa,
			},
	};

	// use function matrix
	return functable[v0->type][v1->type](v0,v1,contactcallback);
}
