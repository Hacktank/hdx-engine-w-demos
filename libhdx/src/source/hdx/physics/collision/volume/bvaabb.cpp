#include "hdx/physics/collision/volume/bvaabb.h"
#include "hdx/physics/collision/volume/bvobb.h"

#include "hdx/physics/physics.h"

#include "hdx/util.h"
#include "hdx/hdx9math.h"

float BVAABB::getRadius() const {
	return halfsize.magnitude();
}

const HDXMatrix4x4& BVAABB::getTransform() const {
	HDXMatrix4x4 rtrans = BaseBV::getTransform();
	HDXVector3 rscale = rtrans.getScale();
	HDXMatrix4x4 &_mtrans = *(HDXMatrix4x4*)&mtrans;
	_mtrans.setIdentity();
	_mtrans[0] = rscale.x;
	_mtrans[5] = rscale.y;
	_mtrans[10] = rscale.z;
	_mtrans.setColumn(3,rtrans.getColumn(3));
	return mtrans;
}

BVAABB::BVAABB(const BVOBB &r) : BaseBV(BV_AABB) {
	body = r.body;
}

const HDXVector3 BVAABB::getFurthestPointInDirection(const HDXVector3 &d) {
	return (HDXVector3(SIGN(d.x)*halfsize.x,
					  SIGN(d.y)*halfsize.y,
					  SIGN(d.z)*halfsize.z) + d.getNormal()*HDX_PHYSICS_COLLISION_SKIN) * getTransform();
}