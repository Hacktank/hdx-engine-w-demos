#include "hdx/physics/collision/volume/bvobb.h"
#include "hdx/physics/collision/volume/bvaabb.h"
#include "hdx/physics/physics.h"

BVOBB::BVOBB(const BVAABB &r) : BaseBV(BV_OBB) {
	body = r.body;
}

float BVOBB::getRadius() const {
	return halfsize.magnitude();
}

const HDXVector3 BVOBB::getFurthestPointInDirection(const HDXVector3 &d) {
	const HDXQuaternion &trans_rot = body->getComp()->getTrans()->getOrientation();
	HDXVector3 rd = HDXVector3::gen::transformQuaternion(d,trans_rot.getInverse()).getNormal();

	HDXVector3 ret = (HDXVector3(SIGN(rd.x)*halfsize.x,
								SIGN(rd.y)*halfsize.y,
								SIGN(rd.z)*halfsize.z) + rd*HDX_PHYSICS_COLLISION_SKIN) * getTransform();
	return ret;
}