#include "hdx/physics/collision/volume/basebv.h"
#include "hdx/util.h"
#include "hdx/hdx9math.h"
#include "hdx/component_transform.h"
#include "hdx/physics/rigidbody.h"
#include "hdx/physics/physics.h"

const HDXVector3 BaseBV::getAxis(int ind) const {
	return getTransform().getRow(ind);
}

const HDXVector3 BaseBV::getCenter() const {
	return getAxis(3);
}

const HDXMatrix4x4& BaseBV::getTransform() const {
	return body->getComp()->getTrans()->getTransform();
}