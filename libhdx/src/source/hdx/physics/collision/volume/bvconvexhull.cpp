#include "hdx/physics/collision/volume/bvconvexhull.h"
#include "hdx/physics/physics.h"

float BVConvexHull::getRadius() const {
	return mhull.maabbhs.magnitude();
}

void BVConvexHull::setHull(HDXConvexHull hull) {
	mhull = std::move(hull);
	//center all of the points around the centroid
	for(unsigned i = 0; i < mhull.mverts.size(); i++) {
		mhull.mverts[i] -= mhull.mcentroid;
	}
	mhull.mcentroid.clear();
}

const HDXConvexHull& BVConvexHull::getHull() const {
	return mhull;
}

const HDXVector3 BVConvexHull::getFurthestPointInDirection(const HDXVector3 &d) {
	if(mhull.mverts.size()==0) return HDXVector3(0,0,0);

	const HDXQuaternion &trans_rot = body->getComp()->getTrans()->getOrientation();
	HDXVector3 rd = HDXVector3::gen::transformQuaternion(d,trans_rot.getInverse()).getNormal();

	//hill-climbing algorithm using the hull's adjacency data:
	//pick an arbetrary vert to start with, then move to the best adjacent
	//one, continue until no better adjacent vert exists

	unsigned cur_vid = 0;
	float cur_dst = mhull.mverts[cur_vid] * rd;
	while(true) {
		unsigned new_vid = cur_vid;
		float new_dst = cur_dst;
		; {
			for(unsigned i = 0; i < mhull.madjacency[cur_vid].size(); i++) {
				unsigned vid = mhull.madjacency[cur_vid][i];
				float dst = mhull.mverts[vid] * rd;
				if(dst > new_dst) {
					new_vid = vid;
					new_dst = dst;
				}
			}
		}
		if(new_vid == cur_vid) break;
		cur_vid = new_vid;
		cur_dst = new_dst;
	}

	return (mhull.mverts[cur_vid] + rd*HDX_PHYSICS_COLLISION_SKIN) * getTransform();
}