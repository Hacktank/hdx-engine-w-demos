#include "hdx/physics/collision/volume/bvplane.h"

#include "hdx/physics/physics.h"

float BVPlane::getRadius() const {
	return FLT_MAX;
}

const HDXVector3 BVPlane::getFurthestPointInDirection(const HDXVector3 &d) {
	return getAxis(3) + d.getNormal()*HDX_PHYSICS_COLLISION_SKIN;
}
