#include "hdx/physics/collision/volume/bvsphere.h"
#include "hdx/physics/physics.h"

float BVSphere::getRadius() const {
	return radius;
}

const HDXVector3 BVSphere::getFurthestPointInDirection(const HDXVector3 &d) {
	const HDXQuaternion &trans_rot = body->getComp()->getTrans()->getOrientation();
	HDXVector3 rd = HDXVector3::gen::transformQuaternion(d,trans_rot.getInverse()).getNormal();

	HDXVector3 ret = (getRadius() * rd + rd*HDX_PHYSICS_COLLISION_SKIN) * getTransform();
	return ret;
}

