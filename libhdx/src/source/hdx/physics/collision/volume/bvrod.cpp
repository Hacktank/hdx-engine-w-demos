#include "hdx/physics/collision/volume/bvrod.h"
#include "hdx/physics/physics.h"

float BVRod::getRadius() const {
	return halflength;
}

const HDXVector3 BVRod::getFurthestPointInDirection(const HDXVector3 &d) {
	const HDXQuaternion &trans_rot = body->getComp()->getTrans()->getOrientation();
	HDXVector3 rd = HDXVector3::gen::transformQuaternion(d,trans_rot.getInverse()).getNormal();

	return ((float)SIGN(XUNIT3*rd)*XUNIT3*halflength + rd*HDX_PHYSICS_COLLISION_SKIN) * getTransform();
}