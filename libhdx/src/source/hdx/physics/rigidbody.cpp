#include <cassert>
#include <algorithm>

#include "hdx/physics/rigidbody.h"

#include "hdx/physics/physics.h"
#include "hdx/component_transform.h"
#include "hdx/util.h"
#include "hdx/hdx9math.h"

#include "hdx/physics/collision/volume/bvsphere.h"
#include "hdx/physics/collision/volume/bvobb.h"
#include "hdx/physics/collision/volume/bvplane.h"
#include "hdx/physics/collision/volume/bvhalfspace.h"
#include "hdx/physics/collision/volume/bvrod.h"
#include "hdx/physics/collision/volume/bvconvexhull.h"

#include "hdx/physics/force/force.h"

RigidBody::RigidBody(component_Physics *comp) {
	mcomp = comp;
	mbv = 0;
	minversemass = 0.0f;
	mfriction = 0.5f;
	mrestitution = 0.5f;
	mlastframeacceleration.clear();
	mawake = false; // ensure the full effect of setAwake()
	setAwake(true);
	mdamping_linear = 0.95f;
	mdamping_angular = 0.75f;
	mmotion = (float)1.0;
	mawake = true;
	mcansleep = true;
	minverseinertiatensor.clear();
	msleeptimer = HDX_PHYSICS_RIGIDBODY_SLEEP_TIMER*10;

	mcancachecontacts = true;

	mlastframe_pos.clear();
	mlastframe_ori.setIdentity();

	_syncInternals(); // do it once initially
}

RigidBody::~RigidBody() {
	// invalidate all of this body's contacts
	for(auto &contact : mcontactinvolvement) {
		contact->mflag_remove = true;
		if(contact->mbody[0] == this)
			contact->mbody[0] = 0;
		else
			contact->mbody[1] = 0;
	}

	; {
		// remove this body from the omnipresent collision candidate list

		auto &opc = HDX_PHYMAN->momnipresentcandidates;
		opc.erase(std::remove(opc.begin(),opc.end(),mcomp),opc.end());
	}

	HDX_FORCEMAN->remove(this);
	deinitialize();
}

void RigidBody::initializeSphere(float radius,float mass) {
	deinitialize();
	auto bv = new BVSphere();
	bv->body = this;
	bv->radius = radius;
	mbv = bv;
	setMass(mass);
	setInertiaTensor(inertialTensor_sphere(radius,mass));
	mcancachecontacts = false;
}

void RigidBody::initializeBox(const HDXVector3 &halfsize,float mass) {
	deinitialize();
	auto bv = new BVOBB();
	bv->body = this;
	bv->halfsize = halfsize;
	mbv = bv;
	setMass(mass);
	setInertiaTensor(inertialTensor_box(halfsize,mass));
}

void RigidBody::initializeHalfspace(const HDXVector3 &pos,const HDXVector3 &normal) {
	deinitialize();
	auto bv = new BVHalfspace();
	bv->body = this;
	mbv = bv;
	HDX_PHYMAN->momnipresentcandidates.push_back(mcomp);
	setMass(0);
	mcomp->getTrans()->setPosition(pos);
	mcomp->getTrans()->setOrientation(HDXQuaternion::gen::rotateTo(XUNIT3,normal));
	minverseinertiatensor.clear();
}

void RigidBody::initializePlane(const HDXVector3 &pos,const HDXVector3 &normal) {
	deinitialize();
	auto bv = new BVPlane();
	bv->body = this;
	mbv = bv;
	HDX_PHYMAN->momnipresentcandidates.push_back(mcomp);
	setMass(0);
	mcomp->getTrans()->setPosition(pos);
	mcomp->getTrans()->setOrientation(HDXQuaternion::gen::rotateTo(XUNIT3,normal));
	minverseinertiatensor.clear();
}

void RigidBody::initializeRod(float halflength,float mass) {
	deinitialize();
	auto bv = new BVRod();
	bv->body = this;
	bv->halflength = halflength;
	mbv = bv;
	setMass(mass);
	minverseinertiatensor.clear();
	setInertiaTensor(inertialTensor_rod(halflength,mass));
}

void RigidBody::initializeConvexHull(HDXConvexHull hull,float mass) {
	deinitialize();
	auto bv = new BVConvexHull();
	bv->body = this;
	bv->setHull(std::move(hull));
	mbv = bv;
	setMass(mass);
	//setInertiaTensor(inertialTensor_convexhull(bv->getHull(),mass));
	setInertiaTensor(inertialTensor_box(bv->getHull().maabbhs,mass));
}

void RigidBody::deinitialize() {
	if(mbv) {
		switch(mbv->type) {
			case BaseBV::BV_PLANE:
			case BaseBV::BV_ROD: {
				auto &tvec = HDX_PHYMAN->momnipresentcandidates;
				tvec.erase(std::remove(tvec.begin(),tvec.end(),mcomp),tvec.end());
				break;
			}
		}
		delete mbv;
		mbv = 0;
	}
	setMass(0);
}

void RigidBody::setInverseMass(const float inversemass) {
	minversemass = inversemass;
}

void RigidBody::setMass(const float mass) {
	if(mass > 0)
		setInverseMass(((float)1.0)/mass);
	else
		setInverseMass(0);
}

float RigidBody::getMass() const {
	if(minversemass == 0)
		return (float)999999;
	else
		return ((float)1.0)/minversemass;
}

bool RigidBody::hasFiniteMass() const {
	return minversemass > 0.0f;
}

const std::vector<PhysicsContact*>& RigidBody::getContacts() const {
	return mcontactinvolvement;
}

void RigidBody::update(float dt) {
	if(hasFiniteMass() && mawake) {
		mlastframeacceleration = macceleration;
		mlastframeacceleration += mforceaccum*minversemass;
		HDXVector3 tmpacceleration = mlastframeacceleration*dt;

		; {
			// calculate new motion after the last frames collision resolution, but before this frame's changes
			float currentmotion = mvelocity*mvelocity + mangularvelocity*mangularvelocity*30.0f;
			float bias = (float)pow(0.9,dt);
			mmotion = bias*mmotion + (1.0f-bias)*currentmotion;
		}

		mvelocity += tmpacceleration;
		mangularacceleration = mtorqueaccum * minverseinertiatensorworld;
		mangularvelocity += mangularacceleration*dt;

		// damping
		mvelocity *= pow(mdamping_linear,dt);
		mangularvelocity *= pow(mdamping_angular,dt);

		mcomp->getTrans()->addPosition(mvelocity*dt);
		mcomp->getTrans()->addOrientation(mangularvelocity*dt);

		clearAccumulator();

		if(mcansleep) {
			if(mmotion < HDX_PHYSICS_RIGIDBODY_SLEEP_EPSILON || mcomp->getTrans()->getPosition().magnitude() > 10000) {
				msleeptimer -= dt;
				if(msleeptimer<=0)	setAwake(false);
			} else {
				// spread the awake state to all contacts
				for(auto &contact : mcontactinvolvement) {
					if(contact->mbody[0]==this)
						contact->mbody[1]->setAwake(true);
					else
						contact->mbody[0]->setAwake(true);
				}
				msleeptimer = HDX_PHYSICS_RIGIDBODY_SLEEP_TIMER;
				mmotion = CLAMP(mmotion,0,10*HDX_PHYSICS_RIGIDBODY_SLEEP_EPSILON*dt);
			}
		}
	}

	_syncInternals();
}

void RigidBody::clearAccumulator() {
	mforceaccum.clear();
	mtorqueaccum.clear();
}

void RigidBody::addForce(const HDXVector3 &force) {
	mforceaccum += force;
	mawake = true;
}

void RigidBody::addForceAtPoint(const HDXVector3 &force,const HDXVector3 &point) {
	HDXVector3 pt = point-mcomp->getTrans()->getPosition();

	mforceaccum += force;
	mtorqueaccum += pt % force;
	mawake = true;
}

void RigidBody::addForceAtBodyPoint(const HDXVector3 &force,const HDXVector3 &point) {
	addForceAtPoint(force,getPointInWorldSpace(point));
}

void RigidBody::addTorque(const HDXVector3 &torque) {
	mtorqueaccum += torque;
	mawake = true;
}

void RigidBody::setAwake(bool awake) {
	if(!mcansleep && !awake || awake==mawake) return;

	if(awake) {
		msleeptimer = HDX_PHYSICS_RIGIDBODY_SLEEP_TIMER;
	}
	mvelocity.clear();
	mangularvelocity.clear();
	mangularacceleration.clear();
	macceleration.clear();
	clearAccumulator();

	mawake = awake;
}

void RigidBody::setCanSleep(bool cansleep) {
	if(!mawake && !cansleep) setAwake(true);
	mcansleep = cansleep;
}

const HDXVector3 RigidBody::getPointInLocalSpace(const HDXVector3 &point) const {
	return point * mcomp->getTrans()->getTransform().getInvert();
}

const HDXVector3 RigidBody::getPointInWorldSpace(const HDXVector3 &point) const {
	return point * mcomp->getTrans()->getTransform();
}

void RigidBody::setInverseInertiaTensor(const HDXMatrix3x3 &inverseinertiatensor) {
	minverseinertiatensor = inverseinertiatensor;
}

void RigidBody::setInertiaTensor(const HDXMatrix3x3 &inertiatensor) {
	HDXMatrix3x3 inv;
	if(getInverseMass()>0) {
		inv = inertiatensor.getInvert();
	} else {
		inv.clear();
	}
	setInverseInertiaTensor(inv);
	_syncInternals();
}

const HDXMatrix3x3& RigidBody::getInverseInertiaTensor() const {
	return minverseinertiatensor;
}

const HDXMatrix3x3 RigidBody::getInertiaTensor() const {
	HDXMatrix3x3 inv;
	if(getInverseMass()>0) {
		inv = getInverseInertiaTensor().getInvert();
	} else {
		inv.clear();
	}
	return inv;
}

const HDXMatrix3x3& RigidBody::getInverseInertiaTensorWorld() const {
	return minverseinertiatensorworld;
}

const HDXMatrix3x3 RigidBody::getInertiaTensorWorld() const {
	return getInverseInertiaTensorWorld().getInvert();
}

void _transformInertiaTensor(HDXMatrix3x3 &iitWorld,
							 const HDXMatrix3x3 &iitBody,
							 const HDXMatrix4x4 &rotmat) {
	float t4 = rotmat[0]*iitBody[0]+
		rotmat[1]*iitBody[3]+
		rotmat[2]*iitBody[6];
	float t9 = rotmat[0]*iitBody[1]+
		rotmat[1]*iitBody[4]+
		rotmat[2]*iitBody[7];
	float t14 = rotmat[0]*iitBody[2]+
		rotmat[1]*iitBody[5]+
		rotmat[2]*iitBody[8];
	float t28 = rotmat[4]*iitBody[0]+
		rotmat[5]*iitBody[3]+
		rotmat[6]*iitBody[6];
	float t33 = rotmat[4]*iitBody[1]+
		rotmat[5]*iitBody[4]+
		rotmat[6]*iitBody[7];
	float t38 = rotmat[4]*iitBody[2]+
		rotmat[5]*iitBody[5]+
		rotmat[6]*iitBody[8];
	float t52 = rotmat[8]*iitBody[0]+
		rotmat[9]*iitBody[3]+
		rotmat[10]*iitBody[6];
	float t57 = rotmat[8]*iitBody[1]+
		rotmat[9]*iitBody[4]+
		rotmat[10]*iitBody[7];
	float t62 = rotmat[8]*iitBody[2]+
		rotmat[9]*iitBody[5]+
		rotmat[10]*iitBody[8];

	iitWorld[0] = t4*rotmat[0]+
		t9*rotmat[1]+
		t14*rotmat[2];
	iitWorld[1] = t4*rotmat[4]+
		t9*rotmat[5]+
		t14*rotmat[6];
	iitWorld[2] = t4*rotmat[8]+
		t9*rotmat[9]+
		t14*rotmat[10];
	iitWorld[3] = t28*rotmat[0]+
		t33*rotmat[1]+
		t38*rotmat[2];
	iitWorld[4] = t28*rotmat[4]+
		t33*rotmat[5]+
		t38*rotmat[6];
	iitWorld[5] = t28*rotmat[8]+
		t33*rotmat[9]+
		t38*rotmat[10];
	iitWorld[6] = t52*rotmat[0]+
		t57*rotmat[1]+
		t62*rotmat[2];
	iitWorld[7] = t52*rotmat[4]+
		t57*rotmat[5]+
		t62*rotmat[6];
	iitWorld[8] = t52*rotmat[8]+
		t57*rotmat[9]+
		t62*rotmat[10];
}

void RigidBody::_syncInternals() {
	// function pulled from "Game Physics Engine Development" by Ian Millington
	_transformInertiaTensor(minverseinertiatensorworld,minverseinertiatensor,mcomp->getTrans()->getTransform());

	// update all of the body's contacts
	HDXVector3 &cur_pos = *mcomp->getTrans()->getPositionPtr();
	HDXQuaternion &cur_ori = *mcomp->getTrans()->getOrientationPtr();
	cur_ori.setNormal();
	const HDXVector3 delta_pos = cur_pos - mlastframe_pos;
	const HDXVector3 delta_ori = (mlastframe_ori.getInverse() * cur_ori).getEular();

	// lastframe vars are updated in _syncInternals_onTransformChange()
	_syncInternals_onTransformChange(delta_pos,delta_ori);
	// dont need to call the kinetic change as that information will be recalculated when it is needed
	// the method exists for consistancy, it is called from the contact resoultion code
}

void RigidBody::_syncInternals_onTransformChange(const HDXVector3 &delta_pos,const HDXVector3 &delta_ori) {
	for(auto &contact : mcontactinvolvement) {
		for(unsigned b = 0; b < 2; b++) {
			if(contact->mbody[b]==this) {
				const HDXVector3 delta = delta_pos + (delta_ori % contact->mrelativecontactposition[b]);
				contact->mpenetration += delta * contact->mnormal * (float)(b ? 1 : -1);
				contact->mlife_body_change_linear[b] += delta;
				//contact->mrelativecontactposition[b] += delta_pos;
				break;
			}
		}
	}

	mlastframe_pos = mcomp->getTrans()->getPosition();
	mlastframe_ori = mcomp->getTrans()->getOrientation();
}

void RigidBody::_syncInternals_onKineticChange(float dt,const HDXVector3 &delta_vel,const HDXVector3 &delta_avel) {
	for(auto &contact : mcontactinvolvement) {
		for(unsigned b = 0; b < 2; b++) {
			if(contact->mbody[b]==this) {
				const HDXVector3 delta = delta_vel + (delta_avel % contact->mrelativecontactposition[b]);
				contact->mcontactvelocity += (delta * contact->mcttw_trans) * (float)(b ? -1 : 1);
				contact->calculateDesiredDeltaVelocity(dt);
				break;
			}
		}
	}
}
