#include "hdx/camera/hdx9camera.h"
#include "hdx/util.h"
#include "hdx/hdx9math.h"

HDXCamera::HDXCamera() {
	mpos.x = mpos.y = mpos.z = 0.0f;

	mori.x = mori.y = mori.z = 0.0f;
	mori.w = 1.0f;

	mchanged = true;
}

HDXCamera::~HDXCamera() {
}

HDXVector3 HDXCamera::getPosition() const {
	return mpos;
}

void HDXCamera::setPosition(const HDXVector3 &pos) {
	mchanged = true;
	mpos = pos;
}

void HDXCamera::setPosition(float x,float y,float z) {
	setPosition(HDXVector3(x,y,z));
}

HDXQuaternion HDXCamera::getOrientation() const {
	return mori;
}

void HDXCamera::setOrientation(const HDXQuaternion &ori) {
	mchanged = true;
	mori = ori;
}

void HDXCamera::translate(const HDXVector3 &v) {
	mchanged = true;
	mpos += v;
}

void HDXCamera::rotateAxis(const HDXVector3 &axis,float a) {
	mchanged = true;
	HDXQuaternion rotquat(axis,-a);//rotation must be negative
	mori = rotquat * mori;
}

void HDXCamera::rotateYaw(float a) {
	rotateAxis(YUNIT3,a);
}

void HDXCamera::rotatePitch(float a) {
	rotateAxis(XUNIT3,a);
}

void HDXCamera::rotateRoll(float a) {
	rotateAxis(ZUNIT3,a);
}

HDXVector3 HDXCamera::getForward() const {
	return getViewMatrix().getColumn(2);
}

HDXVector3 HDXCamera::getRight() const {
	return getViewMatrix().getColumn(0);
}

HDXVector3 HDXCamera::getUp() const {
	return getViewMatrix().getColumn(1);
}

void HDXCamera::setLookAt(const HDXVector3 &point) {
	mchanged = true;
	mori = HDXMatrix3x3::gen::lookDirection(ZUNIT3,point-mpos,YUNIT3).toRotationQuaternion();
}

void HDXCamera::setLookDirection(const HDXVector3 &dir) {
	setLookAt(mpos+dir);
}

const HDXMatrix4x4& HDXCamera::getViewMatrix() const {
	if(mchanged) {
		//const...
		*((bool*)&mchanged) = false;
		*(HDXMatrix4x4*)&mview = HDXMatrix4x4::gen::translation3d(-mpos)*mori.toRotationMatrix();
	}
	return mview;
}