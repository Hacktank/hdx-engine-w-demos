
#include "hdx/camera/hdx9cameramouselook.h"
#include "hdx/hdx9inputmanager.h"
#include "hdx/hdx9.h"

#include <algorithm>

HDXCamera_Mouselook::HDXCamera_Mouselook() {
	for(int i = 0; i < CML_NUM; i++) {
		mcontrols[i].mkey = -1;
		mcontrols[i].mpressed = false;
	}

	mcbid_onupdate = HDX_MAIN->registerFuncUpdate([&](float dt)->void {
		dt = dt / HDX_MAIN->getTimeScale(); //camera should move just as fast no matter what

		float speed = mspeed_move*dt;
		if(mcontrols[CML_SPD_FINE].mpressed) {
			speed *= 0.15f;
		} else if(mcontrols[CML_SPD_FAST].mpressed) {
			speed *= 3.0f;
		}

		for(int i = 0; i < CML_NUM; i++) {
			if(mcontrols[i].mpressed) {
				switch(i) {
					case CML_F: {
						setPosition(getPosition()+getForward()*speed);
						break;
					}
					case CML_L: {
						setPosition(getPosition()+-getRight()*speed);
						break;
					}
					case CML_B: {
						setPosition(getPosition()+-getForward()*speed);
						break;
					}
					case CML_R: {
						setPosition(getPosition()+getRight()*speed);
						break;
					}
					case CML_U: {
						setPosition(getPosition()+HDXVector3(0,1,0)*speed);
						break;
					}
					case CML_D: {
						setPosition(getPosition()+-HDXVector3(0,1,0)*speed);
						break;
					}
				}
			}
		}
	});

	mcbid_onmousemove = HDX_INPUT->registerCallbackOnMouseMove([&](const POINT &oldpos,const POINT &newpos)->void {
		if(mcontrols[CML_SHIFT].mpressed) {
			int dx = newpos.x - oldpos.x;
			int dy = newpos.y - oldpos.y;

			//problems arise when forward is parallel with world up, clamp it
			float dotwup = getForward() * YUNIT3;
			if(dotwup>0.98) dy = (std::max)(dy,0);
			if(dotwup<-0.98) dy = (std::min)(dy,0);

			setLookDirection((getForward() + (getRight()*(float)dx*msensitivity_x) + (getUp()*-(float)dy*msensitivity_y)));
		}
	});

	mcbid_onkeypress = HDX_INPUT->registerCallbackOnKeyPress([&](WPARAM key)->void {
		for(int i = 0; i < CML_NUM; i++) {
			if(mcontrols[i].mkey == key) {
				mcontrols[i].mpressed = true;
				if(i == CML_SHIFT) {
					HDX_INPUT->setMouseFPSMode(true);
				}
			}
		}
	});

	mcbid_onkeyrelease = HDX_INPUT->registerCallbackOnKeyRelease([&](WPARAM key,float timeheld)->void {
		for(int i = 0; i < CML_NUM; i++) {
			if(mcontrols[i].mkey == key) {
				mcontrols[i].mpressed = false;
				if(i == CML_SHIFT) {
					HDX_INPUT->setMouseFPSMode(false);
				}
			}
		}
	});

	mcbid_onmousepress = HDX_INPUT->registerCallbackOnMousePress([&](WPARAM key)->void {
		for(int i = 0; i < CML_NUM; i++) {
			if(mcontrols[i].mkey == key) {
				mcontrols[i].mpressed = true;
				if(i == CML_SHIFT) {
					HDX_INPUT->setMouseFPSMode(true);
				}
			}
		}
	});

	mcbid_onmouserelease = HDX_INPUT->registerCallbackOnMouseRelease([&](WPARAM key,float timeheld)->void {
		for(int i = 0; i < CML_NUM; i++) {
			if(mcontrols[i].mkey == key) {
				mcontrols[i].mpressed = false;
				if(i == CML_SHIFT) {
					HDX_INPUT->setMouseFPSMode(false);
				}
			}
		}
	});

	setSensitivityX(0.001f);
	setSensitivityY(0.001f);
	setMoveSpeed(0.001f);
}

HDXCamera_Mouselook::~HDXCamera_Mouselook() {
	HDX_INPUT->setMouseFPSMode(false);
	HDX_MAIN->unregisterFuncUpdate(mcbid_onupdate);
	HDX_INPUT->unregisterCallbackOnMouseMove(mcbid_onmousemove);
	HDX_INPUT->unregisterCallbackOnKeyPress(mcbid_onkeypress);
	HDX_INPUT->unregisterCallbackOnKeyRelease(mcbid_onkeyrelease);
	HDX_INPUT->unregisterCallbackOnMousePress(mcbid_onmousepress);
	HDX_INPUT->unregisterCallbackOnMouseRelease(mcbid_onmouserelease);
}

void HDXCamera_Mouselook::initialize(float sensitivity_x,
									 float sensitivity_y,
									 float movespeed,
									 int key_f,
									 int key_l,
									 int key_b,
									 int key_r,
									 int key_u,
									 int key_d,
									 int key_spd_fine,
									 int key_spd_fast,
									 int key_shift) {
	setSensitivityX(sensitivity_x);
	setSensitivityY(sensitivity_y);
	setMoveSpeed(movespeed);
	setKeyF(key_f);
	setKeyL(key_l);
	setKeyB(key_b);
	setKeyR(key_r);
	setKeyU(key_u);
	setKeyD(key_d);
	setKeySpeedFine(key_spd_fine);
	setKeySpeedFast(key_spd_fast);
	setKeyShift(key_shift);
}

int HDXCamera_Mouselook::getKeyF() const {
	return mcontrols[CML_F].mkey;
}

void HDXCamera_Mouselook::setKeyF(int key) {
	mcontrols[CML_F].mkey = key;
}

int HDXCamera_Mouselook::getKeyL() const {
	return mcontrols[CML_L].mkey;
}

void HDXCamera_Mouselook::setKeyL(int key) {
	mcontrols[CML_L].mkey = key;
}

int HDXCamera_Mouselook::getKeyB() const {
	return mcontrols[CML_B].mkey;
}

void HDXCamera_Mouselook::setKeyB(int key) {
	mcontrols[CML_B].mkey = key;
}

int HDXCamera_Mouselook::getKeyR() const {
	return mcontrols[CML_R].mkey;
}

void HDXCamera_Mouselook::setKeyR(int key) {
	mcontrols[CML_R].mkey = key;
}

int HDXCamera_Mouselook::getKeyU() const {
	return mcontrols[CML_U].mkey;
}

void HDXCamera_Mouselook::setKeyU(int key) {
	mcontrols[CML_U].mkey = key;
}

int HDXCamera_Mouselook::getKeyD() const {
	return mcontrols[CML_D].mkey;
}

void HDXCamera_Mouselook::setKeyD(int key) {
	mcontrols[CML_D].mkey = key;
}

int HDXCamera_Mouselook::getKeySpeedFine() const {
	return mcontrols[CML_SPD_FINE].mkey;
}

void HDXCamera_Mouselook::setKeySpeedFine(int key) {
	mcontrols[CML_SPD_FINE].mkey = key;
}

int HDXCamera_Mouselook::getKeySpeedFast() const {
	return mcontrols[CML_SPD_FAST].mkey;
}

void HDXCamera_Mouselook::setKeySpeedFast(int key) {
	mcontrols[CML_SPD_FAST].mkey = key;
}

int HDXCamera_Mouselook::getKeyShift() const {
	return mcontrols[CML_SHIFT].mkey;
}

void HDXCamera_Mouselook::setKeyShift(int key) {
	mcontrols[CML_SHIFT].mkey = key;
}

float HDXCamera_Mouselook::getSensitivityX() const {
	return msensitivity_x;
}

void HDXCamera_Mouselook::setSensitivityX(float sen) {
	msensitivity_x = sen;
}

float HDXCamera_Mouselook::getSensitivityY() const {
	return msensitivity_y;
}

void HDXCamera_Mouselook::setSensitivityY(float sen) {
	msensitivity_y = sen;
}

float HDXCamera_Mouselook::getMoveSpeed() const {
	return mspeed_move;
}

void HDXCamera_Mouselook::setMoveSpeed(float spd) {
	mspeed_move = spd;
}
