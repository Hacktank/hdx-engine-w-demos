
#include "hdx/hdx9video.h"

#include <algorithm>

#include <dshow.h>
#pragma comment(lib,"strmiids.lib")

void HDX9PlayVideoFile(const WCHAR *file,HWND hwnd) {
	IGraphBuilder *graph = 0;
	IMediaControl *control = 0;
	IMediaEvent *evt = 0;
	IVideoWindow *window = 0;
	

	HRESULT hr;

	hr = CoCreateInstance(CLSID_FilterGraph,
						  0,
						  CLSCTX_INPROC_SERVER,
						  IID_IGraphBuilder,
						  (void**)&graph);
	if(hr != S_OK) {
		return;
	}

	hr = graph->QueryInterface(IID_IMediaControl,(void**)&control);
	if(hr != S_OK) {
		graph->Release();
		return;
	}

	hr = control->QueryInterface(IID_IVideoWindow,(void**)&window);
	if(hr != S_OK) {
		control->Release();
		graph->Release();
		return;
	}

	window->put_Owner((OAHWND)hwnd);

	hr = graph->QueryInterface(IID_IMediaEvent,(void**)&evt);
	if(hr != S_OK) {
		window->Release();
		control->Release();
		graph->Release();
		return;
	}

	hr = graph->RenderFile(file,0);
	if(hr == S_OK) {
		hr = control->Run();

		//WINDOW IS NOT CREATED UNTIL CONTROL->RUN() IS CALLED
		{
			window->put_WindowStyle(WS_VISIBLE | WS_POPUP);

			RECT rect;
			GetClientRect(hwnd,&rect);
			ClientToScreen(hwnd,reinterpret_cast<POINT*>(&rect.left));
			ClientToScreen(hwnd,reinterpret_cast<POINT*>(&rect.right));

			const long cw = rect.right - rect.left;
			const long ch = rect.bottom - rect.top;

			window->SetWindowPosition(rect.left,rect.top,cw,ch);
		}

		OAFilterState state;
		while(true) {
			Sleep(100);

			long eventcode;
			LONG_PTR lparam1;
			LONG_PTR lparam2;
			while(evt->GetEvent(&eventcode,&lparam1,&lparam2,0)==S_OK) {
				switch(eventcode) {
					case EC_COMPLETE: {
						control->Stop();
						break;
					}
				}
			}

			for(int i = 0; i < 256; i++) {
				if(GetAsyncKeyState(i) & 0x8000) control->Stop();
			}

			control->GetState(0,&state);
			if(state == State_Stopped) break;
		}
	}

	window->Release();
	control->Release();
	evt->Release();
	graph->Release();
}
