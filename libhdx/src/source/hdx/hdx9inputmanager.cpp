#include "hdx/hdx9inputmanager.h"

#include <windowsx.h>
#include <winuser.h>

#include "hdx/hdx9.h"

HDXInputManager::HDXInputManager() {
	mmousepos_old.x = mmousepos_old.y = 0;
	mmouse_fpsmode = false;

	for(int i = 0; i < 256; i++) {
		mkeys[i].mdown = false;
		mkeys[i].mtime_last = std::chrono::high_resolution_clock::now();
	}

	auto lam_onKeyDown = [&](WPARAM key)->void {
		if(key == VK_LBUTTON || key == VK_RBUTTON) {
			mcallback_mouse_press.doIterate([&](std::function<void(WPARAM key)> &difunc)->bool {
				difunc(key);
				return false;
			});
		}

		mcallback_key_press.doIterate([&](std::function<void(WPARAM key)> &difunc)->bool {
			difunc(key);
			return false;
		});
		mkeys[key].mdown = true;
		mkeys[key].mtime_last = std::chrono::high_resolution_clock::now();
	};

	auto lam_onKeyUp = [&](WPARAM key)->void {
		std::chrono::high_resolution_clock::time_point curtime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<float> dur = (curtime-mkeys[key].mtime_last);

		if(key == VK_LBUTTON || key == VK_RBUTTON) {
			mcallback_mouse_release.doIterate([&](std::function<void(WPARAM key,float timeheld)> &difunc)->bool {
				difunc(key,dur.count());
				return false;
			});
		}

		mcallback_key_release.doIterate([&](std::function<void(WPARAM key,float timeheld)> &difunc)->bool {
			difunc(key,dur.count());
			return false;
		});

		mkeys[key].mdown = false;
		mkeys[key].mtime_last = curtime;
	};

	mcbid_onupdate = HDX_MAIN->registerFuncUpdate([lam_onKeyDown,lam_onKeyUp,this](float dt) {
		POINT mousepos_new = getMousePostion();
		GetCursorPos(&mousepos_new);
		ScreenToClient(HDX_MAIN->getWindowHWND(),&mousepos_new);

		if(mousepos_new.x!=mmousepos_old.x || mousepos_new.y!=mmousepos_old.y) {
			mcallback_mouse_move.doIterate([&](std::function<void(const POINT &oldpos,const POINT &newpos)> &difunc)->bool {
				difunc(mmousepos_old,mousepos_new);
				return false;
			});

			if(mmouse_fpsmode) {
				setMousePosition(mmousepos_old);
			} else {
				mmousepos_old = mousepos_new;
			}
		}

		for(int i = 0; i < 256; i++) {
			if(GetAsyncKeyState(i)&0x8000) {
				if(!mkeys[i].mdown) {
					lam_onKeyDown(i);
				}
			} else {
				if(mkeys[i].mdown) {
					lam_onKeyUp(i);
				}
			}
		}
	});
}

HDXInputManager::~HDXInputManager() {
}

bool HDXInputManager::getMouseFPSMode() const {
	return mmouse_fpsmode;
}

void HDXInputManager::setMouseFPSMode(bool fpsmode) {
	if(mmouse_fpsmode != fpsmode) {
		if(fpsmode) {
			mmmousepos_fpstmp = getMousePostion();

			ShowCursor(FALSE);

			int ww,wh;
			HDX_MAIN->getWindowSize(&ww,&wh);

			mmousepos_old.x = ww/2;
			mmousepos_old.y = wh/2;
			setMousePosition(mmousepos_old);
		} else {
			ShowCursor(TRUE);
			mmousepos_old = mmmousepos_fpstmp;
			setMousePosition(mmmousepos_fpstmp);
		}
	}
	mmouse_fpsmode = fpsmode;
}

int HDXInputManager::registerCallbackOnKeyPress(std::function<void(WPARAM key)> &&func) {
	return mcallback_key_press.addItem(func);
}

void HDXInputManager::unregisterCallbackOnKeyPress(int id) {
	mcallback_key_press.removeItem(id);
}

int HDXInputManager::registerCallbackOnKeyRelease(std::function<void(WPARAM key,float timeheld)> &&func) {
	return mcallback_key_release.addItem(func);
}

void HDXInputManager::unregisterCallbackOnKeyRelease(int id) {
	mcallback_key_release.removeItem(id);
}

int HDXInputManager::registerCallbackOnMousePress(std::function<void(WPARAM key)> &&func) {
	return mcallback_mouse_press.addItem(func);
}

void HDXInputManager::unregisterCallbackOnMousePress(int id) {
	mcallback_mouse_press.removeItem(id);
}

int HDXInputManager::registerCallbackOnMouseRelease(std::function<void(WPARAM key,float timeheld)> &&func) {
	return mcallback_mouse_release.addItem(func);
}

void HDXInputManager::unregisterCallbackOnMouseRelease(int id) {
	mcallback_mouse_release.removeItem(id);
}

int HDXInputManager::registerCallbackOnMouseMove(std::function<void(const POINT &oldpos,const POINT &newpos)> &&func) {
	return mcallback_mouse_move.addItem(func);
}

void HDXInputManager::unregisterCallbackOnMouseMove(int id) {
	mcallback_mouse_move.removeItem(id);
}

int HDXInputManager::registerCallbackOnMouseWheel(std::function<void(short delta)> &&func) {
	return mcallback_mouse_wheel.addItem(func);
}

void HDXInputManager::unregisterCallbackOnMouseWheel(int id) {
	mcallback_mouse_wheel.removeItem(id);
}

bool HDXInputManager::getKeyDown(WPARAM key) {
	return mkeys[key].mdown;
}

float HDXInputManager::getKeyTimeSinceLastEvent(WPARAM key) {
	std::chrono::high_resolution_clock::time_point curtime = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> dur = (curtime-mkeys[key].mtime_last);
	return dur.count();
}

POINT HDXInputManager::getMousePostion() {
	POINT pos;
	pos = mmousepos_old;//GetCursorPos(&pos);
	//ScreenToClient(HDX_MAIN->getWindowHWND(),&pos);
	return pos;
}

void HDXInputManager::setMousePosition(POINT pos) {
	ClientToScreen(HDX_MAIN->getWindowHWND(),&pos);
	SetCursorPos(pos.x,pos.y);
}

void HDXInputManager::_procMessage(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam) {
	switch(msg) {
		case WM_MOUSEWHEEL: {
			short zdelta = GET_WHEEL_DELTA_WPARAM(wparam);

			mcallback_mouse_wheel.doIterate([&](std::function<void(short delta)> &difunc)->bool {
				difunc(zdelta);
				return false;
			});
		}
	}
}
