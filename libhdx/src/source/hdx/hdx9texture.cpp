#include "hdx/hdx9texture.h"

#include "hdx/hdx9.h"

std::unordered_map<std::string,HDXTexture*> *HDXTexture::__managed_items = new std::unordered_map<std::string,HDXTexture*>;

HDXTexture::HDXTexture() {
	mmanaged = false;
	musers = 0;
	md3dtexture = 0;
}

HDXTexture::~HDXTexture() {
	if(md3dtexture) {
		md3dtexture->Release();
	}

	if(mmanaged) {
		__managed_items->erase(mname);
	}
}

IDirect3DTexture9* HDXTexture::getD3DTexture() const {
	return md3dtexture;
}

unsigned int HDXTexture::getWidth() const {
	if(md3dtexture) {
		D3DSURFACE_DESC desc;
		md3dtexture->GetLevelDesc(0,&desc);
		return desc.Width;
	}
	return 0;
}

unsigned int HDXTexture::getHeight() const {
	if(md3dtexture) {
		D3DSURFACE_DESC desc;
		md3dtexture->GetLevelDesc(0,&desc);
		return desc.Height;
	}
	return 0;
}

std::string HDXTexture::getName() const {
	return mname;
}

int HDXTexture::release() {
	int ret = --musers;
	if(musers==0) {
		delete this;
	}
	return ret;
}

HDXTexture* HDXTexture::__create(const std::string &name,bool managed) {
	HDXTexture *ret = 0;
	if(managed) {
		if(__managed_items->count(name) > 0) {
			ret = __managed_items->at(name);
		}
	}

	if(ret == 0) {
		ret = new HDXTexture();
		ret->mname = name;
		ret->mmanaged = managed;

		if(managed) {
			__managed_items->insert(std::make_pair(name,ret));
		}
	}

	ret->musers++;

	return ret;
}

HDXTexture* HDXTextureCreateBlank(const std::string &name,bool managed,int w,int h) {
	HDXTexture *ret = HDXTexture::__create(name,managed);

	D3DXCreateTexture(HDX_MAIN->getD3DDevice(),w,h,0,0,D3DFMT_A8R8G8B8,D3DPOOL_MANAGED,&ret->md3dtexture);

	return ret;
}

HDXTexture* HDXTextureCreateFromFile(const std::string &fname,bool managed) {
	HDXTexture *ret = HDXTexture::__create(fname,managed);

	HRESULT hr = D3DXCreateTextureFromFile(HDX_MAIN->getD3DDevice(),fname.c_str(),&ret->md3dtexture);
	if(hr != D3D_OK) {
		char buff[256];
		sprintf_s(buff,"Failed to load texture \"%s\".",fname.c_str());
		MessageBox(0,buff,0,0);
	}

	return ret;
}
