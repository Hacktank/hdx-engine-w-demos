#include "hdx/ui/effect/hdx9ui_elementeffect_textdraw.h"

#include "hdx/hdx9.h"
#include "hdx/hdx9font.h"
#include "hdx/ui/hdx9ui.h"

#include "hdx/hdx9color.h"

HDXUIElementEffect_TextDraw::HDXUIElementEffect_TextDraw(const std::string &name,
														 HDXUIElementBase *parent,
														 const std::string &font,
														 const std::string &text,
														 DWORD fontdrawflags,
														 D3DXCOLOR color) : HDXUIElementEffect_Base(name,parent) {
	mfont = font;
	mfontdrawflags = fontdrawflags;
	mfontdrawcolor = color;
	setText(text);

	rectSetPosSize(&mfontdrawrect,0,0,(int)parent->getActionWidth(),(int)parent->getActionHeight());
}

HDXUIElementEffect_TextDraw::HDXUIElementEffect_TextDraw(HDXUIElementBase *parent) : HDXUIElementEffect_Base(parent) {
}

HDXUIElementEffect_TextDraw::~HDXUIElementEffect_TextDraw() {
}

std::string HDXUIElementEffect_TextDraw::getFont() const {
	return mfont;
}

void HDXUIElementEffect_TextDraw::setFont(const std::string &font) {
	mfont = font;
}

std::string HDXUIElementEffect_TextDraw::getText() const {
	return mtext;
}

void HDXUIElementEffect_TextDraw::setText(const std::string &text) {
	mtext = text;
}

DWORD HDXUIElementEffect_TextDraw::getFontDrawFlags() const {
	return mfontdrawflags;
}

void HDXUIElementEffect_TextDraw::setFontDrawFlags(const DWORD &flags) {
	mfontdrawflags = flags;
}

D3DXCOLOR HDXUIElementEffect_TextDraw::getFontDrawColor() const {
	return mfontdrawcolor;
}

void HDXUIElementEffect_TextDraw::setFontDrawColor(const D3DXCOLOR &color) {
	mfontdrawcolor = color;
}

void HDXUIElementEffect_TextDraw::getFontDrawRect(RECT *rect) const {
	*rect = mfontdrawrect;
}

void HDXUIElementEffect_TextDraw::setFontDrawRect(const RECT *rect) {
	mfontdrawrect = *rect;
}

void HDXUIElementEffect_TextDraw::onMenuTransitionEnter(bool dotransition) {
}

void HDXUIElementEffect_TextDraw::onMenuTransitionExit(bool dotransiton) {
}

void HDXUIElementEffect_TextDraw::update(float dt) {
}

void HDXUIElementEffect_TextDraw::render() {
	HDXFontBase *testfont = HDX_FONTMAN->getFont(mfont);
	if(testfont) {
		ID3DXSprite *com_sprite = HDX_MAIN->getD3DSprite();

		HDXMatrix4x4 trans_parent;
		getParent()->getTransform(&trans_parent);

		com_sprite->SetTransform(trans_parent.getD3DX());

		D3DXCOLOR pcol;
		if(getParent()) {
			pcol = getParent()->getBlendColor();
		} else {
			pcol = 0xffffffff;
		}
		D3DXCOLOR mcol = getFontDrawColor();
		D3DXCOLOR tcol = colorMultiply(pcol,mcol);

		testfont->drawText(mtext.c_str(),mfontdrawrect,mfontdrawflags,tcol,com_sprite);
	}
}

void HDXUIElementEffect_TextDraw::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	setFont("");
	setFontDrawFlags(0);
	setFontDrawColor(0xffffffff);
	setText("");
	rectSetPosSize(&mfontdrawrect,0,0,(int)getParent()->getActionWidth(),(int)getParent()->getActionHeight());

	HDXUIElementEffect_Base::xml_initialize(xmlscript,xmlnode,func_oninitialize);
}

void HDXUIElementEffect_TextDraw::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementEffect_Base::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"font")==0) {
		std::string fontname;
		XMLRead(&fontname,xmlscript,xmlnode);
		setFont(fontname);
	} else if(strcmp(xmlnode->name(),"color")==0) {
		XMLRead(&mfontdrawcolor,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"drawflags")==0) {
		XMLRead(&mfontdrawflags,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"text")==0) {
		std::string text;
		XMLRead(&text,xmlscript,xmlnode);
		setText(text);
	} else if(strcmp(xmlnode->name(),"drawrect")==0) {
		if(strcmp(xmlnode->first_attribute("mode")->value(),"actionsize")==0) {
			rectSetPosSize(&mfontdrawrect,0,0,(int)getParent()->getActionWidth(),(int)getParent()->getActionHeight());
		} else if(strcmp(xmlnode->first_attribute("mode")->value(),"rect")==0) {
			XMLRead(&mfontdrawrect,xmlscript,xmlnode);
		}
	}
}

bool HDXUIElementEffect_TextDraw::xml_getVarByName(std::string *out,const std::string &name) const {
	if(HDXUIElementEffect_Base::xml_getVarByName(out,name)) return true;

	std::string tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="font") {
		return XMLToString(out,mfont,arg);
	} else if(tok=="blendcolor") {
		return XMLToString(out,mfontdrawcolor,arg);
	} else if(tok=="drawrect") {
		return XMLToString(out,mfontdrawrect,arg);
	} else if(tok=="text") {
		return XMLToString(out,mtext,arg);
	}

	return false;
}