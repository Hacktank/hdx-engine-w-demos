#include "hdx/ui/effect/hdx9ui_elementeffect_texturedraw.h"

#include "hdx/hdx9.h"
#include "hdx/hdx9texture.h"
#include "hdx/ui/hdx9ui.h"

#include "hdx/hdx9color.h"

HDXUIElementEffect_TextureDraw::HDXUIElementEffect_TextureDraw(const std::string &name,
															   HDXUIElementBase *parent,
															   const std::string &tex) : HDXUIElementEffect_Base(name,parent) {
	mtexture = HDXTextureCreateFromFile(tex,true);
	mblendcolor = 0xffffffff; //white, no blend
	setSourceRectToWholeTexture();
	setSourceCenterToCenterSourceRect();
}

HDXUIElementEffect_TextureDraw::HDXUIElementEffect_TextureDraw(HDXUIElementBase *parent) : HDXUIElementEffect_Base(parent) {
}

HDXUIElementEffect_TextureDraw::~HDXUIElementEffect_TextureDraw() {
	if(mtexture) mtexture->release();
}

std::string HDXUIElementEffect_TextureDraw::getTexture() const {
	if(mtexture) return mtexture->getName();
	return "";
}

void HDXUIElementEffect_TextureDraw::setTexture(const std::string &tex) {
	if(mtexture) mtexture->release();
	mtexture = HDXTextureCreateFromFile(tex,true);
}

D3DXCOLOR HDXUIElementEffect_TextureDraw::getBlendColor() const {
	return mblendcolor;
}

void HDXUIElementEffect_TextureDraw::setBlendColor(D3DXCOLOR blendcolor) {
	mblendcolor = blendcolor;
}

void HDXUIElementEffect_TextureDraw::getSourceRect(RECT *rect) const {
	*rect = msource_rect;
}

void HDXUIElementEffect_TextureDraw::setSourceRect(const RECT *rect) {
	msource_rect = *rect;
}

int HDXUIElementEffect_TextureDraw::getSourceWidth() const {
	return (msource_rect.right-msource_rect.left);
}

int HDXUIElementEffect_TextureDraw::getSourceHeight() const {
	return (msource_rect.bottom-msource_rect.top);
}

void HDXUIElementEffect_TextureDraw::setSourceRectToWholeTexture() {
	SetRect(&msource_rect,0,0,mtexture->getWidth(),mtexture->getHeight());
}

HDXVector3 HDXUIElementEffect_TextureDraw::getSourceCenter() const {
	HDXVector3 ret;
	getSourceCenter(&ret);
	return ret;
}

void HDXUIElementEffect_TextureDraw::getSourceCenter(HDXVector3 *center) const {
	*center = msource_center;
}

void HDXUIElementEffect_TextureDraw::setSourceCenter(const HDXVector3 *center) {
	msource_center = *center;
}

void HDXUIElementEffect_TextureDraw::setSourceCenterToCenterSourceRect() {
	msource_center.x = (msource_rect.right-msource_rect.left)/2.0f;
	msource_center.y = (msource_rect.bottom-msource_rect.top)/2.0f;
	msource_center.z = 0;
}

float HDXUIElementEffect_TextureDraw::getZPos() const {
	return HDXUI_GETZABOVE(getParent()->getZPos());
}

void HDXUIElementEffect_TextureDraw::onMenuTransitionEnter(bool dotransition) {
}

void HDXUIElementEffect_TextureDraw::onMenuTransitionExit(bool dotransiton) {
}

void HDXUIElementEffect_TextureDraw::update(float dt) {
}

void HDXUIElementEffect_TextureDraw::render() {
	if(mtexture) {
		ID3DXSprite *com_sprite = HDX_MAIN->getD3DSprite();

		HDXMatrix4x4 trans_parent;
		getParent()->getTransform(&trans_parent);

		HDXVector3 pos;
		pos.x = pos.y = 0;
		pos.z = getZPos();

		com_sprite->SetTransform(trans_parent.getD3DX());

		D3DXCOLOR pcol;
		if(getParent()) {
			pcol = getParent()->getBlendColor();
		} else {
			pcol = 0xffffffff;
		}
		D3DXCOLOR mcol = getBlendColor();
		D3DXCOLOR tcol = colorMultiply(pcol,mcol);

		com_sprite->Draw(mtexture->getD3DTexture(),&msource_rect,msource_center.getD3DX(),pos.getD3DX(),tcol);

		com_sprite->Flush();
	}
}

void HDXUIElementEffect_TextureDraw::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	mtexture = 0;
	setBlendColor(0xffffffff);

	HDXUIElementEffect_Base::xml_initialize(xmlscript,xmlnode,func_oninitialize);
}

void HDXUIElementEffect_TextureDraw::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementEffect_Base::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"file")==0) {
		std::string texfname;
		XMLRead(&texfname,xmlscript,xmlnode);
		setTexture(texfname);
	} else if(strcmp(xmlnode->name(),"blendcolor")==0) {
		XMLRead(&mblendcolor,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"sourcerect")==0) {
		if(strcmp(xmlnode->first_attribute("mode")->value(),"texture")==0) {
			setSourceRectToWholeTexture();
		} else if(strcmp(xmlnode->first_attribute("mode")->value(),"rect")==0) {
			XMLRead(&msource_rect,xmlscript,xmlnode);
		}
	} else if(strcmp(xmlnode->name(),"sourcecenter")==0) {
		if(strcmp(xmlnode->first_attribute("mode")->value(),"sourcerect")==0) {
			setSourceCenterToCenterSourceRect();
		} else if(strcmp(xmlnode->first_attribute("mode")->value(),"pos")==0) {
			XMLRead(&msource_center,xmlscript,xmlnode);
		}
	} else if(strcmp(xmlnode->name(),"setparentactionrect")==0) {
		HDXVector2 scale;
		XMLRead(&scale,xmlscript,xmlnode);
		getParent()->setActionWidth(scale.x * (float)RECTWIDTH(msource_rect));
		getParent()->setActionHeight(scale.y * (float)RECTHEIGHT(msource_rect));
	}
}

bool HDXUIElementEffect_TextureDraw::xml_getVarByName(std::string *out,const std::string &name) const {
	if(HDXUIElementEffect_Base::xml_getVarByName(out,name)) return true;

	std::string tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="file") {
		return XMLToString(out,mtexture ? mtexture->getName() : "",arg);
	} else if(tok=="blendcolor") {
		return XMLToString(out,mblendcolor,arg);
	} else if(tok=="sourcerect") {
		return XMLToString(out,msource_rect,arg);
	} else if(tok=="sourcecenter") {
		return XMLToString(out,msource_center,arg);
	}

	return false;
}