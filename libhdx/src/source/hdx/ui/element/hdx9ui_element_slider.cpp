#include "hdx/ui/element/hdx9ui_element_slider.h"

#include "hdx/ui/effect/hdx9ui_elementeffect_texturedraw.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_textdraw.h"

#include "hdx/hdx9.h"
#include "hdx/hdx9font.h"
#include "hdx/hdx9inputmanager.h"

#include "hdx/hdx9color.h"

#include "hdx/util.h"
#include "hdx/hdx9math.h" 

HDXUIElement_Slider::HDXUIElement_Slider_Handle::HDXUIElement_Slider_Handle(const std::string &name,
																			HDXUIElementBase *parent,
																			const std::string &maintexture,
																			float size,
																			float xoff,
																			float yoff) : HDXUIElementBase(name,parent) {
	meffect_texture_main = createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																		maintexture);
	RECT texture_source_rect;
	meffect_texture_main->getSourceRect(&texture_source_rect);
	setActionSize((float)RECTWIDTH(texture_source_rect),
				  (float)RECTHEIGHT(texture_source_rect));

	msize = size;
	moffset.x = xoff;
	moffset.y = yoff;

	mdragging = false;
	mcallbackid_clicked = -1;
	mcallbackid_mouse_release = -1;
	mcallbackid_mouse_move = -1;

	mcallbackid_clicked = registerCallbackOnMousePress([&](WPARAM key)->void {
		if(key==VK_LBUTTON) mdragging = true;
	});

	mcallbackid_mouse_release = HDX_INPUT->registerCallbackOnMouseRelease([&](WPARAM key,float timeheld)->void {
		if(key==VK_LBUTTON) mdragging = false;
	});

	mcallbackid_mouse_move = HDX_INPUT->registerCallbackOnMouseMove([&](const POINT &oldpos,const POINT &newpos)->void {
		if(mdragging) {
			HDXUIElement_Slider *parent = (HDXUIElement_Slider*)getParent();

			HDXVector2 pos_slider,pos_slider_new,pos_cursor_parent;
			HDXMatrix4x4 mat_trans_parent,mat_trans_parent_inv;
			parent->getTransform(&mat_trans_parent);
			mat_trans_parent_inv = mat_trans_parent.getInvert();
			getPosition(&pos_slider);
			pos_cursor_parent += HDXVector2(VEC2TOARG(newpos)) * mat_trans_parent_inv;

			pos_slider_new = pos_slider;
			pos_slider_new.x = CLAMP((float)pos_cursor_parent.x,
									 -msize/2.0f,
									 msize/2.0f);

			setPosition(pos_slider_new);
		}
	});

	mblendreset = mblendprev = meffect_texture_main->getBlendColor();
	mprevhighlight = false;
}

HDXUIElement_Slider::HDXUIElement_Slider_Handle::~HDXUIElement_Slider_Handle() {
	unregisterCallbackOnMousePress(mcallbackid_clicked);
	HDX_INPUT->unregisterCallbackOnMouseRelease(mcallbackid_mouse_release);
	HDX_INPUT->unregisterCallbackOnMouseMove(mcallbackid_mouse_move);
}

void HDXUIElement_Slider::HDXUIElement_Slider_Handle::onMenuTransitionEnter(bool dotransition) {
	HDXUIElementBase::onMenuTransitionEnter(dotransition);
}

void HDXUIElement_Slider::HDXUIElement_Slider_Handle::onMenuTransitionExit(bool dotransition) {
	HDXUIElementBase::onMenuTransitionExit(dotransition);
}

void HDXUIElement_Slider::HDXUIElement_Slider_Handle::update(float dt) {
	HDXUIElementBase::update(dt);
	if(meffect_texture_main) {
		D3DXCOLOR blendcur = meffect_texture_main->getBlendColor();
		if(blendcur != mblendprev) {
			D3DXCOLOR delta = blendcur - mblendprev;
			mblendreset += delta;
		}

		if(mdragging || isMouseOver()) {
			if(!mprevhighlight) {
				meffect_texture_main->setBlendColor(colorMultiply(mblendreset,0xffff5555));
			}
			mprevhighlight = true;
		} else {
			if(mprevhighlight) {
				meffect_texture_main->setBlendColor(mblendreset);
			}
			mprevhighlight = false;
		}

		mblendprev = meffect_texture_main->getBlendColor();
	}
}

HDXUIElement_Slider::HDXUIElement_Slider(const std::string &name,
										 HDXUIElementBase *parent,
										 const std::string &maintexture,
										 const std::string &handletexture,
										 const std::string &textfont,
										 const std::string &text,
										 DWORD textflags,
										 D3DXCOLOR textcolor) : HDXUIElementBase(name,parent) {
	meffect_texture_main = createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																		maintexture);

	meffect_text = createEffect<HDXUIElementEffect_TextDraw>("text_main",
															 textfont,
															 text,
															 textflags,
															 textcolor);

	RECT texture_source_rect;
	meffect_texture_main->getSourceRect(&texture_source_rect);
	setActionSize((float)RECTWIDTH(texture_source_rect),
				  (float)RECTHEIGHT(texture_source_rect));

	RECT text_draw_rect;
	rectSetPosSize(&text_draw_rect,(int)getPosition().x,(int)getPosition().y,(int)getActionWidth(),(int)getActionHeight());
	rectShrink(&text_draw_rect,5,5);

	meffect_text->setFontDrawRect(&text_draw_rect);

	mhandle = createChildElement<HDXUIElement_Slider_Handle>("handle",
															 handletexture,
															 getActionWidth()*0.825f,
															 0.0f,
															 0.0f);

	mprevvalue = getValue();
}

HDXUIElement_Slider::HDXUIElement_Slider(HDXUIElementBase *parent) : HDXUIElementBase(parent) {
}

HDXUIElement_Slider::~HDXUIElement_Slider() {
	for(auto it = mcallbackid_onvaluechange.begin(); it != mcallbackid_onvaluechange.end(); it++) {
		HDX_INPUT->unregisterCallbackOnMouseMove(*it);
	}
	mcallbackid_onvaluechange.clear();
}

HDXUIElementEffect_TextureDraw* HDXUIElement_Slider::getMainTextureEffect() const {
	return meffect_texture_main;
}

HDXUIElementEffect_TextureDraw* HDXUIElement_Slider::getHandleTextureEffect() const {
	return mhandle ? mhandle->meffect_texture_main : 0;
}

HDXUIElementEffect_TextDraw* HDXUIElement_Slider::getTextEffect() const {
	return meffect_text;
}

HDXUIElement_Slider::HDXUIElement_Slider_Handle* HDXUIElement_Slider::getHandle() const {
	return mhandle;
}

float HDXUIElement_Slider::getValue() const {
	return mhandle ? (mhandle->getPosition().x+mhandle->msize/2)/mhandle->msize : 0;
}

void HDXUIElement_Slider::setValue(float value) {
	value = (std::min)(1.0f,(std::max)(0.0f,value)); //clamp value to workable area
	mhandle->setPosition((value-0.5f)*(mhandle->msize),
						 mhandle->getPosition().y);
}

int HDXUIElement_Slider::registerCallbackOnValueChange(std::function<void(float value)> &&func) {
	//this should be placed in the call queue after the slider's mouse move callback
	mcallbackid_onvaluechange.push_back(HDX_INPUT->registerCallbackOnMouseMove([this,func](const POINT &oldpos,const POINT &newpos)->void {
		if(mhandle->mdragging) {
			float newvalue = getValue();
			if(newvalue != mprevvalue) {
				func(newvalue);
			}
		}
	}));
	return mcallbackid_onvaluechange.back();
}

void HDXUIElement_Slider::unregisterCallbackOnValueChange(int id) {
	HDX_INPUT->unregisterCallbackOnMouseMove(id);
	std::remove(mcallbackid_onvaluechange.begin(),mcallbackid_onvaluechange.end(),id);
}

void HDXUIElement_Slider::update(float dt) {
	HDXUIElementBase::update(dt);
	mprevvalue = getValue();
}

void HDXUIElement_Slider::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	mhandle = 0;

	HDXUIElementBase::xml_initialize(xmlscript,xmlnode,func_oninitialize);

	meffect_text = (HDXUIElementEffect_TextDraw*)getEffect("text_main");
	meffect_texture_main = (HDXUIElementEffect_TextureDraw*)getEffect("texture_main");
}

void HDXUIElement_Slider::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementBase::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"texture_handle")==0) {
		std::string handletexture;
		XMLRead(&handletexture,xmlscript,xmlnode);
		mhandle = createChildElement<HDXUIElement_Slider_Handle>("handle",
																 handletexture,
																 getActionWidth()*0.825f,
																 0.0f,
																 0.0f);
	} else if(strcmp(xmlnode->name(),"value")==0) {
		float value;
		XMLRead(&value,xmlscript,xmlnode);
		setValue(value);
	}
}

bool HDXUIElement_Slider::xml_getVarByName(std::string *out,const std::string &name) const {
	if(HDXUIElementBase::xml_getVarByName(out,name)) return true;

	std::string tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="texture_handle") {
		if(mhandle) return XMLToString(out,mhandle->meffect_texture_main->getTexture(),arg);
	} else if(tok=="value") {
		return XMLToString(out,getValue(),arg);
	}

	return false;
}