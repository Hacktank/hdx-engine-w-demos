#include "hdx/ui/element/hdx9ui_element_checkbox.h"

#include "hdx/ui/effect/hdx9ui_elementeffect_texturedraw.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_textdraw.h"

#include "hdx/hdx9.h"
#include "hdx/hdx9font.h"

HDXUIElement_Checkbox::HDXUIElement_Checkbox(const std::string &name,
											 HDXUIElementBase *parent,
											 const std::string &maintexture,
											 const std::string &hovertexture,
											 const std::string &checktexture,
											 const std::string &textfont,
											 const std::string &text,
											 DWORD textflags,
											 D3DXCOLOR textcolor) : HDXUIElementBase(name,parent) {
	meffect_texture_main = createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																		maintexture);

	meffect_texture_hover = createEffect<HDXUIElementEffect_TextureDraw>("texture_hover",
																		 hovertexture);

	meffect_texture_check = createEffect<HDXUIElementEffect_TextureDraw>("texture_check",
																		 checktexture);

	meffect_text = createEffect<HDXUIElementEffect_TextDraw>("text_main",
															 textfont,
															 text,
															 textflags,
															 textcolor);

	RECT texture_source_rect;
	meffect_texture_main->getSourceRect(&texture_source_rect);
	setActionSize((float)RECTWIDTH(texture_source_rect),
				  (float)RECTHEIGHT(texture_source_rect));

	RECT text_draw_rect;
	rectSetPosSize(&text_draw_rect,(int)getPosition().x,(int)getPosition().y,(int)getActionWidth(),(int)getActionHeight());
	text_draw_rect.right -= 32; // dont draw text on the checkbox
	rectShrink(&text_draw_rect,5,5);

	meffect_text->setFontDrawRect(&text_draw_rect);

	mcallbackid_clicked = registerCallbackOnMousePress([&](WPARAM key)->void {
		if(key==VK_LBUTTON) mchecked = !mchecked;
	});

	mchecked = false;
}

HDXUIElement_Checkbox::HDXUIElement_Checkbox(HDXUIElementBase *parent) : HDXUIElementBase(parent) {
	mcallbackid_clicked = registerCallbackOnMousePress([&](WPARAM key)->void {
		if(key==VK_LBUTTON) mchecked = !mchecked;
	});
}

HDXUIElement_Checkbox::~HDXUIElement_Checkbox() {
	unregisterCallbackOnMousePress(mcallbackid_clicked);
}

HDXUIElementEffect_TextureDraw* HDXUIElement_Checkbox::getMainTextureEffect() const {
	return meffect_texture_main;
}

HDXUIElementEffect_TextureDraw* HDXUIElement_Checkbox::getHoverTextureEffect() const {
	return meffect_texture_hover;
}

HDXUIElementEffect_TextDraw* HDXUIElement_Checkbox::getTextEffect() const {
	return meffect_text;
}

bool HDXUIElement_Checkbox::getChecked() const {
	return mchecked;
}

void HDXUIElement_Checkbox::setChecked(bool checked) {
	mchecked = checked;
}

void HDXUIElement_Checkbox::onMenuTransitionEnter(bool dotransition) {
	HDXUIElementBase::onMenuTransitionEnter(dotransition);
}

void HDXUIElement_Checkbox::onMenuTransitionExit(bool dotransition) {
	HDXUIElementBase::onMenuTransitionExit(dotransition);
}

void HDXUIElement_Checkbox::update(float dt) {
	HDXUIElementBase::update(dt);
	if(meffect_texture_hover) meffect_texture_hover->setActive(isMouseOver());
	if(meffect_texture_check) meffect_texture_check->setActive(getChecked());
}

void HDXUIElement_Checkbox::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	mchecked = false;

	HDXUIElementBase::xml_initialize(xmlscript,xmlnode,func_oninitialize);

	meffect_text = (HDXUIElementEffect_TextDraw*)getEffect("text_main");
	meffect_texture_hover = (HDXUIElementEffect_TextureDraw*)getEffect("texture_hover");
	meffect_texture_main = (HDXUIElementEffect_TextureDraw*)getEffect("texture_main");
	meffect_texture_check = (HDXUIElementEffect_TextureDraw*)getEffect("texture_check");
}

void HDXUIElement_Checkbox::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	HDXUIElementBase::xml_node_read(xmlscript,xmlnode,func_oninitialize);

	if(strcmp(xmlnode->name(),"checked")==0) {
		XMLRead(&mchecked,xmlscript,xmlnode);
	}
}

bool HDXUIElement_Checkbox::xml_getVarByName(std::string *out,const std::string &name) const {
	if(HDXUIElementBase::xml_getVarByName(out,name)) return true;

	std::string tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="checked") {
		return XMLToString(out,mchecked,arg);
	}

	return false;
}