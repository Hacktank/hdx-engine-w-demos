#include "hdx/ui/hdx9ui.h"

#include "hdx/hdx9.h"
#include "hdx/hdx9inputmanager.h"
#include "hdx/hdx9intersection.h"
#include "hdx/sound/hdx9sound.h"

#include "hdx/ui/effect/hdx9ui_elementeffect_texturedraw.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_textdraw.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_transition_lerp.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_var_oscillate.h"

#include "hdx/ui/element/hdx9ui_element_alignment.h"
#include "hdx/ui/element/hdx9ui_element_button.h"
#include "hdx/ui/element/hdx9ui_element_checkbox.h"
#include "hdx/ui/element/hdx9ui_element_slider.h"

#include "hdx/hdx9color.h"

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include <string>
#include <sstream>
#include <iomanip>

#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_utils.hpp>

#include "hdx/xmlutils.h"
#include "hdx/ui/hdx9ui_xmlscript.h"

void rectSetPosSize(RECT *rect,int x,int y,int w,int h) {
	rect->left = x-w/2;
	rect->top = y-h/2;
	rect->right = x+w/2;
	rect->bottom = y+h/2;
}

void rectShrink(RECT *rect,int sx,int sy) {
	rect->left += sx/2;
	rect->right -= sx/2;
	rect->top += sy/2;
	rect->bottom -= sy/2;
}

void rectGrow(RECT *rect,int gx,int gy) {
	rectShrink(rect,-gx,-gy);
}

void rectGetSubRect(RECT *rect,int sw,int sh,int i,int j) {
	int x = sw*i;
	int y = sh*j;

	SetRect(rect,x,y,x+sw,y+sh);
}

HDXUIElementBase::HDXUIElementBase(const std::string name,HDXUIElementBase *parent) {
	mname = name;
	mparent = parent;

	mactionwidth = mactionheight = 0;
	mpos.x = mpos.y = 0;
	mscale.x = mscale.y = 1.0f;
	mrotcenter.x = mrotcenter.y = 0;
	mrot = 0;
	mzpos = 0.5f;

	mactive = true;
	menabled = true;
	mmousepressed = false;
	mwasmouseover = false;

	mblendcolor = 0xffffffff;
}

HDXUIElementBase::HDXUIElementBase(HDXUIElementBase *parent) {
	mparent = parent;
}

HDXUIElementBase::~HDXUIElementBase() {
	mcallback_ondestroy.doIterate([&](std::function<void(void)> &difunc)->bool {
		difunc();
		return false;
	});

	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		delete item;
		return false;
	});
	meffects.clear();

	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		delete item;
		return false;
	});
	mchilderen.clear();

	for(auto it = mcallbackid_onmousepress.begin(); it != mcallbackid_onmousepress.end(); it++) {
		HDX_INPUT->unregisterCallbackOnMousePress(*it);
	}
	mcallbackid_onmousepress.clear();

	for(auto it = mcallbackid_onkeypress.begin(); it != mcallbackid_onkeypress.end(); it++) {
		HDX_INPUT->unregisterCallbackOnKeyPress(*it);
	}
	mcallbackid_onkeypress.clear();
}

HDXUIElementBase* HDXUIElementBase::getParent() const {
	return mparent;
}

const HDXUIElementBase* HDXUIElementBase::getTopElement() const {
	if(mparent) return mparent->getTopElement();
	return this;
}

HDXUIElementBase* HDXUIElementBase::getTopElement() {
	if(mparent) return mparent->getTopElement();
	return this;
}

bool HDXUIElementBase::isActive() const {
	return mactive;
}

void HDXUIElementBase::setActive(bool active) {
	if(mactive && !active) {
		mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
			if(item->isActive()) {
				mreactivate_childeren.push_back(item);
				item->setActive(false);
			}
			return false;
		});
		meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
			if(item->isActive()) {
				mreactivate_effects.push_back(item);
				item->setActive(false);
			}
			return false;
		});
	}
	if(!mactive && active) {
		for(auto it = mreactivate_childeren.begin(); it != mreactivate_childeren.end(); it++) {
			(*it)->setActive(true);
		}
		for(auto it = mreactivate_effects.begin(); it != mreactivate_effects.end(); it++) {
			(*it)->setActive(true);
		}
	}
	mactive = active;
}

bool HDXUIElementBase::isEnabled() const {
	return menabled;
}

void HDXUIElementBase::setEnabled(bool enabled) {
	if(menabled && !enabled) {
		mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
			if(item->isEnabled()) {
				mreenable_childeren.push_back(item);
				item->setEnabled(false);
			}
			return false;
		});
		meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
			if(item->isEnabled()) {
				mreenable_effects.push_back(item);
				item->setEnabled(false);
			}
			return false;
		});
	}
	if(!menabled && enabled) {
		for(auto it = mreenable_childeren.begin(); it != mreenable_childeren.end(); it++) {
			(*it)->setEnabled(true);
		}
		for(auto it = mreenable_effects.begin(); it != mreenable_effects.end(); it++) {
			(*it)->setEnabled(true);
		}
	}
	menabled = enabled;
}

void HDXUIElementBase::getTransform(HDXMatrix4x4 *mat) const {
	//recursively get this element's transform based on the whole menu hierarchy
	HDXMatrix4x4 parenttrans;
	if(mparent) {
		mparent->getTransform(&parenttrans);
	} else {
		parenttrans.setIdentity();
	}
	const HDXMatrix4x4 mytrans = HDXMatrix3x3::gen::transformation2d(mscale,mrotcenter,mrot,mpos);

	*mat = mytrans * parenttrans;
}

void HDXUIElementBase::getPosition(HDXVector2 *pos) const {
	*pos = mpos;
}

HDXVector2 HDXUIElementBase::getPosition() const {
	HDXVector2 ret;
	getPosition(&ret);
	return ret;
}

HDXVector2* HDXUIElementBase::getPositionPtr() {
	return &mpos;
}

void HDXUIElementBase::setPosition(const HDXVector2 *pos) {
	mpos = *pos;
}

void HDXUIElementBase::setPosition(const float &x,const float &y) {
	mpos.x = x;
	mpos.y = y;
}

void HDXUIElementBase::setPosition(const HDXVector2 &pos) {
	setPosition(&pos);
}

void HDXUIElementBase::getScale(HDXVector2 *scale) const {
	*scale = mscale;
}

HDXVector2 HDXUIElementBase::getScale() const {
	HDXVector2 ret;
	getScale(&ret);
	return ret;
}

HDXVector2* HDXUIElementBase::getScalePtr() {
	return &mscale;
}

void HDXUIElementBase::setScale(const HDXVector2 *scale) {
	mscale = *scale;
}

void HDXUIElementBase::setScale(const float &x,const float &y) {
	mscale.x = x;
	mscale.y = y;
}

void HDXUIElementBase::setScale(const HDXVector2 &scale) {
	setScale(&scale);
}

void HDXUIElementBase::getRotationCenter(HDXVector2 *center) const {
	*center = mrotcenter;
}

HDXVector2 HDXUIElementBase::getRotationCenter() const {
	HDXVector2 ret;
	getRotationCenter(&ret);
	return ret;
}

void HDXUIElementBase::setRotationCenter(const HDXVector2 *center) {
	mrotcenter = *center;
}

void HDXUIElementBase::setRotationCenter(const float &x,const float &y) {
	mrotcenter.x = x;
	mrotcenter.y = y;
}

void HDXUIElementBase::setRotationCenter(const HDXVector2 &center) {
	setRotationCenter(&mrotcenter);
}

float HDXUIElementBase::getRotation() const {
	return mrot;
}

float* HDXUIElementBase::getRotationPtr() {
	return &mrot;
}

void HDXUIElementBase::setRotation(const float &rot) {
	mrot = rot;
}

float HDXUIElementBase::getActionWidth() const {
	return mactionwidth;
}

void HDXUIElementBase::setActionWidth(const float &actionwidth) {
	mactionwidth = actionwidth;
}

float HDXUIElementBase::getActionHeight() const {
	return mactionheight;
}

void HDXUIElementBase::setActionHeight(const float &actionheight) {
	mactionheight = actionheight;
}

HDXVector2 HDXUIElementBase::getActionSize() const {
	return HDXVector2(getActionWidth(),getActionHeight());
}

void HDXUIElementBase::setActionSize(const HDXVector2 &size) {
	setActionSize(VEC2TOARG(size));
}

void HDXUIElementBase::setActionSize(const float &w,const float &h) {
	setActionWidth(w);
	setActionHeight(h);
}

void HDXUIElementBase::getEffectiveActionRect(RECT *rect) const {
	HDXMatrix4x4 mytrans;
	getTransform(&mytrans);
	HDXVector2 points_pretrans[4] = { //CCW
			{
				-mactionwidth/2,
				-mactionheight/2
			},
			{
				-mactionwidth/2,
				mactionheight/2
			},
			{
				mactionwidth/2,
				mactionheight/2
			},
			{
				mactionwidth/2,
				-mactionheight/2
			}
	};

	SetRect(rect,INT_MAX,INT_MAX,INT_MIN,INT_MIN);
	for(int i = 0; i < 4; i++) {
		HDXVector2 transpoint = points_pretrans[i] * mytrans;
		rect->left = (int)(std::min)((float)rect->left,transpoint.x);
		rect->right = (int)(std::max)((float)rect->right,transpoint.x);
		rect->top = (int)(std::min)((float)rect->top,transpoint.y);
		rect->bottom = (int)(std::max)((float)rect->bottom,transpoint.y);
	}
}

float HDXUIElementBase::getZPos() const {
	return mzpos;
}

void HDXUIElementBase::setZPos(const float &zpos) {
	mzpos = zpos;
}

float HDXUIElementBase::getMaxZPos() const {
	float ret = 0.0f;
	meffects.doIterate([&](const HDXUIElementEffect_Base *item)->bool {
		ret = (std::max)(ret,item->getZPos());
		return false;
	});
	mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
		ret = (std::max)(ret,item->getMaxZPos());
		return false;
	});
	return ret;
}

float HDXUIElementBase::getMinZPos() const {
	float ret = 1.0f;
	meffects.doIterate([&](const HDXUIElementEffect_Base *item)->bool {
		ret = (std::min)(ret,item->getZPos());
		return false;
	});
	mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
		ret = (std::min)(ret,item->getMaxZPos());
		return false;
	});
	return ret;
}

D3DXCOLOR HDXUIElementBase::getBlendColor() const {
	//recursively get this element's transform based on the whole menu hierarchy
	D3DXCOLOR parentblendcolor;
	if(mparent) {
		parentblendcolor = mparent->getBlendColor();
	} else {
		parentblendcolor = 0xffffffff;
	}

	return colorMultiply(parentblendcolor,mblendcolor);
}

void HDXUIElementBase::setBlendColor(D3DXCOLOR color) {
	mblendcolor = color;
}

bool HDXUIElementBase::isMouseOver() const {
	if(mactive && menabled) {
		HDXMatrix4x4 mytrans;
		getTransform(&mytrans);
		HDXVector3 ext(mactionwidth/2,mactionheight/2,0);
		HDXVector3 mouse((float)HDX_INPUT->getMousePostion().x,(float)HDX_INPUT->getMousePostion().y,0);

		return HDX9IntersectionTests::obb_point(&ext,&mytrans,&mouse);
	}
	return false;
}

int HDXUIElementBase::registerCallbackOnMousePress(std::function<void(WPARAM key)> &&func) {
	mcallbackid_onmousepress.push_back(HDX_INPUT->registerCallbackOnMousePress([this,func](WPARAM key)->void {
		if(mactive && menabled) {
			if(isMouseOver()) {
				mmousepressed = true;
				func(key);
			}
		}
	}));
	return mcallbackid_onmousepress.back();
}

void HDXUIElementBase::unregisterCallbackOnMousePress(int id) {
	HDX_INPUT->unregisterCallbackOnMousePress(id);
	std::remove(mcallbackid_onmousepress.begin(),mcallbackid_onmousepress.end(),id);
}

int HDXUIElementBase::registerCallbackOnMouseRelease(std::function<void(WPARAM key,float timeheld)> &&func) {
	mcallbackid_onmousepress.push_back(HDX_INPUT->registerCallbackOnMouseRelease([this,func](WPARAM key,float timeheld)->void {
		if(mactive && menabled) {
			if(mmousepressed) {
				func(key,timeheld);
			}
		}
	}));
	return mcallbackid_onmouserelease.back();
}

void HDXUIElementBase::unregisterCallbackOnMouseRelease(int id) {
	HDX_INPUT->unregisterCallbackOnMouseRelease(id);
	std::remove(mcallbackid_onmouserelease.begin(),mcallbackid_onmouserelease.end(),id);
}

int HDXUIElementBase::registerCallbackOnMouseEnter(std::function<void(void)> &&func) {
	return mcallback_onmouseenter.addItem(func);
}

void HDXUIElementBase::unregisterCallbackOnMouseEnter(int id) {
	mcallback_onmouseenter.removeItem(id);
}

int HDXUIElementBase::registerCallbackOnMouseLeave(std::function<void(void)> &&func) {
	return mcallback_onmouseleave.addItem(func);
}

void HDXUIElementBase::unregisterCallbackOnMouseLeave(int id) {
	mcallback_onmouseleave.removeItem(id);
}

int HDXUIElementBase::registerCallbackOnKeyPress(std::function<void(WPARAM key)> &&func) {
	mcallbackid_onkeypress.push_back(HDX_INPUT->registerCallbackOnKeyPress([this,func](WPARAM key)->void {
		if(mactive && menabled) {
			func(key);
		}
	}));
	return mcallbackid_onkeypress.back();
}

int HDXUIElementBase::registerCallbackOnTransitionEnter(std::function<void(bool dotransition)> &&func) {
	return mcallback_ontransitionenter.addItem(func);
}

void HDXUIElementBase::unregisterCallbackOnTransitionEnter(int id) {
	mcallback_ontransitionenter.removeItem(id);
}

int HDXUIElementBase::registerCallbackOnTransitionExit(std::function<void(bool dotransition)> &&func) {
	return mcallback_ontransitionexit.addItem(func);
}

void HDXUIElementBase::unregisterCallbackOnTransitionExit(int id) {
	mcallback_ontransitionexit.removeItem(id);
}

void HDXUIElementBase::unregisterCallbackOnKeyPress(int id) {
	HDX_INPUT->unregisterCallbackOnKeyPress(id);
	std::remove(mcallbackid_onkeypress.begin(),mcallbackid_onkeypress.end(),id);
}

int HDXUIElementBase::registerCallbackOnDestroy(std::function<void(void)> &&func) {
	return mcallback_ondestroy.addItem(func);
}

void HDXUIElementBase::unregisterCallbackOnDestroy(int id) {
	mcallback_ondestroy.removeItem(id);
}

const BaseManagerKey<HDXUIElementBase*,std::string>& HDXUIElementBase::getChilderen() const {
	return mchilderen;
}

const BaseManagerKey<HDXUIElementEffect_Base*,std::string>& HDXUIElementBase::getEffects() const {
	return meffects;
}

HDXUIElementEffect_Base* HDXUIElementBase::getEffect(const std::string &name) const {
	return meffects.hasItem(name) ? meffects.getItem(name) : 0;
}

void HDXUIElementBase::destroyEffect(const std::string &name) {
	HDXUIElementEffect_Base *effect = getEffect(name);
	if(effect) {
		delete effect;
		meffects.removeItem(name);
	}
}

HDXUIElementEffect_Base* HDXUIElementBase::getSubEffect(const std::string &name) const {
	HDXUIElementEffect_Base *ret = getEffect(name);
	if(ret==0) {
		mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
			ret = item->getSubEffect(name);
			return ret==0;
		});
	}
	return ret;
}

HDXUIElementBase* HDXUIElementBase::createChildElementFromXML(const std::string &name,const std::string &file) {
	rapidxml::file<> xmlfile(file.c_str());
	rapidxml::xml_document<> doc;
	doc.parse<0>(xmlfile.data());

	rapidxml::xml_node<> *node_root = doc.first_node("uielement");

	for(rapidxml::xml_node<> *node_element = node_root->first_node(); node_element; node_element = node_element->next_sibling()) {
		if(strcmp(node_element->name(),"button")==0) {
			std::string xmlstring = node_element->first_node("texturexml")->value();
		} else if(strcmp(node_element->name(),"checkbox")==0) {
		}
	}

	return 0;
}

HDXUIElementBase* HDXUIElementBase::getChildElement(const std::string &name) const {
	return mchilderen.hasItem(name) ? mchilderen.getItem(name) : 0;
}

void HDXUIElementBase::destroyChildElement(const std::string &name) {
	HDXUIElementBase *child = getChildElement(name);
	if(child) {
		delete child;
		mchilderen.removeItem(name);
	}
}

HDXUIElementBase* HDXUIElementBase::getSubElement(const std::string &name) const {
	HDXUIElementBase *ret = getChildElement(name);
	if(ret==0) {
		mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
			ret = item->getSubElement(name);
			return ret==0;
		});
	}
	return ret;
}

float HDXUIElementBase::getDesiredTransitionTime() const {
	return 0.0f;
}

float HDXUIElementBase::getMaxDesiredTransitionTime() const {
	float ret = getDesiredTransitionTime();
	meffects.doIterate([&](const HDXUIElementEffect_Base *item)->bool {
		ret = (std::max)(ret,item->getDesiredTransitionTime());
		return false;
	});
	mchilderen.doIterate([&](const HDXUIElementBase *item)->bool {
		ret = (std::max)(ret,item->getMaxDesiredTransitionTime());
		return false;
	});
	return ret;
}

void HDXUIElementBase::onMenuTransitionEnter(bool dotransition) {
	mcallback_ontransitionenter.doIterate([&](std::function<void(bool dotransition)> &difunc)->bool {
		difunc(dotransition);
		return false;
	});

	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		item->onMenuTransitionEnter(dotransition);
		return false;
	});
	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		item->onMenuTransitionEnter(dotransition);
		return false;
	});
}

void HDXUIElementBase::onMenuTransitionExit(bool dotransition) {
	mcallback_ontransitionexit.doIterate([&](std::function<void(bool dotransition)> &difunc)->bool {
		difunc(dotransition);
		return false;
	});

	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		item->onMenuTransitionExit(dotransition);
		return false;
	});
	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		item->onMenuTransitionExit(dotransition);
		return false;
	});
}

void HDXUIElementBase::update(float dt) {
	mmousepressed = false;

	if(isMouseOver()) {
		if(!mwasmouseover) {
			mcallback_onmouseenter.doIterate([&](std::function<void(void)> &difunc)->bool {
				difunc();
				return false;
			});
		}
		mwasmouseover = true;
	} else {
		if(mwasmouseover) {
			mcallback_onmouseleave.doIterate([&](std::function<void(void)> &difunc)->bool {
				difunc();
				return false;
			});
		}
		mwasmouseover = false;
	}

	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		if(item->isActive()) item->update(dt);
		return false;
	});

	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		if(item->isActive()) item->update(dt);
		return false;
	});
}

void HDXUIElementBase::render() {
	meffects.doIterate([&](HDXUIElementEffect_Base *item)->bool {
		if(item->isActive()) item->render();
		return false;
	});

	mchilderen.doIterate([&](HDXUIElementBase *item)->bool {
		item->render();
		return false;
	});
}

void HDXUIElementBase::xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	mname = xmlnode->first_attribute("name")->value();

	mactionwidth = mactionheight = 0;
	mpos.x = mpos.y = 0;
	mscale.x = mscale.y = 1.0f;
	mrotcenter.x = mrotcenter.y = 0;
	mrot = 0;
	mzpos = 0.5f;

	mactive = true;
	menabled = true;
	mmousepressed = false;
	mwasmouseover = false;

	mblendcolor = 0xffffffff;

	for(rapidxml::xml_node<> *node_i = xmlnode->first_node(); node_i; node_i = node_i->next_sibling()) {
		xml_node_read(xmlscript,node_i,func_oninitialize);
	}

	if(func_oninitialize != nullptr) func_oninitialize(this,xmlnode);
}

void HDXUIElementBase::xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	xmlscript->setScriptData_this(this,true);

	if(strcmp(xmlnode->name(),"position")==0) {
		XMLRead(&mpos,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"scale")==0) {
		XMLRead(&mscale,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"rotation")==0) {
		XMLRead(&mrot,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"blendcolor")==0) {
		XMLRead(&mblendcolor,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"enabled")==0) {
		XMLRead(&menabled,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"active")==0) {
		XMLRead(&mactive,xmlscript,xmlnode);
	} else if(strcmp(xmlnode->name(),"actionsize")==0) {
		HDXVector2 size;
		XMLRead(&size,xmlscript,xmlnode);
		setActionSize(size);
	} else if(strcmp(xmlnode->name(),"uieffect")==0) {
		HDXUIElementEffect_Base *neweffect = 0;

		const char *type = xmlnode->first_attribute("type")->value();
		if(strcmp(type,"draw_texture")==0) {
			neweffect = new HDXUIElementEffect_TextureDraw(this);
		} else if(strcmp(type,"draw_text")==0) {
			neweffect = new HDXUIElementEffect_TextDraw(this);
		} else if(strcmp(type,"var_oscillate")==0) {
			const char *var = xmlnode->first_attribute("variable")->value();
			if(strcmp(var,"position")==0) {
				neweffect = new HDXUIElementEffect_Var_Oscillate<HDXVector2>(this,&mpos);
			} else if(strcmp(var,"scale")==0) {
				neweffect = new HDXUIElementEffect_Var_Oscillate<HDXVector2>(this,&mscale);
			} else if(strcmp(var,"rotation")==0) {
				neweffect = new HDXUIElementEffect_Var_Oscillate<float>(this,&mrot);
			}
		} else if(strcmp(type,"transition_lerp")==0) {
			const char *var = xmlnode->first_attribute("variable")->value();
			if(strcmp(var,"position")==0) {
				neweffect = new HDXUIElementEffect_Transition_LERP<HDXVector2>(this,&mpos);
			} else if(strcmp(var,"scale")==0) {
				neweffect = new HDXUIElementEffect_Transition_LERP<HDXVector2>(this,&mscale);
			} else if(strcmp(var,"rotation")==0) {
				neweffect = new HDXUIElementEffect_Transition_LERP<float>(this,&mrot);
			}
		}

		if(neweffect) {
			neweffect->xml_initialize(xmlscript,xmlnode,func_oninitialize);
			_addEffect(neweffect);
		}
	} else if(strcmp(xmlnode->name(),"uielement")==0) {
		HDXUIElementBase *newelement = 0;

		const char *type = xmlnode->first_attribute("type")->value();
		if(strcmp(type,"menu")==0) {
			newelement = new HDXUIMenu();
		} else if(strcmp(type,"alignment")==0) {
			newelement = new HDXUIElement_Alignment(this);
		} else if(strcmp(type,"button")==0) {
			newelement = new HDXUIElement_Button(this);
		} else if(strcmp(type,"slider")==0) {
			newelement = new HDXUIElement_Slider(this);
		} else if(strcmp(type,"checkbox")==0) {
			newelement = new HDXUIElement_Checkbox(this);
		}

		if(newelement) {
			newelement->xml_initialize(xmlscript,xmlnode,func_oninitialize);
			_addChild(newelement);
		}
	} else if(strcmp(xmlnode->name(),"uievent")==0) {
		const char *evt = xmlnode->first_attribute("event")->value();
		const char *action = xmlnode->first_attribute("action")->value();

		std::function<void(void)> func_doaction = nullptr;

		if(strcmp(action,"MENU_NAVIGATE_MENU")==0) {
			std::string arg_target = "";
			bool arg_dotransition = true;
			rapidxml::xml_node<> *argnode;

			if(argnode = xmlnode->first_node("target")) {
				XMLRead(&arg_target,xmlscript,argnode);
			}

			if(argnode = xmlnode->first_node("dotransition")) {
				XMLRead(&arg_dotransition,xmlscript,argnode);
			}

			func_doaction = [=]()->void {
				HDXUIMenu *menu = dynamic_cast<HDXUIMenu*>(getTopElement());
				if(menu) {
					menu->getMenuMap()->navigateToMenu(arg_target,arg_dotransition);
				}
			};
		} else if(strcmp(action,"MENU_NAVIGATE_BACK")==0) {
			bool arg_dotransition = true;
			rapidxml::xml_node<> *argnode;

			if(argnode = xmlnode->first_node("dotransition")) {
				XMLRead(&arg_dotransition,xmlscript,argnode);
			}

			func_doaction = [=]()->void {
				HDXUIMenu *menu = dynamic_cast<HDXUIMenu*>(getTopElement());
				if(menu) {
					menu->getMenuMap()->navigateBackwards(arg_dotransition);
				}
			};
		} else if(strcmp(action,"SOUND_PLAY")==0) {
			std::string arg_file = "";
			float arg_volume = 1.0f;
			rapidxml::xml_node<> *argnode;

			if(argnode = xmlnode->first_node("file")) {
				XMLRead(&arg_file,xmlscript,argnode);
			}

			if(argnode = xmlnode->first_node("volume")) {
				XMLRead(&arg_volume,xmlscript,argnode);
			}

			HDX9Sound *snd = 0;
			if(arg_file.length() > 0) {
				snd = HDX_SOUNDMAN->loadSoundFAF(arg_file,FMOD_DEFAULT);
				if(snd) {
					registerCallbackOnDestroy([=]() {
						snd->release();
					});
				}
			}

			func_doaction = [=]()->void {
				auto chanel = HDX_SOUNDMAN->playSoundChannelGroup(snd,"gui",true);
				chanel->setVolume(arg_volume);
				chanel->setPaused(false);
			};
		}

		if(func_doaction != nullptr) {
			if(strcmp(evt,"MOUSE_CLICK")==0) {
				registerCallbackOnMousePress([=](WPARAM key) {
					if(key==VK_LBUTTON) {
						func_doaction();
					}
				});
			} else if(strcmp(evt,"MOUSE_HOVER_ENTER")==0) {
				registerCallbackOnMouseEnter([=]() {
					func_doaction();
				});
			} else if(strcmp(evt,"MOUSE_HOVER_EXIT")==0) {
				registerCallbackOnMouseLeave([=]() {
					func_doaction();
				});
			} else if(strcmp(evt,"TRANSITION_ENTER")==0) {
				registerCallbackOnTransitionEnter([=](bool dotransition) {
					func_doaction();
				});
			} else if(strcmp(evt,"TRANSITION_EXIT")==0) {
				registerCallbackOnTransitionExit([=](bool dotransition) {
					func_doaction();
				});
			}
		}
	}
}

bool HDXUIElementBase::xml_getVarByName(std::string *out,const std::string &name) const {
	std::string tok,arg;
	XMLGetTokArg(name,&tok,&arg);

	if(tok=="position") {
		return XMLToString(out,mpos,arg);
	} else if(tok=="scale") {
		return XMLToString(out,mscale,arg);
	} else if(tok=="rotation") {
		return XMLToString(out,mrot,arg);
	} else if(tok=="blendcolor") {
		return XMLToString(out,mblendcolor,arg);
	} else if(tok=="active") {
		return XMLToString(out,mactive,arg);
	} else if(tok=="enabled") {
		return XMLToString(out,menabled,arg);
	} else if(tok=="actionsize") {
		return XMLToString(out,HDXVector2(mactionwidth,mactionheight),arg);
	}

	return false;
}

void HDXUIElementBase::_addChild(HDXUIElementBase *child) {
	if(!mactive) {
		child->setActive(false);
		mreactivate_childeren.push_back(child);
	}
	if(!menabled) {
		child->setEnabled(false);
		mreenable_childeren.push_back(child);
	}
	mchilderen.addItem(child->getName(),child);
}

void HDXUIElementBase::_addEffect(HDXUIElementEffect_Base *effect) {
	if(!mactive) {
		effect->setActive(false);
		mreactivate_effects.push_back(effect);
	}
	if(!menabled) {
		effect->setEnabled(false);
		mreenable_effects.push_back(effect);
	}
	meffects.addItem(effect->getName(),effect);
}

HDXUIMenu::HDXUIMenu(const std::string name,HDXUIElementBase *parent) : HDXUIElementBase(name,parent) {
	setActive(false);
}

HDXUIMenu::HDXUIMenu() : HDXUIElementBase(0) {
	mmenumap = 0;
}

HDXUIMenu::~HDXUIMenu() {
}

HDXUIMenuMap* HDXUIMenu::getMenuMap() const {
	return mmenumap;
}

HDXUIMenuMap::HDXUIMenuMap() {
}

HDXUIMenuMap::~HDXUIMenuMap() {
	clear();
}

void HDXUIMenuMap::createFromXML(const std::string &fname,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize) {
	rapidxml::file<> xmlfile(fname.c_str());
	if(xmlfile.data() == 0) return;

	HDXUIXMLScript curscript;

	auto source_window_main = curscript.createSource("window_main");
	curscript.getSource("")->addSource(source_window_main->mname,source_window_main);
	source_window_main->addVarAccess("width",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDX_MAIN->getWindowWidth());
	});
	source_window_main->addVarAccess("height",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDX_MAIN->getWindowHeight());
	});
	source_window_main->addVarAccess("name",[](const XMLScript *script,const std::string &arg)->std::string {
		return HDX_MAIN->getWindowTitle();
	});

	curscript.getSource("")->addVarAccess("DT_LEFT",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(DT_LEFT);
	});
	curscript.getSource("")->addVarAccess("DT_RIGHT",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(DT_RIGHT);
	});
	curscript.getSource("")->addVarAccess("DT_CENTER",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(DT_CENTER);
	});
	curscript.getSource("")->addVarAccess("DT_TOP",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(DT_TOP);
	});
	curscript.getSource("")->addVarAccess("DT_VCENTER",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(DT_VCENTER);
	});
	curscript.getSource("")->addVarAccess("DT_BOTTOM",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(DT_BOTTOM);
	});

	curscript.getSource("")->addVarAccess("HDXAL_LEFT",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDXAL_LEFT);
	});
	curscript.getSource("")->addVarAccess("HDXAL_RIGHT",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDXAL_RIGHT);
	});
	curscript.getSource("")->addVarAccess("HDXAL_CENTER",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDXAL_CENTER);
	});
	curscript.getSource("")->addVarAccess("HDXAL_TOP",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDXAL_TOP);
	});
	curscript.getSource("")->addVarAccess("HDXAL_VCENTER",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDXAL_VCENTER);
	});
	curscript.getSource("")->addVarAccess("HDXAL_BOTTOM",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDXAL_BOTTOM);
	});

	curscript.getSource("")->addVarAccess("TRF_ONENTER",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDX_TRANS_ENTER);
	});
	curscript.getSource("")->addVarAccess("TRF_ONEXIT",[](const XMLScript *script,const std::string &arg)->std::string {
		return std::to_string(HDX_TRANS_EXIT);
	});

	auto source_ui = curscript.createSource("ui");
	curscript.getSource("")->addSource(source_ui->mname,source_ui);
	source_ui->addVarAccess("font_head",[](const XMLScript *script,const std::string &arg)->std::string {
		return "DX:Arial_15";
	});

	rapidxml::xml_document<> doc;
	doc.parse<0>(xmlfile.data());

	rapidxml::xml_node<> *node_root = doc.first_node("ui");

	if(node_root) {
		for(rapidxml::xml_node<> *node_i = node_root->first_node("uielement"); node_i; node_i = node_i->next_sibling("uielement")) {
			if(strcmp(node_i->first_attribute("type")->value(),"menu")==0) {
				HDXUIMenu *newmenu = new HDXUIMenu();
				newmenu->mmenumap = this;
				newmenu->xml_initialize(&curscript,node_i,func_oninitialize);
				mmenus.addItem(newmenu->getName(),newmenu);
			}
		}
	}
}

HDXUIMenu* HDXUIMenuMap::createMenu(const std::string &name) {
	assert(name.size()>0);

	HDXUIMenu *newmenu = new HDXUIMenu(name);
	newmenu->mmenumap = this;
	mmenus.addItem(name,newmenu);
	return newmenu;
}

HDXUIMenu* HDXUIMenuMap::getMenu(const std::string &name) {
	return mmenus.hasItem(name) ? mmenus.getItem(name) : 0;
}

void HDXUIMenuMap::destroyMenu(const std::string &name) {
	HDXUIMenu *newmenu = getMenu(name);
	if(newmenu) {
		delete newmenu;
		mmenus.removeItem(name);
	}
}

void HDXUIMenuMap::clear() {
	mtraversal.clear();
	mmenus.doIterate([&](HDXUIMenu *item)->bool {
		delete item;
		return false;
	});
	mmenus.clear();
}

HDXUIMenu* HDXUIMenuMap::getCurrentMenu() {
	if(mtraversal.size() > 0) {
		return mtraversal.back();
	}
	return 0;
}

void HDXUIMenuMap::navigateToMenu(const std::string &menu,bool dotransnition) {
	HDXUIMenu *targetmenu = getMenu(menu);
	//if the targetmenu exists and mtraversal is either empty or the targetmenu is not the current menu
	if(mtraversal.size()==0 || (mtraversal.back()!=targetmenu)) {
		if(mtraversal.size() > 0) {
			//current menu onMenuTransitionExit
			mtransitions.emplace(menu,dotransnition ? mtraversal.back()->getMaxDesiredTransitionTime() : 0,false);
		}
		//new menu onMenuTransitionEnter
		mtransitions.emplace(menu,targetmenu&&dotransnition ? targetmenu->getMaxDesiredTransitionTime() : 0,true);
	}
}

void HDXUIMenuMap::navigateBackwards(bool dotransnition) {
	if(mtraversal.size()>1) {
		navigateToMenu(mtraversal[mtraversal.size()-2]->getName(),dotransnition);
	} else if(mtraversal.size()==1) {
		navigateToMenu("",dotransnition);
	}
}

void HDXUIMenuMap::doIterate(std::function<bool(HDXUIMenu *item)> &&func) {
	mmenus.doIterate([&](HDXUIMenu *item)->bool {
		return func(item);
	});
}

void HDXUIMenuMap::update(float dt) {
	dt = dt / HDX_MAIN->getTimeScale(); //ui should move just as fast no matter what

	if(mtransitions.size() > 0) {
		_transition &curtrans = mtransitions.front();
		if(!curtrans.mstarted) {
			curtrans.mstarted = true;
			if(curtrans.menter) {
				_setMenu(curtrans.mtarget);
				if(mtraversal.size()>0) {
					mtraversal.back()->setEnabled(false); //interaction disabled, still visible and still updates
					mtraversal.back()->onMenuTransitionEnter(curtrans.mtimer>0.0f);
				}
			} else if(mtraversal.size()>0) {
				mtraversal.back()->setEnabled(false); //interaction disabled, still visible and still updates
				mtraversal.back()->onMenuTransitionExit(curtrans.mtimer>0.0f);
			}
		}
		if(curtrans.mtimer > 0) {
			curtrans.mtimer -= dt;
		}
		if(curtrans.mtimer <= 0) {
			if(curtrans.mextraframes == 0) {
				//_setMenu(curtrans.mtarget);
				if(mtraversal.size()>0 && mtraversal.back()) mtraversal.back()->setEnabled(true);
				mtransitions.pop();
			} else {
				curtrans.mextraframes--;
			}
		}
	}

	if(mtraversal.size()>0) {
		if(mtraversal.back()->isActive()) mtraversal.back()->update(dt);
	}
}

void HDXUIMenuMap::render() {
	if(mtraversal.size()>0) {
		mtraversal.back()->render();
	}
}

void HDXUIMenuMap::_setMenu(const std::string &menu) {
	HDXUIMenu *targetmenu = getMenu(menu);
	if(mtraversal.size()==0 || (mtraversal.back()!=targetmenu)) {
		if(mtraversal.size()>0) {
			mtraversal.back()->setActive(false);
			//if the target menu is already in the traversal remove it and whatever is after it
			auto fit = std::find(mtraversal.begin(),mtraversal.end(),targetmenu);
			while(fit != mtraversal.end()) {
				fit = mtraversal.erase(fit);
			}
		}
		if(targetmenu) {
			mtraversal.push_back(targetmenu);
		} else {
			mtraversal.clear();
		}
	}
	if(targetmenu) {
		targetmenu->setActive(true);
		targetmenu->setEnabled(true);
	}
}