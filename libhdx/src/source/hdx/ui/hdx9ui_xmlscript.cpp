#include "hdx/ui/hdx9ui_xmlscript.h"

#include "hdx/ui/hdx9ui.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_base.h"

#include <string>
#include <sstream>
#include <iomanip>

HDXUIXMLScript::Source_UIActor::Source_UIActor(const std::string &name,XMLScript *parentscript) : Source(name,parentscript) {
	mcuractor = 0;
	miselement = false;
}

std::string HDXUIXMLScript::Source_UIActor::dereference(Source *prevsource,const std::string &entryname,const std::string &str) {
	std::string tok,arg;
	XMLGetTokArg(str,&tok,&arg);

	auto sd_this = ((HDXUIXMLScript*)mparentscript)->getScriptData_this();

	if(entryname=="this") {
		if(prevsource->mname=="") {
			mcuractor = sd_this.first;
			miselement = sd_this.second;
		}
	} else if(entryname=="parent") {
		if(miselement) {
			mcuractor = ((HDXUIElementBase*)mcuractor)->getParent();
		} else {
			mcuractor = ((HDXUIElementEffect_Base*)mcuractor)->getParent();
		}
		miselement = true;
	} else if(entryname=="element") {
		if(mcuractor == 0) {
			mcuractor = sd_this.first;
			miselement = sd_this.second;
		}
		HDXUIElementBase *parent = 0;
		if(miselement)
			parent = ((HDXUIElementBase*)mcuractor)->getParent();
		else
			parent = ((HDXUIElementEffect_Base*)mcuractor)->getParent();
		if(parent) {
			mcuractor = parent;
			miselement = true;
			return dereference(this,entryname,str);
		} else {
			if(tok==((HDXUIElementBase*)mcuractor)->getName()) {

			} else {
				mcuractor = ((HDXUIElementBase*)mcuractor)->getSubElement(tok);
			}
			return dereference(this,tok,arg);
		}
	} else if(entryname=="child") {
		if(miselement) {
			mcuractor = ((HDXUIElementBase*)mcuractor)->getChildElement(tok);
			miselement = true;
			return dereference(this,tok,arg);
		}
	} else if(entryname=="effect") {
		if(miselement) {
			mcuractor = ((HDXUIElementBase*)mcuractor)->getEffect(tok);
			miselement = false;
			return dereference(this,tok,arg);
		}
	}

	if(msources.count(tok)>0) {
		return msources[tok]->dereference(this,tok,arg);
	}

	if(mvars.count(tok)>0) {
		return mvars[tok](mparentscript,arg);
	}

	{
		std::string ret;
		if(mcuractor->xml_getVarByName(&ret,str)) {
			return ret;
		}
	}

	return "";
}

HDXUIXMLScript::HDXUIXMLScript() {
	setScriptData_this(0,false);

	auto source_main = getSource("");
	auto source_uiactor = createSource<Source_UIActor>("uiactor");

	source_main->addSource("this",source_uiactor);
	source_main->addSource("element",source_uiactor);

	source_uiactor->addSource("parent",source_uiactor);
	source_uiactor->addSource("child",source_uiactor);
	source_uiactor->addSource("effect",source_uiactor);
}

HDXUIXMLScript::~HDXUIXMLScript() {
}

void HDXUIXMLScript::setScriptData_this(IHDXUIIActor *data,bool iselement) {
	mthis = data;
	mthis_iselement = iselement;
}

void HDXUIXMLScript::setScriptData_this(std::pair<IHDXUIIActor*,bool> data) {
	setScriptData_this(data.first,data.second);
}

std::pair<IHDXUIIActor*,bool> HDXUIXMLScript::getScriptData_this() const {
	return std::make_pair(mthis,mthis_iselement);
}

bool XMLRead(D3DXCOLOR *out,XMLScript *script,rapidxml::xml_node<> *node) {
	*out = std::stoul(script->scriptGetValue(node),0,16);
	return true;
}

bool XMLRead(HDXVector2 *out,XMLScript *script,rapidxml::xml_node<> *node) {
	return (XMLRead(&out->x,script,node->first_node("x")) &&
			XMLRead(&out->y,script,node->first_node("y")));
}

bool XMLRead(HDXVector3 *out,XMLScript *script,rapidxml::xml_node<> *node) {
	return (XMLRead(&out->x,script,node->first_node("x")) &&
			XMLRead(&out->y,script,node->first_node("y")) &&
			XMLRead(&out->z,script,node->first_node("z")));
}

bool XMLRead(RECT *out,XMLScript *script,rapidxml::xml_node<> *node) {
	return (XMLRead(&out->left,script,node->first_node("l")) &&
			XMLRead(&out->top,script,node->first_node("t")) &&
			XMLRead(&out->right,script,node->first_node("r")) &&
			XMLRead(&out->bottom,script,node->first_node("b")));
}

template<> bool XMLToString<D3DXCOLOR>(std::string *out,const D3DXCOLOR &in,std::string arg) {
	if(arg=="r"||
	   arg=="x") {
		*out = std::to_string(in.r);
		return true;
	} else if(arg=="g"||
			  arg=="y") {
		*out = std::to_string(in.g);
		return true;
	} else if(arg=="b"||
			  arg=="z") {
		*out = std::to_string(in.b);
		return true;
	} else if(arg=="a"||
			  arg=="w") {
		*out = std::to_string(in.a);
		return true;
	} else {
		std::stringstream ss;
		ss << std::hex << in;
		*out = ss.str();
		return true;
	}

	return false;
}

template<> bool XMLToString<HDXVector2>(std::string *out,const HDXVector2 &in,std::string arg) {
	if(arg=="x") {
		*out = std::to_string(in.x);
		return true;
	} else if(arg=="y") {
		*out = std::to_string(in.y);
		return true;
	}

	return false;
}

template<> bool XMLToString<HDXVector3>(std::string *out,const HDXVector3 &in,std::string arg) {
	if(arg=="x") {
		*out = std::to_string(in.x);
		return true;
	} else if(arg=="y") {
		*out = std::to_string(in.y);
		return true;
	} else if(arg=="z") {
		*out = std::to_string(in.z);
		return true;
	}

	return false;
}

template<> bool XMLToString<RECT>(std::string *out,const RECT &in,std::string arg) {
	if(arg=="l") {
		*out = std::to_string(in.left);
		return true;
	} else if(arg=="t") {
		*out = std::to_string(in.top);
		return true;
	} else if(arg=="r") {
		*out = std::to_string(in.right);
		return true;
	} else if(arg=="b") {
		*out = std::to_string(in.bottom);
		return true;
	}

	return false;
}