#include "hdx/util.h"
#include "hdx/hdx9math.h"
#include "hdx/hdx9geometry.h"
#include "hdx/hdx9mesh.h"
#include "hdx/hdx9material.h"
#include "hdx/hdx9vertex.h"
#include "hdx/hdx9.h"

std::default_random_engine rand_generator;

unsigned int rand_getint() {
	return rand_generator();
}

#ifdef max
#undef max
#endif
float rand_getfloat() {
	return rand_generator()/(float)rand_generator.max();
}

unsigned colorRandom(bool alpha) {
	return ((alpha ? ((rand_getint())&0xff)<<24 : 0xff000000) | ((rand_getint())&0xff)<<16 | ((rand_getint())&0xff)<<8 | ((rand_getint())&0xff));
}

int colorAdd(int color,int coloradd) {
	int ret = color;
	unsigned char *col[3];
	col[0] = (unsigned char*)&color;
	col[1] = (unsigned char*)&coloradd;
	col[2] = (unsigned char*)&ret;
	for(int i = 0; i < 4; i++) {
		col[2][i] = CLAMP(col[0][i] + col[1][i],0,255);
	}
	return ret;
}

int colorLighten(int color,float factor) {
	int ret = color;
	unsigned char *col[2];
	col[0] = (unsigned char*)&color;
	col[1] = (unsigned char*)&ret;
	for(int i = 1; i < 4; i++) {
		col[1][i] = (unsigned char)CLAMP(col[0][i] + (float)(col[0][i]) * factor,0,255);
	}
	return ret;
}

int colorDarken(int color,float factor) {
	int ret = color;
	unsigned char *col[2];
	col[0] = (unsigned char*)&color;
	col[1] = (unsigned char*)&ret;
	for(int i = 1; i < 4; i++) {
		col[1][i] = (unsigned char)CLAMP(col[0][i] - (float)(col[0][i]) * factor,0,255);
	}
	return ret;
}

int colorFade(int col_start,int col_end,float progress) {
	if(progress>1.0f) progress = 1.0f;
	int ret = col_start;
	unsigned char *col[3];
	col[0] = (unsigned char*)&col_start;
	col[1] = (unsigned char*)&col_end;
	col[2] = (unsigned char*)&ret;
	for(int i = 0; i < 4; i++) {
		col[2][i] = (unsigned char)CLAMP(col[0][i] + (float)(col[1][i]-col[0][i]) * progress,0,255);
	}
	return ret;
}

int colorFade(int col_start,int col_end,int steps,int curstep) {
	return colorFade(col_start,col_end,curstep/(float)steps);
}

HDXMesh* HDXMeshCreateFromConvexHull(const HDXConvexHull *hull,const HDXMaterial &mat,bool uniqueverts) {
	HDXMesh *ret = HDXMeshCreateBlank("",false);

	unsigned num_verts;
	unsigned num_faces;

	num_faces = hull->mtriangles.size();
	if(uniqueverts) {
		num_verts = num_faces*3;
	} else {
		num_verts = hull->mverts.size();
	}

	; {
		D3DVERTEXELEMENT9 elements[MAX_FVF_DECL_SIZE-1];
		UINT numelements = 0;
		HDXVERTPN::decl->GetDeclaration(elements,&numelements);
		D3DXCreateMesh(num_faces,
					   num_verts,
					   D3DXMESH_SYSTEMMEM,
					   elements,
					   HDX_MAIN->getD3DDevice(),
					   &ret->md3dmesh);
	}

// 	std::vector<HDXVector3> vert_normals(hull->mverts.size(),HDXVector3(0,0,0));
// 	for(unsigned i = 0; i < hull->mtriangles.size(); i++) {
// 		HDXVector3 fn;
// 		fn = (hull->mverts[hull->mtriangles[i][1]] - hull->mverts[hull->mtriangles[i][0]]) % (hull->mverts[hull->mtriangles[i][2]] - hull->mverts[hull->mtriangles[i][0]]);
// 		for(unsigned j = 0; j < 3; j++) {
// 			vert_normals[hull->mtriangles[i][j]] += fn;
// 		}
// 	}
// 	for(unsigned i = 0; i < vert_normals.size(); i++) {
// 		D3DXVec3Normalize(&vert_normals[i],&vert_normals[i]);
// 	}

	HDXVERTPN *verts;
	ret->md3dmesh->LockVertexBuffer(0,(void**)&verts);
	
	if(uniqueverts) {
		for(unsigned f = 0; f < num_faces; f++) {
			for(unsigned v = 0; v < 3; v++) {
				verts[f*3+v] = HDXVERTPN(*hull->mverts[hull->mtriangles[f][v]].getD3DX(),
										 D3DXVECTOR3(0,1,0));
			}
		}
	} else {
		for(unsigned i = 0; i < num_verts; i++) {
			verts[i] = HDXVERTPN(*hull->mverts[i].getD3DX(),
								 D3DXVECTOR3(0,1,0));
		}
	}
	
	ret->md3dmesh->UnlockVertexBuffer();

	unsigned ind_stride;

	; {
		D3DINDEXBUFFER_DESC ind_desc;
		IDirect3DIndexBuffer9 *ind_buff;
		ret->md3dmesh->GetIndexBuffer(&ind_buff);
		ind_buff->GetDesc(&ind_desc);
		ind_stride = ind_desc.Size / (ret->md3dmesh->GetNumFaces()*3);
	}

	BYTE *inds;
	ret->md3dmesh->LockIndexBuffer(0,(void**)&inds);

	if(uniqueverts) {
		for(unsigned i = 0; i < num_verts; i++) {
			memcpy(&inds[(i)*ind_stride],&i,ind_stride);
		}
	} else {
		for(unsigned i = 0; i < num_faces; i++) {
			for(unsigned j = 0; j < 3; j++) {
				memcpy(&inds[(i*3+j)*ind_stride],&hull->mtriangles[i][j],ind_stride);
			}
		}
	}


	ret->md3dmesh->UnlockIndexBuffer();

	D3DXComputeNormals(ret->md3dmesh,0);

	D3DXATTRIBUTERANGE attribtable[1];
	attribtable[0].AttribId = 0;
	attribtable[0].FaceStart = 0;
	attribtable[0].FaceCount = num_faces;
	attribtable[0].VertexStart = 0;
	attribtable[0].VertexCount = num_verts;
	ret->md3dmesh->SetAttributeTable(attribtable,1);

	ret->md3dmesh->Optimize(D3DXMESH_MANAGED |
							D3DXMESHOPT_COMPACT |
							D3DXMESHOPT_ATTRSORT |
							D3DXMESHOPT_VERTEXCACHE,
							0,
							0,
							0,
							0,
							&ret->md3dmesh);

	ret->mtextures.push_back(0);
	ret->mmaterials.push_back(mat);

	return ret;
}

HDXConvexHull HDXConvexHullCreateFromMesh(const HDXMesh *mesh) {
	//todo: make a convex hull for each subset
	//subset can be obtained via the ID3DXBaseMesh::GetAttributeTable() function
	std::vector<HDXVector3> pointcloud;

	HDXVERTPCTN *verts;
	mesh->md3dmesh->LockVertexBuffer(0,(void**)&verts);

	for(unsigned i = 0; i < mesh->md3dmesh->GetNumVertices(); i++) {
		pointcloud.push_back(verts[i].p);
	}

	mesh->md3dmesh->UnlockVertexBuffer();

	return HDXQuickHull(pointcloud.data(),pointcloud.size());
}
