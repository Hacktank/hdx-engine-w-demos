#pragma once

#include <chrono>
#include <random>

#include <d3dx9.h>
#include "hdx/hdx9math.h"

#define MAX3(a,b,c) ((a)>(b) ? ((a)>(c) ? (a) : (c)) : ((b)>(c) ? (b) : (c)))
#define VEC3MAXSCALAR(v) (MAX3((v).x,(v).y,(v).z))
#define VEC3MAXABSSCALAR(v) (MAX3(fabs((v).x),fabs((v).y),fabs((v).z)))
#define VEC2TOARG(v) (float)((v).x),(float)((v).y)
#define VEC3TOARG(v) (float)((v).x),(float)((v).y),(float)((v).z)
#define VEC2STREAM(v) "[" << (v).x << ", " << (v).y << "]"
#define VEC3STREAM(v) "[" << (v).x << ", " << (v).y << ", " << (v).z << "]"
#define SQ(x) ((x)*(x))
#define SIGN(n) ((n)<0 ? -1:1)

#define CLAMP(n,nummin,nummax) ((n)<(nummin) ? (nummin):((n)>(nummax) ?(nummax):(n)))
#define CLAMPVEC3(n,nummin,nummax) (HDXVector3(CLAMP((n).x,(nummin).x,(nummax).x),CLAMP((n).y,(nummin).y,(nummax).y),CLAMP((n).z,(nummin).z,(nummax).z)))

#define RANDRANGE(a,b) ((rand_getint()%(int)((b)+1-(a)))+(int)(a))
#define FRANDRANGE(a,b) (rand_getfloat()*((a)-(b))+(b))

#define gettime_ms() (std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()).time_since_epoch().count())

extern std::default_random_engine rand_generator;
unsigned int rand_getint();
float rand_getfloat();

// hash combine algorithm from boost
template<typename T>
static void hash_combine(std::size_t &seed,const T &v) {
	std::hash<T> hasher;
	seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

template<typename T>
struct hthash {
	std::size_t operator()(const T &a) {
		return std::hash<T>(a);
	}
};

template<typename A,typename B>
struct hthash_pair {
	std::size_t operator()(const std::pair<A,B> &p) {
		std::size_t seed = 0;
		hash_combine(seed,p.first);
		hash_combine(seed,p.second);
		return seed;
	}
};

class HDXConvexHull;
class HDXMesh;
struct HDXMaterial;

HDXMesh* HDXMeshCreateFromConvexHull(const HDXConvexHull *hull,const HDXMaterial &mat,bool uniqueverts = true);
HDXConvexHull HDXConvexHullCreateFromMesh(const HDXMesh *mesh);

unsigned colorRandom(bool alpha = false);
int colorAdd(int color,int coloradd);
int colorDarken(int color,float factor);
int colorLighten(int color,float factor);
int colorFade(int col_start,int col_end,float progress);
int colorFade(int col_start,int col_end,int steps,int curstep);