#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <queue>
#include <tuple>
#include <functional>
#include <chrono>

#define MSGCBFROMLAM(cap,...) (std::function<void (__VA_ARGS__)>)[cap](__VA_ARGS__)->void
#define MSGCBFROMFP(func,...) (std::function<void (__VA_ARGS__)>)(func)

#define MSGMAN MessageManager::instance()

class tuplecall {
	// code from https://stackoverflow.com/questions/10766112/c11-i-can-go-from-multiple-args-to-tuple-but-can-i-go-from-tuple-to-multiple?lq=1
private:
	template<typename FUNC,typename TUPLE,bool DONE,int TOTAL,int... N>
	struct call_impl {
		static void call(FUNC &&f,TUPLE &&t) {
			call_impl<FUNC,TUPLE,TOTAL == 1 + sizeof...(N),TOTAL,N...,sizeof...(N)>::call(std::forward<FUNC>(f),std::forward<TUPLE>(t));
		}
	};

	template<typename FUNC,typename TUPLE,int TOTAL,int... N>
	struct call_impl<FUNC,TUPLE,true,TOTAL,N...> {
		static void call(FUNC &&f,TUPLE &&t) {
			f(std::get<N>(std::forward<TUPLE>(t))...);
		}
	};

public:

	//************************************
	// Method:    call
	// Returns:   void
	// Parameter: FUNC &&f (callable function object/pointer)
	// Parameter: TUPLE &&t (tuple containing necessary parameters to call FUNC)
	//************************************
	template<typename FUNC,typename TUPLE>
	static void call(FUNC &&f,TUPLE &&t) {
		typedef typename std::decay<TUPLE>::type ttype;
		call_impl<FUNC,TUPLE,0 == std::tuple_size<ttype>::value,std::tuple_size<ttype>::value>::call(std::forward<FUNC>(f),std::forward<TUPLE>(t));
	}
};


// code from: https://stackoverflow.com/questions/11893141/inferring-the-call-signature-of-a-lambda-or-arbitrary-callable-for-make-functio
/*
template<typename T> struct remove_class {};
template<typename C,typename R,typename... A>
struct remove_class<R(C::*)(A...)> { using type = R(A...); };
template<typename C,typename R,typename... A>
struct remove_class<R(C::*)(A...) const> { using type = R(A...); };
template<typename C,typename R,typename... A>
struct remove_class<R(C::*)(A...) volatile> { using type = R(A...); };
template<typename C,typename R,typename... A>
struct remove_class<R(C::*)(A...) const volatile> { using type = R(A...); };

template<typename T>
struct get_signature_impl {
using type = typename remove_class<
decltype(&std::remove_reference<T>::type::operator())>::type;
};
template<typename R,typename... A>
struct get_signature_impl<R(A...)> { using type = R(A...); };
template<typename R,typename... A>
struct get_signature_impl<R(&)(A...)> { using type = R(A...); };
template<typename R,typename... A>
struct get_signature_impl<R(*)(A...)> { using type = R(A...); };
template<typename T> using get_signature = typename get_signature_impl<T>::type;

template<typename F> using make_function_type = std::function<get_signature<F>>;
template<typename F> make_function_type<F> make_function(F &&f) {
return make_function_type<F>(std::forward<F>(f));
}*/

class MessageManager {
	friend class MessageListener;
	friend class MessageSender;

	struct QueuedMessage {
		__int64 mdistribuitetime;
		std::function<void(void*)> *mboundfunc;
		void *margtuple;

		QueuedMessage(__int64 distribuitetime,std::function<void(void*)> *boundfunc,void *argtuple) :
			mdistribuitetime(distribuitetime),
			mboundfunc(boundfunc),
			margtuple(argtuple) {
			//
		}

		friend bool operator>(const QueuedMessage &l,const QueuedMessage &r) { return l.mdistribuitetime > r.mdistribuitetime; } // comparator for the priority queue
	};

	/*
	* workaround for compiler bug: https://connect.microsoft.com/VisualStudio/feedback/details/777565/variadic-template-bug
	* the bug only occurs when the parameter pack contains nothing
	*/
	template<typename Ret,typename ...Args>
	struct DeduceFuncType {
		typedef std::function<Ret(Args...)> type;
	};

	template<typename Ret>
	struct DeduceFuncType<Ret> {
		typedef std::function<Ret(void)> type;
	};

public:
	static MessageManager* instance() { static MessageManager *gnew = new MessageManager(); return gnew; }

	~MessageManager() {
		// clear out all hanging memory
		for(auto it0 = mlistenerregistration.begin(); it0 != mlistenerregistration.end(); it0++) {
			for(auto it1 = it0->second.begin(); it1 != it0->second.end(); it1++) {
				for(auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {
					delete (*it2).second;
				}
			}
		}
		while(!mmessagequeue.empty()) {
			delete mmessagequeue.top().mboundfunc;
			delete mmessagequeue.top().margtuple;
			mmessagequeue.pop();
		}
	}

	//************************************
	// Method:    addSubscription
	// Returns:   int (the new subscription id)
	// Parameter: int sid (sender id to listen to)
	// Parameter: std::string type (message type to listen to)
	// Parameter: std::function<void(Args...)> &&func (callback)
	// Notes:
	// Use MSGCBFROMLAM(capture,args) {code} to generate the function object from a lambda
	// Use MSGCBFROMLFP(func,args) to generate the function object from a function pointer
	//************************************
	template<typename ...Args>
	int addSubscription(int sid,std::string type,std::function<void(Args...)> &&func) {
		int newid = _getNVSID();
		typedef DeduceFuncType<void,Args...>::type ttype;
		mlistenerregistration[type][sid].emplace_back(newid,new ttype(std::forward<ttype>(func)));
		auto &newiditem = mlisteneridmap[newid];
		newiditem.first = sid;
		newiditem.second = type;
		return newid;
	}

	//************************************
	// Method:    removeSubscription
	// Returns:   void
	// Parameter: int id (subscription id, returned from addSubscription()
	// Notes:
	//************************************
	void removeSubscription(int id) {
		if(mlisteneridmap.count(id) > 0) {
			auto &listenerdesc = mlisteneridmap[id];
			auto &typecont = mlistenerregistration[listenerdesc.second];
			auto &lisvec = typecont[listenerdesc.first];

			for(unsigned int i = 0; i < lisvec.size(); i++) {
				if(lisvec[i].first == id) {
					delete lisvec[i].second;
					lisvec.erase(lisvec.begin() + i);
					break;
				}
			}

			if(lisvec.size() == 0) typecont.erase(listenerdesc.first);
			if(typecont.size() == 0) mlistenerregistration.erase(listenerdesc.second);
			mlisteneridmap.erase(id);
		}
	}

	//************************************
	// Method:    enqueueMessage
	// Returns:   void
	// Parameter: unsigned int delay (distribution delay, in milliseconds)
	// Parameter: int sid (the sender-id)
	// Parameter: std::string type (the message type)
	// Parameter: Args ...args (the message arguments)
	// Notes:
	// Incorrect number of parameters can(will) cause ESP corruption
	//************************************
	template<typename ...Args>
	void enqueueMessage(unsigned int delay,int sid,std::string type,Args ...args) {
		__int64 curtime = (std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()).time_since_epoch().count());
		//creates a delayed calling of the function _distributeMessage with the arguments passed
		mmessagequeue.emplace(
			curtime + delay,
			new std::function<void(void*)>(
			std::bind(
			&MessageManager::_distributeMessage<Args...>,
			this,
			sid,
			type,
			std::placeholders::_1 //the argument tuple pointer
			)),
			new std::tuple<Args...>(std::forward<Args>(args)...)
			);
	}

	//************************************
	// Method:    update
	// Returns:   void
	// Notes:
	// Distributes any messages who's time has come
	//************************************
	void update() {
		__int64 curtime = (std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()).time_since_epoch().count());
		std::vector<QueuedMessage> framemessages;
		//stores this frame's messages distributions, to prevent possible infinite loops
		while(!mmessagequeue.empty() && mmessagequeue.top().mdistribuitetime <= curtime) {
			framemessages.push_back(mmessagequeue.top());
			mmessagequeue.pop();
		}
		for(unsigned int i = 0; i < framemessages.size(); i++) {
			(*(framemessages[i].mboundfunc))(framemessages[i].margtuple); // calls _distributeMessage with bound parameters
			delete framemessages[i].mboundfunc;
			delete framemessages[i].margtuple;
		}
	}

private:
	int mnvid;
	//mmessage_listenerregistration[MESSAGETYPE][SENDERID][#]<id,callback> SENDERID==-1 for any sender
	std::unordered_map<std::string,std::unordered_map<int,std::vector<std::pair<int,void*>>>> mlistenerregistration;
	std::unordered_map<int,std::pair<int,std::string>> mlisteneridmap; //allows access to the SENDERID and MESSAGETYPE via the registration id
	std::priority_queue<QueuedMessage,std::vector<QueuedMessage>,std::greater<QueuedMessage>> mmessagequeue;

	MessageManager() {
		mnvid = 0;
	}

	template<typename ...Args>
	void _distributeMessage(int sid,std::string type,void *args) {
		typedef DeduceFuncType<void,Args...>::type ttype;
		if(mlistenerregistration.count(type) > 0) {
			auto lam_distributeToVector = [&](std::vector<std::pair<int,void*>> &vec) {
				for(unsigned int i = 0; i < vec.size(); i++) {
					tuplecall::call(*((ttype*)vec[i].second),std::forward<std::tuple<Args...>>(*((std::tuple<Args...>*)args)));
				}
			};

			auto &typecont = mlistenerregistration[type];
			if(typecont.count(sid) > 0) {
				lam_distributeToVector(typecont[sid]);
			}
			if(sid != -1) if(typecont.count(-1) > 0) {
				lam_distributeToVector(typecont[-1]);
			}
		}
	}

	int _getNVSID() { return mnvid++; }
};
