#pragma once

//macros from: http://stackoverflow.com/questions/5919663/how-does-photoshop-blend-two-images-together

#define ChannelBlend_Normal(A,B)     ((unsigned char)(A))
#define ChannelBlend_Lighten(A,B)    ((unsigned char)((B > A) ? B:A))
#define ChannelBlend_Darken(A,B)     ((unsigned char)((B > A) ? A:B))
#define ChannelBlend_Multiply(A,B)   ((unsigned char)((A * B) / 255))
#define ChannelBlend_Average(A,B)    ((unsigned char)((A + B) / 2))
#define ChannelBlend_Add(A,B)        ((unsigned char)(min(255, (A + B))))
#define ChannelBlend_Subtract(A,B)   ((unsigned char)((A + B < 255) ? 0:(A + B - 255)))
#define ChannelBlend_Difference(A,B) ((unsigned char)(abs(A - B)))
#define ChannelBlend_Negation(A,B)   ((unsigned char)(255 - abs(255 - A - B)))
#define ChannelBlend_Screen(A,B)     ((unsigned char)(255 - (((255 - A) * (255 - B)) >> 8)))
#define ChannelBlend_Exclusion(A,B)  ((unsigned char)(A + B - 2 * A * B / 255))
#define ChannelBlend_Overlay(A,B)    ((unsigned char)((B < 128) ? (2 * A * B / 255):(255 - 2 * (255 - A) * (255 - B) / 255)))
#define ChannelBlend_SoftLight(A,B)  ((unsigned char)((B < 128)?(2*((A>>1)+64))*((float)B/255):(255-(2*(255-((A>>1)+64))*(float)(255-B)/255))))
#define ChannelBlend_HardLight(A,B)  (ChannelBlend_Overlay(B,A))
#define ChannelBlend_ColorDodge(A,B) ((unsigned char)((B == 255) ? B:min(255, ((A << 8 ) / (255 - B)))))
#define ChannelBlend_ColorBurn(A,B)  ((unsigned char)((B == 0) ? B:max(0, (255 - ((255 - A) << 8 ) / B))))
#define ChannelBlend_LinearDodge(A,B)(ChannelBlend_Add(A,B))
#define ChannelBlend_LinearBurn(A,B) (ChannelBlend_Subtract(A,B))
#define ChannelBlend_LinearLight(A,B)((unsigned char)(B < 128)?ChannelBlend_LinearBurn(A,(2 * B)):ChannelBlend_LinearDodge(A,(2 * (B - 128))))
#define ChannelBlend_VividLight(A,B) ((unsigned char)(B < 128)?ChannelBlend_ColorBurn(A,(2 * B)):ChannelBlend_ColorDodge(A,(2 * (B - 128))))
#define ChannelBlend_PinLight(A,B)   ((unsigned char)(B < 128)?ChannelBlend_Darken(A,(2 * B)):ChannelBlend_Lighten(A,(2 * (B - 128))))
#define ChannelBlend_HardMix(A,B)    ((unsigned char)((ChannelBlend_VividLight(A,B) < 128) ? 0:255))
#define ChannelBlend_Reflect(A,B)    ((unsigned char)((B == 255) ? B:min(255, (A * A / (255 - B)))))
#define ChannelBlend_Glow(A,B)       (ChannelBlend_Reflect(B,A))
#define ChannelBlend_Phoenix(A,B)    ((unsigned char)(min(A,B) - max(A,B) + 255))
#define ChannelBlend_Alpha(A,B,O)    ((unsigned char)(O * A + (1 - O) * B))
#define ChannelBlend_AlphaF(A,B,F,O) (ChannelBlend_Alpha(F(A,B),A,O))

#define ColorBlend_Buffer(T,A,B,M) \
	((unsigned char*)(T))[0] = ChannelBlend_##M(((unsigned char*)(A))[0],((unsigned char*)(B))[0]),\
	((unsigned char*)(T))[1] = ChannelBlend_##M(((unsigned char*)(A))[1],((unsigned char*)(B))[1]),\
	((unsigned char*)(T))[2] = ChannelBlend_##M(((unsigned char*)(A))[2],((unsigned char*)(B))[2]),\
	((unsigned char*)(T))[3] = ChannelBlend_##M(((unsigned char*)(A))[3],((unsigned char*)(B))[3])\

#define ColorBlend_Normal(T,A,B)        (ColorBlend_Buffer(T,A,B,Normal))
#define ColorBlend_Lighten(T,A,B)       (ColorBlend_Buffer(T,A,B,Lighten))
#define ColorBlend_Darken(T,A,B)        (ColorBlend_Buffer(T,A,B,Darken))
#define ColorBlend_Multiply(T,A,B)      (ColorBlend_Buffer(T,A,B,Multiply))
#define ColorBlend_Average(T,A,B)       (ColorBlend_Buffer(T,A,B,Average))
#define ColorBlend_Add(T,A,B)           (ColorBlend_Buffer(T,A,B,Add))
#define ColorBlend_Subtract(T,A,B)      (ColorBlend_Buffer(T,A,B,Subtract))
#define ColorBlend_Difference(T,A,B)    (ColorBlend_Buffer(T,A,B,Difference))
#define ColorBlend_Negation(T,A,B)      (ColorBlend_Buffer(T,A,B,Negation))
#define ColorBlend_Screen(T,A,B)        (ColorBlend_Buffer(T,A,B,Screen))
#define ColorBlend_Exclusion(T,A,B)     (ColorBlend_Buffer(T,A,B,Exclusion))
#define ColorBlend_Overlay(T,A,B)       (ColorBlend_Buffer(T,A,B,Overlay))
#define ColorBlend_SoftLight(T,A,B)     (ColorBlend_Buffer(T,A,B,SoftLight))
#define ColorBlend_HardLight(T,A,B)     (ColorBlend_Buffer(T,A,B,HardLight))
#define ColorBlend_ColorDodge(T,A,B)    (ColorBlend_Buffer(T,A,B,ColorDodge))
#define ColorBlend_ColorBurn(T,A,B)     (ColorBlend_Buffer(T,A,B,ColorBurn))
#define ColorBlend_LinearDodge(T,A,B)   (ColorBlend_Buffer(T,A,B,LinearDodge))
#define ColorBlend_LinearBurn(T,A,B)    (ColorBlend_Buffer(T,A,B,LinearBurn))
#define ColorBlend_LinearLight(T,A,B)   (ColorBlend_Buffer(T,A,B,LinearLight))
#define ColorBlend_VividLight(T,A,B)    (ColorBlend_Buffer(T,A,B,VividLight))
#define ColorBlend_PinLight(T,A,B)      (ColorBlend_Buffer(T,A,B,PinLight))
#define ColorBlend_HardMix(T,A,B)       (ColorBlend_Buffer(T,A,B,HardMix))
#define ColorBlend_Reflect(T,A,B)       (ColorBlend_Buffer(T,A,B,Reflect))
#define ColorBlend_Glow(T,A,B)          (ColorBlend_Buffer(T,A,B,Glow))
#define ColorBlend_Phoenix(T,A,B)       (ColorBlend_Buffer(T,A,B,Phoenix))

struct D3DXCOLOR;

#define HDXCOL_WHITE (D3DXCOLOR(0xffffffff))
#define HDXCOL_BLACK (D3DXCOLOR(0xff000000))
#define HDXCOL_RED (D3DXCOLOR(0xffff0000))
#define HDXCOL_GREEN (D3DXCOLOR(0xff00ff00))
#define HDXCOL_BLUE (D3DXCOLOR(0xff0000ff))

D3DXCOLOR colorAverage(const D3DXCOLOR &a,const D3DXCOLOR &b);
D3DXCOLOR colorMultiply(const D3DXCOLOR &a,const D3DXCOLOR &b);
