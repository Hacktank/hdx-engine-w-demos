#pragma once

#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <directxmath.h>

//#define DIRECTINPUT_VERSION 0x0800
//#include <dinput.h>

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"dxerr.lib")
#pragma comment(lib,"dinput8.lib")
//#pragma comment(lib,"dxguid.lib")

#ifdef _DEBUG
#pragma comment(lib, "d3dx9d.lib")
#else
#pragma comment(lib, "d3dx9.lib")
#endif

#include <chrono>
#include <string>
#include <functional>

#include "hdx/basemanager.h"

#define DXERR_FATAL(errormsg) do {MessageBox(0,errormsg"\n\nApplication will now exit","Error",0); PostQuitMessage(0);} while(0)
#define DXERR_FATAL_IF(pred,errormsg) do {if(pred) DXERR_FATAL(errormsg);} while(0)

#define WNDCLASSNAME "_HTDX9"

#define TIME_MAXDT (1.0f/30.0f) // maximum frame time

#define HDX_MAIN HDX9::instance()

#include "hdx/util.h"
#include "hdx/camera/hdx9camera.h"
#include "hdx/shader/hdx9shader_base.h"

class HDX9App;

class HDX9 {
	friend int WINAPI WinMain(HINSTANCE,HINSTANCE,LPSTR,int);

public:
	static HDX9* instance() { static HDX9 *gnew = new HDX9(); return gnew; }
	~HDX9();

	void initializeNewWindow(HINSTANCE hinstance,int x,int y,int w,int h);
	void initializeEmbeddedWindow(HWND wnd);
	void deinitialize();

	std::string getWindowTitle();
	void setWindowTitle(const std::string &title);

	bool getWindowBorderless();
	void setWindowBorderless(bool borderless);

	void getWindowPosition(int *x,int *y);
	void setWindowPosition(int x,int y);

	void getWindowSize(int *w,int *h);
	int getWindowWidth();
	int getWindowHeight();
	void setWindowSize(int w,int h);

	HWND getWindowHWND();
	HINSTANCE getHINSTANCE();

	DWORD getWindowCurrentStyle();

	D3DCOLOR getWindowClearColor();
	void setWindowClearColor(D3DCOLOR color);

	bool getWindowFullscreen();
	void setWindowFullscreen(bool fullscreen);

	bool getWindowVsync();
	void setWindowVsync(bool vsync);

	IDirect3DDevice9* getD3DDevice();
	ID3DXSprite* getD3DSprite();
	D3DPRESENT_PARAMETERS* getD3DPresentationParameters();

	HDXCamera* getCameraCurrent();
	void setCamera(HDXCamera *camera);
	void setCameraDefault();

	HDXShader_Base* getShaderCurrent();
	void setShader(HDXShader_Base *shader);

	unsigned int getFPS();

	float getTimeScale();
	void setTimeScale(float scale);

	void step();

	int registerFuncOnDeviceLost(std::function<void(void)> &&func);
	void unregisterFuncOnDeviceLost(int id);
	int registerFuncOnDeviceReset(std::function<void(void)> &&func);
	void unregisterFuncOnDeviceReset(int id);

	int registerFuncOnWndProc(std::function<void(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam)> &&func);
	void unregisterFuncOnWndProc(int id);

	int registerFuncUpdatePre(std::function<void(float dt)> &&func);
	void unregisterFuncUpdatePre(int id);
	int registerFuncUpdate(std::function<void(float dt)> &&func);
	void unregisterFuncUpdate(int id);
	int registerFuncUpdatePost0(std::function<void(float dt)> &&func);
	void unregisterFuncUpdatePost0(int id);
	int registerFuncUpdatePost1(std::function<void(float dt)> &&func);
	void unregisterFuncUpdatePost1(int id);

	int registerFuncRenderDevice(std::function<void(IDirect3DDevice9 *com)> &&func);
	void unregisterFuncRenderDevice(int id);
	int registerFuncRenderSprite(std::function<void(ID3DXSprite *com)> &&func);
	void unregisterFuncRenderSprite(int id);
	int registerFuncRenderShader(std::function<void(HDXShader_Base *shader)> &&func);
	void unregisterFuncRenderShader(int id);

private:
	enum WNDMODE {
		WNM_NEWWINDOW,
		WNM_EMBEDDEDWINDOW,
	};

	HINSTANCE mhinstance;

	IDirect3D9 *mcom_d3d;
	IDirect3DDevice9 *mcom_d3ddevice;
	ID3DXSprite *mcom_d3dsprite;
	HDXShader_Base *mshader_cur;
	D3DPRESENT_PARAMETERS md3dpp;
	D3DCAPS9 md3dcaps;
	DWORD mdevicebehaviorflags;
	WNDMODE mwnd_mode;
	HWND mwnd_hwnd;
	bool mwnd_vsync;
	bool mwnd_fullscreen;
	bool mwnd_borderless;
	int mwnd_windowedwidth;
	int mwnd_windowedheight;
	int mwnd_windowedx;
	int mwnd_windowedy;
	D3DCOLOR mwnd_clearcolor;
	std::string mwnd_title;
	unsigned mframes_cur;
	unsigned mframes_prev;
	float mtimescale;

	HDXCamera mcamera_default;
	HDXCamera *mcamera_cur;

	std::chrono::high_resolution_clock::time_point mframes_prevtime,mupdate_prevtime;

	BaseManagerID<std::function<void(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam)>> mfunc_onwndproc;
	BaseManagerID<std::function<void(void)>> mfunc_ondevice_lost,mfunc_ondevice_reset;
	BaseManagerID<std::function<void(float dt)>> mfunc_update_pre,mfunc_update,mfunc_update_post0,mfunc_update_post1;
	BaseManagerID<std::function<void(IDirect3DDevice9 *com)>> mfunc_render_device;
	BaseManagerID<std::function<void(ID3DXSprite *com)>> mfunc_render_sprite;
	BaseManagerID<std::function<void(HDXShader_Base *shader)>> mfunc_render_shader;

	HDX9();

	static LRESULT WINAPI _WndProc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam);

	void _attachDXToWindow();

	void _onLostDevice();
	HRESULT _resetDevice();
	void _onResetDevice();

	DWORD _getWindowStyle(bool fullscreen,bool border);
};