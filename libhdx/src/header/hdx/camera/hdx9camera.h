#pragma once

#include "hdx/hdx9math.h"

class HDXCamera {
public:
	HDXCamera();
	virtual ~HDXCamera();

	virtual HDXVector3 getPosition() const;
	virtual void setPosition(const HDXVector3 &pos);
	virtual void setPosition(float x,float y,float z);

	virtual HDXQuaternion getOrientation() const;
	virtual void setOrientation(const HDXQuaternion &ori);

	virtual void translate(const HDXVector3 &v);
	virtual void rotateAxis(const HDXVector3 &axis,float a);
	//y axis
	virtual void rotateYaw(float a);
	//x axis
	virtual void rotatePitch(float a);
	//z axis
	virtual void rotateRoll(float a);

	virtual HDXVector3 getForward() const;
	virtual HDXVector3 getRight() const;
	virtual HDXVector3 getUp() const;

	virtual void setLookAt(const HDXVector3 &point);
	virtual void setLookDirection(const HDXVector3 &dir);

	virtual const HDXMatrix4x4& getViewMatrix() const;

private:
	HDXMatrix4x4 mview;

	HDXVector3 mpos;
	HDXQuaternion mori;
	bool mchanged;
};
