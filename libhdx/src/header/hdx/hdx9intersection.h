#pragma once

#include "hdx/hdx9math.h"

class HDX9IntersectionTests {
public:
	static bool box_point(const HDXVector3 *boxext,const HDXVector3 *boxpos,const HDXVector3 *point);
	static bool box_point(const RECT *box,const POINT *point);

	static bool obb_point(const HDXVector3 *boxext,const HDXMatrix4x4 *trans,const HDXVector3 *point);

	static bool obb_obb(const HDXVector3 *boxext0,const HDXMatrix4x4 *trans0,const HDXVector3 *boxext1,const HDXMatrix4x4 *trans1);
};
