#pragma once

#include <d3d9.h>
#include <d3dx9.h>

#include <functional>
#include <stack>

#include "hdx/hdx9math.h"
#include "hdx/hdx9material.h"

class HDXTexture;

class HDXShader_Base {
public:
	HDXShader_Base(IDirect3DDevice9 *device,const char *fname,const char *techname);
	virtual ~HDXShader_Base();

	ID3DXEffect* getD3DEffect() const;
	IDirect3DDevice9* getD3DDevice() const;

	virtual void setTechnique(const char *name);

	virtual void setMatrixWorld(const HDXMatrix4x4 &mat);
	virtual const HDXMatrix4x4& getMatrixWorld() const;

	virtual void setMatrixView(const HDXMatrix4x4 &mat);
	virtual const HDXMatrix4x4& getMatrixView() const;

	virtual void setMatrixProjection(const HDXMatrix4x4 &mat);
	virtual const HDXMatrix4x4& getMatrixProjection() const;

	virtual void setTexture(HDXTexture *tex);
	virtual HDXTexture* getTexture() const;

	virtual void setMaterial(const HDXMaterial &mat);
	virtual const HDXMaterial& getMaterial() const;

	void setWVPFromDevice();

	void pushMatrixWorld();
	void popMatrixWorld();

	void multMatrixWorld(const HDXMatrix4x4 &mat);

	virtual void doDraw(std::function<void(HDXShader_Base *hdxfx)> func);

private:
	int mcbid_ondevicelost;
	int mcbid_ondevicereset;

	IDirect3DDevice9 *mdevice;
	ID3DXEffect *mfx;

	HDXMatrix4x4 mmat_w,mmat_v,mmat_p;
	HDXTexture *mtex;
	HDXMaterial mmaterial_cur;

	std::stack<HDXMatrix4x4> mmat_world_stack;

	D3DXHANDLE mhandle_tech;
	D3DXHANDLE mhandle_wvp;
	D3DXHANDLE mhandle_texture;
	D3DXHANDLE mhandle_texture_enabled;
	D3DXHANDLE mhandle_material_diffuse;
	D3DXHANDLE mhandle_material_ambient;
	D3DXHANDLE mhandle_material_specular;
	D3DXHANDLE mhandle_material_emissive;
	D3DXHANDLE mhandle_material_power;
};
