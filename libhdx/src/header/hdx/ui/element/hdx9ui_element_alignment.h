#include "hdx/ui/hdx9ui.h"

#define HDXAL_CENTER 0x00000001
#define HDXAL_LEFT 0x00000002
#define HDXAL_RIGHT 0x00000004
#define HDXAL_VCENTER 0x00000008
#define HDXAL_TOP 0x00000010
#define HDXAL_BOTTOM 0x00000020

class HDXUIElement_Alignment : public HDXUIElementBase {
public:
	HDXUIElement_Alignment(const std::string name,
						   HDXUIElementBase *parent,
						   DWORD flags,
						   float dirx,
						   float diry,
						   float offx,
						   float offy,
						   float padding);
	HDXUIElement_Alignment(HDXUIElementBase *parent);
	virtual ~HDXUIElement_Alignment();

	HDXVector2 getDirection() const;
	void getDirection(HDXVector2 *dir) const;
	void setDirection(const HDXVector2 *dir);
	void setDirection(const float &x,const float &y);

	HDXVector2 getCurrentPostion() const;
	void getCurrentPostion(HDXVector2 *pos) const;
	void setCurrentPostion(const HDXVector2 *pos);
	void setCurrentPostion(const float &x,const float &y);

	HDXVector2 getNextPostion(HDXUIElementBase *newelement);
	void getNextPostion(HDXVector2 *pos,HDXUIElementBase *newelement);

	void jumpDistance(float dst);

	DWORD getFlags() const;
	void setFlags(const DWORD &flags);

	HDXVector2 getOffset() const;
	void getOffset(HDXVector2 *pos) const;
	void setOffset(const HDXVector2 *pos);
	void setOffset(const float &x,const float &y);

	float getPadding() const;
	void setPadding(const float &padding);

	virtual std::string xml_getTypeName() const { return "alignment"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(std::string *out,const std::string &name) const;

protected:
	virtual void _addChild(HDXUIElementBase *child);

private:
	DWORD mflags;
	HDXVector2 mdir,mcurpos,moffset;
	float mpadding;
};
