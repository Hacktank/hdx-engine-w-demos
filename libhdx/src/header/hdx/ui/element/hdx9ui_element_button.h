#include "hdx/ui/hdx9ui.h"

class HDXTexture;
class HDXUIElementEffect_TextureDraw;
class HDXUIElementEffect_TextDraw;

class HDXUIElement_Button : public HDXUIElementBase {
public:
	HDXUIElement_Button(const std::string &name,
						HDXUIElementBase *parent,
						const std::string &maintexture,
						const std::string &hovertexture,
						const std::string &textfont,
						const std::string &text,
						DWORD textflags,
						D3DXCOLOR textcolor);
	HDXUIElement_Button(HDXUIElementBase *parent);
	virtual ~HDXUIElement_Button();

	HDXUIElementEffect_TextureDraw* getMainTextureEffect() const;
	HDXUIElementEffect_TextureDraw* getHoverTextureEffect() const;

	HDXUIElementEffect_TextDraw* getTextEffect() const;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransition);

	virtual void update(float dt);
	virtual void render();

	virtual std::string xml_getTypeName() const { return "button"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);

private:
	HDXUIElementEffect_TextureDraw *meffect_texture_main,*meffect_texture_hover;
	HDXUIElementEffect_TextDraw *meffect_text;
};
