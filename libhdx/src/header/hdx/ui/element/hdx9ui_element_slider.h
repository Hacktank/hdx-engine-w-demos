#include "hdx/ui/hdx9ui.h"

class HDXTexture;
class HDXUIElementEffect_TextureDraw;
class HDXUIElementEffect_TextDraw;

class HDXUIElement_Slider : public HDXUIElementBase {
public:
	class HDXUIElement_Slider_Handle : public HDXUIElementBase {
		friend class HDXUIElement_Slider;
	public:
		HDXUIElement_Slider_Handle(const std::string &name,
								   HDXUIElementBase *parent,
								   const std::string &maintexture,
								   float size,
								   float xoff,
								   float yoff);
		virtual ~HDXUIElement_Slider_Handle();

		virtual void onMenuTransitionEnter(bool dotransition);
		virtual void onMenuTransitionExit(bool dotransition);

		virtual void update(float dt);

	private:
		HDXUIElementEffect_TextureDraw *meffect_texture_main;
		HDXVector2 moffset;
		float msize;
		int mcallbackid_clicked;
		int mcallbackid_mouse_release;
		int mcallbackid_mouse_move;
		bool mdragging;
		bool mprevhighlight;
		D3DXCOLOR mblendreset,mblendprev;
	};

	HDXUIElement_Slider(const std::string &name,
						HDXUIElementBase *parent,
						const std::string &maintexture,
						const std::string &handletexture,
						const std::string &textfont,
						const std::string &text,
						DWORD textflags,
						D3DXCOLOR textcolor);
	HDXUIElement_Slider(HDXUIElementBase *parent);
	virtual ~HDXUIElement_Slider();

	HDXUIElementEffect_TextureDraw* getMainTextureEffect() const;
	HDXUIElementEffect_TextureDraw* getHandleTextureEffect() const;

	HDXUIElementEffect_TextDraw* getTextEffect() const;

	HDXUIElement_Slider_Handle* getHandle() const;

	int registerCallbackOnValueChange(std::function<void(float value)> &&func);
	void unregisterCallbackOnValueChange(int id);

	float getValue() const;
	void setValue(float value);

	virtual void update(float dt);

	virtual std::string xml_getTypeName() const { return "slider"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(std::string *out,const std::string &name) const;

private:
	HDXUIElementEffect_TextureDraw *meffect_texture_main;
	HDXUIElementEffect_TextDraw *meffect_text;
	HDXUIElement_Slider_Handle *mhandle;

	float mprevvalue;

	std::vector<int> mcallbackid_onvaluechange;
};
