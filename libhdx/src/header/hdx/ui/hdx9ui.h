#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <directxmath.h>

#include <cfloat>
#include <string>
#include <vector>
#include <queue>
#include <functional>
#include <unordered_map>
#include <algorithm>

#include "hdx/basemanager.h"
#include "hdx/ui/hdx9ui_iuiactor.h"

#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_utils.hpp>
#include "hdx/ui/hdx9ui_xmlscript.h"

#define HDXUI_GETZABOVE(z) ((std::min)(1.0f,(z)+FLT_EPSILON))
#define HDXUI_GETZABELOW(z) ((std::max)(0.0f,(z)-FLT_EPSILON))

class HDXUIElementEffect_Base;

#define RECTWIDTH(rect) ((rect).right-(rect).left)
#define RECTHEIGHT(rect) ((rect).bottom-(rect).top)
void rectSetPosSize(RECT *rect,int x,int y,int w,int h);
void rectShrink(RECT *rect,int sx,int sy);
void rectGrow(RECT *rect,int gx,int gy);
void rectGetSubRect(RECT *rect,int sw,int sh,int i,int j);

class HDXUIElementBase : public IHDXUIIActor {
public:
	HDXUIElementBase(HDXUIElementBase *parent);
	HDXUIElementBase(const std::string name,HDXUIElementBase *parent);
	virtual ~HDXUIElementBase();

	HDXUIElementBase* getParent() const;
	const HDXUIElementBase* getTopElement() const;
	HDXUIElementBase* getTopElement();

	bool isActive() const;
	void setActive(bool active);

	bool isEnabled() const;
	void setEnabled(bool enabled);

	void getTransform(HDXMatrix4x4 *mat) const;

	HDXVector2 getPosition() const;
	HDXVector2* getPositionPtr();
	void getPosition(HDXVector2 *pos) const;
	void setPosition(const HDXVector2 *pos);
	void setPosition(const HDXVector2 &pos);
	void setPosition(const float &x,const float &y);

	HDXVector2 getScale() const;
	HDXVector2* getScalePtr();
	void getScale(HDXVector2 *scale) const;
	void setScale(const HDXVector2 *scale);
	void setScale(const HDXVector2 &scale);
	void setScale(const float &x,const float &y);

	HDXVector2 getRotationCenter() const;
	void getRotationCenter(HDXVector2 *center) const;
	void setRotationCenter(const HDXVector2 *center);
	void setRotationCenter(const HDXVector2 &center);
	void setRotationCenter(const float &x,const float &y);

	float getRotation() const;
	float* getRotationPtr();
	void setRotation(const float &rot);

	float getActionWidth() const;
	void setActionWidth(const float &actionwidth);

	float getActionHeight() const;
	void setActionHeight(const float &actionheight);

	HDXVector2 getActionSize() const;
	void setActionSize(const HDXVector2 &size);
	void setActionSize(const float &w,const float &h);

	void getEffectiveActionRect(RECT *rect) const;

	float getZPos() const;
	void setZPos(const float &zpos); //value must be between 0 and 1 inclusive, default is HDXUI_GETZABOVE(parnet.z), or 0.5 if parent is null
	float getMaxZPos() const; //gets the maximum zpos of this element and all of its children's and effect's zpos's
	float getMinZPos() const; //gets the minimum zpos of this element and all of its children's and effect's zpos's

	D3DXCOLOR getBlendColor() const;
	void setBlendColor(D3DXCOLOR color);

	bool isMouseOver() const;

	int registerCallbackOnMousePress(std::function<void(WPARAM key)> &&func);
	void unregisterCallbackOnMousePress(int id);

	int registerCallbackOnMouseRelease(std::function<void(WPARAM key,float timeheld)> &&func);
	void unregisterCallbackOnMouseRelease(int id);

	int registerCallbackOnMouseEnter(std::function<void(void)> &&func);
	void unregisterCallbackOnMouseEnter(int id);

	int registerCallbackOnMouseLeave(std::function<void(void)> &&func);
	void unregisterCallbackOnMouseLeave(int id);

	int registerCallbackOnKeyPress(std::function<void(WPARAM key)> &&func);
	void unregisterCallbackOnKeyPress(int id);

	int registerCallbackOnTransitionEnter(std::function<void(bool dotransition)> &&func);
	void unregisterCallbackOnTransitionEnter(int id);

	int registerCallbackOnTransitionExit(std::function<void(bool dotransition)> &&func);
	void unregisterCallbackOnTransitionExit(int id);

	int registerCallbackOnDestroy(std::function<void(void)> &&func);
	void unregisterCallbackOnDestroy(int id);

	const BaseManagerKey<HDXUIElementBase*,std::string>& getChilderen() const;
	const BaseManagerKey<HDXUIElementEffect_Base*,std::string>& getEffects() const;

	template<typename T,typename ...Args>
	T* createEffect(const std::string &name,Args ...args);
	HDXUIElementEffect_Base* getEffect(const std::string &name) const;
	void destroyEffect(const std::string &name);

	HDXUIElementEffect_Base* getSubEffect(const std::string &name) const;

	template<typename T,typename ...Args>
	T* createChildElement(const std::string &name,Args ...args);
	HDXUIElementBase* createChildElementFromXML(const std::string &name,const std::string &file);
	HDXUIElementBase* getChildElement(const std::string &name) const;
	void destroyChildElement(const std::string &name);

	//recursively searches for a element below this one in the hierarchy with the specified name
	HDXUIElementBase* getSubElement(const std::string &name) const;

	virtual float getDesiredTransitionTime() const;
	float getMaxDesiredTransitionTime() const;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransition);

	virtual void update(float dt);
	virtual void render();

	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(std::string *out,const std::string &name) const;

protected:
	virtual void _addChild(HDXUIElementBase *child);
	virtual void _addEffect(HDXUIElementEffect_Base *effect);

private:
	HDXUIElementBase *mparent;
	BaseManagerKey<HDXUIElementBase*,std::string> mchilderen;
	BaseManagerKey<HDXUIElementEffect_Base*,std::string> meffects;
	std::vector<HDXUIElementBase*> mreactivate_childeren;
	std::vector<HDXUIElementEffect_Base*> mreactivate_effects;
	std::vector<HDXUIElementBase*> mreenable_childeren;
	std::vector<HDXUIElementEffect_Base*> mreenable_effects;

	bool mactive,menabled;

	std::vector<int> mcallbackid_onmousepress;
	std::vector<int> mcallbackid_onmouserelease;
	std::vector<int> mcallbackid_onkeypress;

	BaseManagerID<std::function<void(void)>> mcallback_ondestroy;

	BaseManagerID<std::function<void(void)>> mcallback_onmouseenter;
	BaseManagerID<std::function<void(void)>> mcallback_onmouseleave;

	BaseManagerID<std::function<void(bool dotransition)>> mcallback_ontransitionenter;
	BaseManagerID<std::function<void(bool dotransition)>> mcallback_ontransitionexit;

	float mactionwidth,mactionheight;
	HDXVector2 mpos,mscale,mrotcenter;
	float mrot,mzpos;
	bool mmousepressed,mwasmouseover;
	D3DXCOLOR mblendcolor;
};

template<typename T,typename ...Args>
T* HDXUIElementBase::createChildElement(const std::string &name,Args ...args) {
	T *child = new T(name,this,args...);
	_addChild(child);
	return child;
}

template<typename T,typename ...Args>
T* HDXUIElementBase::createEffect(const std::string &name,Args ...args) {
	T *effect = new T(name,this,args...);
	_addEffect(effect);
	return effect;
}

class HDXUIMenu : public HDXUIElementBase {
	friend class HDXUIMenuMap;
public:
	HDXUIMenu(const std::string name,HDXUIElementBase *parent = 0);
	HDXUIMenu();
	virtual ~HDXUIMenu();

	HDXUIMenuMap* getMenuMap() const;

	virtual std::string xml_getTypeName() const { return "menu"; }

private:
	HDXUIMenuMap *mmenumap;
};

class HDXUIMenuMap {
public:
	HDXUIMenuMap();
	~HDXUIMenuMap();

	void createFromXML(const std::string &fname,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);

	HDXUIMenu* createMenu(const std::string &name);
	HDXUIMenu* getMenu(const std::string &name);
	void destroyMenu(const std::string &name);

	void clear();

	HDXUIMenu* getCurrentMenu();
	void navigateToMenu(const std::string &menu,bool dotransnition = false);
	void navigateBackwards(bool dotransnition = false);

	void doIterate(std::function<bool(HDXUIMenu *item)> &&func);

	void update(float dt);
	void render();

private:
	struct _transition {
		std::string mtarget;
		float mtimer;
		bool menter;
		bool mstarted;
		int mextraframes;

		_transition(const std::string &target,float timer,bool enter) {
			mtarget = target;
			mtimer = timer;
			menter = enter;
			mstarted = false;
			mextraframes = 10; //extra time is to ensure the transition is complete
		}
	};

	BaseManagerKey<HDXUIMenu*,std::string> mmenus;
	std::vector<HDXUIMenu*> mtraversal; //allows for a "back" button, the last element is the current active menu
	std::queue<_transition> mtransitions;

	void _setMenu(const std::string &menu);
};
