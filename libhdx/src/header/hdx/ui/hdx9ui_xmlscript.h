#pragma once

#include "hdx/xmlutils.h"

#include <windows.h>

#include "hdx/hdx9math.h"

class IHDXUIIActor;

class HDXUIXMLScript : public XMLScript {
public:

	class Source_UIActor : public Source {
	public:
		Source_UIActor(const std::string &name,XMLScript *parentscript);

		virtual std::string dereference(Source *prevsource,const std::string &entryname,const std::string &str);

		IHDXUIIActor *mcuractor;
		bool miselement;
	};

	HDXUIXMLScript();
	HDXUIXMLScript(const HDXUIXMLScript&) = delete;
	HDXUIXMLScript(HDXUIXMLScript&&) = delete;
	~HDXUIXMLScript();

	void setScriptData_this(IHDXUIIActor *data,bool iselement);
	void setScriptData_this(std::pair<IHDXUIIActor*,bool> data);
	std::pair<IHDXUIIActor*,bool> getScriptData_this() const;

private:
	IHDXUIIActor *mthis;
	bool mthis_iselement;
};

bool XMLRead(D3DXCOLOR *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(HDXVector2 *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(HDXVector3 *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(RECT *out,XMLScript *script,rapidxml::xml_node<> *node);

template<> bool XMLToString<D3DXCOLOR>(std::string *out,const D3DXCOLOR &in,std::string arg);
template<> bool XMLToString<HDXVector2>(std::string *out,const HDXVector2 &in,std::string arg);
template<> bool XMLToString<HDXVector3>(std::string *out,const HDXVector3 &in,std::string arg);
template<> bool XMLToString<RECT>(std::string *out,const RECT &in,std::string arg);
