#pragma once

#include "hdx/ui/effect/hdx9ui_elementeffect_base.h"

class HDXTexture;

class HDXUIElementEffect_TextureDraw : public HDXUIElementEffect_Base {
public:
	HDXUIElementEffect_TextureDraw(const std::string &name,
								   HDXUIElementBase *parent,
								   const std::string &tex);
	HDXUIElementEffect_TextureDraw(HDXUIElementBase *parent);
	virtual ~HDXUIElementEffect_TextureDraw();

	std::string getTexture() const;
	void setTexture(const std::string &tex);

	D3DXCOLOR getBlendColor() const;
	void setBlendColor(D3DXCOLOR blendcolor);

	void getSourceRect(RECT *rect) const;
	void setSourceRect(const RECT *rect);
	int getSourceWidth() const;
	int getSourceHeight() const;
	void setSourceRectToWholeTexture();

	HDXVector3 getSourceCenter() const;
	void getSourceCenter(HDXVector3 *center) const;
	void setSourceCenter(const HDXVector3 *center);
	void setSourceCenterToCenterSourceRect();

	virtual float getZPos() const;

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransiton);

	virtual void update(float dt);
	virtual void render();

	virtual std::string xml_getTypeName() const { return "draw_texture"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(std::string *out,const std::string &name) const;

private:
	HDXTexture *mtexture;
	RECT msource_rect;
	HDXVector3 msource_center;
	D3DXCOLOR mblendcolor;
};
