#pragma once

#include "hdx/ui/effect/hdx9ui_elementeffect_base.h"

class HDXUIElementEffect_TextDraw : public HDXUIElementEffect_Base {
public:
	HDXUIElementEffect_TextDraw(const std::string &name,
								HDXUIElementBase *parent,
								const std::string &font,
								const std::string &text,
								DWORD fontdrawflags,
								D3DXCOLOR color);
	HDXUIElementEffect_TextDraw(HDXUIElementBase *parent);
	virtual ~HDXUIElementEffect_TextDraw();

	std::string getFont() const;
	void setFont(const std::string &font);

	std::string getText() const;
	void setText(const std::string &text);

	DWORD getFontDrawFlags() const;
	void setFontDrawFlags(const DWORD &flags);

	D3DXCOLOR getFontDrawColor() const;
	void setFontDrawColor(const D3DXCOLOR &color);

	void getFontDrawRect(RECT *rect) const;
	void setFontDrawRect(const RECT *rect);

	virtual void onMenuTransitionEnter(bool dotransition);
	virtual void onMenuTransitionExit(bool dotransiton);

	virtual void update(float dt);
	virtual void render();

	virtual std::string xml_getTypeName() const { return "draw_text"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr);
	virtual bool xml_getVarByName(std::string *out,const std::string &name) const;

private:
	std::string mfont;
	std::string mtext;
	DWORD mfontdrawflags;
	RECT mfontdrawrect;
	D3DXCOLOR mfontdrawcolor;
};
