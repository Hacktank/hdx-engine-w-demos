#pragma once

#include "hdx/ui/hdx9ui_xmlscript.h"

class IHDXUIIActor {
public:
	IHDXUIIActor();
	virtual ~IHDXUIIActor() {};

	std::string getName() const { return mname; }

	virtual void onMenuTransitionEnter(bool dotransition) = 0;
	virtual void onMenuTransitionExit(bool dotransition) = 0;

	virtual void* getUserData() const { return muserdata; }
	virtual void setUserData(void *data) { muserdata = data; }

	virtual void update(float dt) = 0;
	virtual void render() = 0;

	virtual std::string xml_getTypeName() const { return "void"; }
	virtual void xml_initialize(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr) {}
	virtual void xml_node_read(HDXUIXMLScript *xmlscript,rapidxml::xml_node<> *xmlnode,std::function<void(IHDXUIIActor *actor,rapidxml::xml_node<> *node)> func_oninitialize = nullptr) {}
	virtual bool xml_getVarByName(std::string *out,const std::string &name) const { return false; }

protected:
	std::string mname;

private:
	void *muserdata;
};
