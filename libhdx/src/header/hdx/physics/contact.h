#pragma once

#include "hdx/util.h"
#include "hdx/hdx9math.h"

class RigidBody;

class PhysicsContact {
	friend class PhysicsManager;
	friend class RigidBody;

public:
	bool mflag_remove;

	float mtime_seperated;

	HDXVector3 mcolpoint;
	HDXVector3 mnormal;
	float mpenetration;

	RigidBody *mbody[2];
	HDXVector3 mlife_body_change_linear[2];

	unsigned mtimesresolved;

	float mfriction;
	float mrestitution;

	PhysicsContact(const HDXVector3 &contactpoint,const HDXVector3 &normal,float penetration,RigidBody *b0,RigidBody *b1,float friction,float restitution);
	virtual ~PhysicsContact();

	virtual void swapData(); //inverts the contact data

protected:
	HDXMatrix3x3 mcttw; // transforms from contact space to world space
	HDXMatrix3x3 mcttw_trans;
	HDXVector3 mcontactvelocity;
	float mdesireddeltavelocity;
	HDXVector3 mrelativecontactposition[2];

	void updateInternals(float d); //updates internal data

	void matchAwakeState(float dt);
	void calculateContactBasis(); //calculates an orthonormal bassi for the contact point
	void calculateDesiredDeltaVelocity(float d); //calculates the desired delta velocity
	HDXVector3 calculateLocalVelocity(int bodyind,float d); //calculates and returns the velocity of the contact point on the given body
	void applyImpulse(const HDXVector3 &impulse,RigidBody *body,HDXVector3 *velocitychange,HDXVector3 *angularvelocitychange); //applies an impulse to the given body returning the change in velocities
	void applyVelocityChange(HDXVector3 velocitychange[2],HDXVector3 angularvelocitychange[2]); //performs an inertia weigted velocity resolution of this contact
	void applyPositionChange(HDXVector3 linearchange[2],HDXVector3 angularchange[2],float penetration); //performs an inertia weigted penetration resolution of this contact
	HDXVector3 calculateFrictionlessImpulse(const HDXMatrix3x3 inverseinertiatensor[2]); //calculates the impulse needed to resolve this contact without friction
	HDXVector3 calculateFrictionImpulse(const HDXMatrix3x3 inverseinertiatensor[2]); //calculate the impulse needed to resolve this contact with friction
};
