#pragma once

#include "hdx/util.h"
#include "hdx/hdx9math.h"


struct BaseBV;
class HDXConvexHull;
class component_Physics;

class RigidBody {
	friend class PhysicsContact;
	friend class PhysicsManager;

public:
	void *mdata;

	RigidBody(component_Physics *comp);
	~RigidBody();

	void initializeSphere(float radius,float mass);
	void initializeBox(const HDXVector3 &halfsize,float mass);
	void initializeHalfspace(const HDXVector3 &pos,const HDXVector3 &normal);
	void initializePlane(const HDXVector3 &pos,const HDXVector3 &normal);
	void initializeRod(float halflength,float mass);
	void initializeConvexHull(HDXConvexHull hull,float mass);
	void deinitialize();

	void setMass(const float mass);
	float getMass() const;
	void setInverseMass(const float inversemass);
	float getInverseMass() const { return minversemass; }
	bool hasFiniteMass() const;

	void setLinearDamping(const float damping) { mdamping_linear = damping; }
	float getLinearDamping() const { return mdamping_linear; }

	void setAngularDamping(const float damping) { mdamping_angular = damping; }
	float getAngularDamping() const { return mdamping_angular; }

	void setRestitution(const float restitution) { mrestitution = restitution; }
	float getRestitution() const { return mrestitution; }

	void setFriction(const float friction) { mfriction = friction; }
	float getFriction() const { return mfriction; }

	void setAwake(bool awake);
	bool isAwake() const { return mawake; }
	void setCanSleep(bool cansleep);
	bool getCanSleep() const { return mcansleep; }

	void setVelocity(const HDXVector3 &velocity) { mvelocity = velocity; }
	void setVelocity(const float x,const float y,const float z) { mvelocity.x = x; mvelocity.y = y; mvelocity.z = z; }
	void addVelocity(const HDXVector3 &velocity) { mvelocity += velocity; }
	void getVelocity(HDXVector3 *velocity) const { *velocity = mvelocity; }
	const HDXVector3& getVelocity() const { return mvelocity; }

	void setAngularVelocity(const HDXVector3 &velocity) { mangularvelocity = velocity; }
	void setAngularVelocity(const float x,const float y,const float z) { mangularvelocity.x = x; mangularvelocity.y = y; mangularvelocity.z = z; }
	void addAngularVelocity(const HDXVector3 &velocity) { mangularvelocity += velocity; }
	void getAngularVelocity(HDXVector3 *velocity) const { *velocity = mangularvelocity; }
	const HDXVector3& getAngularVelocity() const { return mangularvelocity; }

	void setAcceleration(const HDXVector3 &acceleration) { macceleration = acceleration; }
	void setAcceleration(const float x,const float y,const float z) { macceleration.x = x; macceleration.y = y; macceleration.z = z; }
	void getAcceleration(HDXVector3 *acceleration) const { *acceleration = macceleration; }
	const HDXVector3& getAcceleration() const { return macceleration; }

	void setAngularAcceleration(const HDXVector3 &angularacceleration) { mangularacceleration = angularacceleration; }
	void setAngularAcceleration(const float x,const float y,const float z) { mangularacceleration.x = x; mangularacceleration.y = y; mangularacceleration.z = z; }
	void getAngularAcceleration(HDXVector3 *angularacceleration) const { *angularacceleration = mangularacceleration; }
	const HDXVector3& getAngularAcceleration() const { return mangularacceleration; }

	void setInverseInertiaTensor(const HDXMatrix3x3 &inverseinertiatensor);
	void setInertiaTensor(const HDXMatrix3x3 &inertiatensor);
	const HDXMatrix3x3& getInverseInertiaTensor() const;
	const HDXMatrix3x3 getInertiaTensor() const;
	const HDXMatrix3x3& getInverseInertiaTensorWorld() const;
	const HDXMatrix3x3 getInertiaTensorWorld() const;

	const std::vector<PhysicsContact*>& getContacts() const;

	const HDXVector3 getPointInLocalSpace(const HDXVector3 &point) const;
	const HDXVector3 getPointInWorldSpace(const HDXVector3 &point) const;

	const HDXVector3& getLastFrameAcceleration() const { return mlastframeacceleration; }

	component_Physics* getComp() { return mcomp; }
	BaseBV* getBV() { return mbv; }

	virtual float getRadius() const { return 0; }

	void update(float dt);

	void clearAccumulator();
	void addForce(const HDXVector3 &force);
	void addForceAtPoint(const HDXVector3 &force,const HDXVector3 &point); //point is in WORLD space
	void addForceAtBodyPoint(const HDXVector3 &force,const HDXVector3 &point); //point is in BODY space
	void addTorque(const HDXVector3 &torque);

private:
	float minversemass;
	component_Physics *mcomp;
	BaseBV *mbv;
	HDXVector3 mvelocity;
	HDXVector3 macceleration;
	HDXVector3 mangularvelocity;
	HDXVector3 mangularacceleration;
	HDXVector3 mforceaccum;
	HDXVector3 mtorqueaccum;
	HDXVector3 mlastframeacceleration;
	HDXMatrix3x3 minverseinertiatensor; //in body's local space
	HDXMatrix3x3 minverseinertiatensorworld; //in world space
	float mfriction;
	float mrestitution;
	float mdamping_linear;
	float mdamping_angular;
	float mmotion;
	bool mawake;
	bool mcansleep;
	bool mcancachecontacts;
	float msleeptimer;

	HDXVector3 mlastframe_pos;
	HDXQuaternion mlastframe_ori;

	std::vector<PhysicsContact*> mcontactinvolvement;

	void _syncInternals();
	void _syncInternals_onTransformChange(const HDXVector3 &delta_pos,const HDXVector3 &delta_ori);
	void _syncInternals_onKineticChange(float dt,const HDXVector3 &delta_vel,const HDXVector3 &delta_avel);
};
