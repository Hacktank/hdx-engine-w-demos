#pragma once

#include "hdx/util.h"
#include "hdx/hdx9math.h"

HDXVector3 closestPoint_sphere_point(const HDXVector3 &p0,float r0,const HDXVector3 &p);
HDXVector3 closestPoint_aabb_point(const HDXVector3 &p0,const HDXVector3 &hs0,const HDXVector3 &p);
HDXVector3 closestPoint_obb_point(const HDXVector3 &p0,const HDXVector3 &hs0,const HDXQuaternion &o,const HDXVector3 &p);
HDXVector3 closestPoint_obb_point(const HDXMatrix4x4 &trans,const HDXVector3 &hs0,const HDXVector3 &p);
HDXVector3 closestPoint_plane_point(const HDXVector3 &n0,float d0,const HDXVector3 &p);
HDXVector3 closestPoint_segment_point(const HDXVector3 &p00,const HDXVector3 &p10,const HDXVector3 &p);
HDXVector3 closestPoint_segment_segment(const HDXVector3 &p0,const HDXVector3 &d0,float l0,const HDXVector3 &p1,const HDXVector3 &d1,float l1);
