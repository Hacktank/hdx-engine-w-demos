#pragma once

#include <functional>

#include "hdx/util.h"
#include "hdx/hdx9math.h"

struct BaseBV;
struct BVOBB;

typedef std::function<void(BaseBV *bv0,BaseBV *bv1,const HDXVector3 &contactpoint,const HDXVector3 &normal,float penetration)> ContactCallback;

bool col_convex_plane(BaseBV *bv0,BaseBV *bv1,ContactCallback contactcallback = nullptr);
bool col_convex_halfspace(BaseBV *bv0,BaseBV *bv1,ContactCallback contactcallback = nullptr);
bool col_convex_convex_gjkepa(BaseBV *bv0,BaseBV *bv1,ContactCallback contactcallback = nullptr);

// calculates and iterates over all vertexes of an obb and calls the passed in lambda for each one
void col_helper_obb_vertexiteratedo(BVOBB *v,std::function<void(const HDXVector3 &vertex)> func);

// determines the type of both volumes and runs the corresponding collision function
bool col_bv_bv(BaseBV *bv0,BaseBV *bv1,ContactCallback contactcallback = nullptr);
