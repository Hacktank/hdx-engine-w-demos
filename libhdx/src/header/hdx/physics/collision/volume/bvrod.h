#pragma once

#include "hdx/physics/collision/volume/basebv.h"

struct BVRod : public BaseBV {
	float halflength;

	BVRod() : BaseBV(BV_ROD) {};
	BVRod(const BVRod &r);

	virtual float getRadius() const;

	virtual const HDXVector3 getFurthestPointInDirection(const HDXVector3 &d);
};
