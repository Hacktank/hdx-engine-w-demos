#pragma once

#include "hdx/physics/collision/volume/basebv.h"

struct BVOBB;

struct BVAABB : public BaseBV {
	HDXVector3 halfsize;

	BVAABB() : BaseBV(BV_AABB) {};
	BVAABB(const BVOBB &r);

	virtual float getRadius() const;
	virtual const HDXMatrix4x4& getTransform() const; //overload returns the transform with rotation removed

	virtual const HDXVector3 getFurthestPointInDirection(const HDXVector3 &d);

private:
	HDXMatrix4x4 mtrans;
};
