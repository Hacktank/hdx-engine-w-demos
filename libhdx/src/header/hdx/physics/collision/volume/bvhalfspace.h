#pragma once

#include "hdx/physics/collision/volume/bvplane.h"

struct BVHalfspace : public BVPlane {
	BVHalfspace() : BVPlane() { type = BV_HALFSPACE; }
	BVHalfspace(const BVPlane &r) : BVPlane(r) { type = BV_HALFSPACE; }
};
