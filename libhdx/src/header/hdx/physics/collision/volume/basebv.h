#pragma once

#include "hdx/util.h"
#include "hdx/hdx9math.h"

class compCollision;
struct compCollisionData;
class RigidBody;

struct BaseBV {
	enum BVType {
		BV_SPHERE = 0,
		BV_AABB,
		BV_OBB,
		BV_PLANE,
		BV_HALFSPACE,
		BV_ROD,
		BV_CONVEXHULL,

		BV_NUM
	};

	BVType type;
	RigidBody *body;

	BaseBV(BVType ptype) : type(ptype) { body = 0; };
	virtual ~BaseBV() {}

	virtual float getRadius() const = 0;
	virtual const HDXVector3 getAxis(int ind) const;
	virtual const HDXMatrix4x4& getTransform() const;
	virtual const HDXVector3 getCenter() const;

	virtual const HDXVector3 getFurthestPointInDirection(const HDXVector3 &d) = 0;
};
