#pragma once

#include "hdx/physics/collision/volume/basebv.h"

#include "hdx/hdx9geometry.h"

struct BVConvexHull : public BaseBV {
	BVConvexHull() : BaseBV(BV_CONVEXHULL) {};

	void setHull(HDXConvexHull hull);
	const HDXConvexHull& getHull() const;

	virtual float getRadius() const;

	virtual const HDXVector3 getFurthestPointInDirection(const HDXVector3 &d);

private:
	HDXConvexHull mhull;
};
