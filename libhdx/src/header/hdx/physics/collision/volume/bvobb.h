#pragma once

#include "hdx/physics/collision/volume/basebv.h"

struct BVAABB;

struct BVOBB : public BaseBV {
	HDXVector3 halfsize;

	BVOBB() : BaseBV(BV_OBB) {};
	BVOBB(const BVAABB &r);

	virtual float getRadius() const;

	virtual const HDXVector3 getFurthestPointInDirection(const HDXVector3 &d);
};
