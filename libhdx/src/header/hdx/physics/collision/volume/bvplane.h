#pragma once

#include "hdx/physics/collision/volume/basebv.h"

struct BVPlane : public BaseBV {
	BVPlane() : BaseBV(BV_PLANE) {};

	virtual float getRadius() const;

	virtual const HDXVector3 getFurthestPointInDirection(const HDXVector3 &d);
};
