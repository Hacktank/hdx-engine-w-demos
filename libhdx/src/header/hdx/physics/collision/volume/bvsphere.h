#pragma once

#include "hdx/physics/collision/volume/basebv.h"

struct BVSphere : public BaseBV {
	float radius;

	BVSphere() : BaseBV(BV_SPHERE) {};

	virtual float getRadius() const;

	virtual const HDXVector3 getFurthestPointInDirection(const HDXVector3 &d);
};

