#ifndef HEADFORCE
#define HEADFORCE

#include "hdx/util.h"
#include "hdx/hdx9math.h"

#include <vector>

#define HDX_FORCEMAN ForceRegistry::instance()

class RigidBody;
class ForceRegistry;

class BaseForce {
	friend class ForceRegistry;
protected:
	ForceRegistry *mregistered;
	bool mawakensbody;

	virtual const bool p_shouldDelete() const { return false; }

public:
	BaseForce();
	virtual ~BaseForce();

	virtual void setAwakensBody(bool awakensbody) { mawakensbody = awakensbody; }
	virtual void updateForce(RigidBody *body,float dt) = 0;
};

class ForceRegistry {
protected:
	struct ForceRegistration {
		RigidBody *body;
		BaseForce *force;
	};
	typedef std::vector<ForceRegistration> REGISTRYCONT;
	REGISTRYCONT mregistrations;
	int mcbid_onupdate_pre;

	ForceRegistry();

public:
	static ForceRegistry* instance() {
		static ForceRegistry *ptr = new ForceRegistry();
		return ptr;
	}

	~ForceRegistry();

	void add(RigidBody *body,BaseForce *force);
	REGISTRYCONT::iterator remove(REGISTRYCONT::iterator it);
	void remove(RigidBody *body,BaseForce *force);
	void remove(RigidBody *body); // remove all forces on this body
	void remove(BaseForce *force); // remove this force off all bodies
	void clear();

	void updateForces(float dt);
};

/*
standard forces
*/

class ForceSpringPoint : public BaseForce {
protected:
	HDXVector3 mpoint;
	HDXVector3 mselfoffset;
	float mspringconstant;
	float mrestlength;

public:
	void setPoint(const HDXVector3 &point) { mpoint = point; }
	const HDXVector3& getPoint() const { return mpoint; }

	void setSelfOffset(const HDXVector3 &selfoffset) { mselfoffset = selfoffset; }
	const HDXVector3& getSelfOffset() const { return mselfoffset; }

	void setSpringConstant(float springconstant) { mspringconstant = springconstant; }
	const float getSpringConstant() const { return mspringconstant; }

	void setRestLength(float restlength) { mrestlength = restlength; }
	const float getRestLength() const { return mrestlength; }

	ForceSpringPoint(const HDXVector3 &point,float springconstant,float restlength);
	virtual void updateForce(RigidBody *body,float dt);
};

class ForceSpringObject : public BaseForce {
protected:
	RigidBody *mother;
	HDXVector3 motheroffset;
	HDXVector3 mselfoffset;
	float mspringconstant;
	float mrestlength;

public:
	void setOther(RigidBody *other) { mother = other; }
	const RigidBody* getOther() const { return mother; }

	void setOtherOffset(const HDXVector3 &otheroffset) { motheroffset = otheroffset; }
	const HDXVector3& getOtherOffset() const { return motheroffset; }
	void setSelfOffset(const HDXVector3 &selfoffset) { mselfoffset = selfoffset; }
	const HDXVector3& getSelfOffset() const { return mselfoffset; }

	void setSpringConstant(float springconstant) { mspringconstant = springconstant; }
	const float getSpringConstant() const { return mspringconstant; }

	void setRestLength(float restlength) { mrestlength = restlength; }
	const float getRestLength() const { return mrestlength; }

	ForceSpringObject(RigidBody *other,float springconstant,float restlength);
	virtual void updateForce(RigidBody *body,float dt);
};

class ForceBungeePoint : public BaseForce {
protected:
	HDXVector3 mpoint;
	HDXVector3 mselfoffset;
	float mspringconstant;
	float mrestlength;

public:
	void setPoint(const HDXVector3 &point) { mpoint = point; }
	const HDXVector3& getPoint() const { return mpoint; }

	void setSelfOffset(const HDXVector3 &selfoffset) { mselfoffset = selfoffset; }
	const HDXVector3& getSelfOffset() const { return mselfoffset; }

	void setSpringConstant(float springconstant) { mspringconstant = springconstant; }
	const float getSpringConstant() const { return mspringconstant; }

	void setRestLength(float restlength) { mrestlength = restlength; }
	const float getRestLength() const { return mrestlength; }

	ForceBungeePoint(HDXVector3 point,float springconstant,float restlength);
	virtual void updateForce(RigidBody *body,float dt);
};

class ForceBungeeObject : public BaseForce {
protected:
	RigidBody *mother;
	HDXVector3 motheroffset;
	HDXVector3 mselfoffset;
	float mspringconstant;
	float mrestlength;
	bool mapplytoboth;

public:
	void setOther(RigidBody *other) { mother = other; }
	const RigidBody* getOther() const { return mother; }

	void setOtherOffset(const HDXVector3 &otheroffset) { motheroffset = otheroffset; }
	const HDXVector3& getOtherOffset() const { return motheroffset; }
	void setSelfOffset(const HDXVector3 &selfoffset) { mselfoffset = selfoffset; }
	const HDXVector3& getSelfOffset() const { return mselfoffset; }

	void setSpringConstant(float springconstant) { mspringconstant = springconstant; }
	const float getSpringConstant() const { return mspringconstant; }

	void setRestLength(float restlength) { mrestlength = restlength; }
	const float getRestLength() const { return mrestlength; }

	void setApplyToBoth(bool apply) { mapplytoboth = apply; }
	bool getApplyToBoth() const { return mapplytoboth; }

	ForceBungeeObject(RigidBody *other,float springconstant,float restlength);
	virtual void updateForce(RigidBody *body,float dt);
};

class ForceGravityAxis : public BaseForce {
protected:
	HDXVector3 mgravity;

public:
	void setGravity(const HDXVector3 &gravity) { mgravity = gravity; }
	const HDXVector3& getGravity() const { return mgravity; }

	ForceGravityAxis(const HDXVector3 &gravity);
	virtual void updateForce(RigidBody *body,float dt);
};

class ForceDrag : public BaseForce {
protected:
	float mdragcoef;
	float marea;
	float mairdensity;

public:
	void setDragCoef(float dragcoef) { mdragcoef = dragcoef; }
	const float getDragCoef() const { return mdragcoef; }

	void setArea(float area) { marea = area; }
	const float getArea() const { return marea; }

	void setAirDensity(float airdensity) { mairdensity = airdensity; }
	const float getAirDensity() const { return mairdensity; }

	ForceDrag(float dragcoef,float area,float airdensity);
	virtual void updateForce(RigidBody *body,float dt);
};

class ForceAirDrag : public BaseForce { // sphere only
protected:
	float mk1;
	float mk2;

public:
	void setk1(float k1) { mk1 = k1; }
	const float getk1() const { return mk1; }

	void setk2(float k2) { mk2 = k2; }
	const float getk2() const { return mk2; }

	ForceAirDrag(float k1,float k2);
	virtual void updateForce(RigidBody *body,float dt);
};

class ForceBuoyancy : public BaseForce {
protected:
	float mfluiddensity;
	float mdisplacedvolume;
	HDXVector3 mgravity;

public:
	void setFluidDensity(float fluiddensity) { mfluiddensity = fluiddensity; }
	const float getFluidDensity() const { return mfluiddensity; }

	void setDisplacedVolume(float displacedvolume) { mdisplacedvolume = displacedvolume; }
	const float getDisplacedVolume() const { return mdisplacedvolume; }

	void setGravity(const HDXVector3 &gravity) { mgravity = gravity; }
	const HDXVector3& getGravity() const { return mgravity; }

	ForceBuoyancy(float fluiddensity,float displacedvolume,const HDXVector3 &gravity);
	virtual void updateForce(RigidBody *body,float dt);
};

class ForceBlast : public BaseForce {
protected:
	HDXVector3 mpoint;
	float mstrength;
	float mmaxdst;

	virtual const bool p_shouldDelete() const;

public:
	void setPoint(const HDXVector3 &point) { mpoint = point; }
	const HDXVector3& getPoint() const { return mpoint; }

	void setStrength(float strength) { mstrength = strength; }
	const float getStrength() const { return mstrength; }

	ForceBlast(const HDXVector3 &point,float maxdst,float strength);
	virtual void updateForce(RigidBody *body,float dt);
};

#endif
