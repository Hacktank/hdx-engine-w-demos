#pragma once

#include "hdx/util.h"
#include "hdx/hdx9math.h"
#include "hdx/component.h"
#include "hdx/component_transform.h"
#include "hdx/physics/rigidbody.h"
#include "hdx/physics/collision/volume/basebv.h"

#include "hdx/physics/contact.h"

#include <unordered_set>
#include <vector>

#define HDX_PHYSICS_CONTACT_COLLISION_VELOCITY_EPSILON 0.025f // contacts with less desired delta velocty than this wont be resolved
#define HDX_PHYSICS_CONTACT_COLLISION_POSITION_EPSILON 0.01f // contacts will less penetration than this wont be resolved
#define HDX_PHYSICS_CONTACT_COLLISION_VELOCITY_RESTLIMIT 0.2f // if the desired delta velocity is less than this all restitution will be ignored
#define HDX_PHYSICS_CONTACT_COLLISION_ANGULARCHANGELIMIT 0.6f // maximum amount of angular change a contact can cause in one frame
#define HDX_PHYSICS_CONTACT_COLLISION_EQUALITY_EPSILON 0.2f // minimum distance one contact must be from another to be concidered unique
#define HDX_PHYSICS_CONTACT_COLLISION_LIFETIME_SEPERATION 0.1f // time (in seconds) that the contact will remain in the contact list, must remain for a little time so that objects that were being supported by this one can be woken up
#define HDX_PHYSICS_CONTACT_COLLISION_LIFETIME_MAXDST 2.0f // maximum body seperation a contact can have before it is removed from the contact list
#define HDX_PHYSICS_CONTACT_COLLISION_WAKEUP_VELOCITY_THRESHOLD 0.1f // the minimum velocity change a contact must 

#define HDX_PHYSICS_COLLISION_SKIN 1.0f // the size of collision volume is artificially increased by this size, generates contacts, but does not affect penetration

#define HDX_PHYSICS_RIGIDBODY_SLEEP_EPSILON (float)0.2
#define HDX_PHYSICS_RIGIDBODY_SLEEP_TIMER 1.0f //time (in seconds) the body must be within the motion tolerance before it can sleep

#define HDX_PHYMAN PhysicsManager::instance()

class HDXConvexHull;
class component_SpacialPartition;

class component_Physics : public component_Base {
public:
	component_Physics(Entity *entity);
	virtual ~component_Physics();

	void initializeSphere(float rad,float mass);
	void initializeBox(const HDXVector3 &ext,float mass);
	void initializeHalfspace(const HDXVector3 &pos,const HDXVector3 &normal);
	void initializePlane(const HDXVector3 &pos,const HDXVector3 &normal);
	void initializeRod(float halflength,float mass);
	void initializeConvexHull(HDXConvexHull hull,float mass);

	void deinitialize();

	RigidBody* getBody() const { return mbody; }
	component_Transform* getTrans() const { return mtrans; }

	virtual const char* getName() const { return _getName(); }
	static const char* _getName() { return "physics"; }

private:
	int mcbid_onupdate;
	component_Transform *mtrans;
	component_SpacialPartition *mspart;
	RigidBody *mbody;
	int mspreg;
};

struct CollisionTest {
	component_Physics *candidates[2];

	CollisionTest() {}
	CollisionTest(component_Physics *ca,component_Physics *cb) { candidates[0] = ca; candidates[1] = cb; }

	bool operator==(const CollisionTest &other) const {
		return (candidates[0]==other.candidates[0] &&
				candidates[1]==other.candidates[1]) ||
				(candidates[0]==other.candidates[1] &&
				candidates[1]==other.candidates[0]);
	}
};

template<>
struct hthash<CollisionTest> {
public:
	std::size_t operator()(const CollisionTest &a) {
		std::size_t seed = 0;
		if(a.candidates[0] < a.candidates[1]) { // ensure the same hash is generated regardless of candidate order
			hash_combine(seed,a.candidates[0]);
			hash_combine(seed,a.candidates[1]);
		} else {
			hash_combine(seed,a.candidates[1]);
			hash_combine(seed,a.candidates[0]);
		}
		return seed;
	}
};

class PhysicsManager {
	friend class component_Physics;
	friend class RigidBody;

	typedef std::unordered_set<CollisionTest,hthash<CollisionTest>> CollisionTestCont;
	typedef std::vector<component_Physics*> OmnipresentCandidateCont;
	typedef std::list<PhysicsContact> ContactCont;

public:
	static PhysicsManager* instance() { static PhysicsManager *gnew = new PhysicsManager(); return gnew; }

	~PhysicsManager();

	const ContactCont& getContacts();

	void setIterations(int velocityiterations,int positioniterations) { miterations_velocity_max = velocityiterations; miterations_position_max = positioniterations; }
	void setVelocityIterations(int velocityiterations) { miterations_velocity_max = velocityiterations; }
	void setPositionIterations(int positioniterations) { miterations_position_max = positioniterations; }
	unsigned getVelocityIterations() const { return miterations_velocity_max; }
	unsigned getPositionsIterations() const { return miterations_position_max; }

private:
	int mcbid_onupdate_post1;
	OmnipresentCandidateCont momnipresentcandidates;
	ContactCont mcontacts;
	unsigned miterations_velocity_max;
	unsigned miterations_position_max;
	bool mvalidsettings;

	PhysicsManager();

	void _collision_removeContactsWith(component_Physics *comp);

	void _processContacts(float d); //resolves a set of contacts, both penetration and velocity

	void _resolveContactVelocities(float d); //resolves the velocities of a set of contacts
	void _resolveContactPositions(float d); //resolves the positions of a set of contacts
};
