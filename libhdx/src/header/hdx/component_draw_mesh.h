#pragma once

#include "hdx/component.h"
#include "hdx/basemanager.h"
#include "hdx/util.h"
#include "hdx/hdx9math.h"

class HDXMesh;


class component_Draw_Mesh : public component_Base {
public:
	class Item {
		friend class component_Draw_Mesh;
	public:
		enum RENDERPHASE {
			RP_NONE = 0,
			RP_DEVICE,
			RP_SPRITE,
			RP_SHADER
		};

		enum PrimType {
			PT_SPHERE = 0,
			PT_BOX,
			PT_LINE,

			PT_MESH
		};

		virtual ~Item();

		PrimType getType() const { return mtype; }

		RENDERPHASE getRenderPhase();
		void setRenderPhase(RENDERPHASE phase);

		const HDXMatrix4x4& getTransform() const;
		void setTransform(const HDXMatrix4x4 &trans);

		bool isWireframe() const;
		void setWireframe(bool wireframe);

		HDXMesh* getMesh();
		const HDXMesh* getMesh() const;

	private:
		PrimType mtype;
		RENDERPHASE mrenderphase;
		component_Draw_Mesh *mcomp;
		HDXMatrix4x4 mtrans;
		std::function<void(void)> mrenderfunc;
		int mcbid_render;
		HDXMesh *mmesh;
		bool mflag_wireframe;

		Item();
	};

	component_Draw_Mesh(Entity *entity);
	virtual ~component_Draw_Mesh();

	// if give_mesh is false then this component will add a user to the mesh
	// and remove it when the item is destroyed, if it is true it will not
	// add a user, but will still remove it, allowing the component to manage
	// its lifespan
	int addMesh(const HDXMatrix4x4 &trans,HDXMesh *mesh,bool give_mesh = false);

	int addSphere(const HDXMatrix4x4 &trans,const HDXMaterial &mat,float rad);
	int addBox(const HDXMatrix4x4 &trans,const HDXMaterial &mat,const HDXVector3 &ext);
	int addRod(const HDXMatrix4x4 &trans,const HDXMaterial &mat,float halflength);

	Item* getItem(int id) const;
	void removeItem(int id);
	void clear();

	BaseManagerID<Item*>& getItems();
	const BaseManagerID<Item*>& getItems() const;

	virtual const char* getName() const { return _getName(); }
	static const char* _getName() { return "draw_mesh"; }

private:
	BaseManagerID<Item*> mitems;
};