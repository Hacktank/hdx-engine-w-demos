#pragma once

#include <unordered_map>
#include <functional>

#include <string>
#include <type_traits>

#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_utils.hpp>
#include <rapidxml/rapidxml_print.hpp>
#include <rapidxml/rapidxml_iterators.hpp>

class XMLScript {
public:
	class Source {
	public:
		typedef std::function<std::string(const XMLScript *script,const std::string &arg)> FUNC_varAccess;

		Source(const std::string &name,XMLScript *parentscript);
		virtual ~Source();

		virtual std::string dereference(Source *prevsource,const std::string &entryname,const std::string &str);

		void addSource(const std::string &name,Source *source);
		void addVarAccess(const std::string &name,FUNC_varAccess func);

		std::string mname;
		XMLScript *mparentscript;
		std::unordered_map<std::string,Source*> msources;
		std::unordered_map<std::string,FUNC_varAccess> mvars;
	};

	XMLScript();
	XMLScript(const XMLScript&) = delete;
	XMLScript(XMLScript&&) = delete;
	~XMLScript();

	std::string scriptGetValue(rapidxml::xml_node<> *node) const;

	template<typename T = Source>
	T* createSource(const std::string &name);
	Source* getSource(const std::string &name) const;

private:
	std::unordered_map<std::string,Source*> mmastersources;
};

template<typename T>
T* XMLScript::createSource(const std::string &name) {
	assert(mmastersources.count(name)==0);
	T *newsource = new T(name,this);
	mmastersources[name] = newsource;
	return newsource;
}

void XMLGetTokArg(std::string str,std::string *tok,std::string *arg);

bool XMLRead(std::string *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(bool *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(int *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(unsigned int *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(long *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(unsigned long *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(long long *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(float *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(double *out,XMLScript *script,rapidxml::xml_node<> *node);
bool XMLRead(long double *out,XMLScript *script,rapidxml::xml_node<> *node);

template<typename T>
bool XMLToString(std::string *out,const T &in,std::string arg = "") {
	*out = std::to_string(in);
	return true;
}

template<> bool XMLToString<std::string>(std::string *out,const std::string &in,std::string arg);
template<> bool XMLToString<bool>(std::string *out,const bool &in,std::string arg);
