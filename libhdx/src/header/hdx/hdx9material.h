#pragma once

#include <d3d9.h>
#include <d3dx9.h>

#include "hdx/hdx9color.h"

struct HDXMaterial {
	D3DXCOLOR diffuse;
	D3DXCOLOR ambient;
	D3DXCOLOR specular;
	D3DXCOLOR emisive;
	float power;

	HDXMaterial(D3DXCOLOR pdiffuse = HDXCOL_WHITE,
				D3DXCOLOR pambient = HDXCOL_WHITE*0.2f,
				D3DXCOLOR pspecular = HDXCOL_WHITE*0.5f,
				D3DXCOLOR pemissive = HDXCOL_BLACK,
				float ppower = 10) {
		diffuse = pdiffuse;
		ambient = pambient;
		specular = pspecular;
		emisive = pemissive;
		power = ppower;
	}

	operator D3DXMATERIAL() {
		D3DXMATERIAL ret;
		ret.MatD3D.Ambient = ambient;
		ret.MatD3D.Diffuse = diffuse;
		ret.MatD3D.Emissive = emisive;
		ret.MatD3D.Power = power;
		ret.MatD3D.Specular = specular;
		return ret;
	}
};

#define HDXMAT_BLACK (HDXMaterial(HDXCOL_BLACK, HDXCOL_BLACK, HDXCOL_BLACK, HDXCOL_BLACK, 0.0f))
#define HDXMAT_WHITE (HDXMaterial(HDXCOL_WHITE, HDXCOL_WHITE, HDXCOL_WHITE, HDXCOL_BLACK, 10.0f))
#define HDXMAT_RED   (HDXMaterial(HDXCOL_RED  , HDXCOL_RED  , HDXCOL_WHITE, HDXCOL_BLACK, 10.0f))
#define HDXMAT_GREEN (HDXMaterial(HDXCOL_GREEN, HDXCOL_GREEN, HDXCOL_WHITE, HDXCOL_BLACK, 10.0f))
#define HDXMAT_BLUE  (HDXMaterial(HDXCOL_BLUE , HDXCOL_BLUE , HDXCOL_WHITE, HDXCOL_BLACK, 10.0f))
