
#pragma once

#include <d3d9.h>
#include <d3dx9.h>

#include "hdx/hdx9math.h"

enum {
	HDXLT_DIRECTIONAL = 0,
	HDXLT_POINT,
	HDXLT_SPOT,

	HDXLT_NUM
};

class HDXLight {
public:
	HDXLight(int id);
	~HDXLight();

	int getType() const;

	int getID() const;

	bool isEnabled() const;
	void setEnabled(bool enabled);

	D3DXCOLOR getColorAmbient() const;
	void setColorAmbient(const D3DXCOLOR &col);

	D3DXCOLOR getColorDiffuse() const;
	void setColorDiffuse(const D3DXCOLOR &col);

	D3DXCOLOR getColorSpecular() const;
	void setColorSpecular(const D3DXCOLOR &col);

	HDXVector3 getPosition() const;
	void setPosition(const HDXVector3 &v);

	HDXVector3 getDirection() const;
	void setDirection(const HDXVector3 &v);

	HDXVector3 getAttenuation() const;
	void setAttenuation(const HDXVector3 &v);

	float getFalloff() const;
	void setFalloff(float falloff);

	float getTheta() const;
	void setTheta(float theta);

	float getPhi() const;
	void setPhi(float phi);

	void setLightDirectional(const D3DXCOLOR &ambient,
							 const D3DXCOLOR &diffuse,
							 const D3DXCOLOR &specular,
							 const HDXVector3 &direction);

	void setLightPoint(const D3DXCOLOR &ambient,
					   const D3DXCOLOR &diffuse,
					   const D3DXCOLOR &specular,
					   const HDXVector3 &pos,
					   const HDXVector3 &attenuation);

	void setLightSpot(const D3DXCOLOR &ambient,
					  const D3DXCOLOR &diffuse,
					  const D3DXCOLOR &specular,
					  const HDXVector3 &pos,
					  const HDXVector3 &attenuation,
					  const HDXVector3 &direction,
					  float falloff,
					  float theta,
					  float phi);

private:
	int mtype;
	int mid;
	bool menabled;

	D3DXCOLOR mcol_ambient,mcol_diffuse,mcol_specular;
	HDXVector3 mpos;
	HDXVector3 mattenuation;
	HDXVector3 mdirection;
	float mfalloff;
	float mtheta;
	float mphi;
};