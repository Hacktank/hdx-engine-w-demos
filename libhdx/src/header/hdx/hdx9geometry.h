#pragma once

#include "hdx/util.h"
#include "hdx/hdx9math.h"

#include <vector>
#include <array>

class HDXConvexHull {
public:
	std::vector<HDXVector3> mverts;
	std::vector<std::array<unsigned,3>> mtriangles;
	std::vector<std::vector<unsigned>> madjacency;
	HDXVector3 maabbhs;
	HDXVector3 mcentroid;

	HDXConvexHull();
	HDXConvexHull(const HDXConvexHull &r);
	HDXConvexHull(HDXConvexHull &&r);
	~HDXConvexHull();

	HDXConvexHull& operator=(const HDXConvexHull &r);
	HDXConvexHull& operator=(HDXConvexHull &&r);

	// offsets all of the points by vec
	void offset(const HDXVector3 &vec);
};

void barycentric(const HDXVector3 &p,const HDXVector3 &a,const HDXVector3 &b,const HDXVector3 &c,float *u,float *v,float *w);
float pointDistanceFromLine(const HDXVector3 &p,const HDXVector3 &a,const HDXVector3 &b);
float pointDistanceFromTriangle(const HDXVector3 &p,const HDXVector3 &a,const HDXVector3 &b,const HDXVector3 &c);
float pointDistanceFromPlane(const HDXVector3 &p,const HDXVector3 &a,const HDXVector3 &b,const HDXVector3 &c);
float pointDistanceFromPlane(const HDXVector3 &p,const HDXVector3 &n,const HDXVector3 &a);
float pointDistanceFromPlane(const HDXVector3 &p,const HDXVector3 &n,float d);

HDXConvexHull HDXQuickHull(HDXVector3 *pointcloud,unsigned n);

// decomposes a arbetrary convex mesh into an assortment of convex hulls
// points: array of points
// triangles: array of triangle point indicies (t1p1,t1p2,t1p3,t2p1...)
// numtriangles: number of triangles
// the target minimum concavity, this should not be 0
std::vector<HDXConvexHull> HDXMeshDecomposeToConvexSet(HDXVector3 *points,unsigned numpoints,unsigned *triangles,unsigned numtriangles,float targetconcavity);

HDXMatrix3x3 inertialTensor_rod(float halflength,float mass);
HDXMatrix3x3 inertialTensor_box(const HDXVector3 &halfsize,float mass);
HDXMatrix3x3 inertialTensor_sphere(float r,float mass);
HDXMatrix3x3 inertialTensor_convexhull(const HDXConvexHull &hull,float mass);
