#pragma once

#include "hdx/sound/hdx9sound.h"

#define HDX_MUSICMAN HDX9MusicManager::instance()

class HDX9MusicManager {
public:
	static HDX9MusicManager* instance() { static HDX9MusicManager *gnew = new HDX9MusicManager(); return gnew; }
	~HDX9MusicManager();

	FMOD::Channel* getCurrentChannel();
	HDX9Sound* getCurrentSound();

	void controlPlay();
	void controlPause();

	void setTrackCrossfade(HDX9Sound *track,float fadetime);

private:
	FMOD::Channel *mchannel_cur;
	HDX9Sound *msound_cur;

	HDX9MusicManager();
};
