#pragma once

#include <windows.h>

#include "hdx/ui/hdx9ui.h"

struct IDirect3DDevice9;
struct ID3DXSprite;
class BaseDX9State;

class HDX9App {
public:
	HDX9App();
	virtual ~HDX9App();

	//************************************
	// Method:    initializeScene
	// Returns:   void
	// Notes:
	//************************************
	virtual void initializeScene();

	//************************************
	// Method:    deinitializeScene
	// Returns:   void
	// Notes:
	//************************************
	virtual void deinitializeScene();
};
