#pragma once

#include "hdx/component.h"
#include "basemanager.h"

#define HDX_ENTMAN EntityManager::instance()

class Entity {
	friend class EntityManager;

public:
	~Entity();

	int getID() const;
	bool isActive() const;

	void setActive(bool active);

	//flags this entity for deletion
	void destroy();

	template<typename T,typename ...Args>
	T* createComponent(Args ...args);

	template<typename T>
	void destroyComponent();

	template<typename T>
	T* getComponent(bool create = false);

private:
	int mid;
	bool mflag_destroy;
	bool mflag_active;
	BaseManagerKey<component_Base*,const char*> mcomponents;

	Entity(int id);
};

template<typename T>
void Entity::destroyComponent() {
	if(mcomponents.hasItem(T::_getName())) {
		auto comp = mcomponents.getItem(T::_getName());
		auto &tvec = HDX_ENTMAN->mcomponents[T::_getName()];
		tvec.erase(std::remove(tvec.begin(),tvec.end(),comp),tvec.end());
		delete comp;
		mcomponents.removeItem(name);
	}
}

template<typename T>
T* Entity::getComponent(bool create /*= false*/) {
	if(create && !mcomponents.hasItem(T::_getName())) {
		return createComponent<T>();
	} else {
		return dynamic_cast<T*>(mcomponents.getItem(T::_getName()));
	}
}

template<typename T,typename ...Args>
T* Entity::createComponent(Args ...args) {
	assert(!mcomponents.hasItem(T::_getName()));

	T *comp = new T(this,args...);
	HDX_ENTMAN->mcomponents[T::_getName()].push_back(comp);
	mcomponents.addItem(T::_getName(),comp);
	return comp;
}

class EntityManager {
	friend class Entity;

public:
	static EntityManager* instance() { static EntityManager *gnew = new EntityManager(); return gnew; }

	~EntityManager();

	Entity* createEntity();
	Entity* getEntity(int id);
	void clear();

	template<typename T>
	std::vector<T*> getComponentList();

private:
	BaseManagerID<Entity*> mentities;
	std::unordered_map<const char*,std::vector<component_Base*>> mcomponents;
	int mcbid_onupdate;

	EntityManager();
};

template<typename T>
std::vector<T*> EntityManager::getComponentList() {
	auto &cvec = mcomponents[T::_getName()];
	std::vector<T*> ret(cvec.size());
	for(unsigned i = 0; i < cvec.size(); i++) {
		ret[i] = (T*)cvec[i];
	}
	return std::move(ret);
}
