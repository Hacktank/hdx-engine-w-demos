#pragma once

#include <d3d9.h>
#include <d3dx9.h>

#include <string>
#include <unordered_map>

class HDXTexture {
	friend HDXTexture* HDXTextureCreateFromFile(const std::string &fname,bool managed);
	friend HDXTexture* HDXTextureCreateBlank(const std::string &name,bool managed,int w,int h);

public:
	~HDXTexture();

	IDirect3DTexture9* getD3DTexture() const;

	unsigned int getWidth() const;
	unsigned int getHeight() const;

	std::string getName() const;

	int release();

private:
	std::string mname;
	int musers;
	bool mmanaged;

	IDirect3DTexture9 *md3dtexture;

	static std::unordered_map<std::string,HDXTexture*> *__managed_items;

	HDXTexture();

	static HDXTexture* __create(const std::string &name,bool managed);
};
