#pragma once

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#include <float.h>

#include <d3dx9.h>
#include <directxmath.h>

#define XUNIT2 (HDXVector2(1,0))
#define YUNIT2 (HDXVector2(0,1))

#define XUNIT3 (HDXVector3(1,0,0))
#define YUNIT3 (HDXVector3(0,1,0))
#define ZUNIT3 (HDXVector3(0,0,1))
#define UNIT3 (HDXVector3(0.57735f,0.57735f,0.57735f))

class HDXVector2 {
	friend class HDXVector3;
	friend class HDXVector4;
	friend class HDXQuaternion;
	friend class HDXMatrix3x3;
	friend class HDXMatrix4x4;
public:
	class gen {
	public:

	};

	float &x,&y;

	HDXVector2();
	HDXVector2(float x,float y);
	HDXVector2(const HDXVector2 &r);
	HDXVector2(const HDXVector3 &r);
	HDXVector2(const D3DXVECTOR2 &r);
	~HDXVector2();

	HDXVector2& operator=(const HDXVector2 &r);

	BOOL operator==(const HDXVector2 &r) const;
	BOOL operator!=(const HDXVector2 &r) const;

	void clear();

	float& operator[](unsigned r);
	const float& operator[](unsigned r) const;

	HDXVector2& operator+=(const HDXVector2 &r);
	HDXVector2& operator-=(const HDXVector2 &r);

	HDXVector2& operator*=(float r);
	HDXVector2& operator/=(float r);

	HDXVector2 operator+(const HDXVector2 &r) const;
	HDXVector2 operator-(const HDXVector2 &r) const;

	friend HDXVector2 operator*(const HDXVector2 &l,float r) { HDXVector2 ret = l; ret *= r; return ret; }
	friend HDXVector2 operator/(const HDXVector2 &l,float r) { HDXVector2 ret = l; ret /= r; return ret; }
	friend HDXVector2 operator*(float l,const HDXVector2 &r) { HDXVector2 ret = r; ret *= l; return ret; }
	friend HDXVector2 operator/(float l,const HDXVector2 &r) { HDXVector2 ret = r; ret /= l; return ret; }

	HDXVector2 operator-() const;

	D3DXVECTOR2* getD3DX();
	const D3DXVECTOR2* getD3DX() const;
	float* data();
	const float* data() const;

	HDXVector2 getNormal() const;
	HDXVector2& setNormal();

	float magnitudeSq() const;
	float magnitude() const;

	float dot(const HDXVector2 &r) const;

	float operator*(const HDXVector2 &r) const;

protected:
	D3DXVECTOR2 _d3dx;
};

class HDXVector3 {
	friend class HDXVector4;
	friend class HDXQuaternion;
	friend class HDXMatrix3x3;
	friend class HDXMatrix4x4;
public:
	class gen {
	public:
		static void makeOrthoNormalBasis(HDXVector3 *x,HDXVector3 *y,HDXVector3 *z);

		static HDXVector3 transformQuaternion(const HDXVector3 &v,const HDXQuaternion &q);
	};

	float &x,&y,&z;

	HDXVector3();
	HDXVector3(float x,float y,float z);
	HDXVector3(const HDXVector3 &r);
	HDXVector3(const HDXVector4 &r);
	HDXVector3(const HDXQuaternion &r);
	HDXVector3(const D3DXVECTOR3 &r);
	HDXVector3(const HDXVector2 &r);
	~HDXVector3();

	HDXVector3& operator=(const HDXVector3 &r);

	BOOL operator==(const HDXVector3 &r) const;
	BOOL operator!=(const HDXVector3 &r) const;

	void clear();

	float& operator[](unsigned r);
	const float& operator[](unsigned r) const;

	HDXVector3& operator+=(const HDXVector3 &r);
	HDXVector3& operator-=(const HDXVector3 &r);

	HDXVector3& operator*=(float r);
	HDXVector3& operator/=(float r);

	HDXVector3 operator+(const HDXVector3 &r) const;
	HDXVector3 operator-(const HDXVector3 &r) const;

	friend HDXVector3 operator*(const HDXVector3 &l,float r) { HDXVector3 ret = l; ret *= r; return ret; }
	friend HDXVector3 operator/(const HDXVector3 &l,float r) { HDXVector3 ret = l; ret /= r; return ret; }
	friend HDXVector3 operator*(float l,const HDXVector3 &r) { HDXVector3 ret = r; ret *= l; return ret; }
	friend HDXVector3 operator/(float l,const HDXVector3 &r) { HDXVector3 ret = r; ret /= l; return ret; }

	HDXVector3 operator-() const;

	D3DXVECTOR3* getD3DX();
	const D3DXVECTOR3* getD3DX() const;
	float* data();
	const float* data() const;

	HDXVector3 getNormal() const;
	HDXVector3& setNormal();

	float magnitudeSq() const;
	float magnitude() const;

	float dot(const HDXVector3 &r) const;
	HDXVector3 cross(const HDXVector3 &r) const;
	HDXMatrix3x3 outer(const HDXVector3 &v) const;
	HDXVector3 mirror(const HDXVector3 &n) const;

	float operator*(const HDXVector3 &r) const;
	HDXVector3 operator%(const HDXVector3 &r) const;

protected:
	D3DXVECTOR3 _d3dx;
};

class HDXVector4 {
	friend class HDXVector2;
	friend class HDXVector3;
	friend class HDXQuaternion;
	friend class HDXMatrix3x3;
	friend class HDXMatrix4x4;
public:
	class gen {
	public:
	};

	float &x,&y,&z,&w;

	HDXVector4();
	HDXVector4(float x,float y,float z,float w);
	HDXVector4(const HDXVector4 &r);
	HDXVector4(const HDXVector3 &r);
	HDXVector4(const D3DXVECTOR4 &r);
	~HDXVector4();

	HDXVector4& operator=(const HDXVector4 &r);

	BOOL operator==(const HDXVector4 &r) const;
	BOOL operator!=(const HDXVector4 &r) const;

	void clear();

	float& operator[](unsigned r);
	const float& operator[](unsigned r) const;

	HDXVector4& operator+=(const HDXVector4 &r);
	HDXVector4& operator-=(const HDXVector4 &r);

	HDXVector4& operator*=(float r);
	HDXVector4& operator/=(float r);

	HDXVector4 operator+(const HDXVector4 &r) const;
	HDXVector4 operator-(const HDXVector4 &r) const;

	friend HDXVector4 operator*(const HDXVector4 &l,float r) { HDXVector4 ret = l; ret *= r; return ret; }
	friend HDXVector4 operator/(const HDXVector4 &l,float r) { HDXVector4 ret = l; ret /= r; return ret; }
	friend HDXVector4 operator*(float l,const HDXVector4 &r) { HDXVector4 ret = r; ret *= l; return ret; }
	friend HDXVector4 operator/(float l,const HDXVector4 &r) { HDXVector4 ret = r; ret /= l; return ret; }

	HDXVector4 operator-() const;

	D3DXVECTOR4* getD3DX();
	const D3DXVECTOR4* getD3DX() const;
	float* data();
	const float* data() const;

	HDXVector4 getNormal() const;
	HDXVector4& setNormal();

	float magnitudeSq() const;
	float magnitude() const;

	float dot(const HDXVector4 &r) const;

	float operator*(const HDXVector4 &r) const;

protected:
	D3DXVECTOR4 _d3dx;
};

class HDXQuaternion {
	friend class HDXVector2;
	friend class HDXVector3;
	friend class HDXVector4;
	friend class HDXMatrix3x3;
	friend class HDXMatrix4x4;
public:
	class gen {
	public:
		static HDXQuaternion identity();

		static HDXQuaternion rotateTo(const HDXVector3 &q,const HDXVector3 &p);
	};

	float &x,&y,&z,&w;

	HDXQuaternion();
	HDXQuaternion(float x,float y,float z,float w);
	HDXQuaternion(const HDXVector3 &axis,float angle);
	HDXQuaternion(float eularx,float eulary,float eularz);
	HDXQuaternion(const HDXVector3 &vec);
	HDXQuaternion(const HDXQuaternion &r);
	HDXQuaternion(const HDXMatrix3x3 &r);
	HDXQuaternion(const D3DXQUATERNION &r);
	~HDXQuaternion();

	HDXQuaternion& operator=(const HDXQuaternion &r);

	BOOL operator==(const HDXQuaternion &r) const;
	BOOL operator!=(const HDXQuaternion &r) const;

	void clear();

	float& operator[](unsigned r);
	const float& operator[](unsigned r) const;

	HDXQuaternion& operator+=(const HDXQuaternion &r);
	HDXQuaternion& operator-=(const HDXQuaternion &r);

	HDXQuaternion& operator*=(float r);
	HDXQuaternion& operator/=(float r);

	HDXQuaternion operator+(const HDXQuaternion &r) const;
	HDXQuaternion operator-(const HDXQuaternion &r) const;

	friend HDXQuaternion operator*(const HDXQuaternion &l,float r) { HDXQuaternion ret = l; ret *= r; return ret; }
	friend HDXQuaternion operator/(const HDXQuaternion &l,float r) { HDXQuaternion ret = l; ret /= r; return ret; }
	friend HDXQuaternion operator*(float l,const HDXQuaternion &r) { HDXQuaternion ret = r; ret *= l; return ret; }
	friend HDXQuaternion operator/(float l,const HDXQuaternion &r) { HDXQuaternion ret = r; ret /= l; return ret; }

	HDXQuaternion operator-() const;

	D3DXQUATERNION* getD3DX();
	const D3DXQUATERNION* getD3DX() const;
	float* data();
	const float* data() const;

	HDXQuaternion getNormal() const;
	HDXQuaternion& setNormal();

	HDXQuaternion& setIdentity();

	float magnitudeSq() const;
	float magnitude() const;

	HDXMatrix3x3 toRotationMatrix() const;

	float getAngle() const;
	void setAngle(float angle);

	HDXVector3 getAxis() const;
	void setAxis(const HDXVector3 &axis);

	HDXQuaternion& setAxisAngle(const HDXVector3 &axis,float angle);

	HDXVector3 getEular() const;

	void setEular(const HDXVector3 &eular);
	void setEular(float x,float y,float z);

	HDXQuaternion getConjugate() const;
	HDXQuaternion& setConjugate();

	HDXQuaternion getInverse() const;
	HDXQuaternion& setInverse();

	HDXQuaternion operator~() const;

	HDXQuaternion& operator*=(const HDXQuaternion &r);
	HDXQuaternion& operator+=(const HDXVector3 &r);

	HDXQuaternion operator*(const HDXQuaternion &r) const;
	HDXVector3 operator*(const HDXVector3 &r) const;
	HDXQuaternion operator+(const HDXVector3 &r) const;

protected:
	D3DXQUATERNION _d3dx;
};

class HDXMatrix3x3 {
	friend class HDXVector2;
	friend class HDXVector3;
	friend class HDXVector4;
	friend class HDXQuaternion;
	friend class HDXMatrix4x4;

public:
	class gen {
	public:
		static HDXMatrix3x3 identity();

		static HDXMatrix3x3 skewSymmetric(const HDXVector3 &vec);

		static HDXMatrix3x3 lookDirection(const HDXVector3 &localforward,const HDXVector3 &targetdir,const HDXVector3 &localup = YUNIT3,const HDXVector3 &worldup = YUNIT3);

		static HDXMatrix3x3 view(const HDXVector3 &right,const HDXVector3 &up,const HDXVector3 &forward);

		static HDXMatrix3x3 rotationAxis(const HDXVector3 &axis,float ang);
		static HDXMatrix3x3 rotationAxisX(float ang);
		static HDXMatrix3x3 rotationAxisY(float ang);
		static HDXMatrix3x3 rotationAxisZ(float ang);

		static HDXMatrix3x3 transformation2d(const HDXVector2 &scale,const HDXVector2 &rotcenter,float rot,const HDXVector2 &pos);
	};

	HDXMatrix3x3();
	HDXMatrix3x3(FLOAT _11,FLOAT _12,FLOAT _13,
				 FLOAT _21,FLOAT _22,FLOAT _23,
				 FLOAT _31,FLOAT _32,FLOAT _33);
	HDXMatrix3x3(const HDXMatrix3x3 &r);
	HDXMatrix3x3(const HDXMatrix4x4 &r);
	HDXMatrix3x3(const HDXQuaternion &r);
	HDXMatrix3x3(const D3DXMATRIX &r);
	~HDXMatrix3x3();

	HDXMatrix3x3& operator=(const HDXMatrix3x3 &r);

	BOOL operator==(const HDXMatrix3x3 &r) const;
	BOOL operator!=(const HDXMatrix3x3 &r) const;

	void clear();

	float& operator[](unsigned r);
	const float& operator[](unsigned r) const;

	HDXMatrix3x3& operator+=(const HDXMatrix3x3 &r);
	HDXMatrix3x3& operator-=(const HDXMatrix3x3 &r);

	HDXMatrix3x3& operator*=(float r);
	HDXMatrix3x3& operator/=(float r);

	HDXMatrix3x3 operator+(const HDXMatrix3x3 &r) const;
	HDXMatrix3x3 operator-(const HDXMatrix3x3 &r) const;

	friend HDXMatrix3x3 operator*(const HDXMatrix3x3 &l,float r) { HDXMatrix3x3 ret = l; ret *= r; return ret; }
	friend HDXMatrix3x3 operator/(const HDXMatrix3x3 &l,float r) { HDXMatrix3x3 ret = l; ret /= r; return ret; }
	friend HDXMatrix3x3 operator*(float l,const HDXMatrix3x3 &r) { HDXMatrix3x3 ret = r; ret *= l; return ret; }
	friend HDXMatrix3x3 operator/(float l,const HDXMatrix3x3 &r) { HDXMatrix3x3 ret = r; ret /= l; return ret; }

	HDXMatrix3x3 operator-() const;

	D3DXMATRIX* getD3DX();
	const D3DXMATRIX* getD3DX() const;
	float* data();
	const float* data() const;

	HDXMatrix4x4 promote() const;

	HDXQuaternion toRotationQuaternion() const;

	HDXVector3 getRow(unsigned i) const;
	void setRow(unsigned i,const HDXVector2 &vec);
	void setRow(unsigned i,const HDXVector3 &vec);
	HDXVector3 getColumn(unsigned i) const;
	void setColumn(unsigned i,const HDXVector2 &vec);
	void setColumn(unsigned i,const HDXVector3 &vec);

	HDXVector3 getScale() const;

	HDXMatrix3x3& orthonormalize();

	float determinant() const;

	HDXMatrix3x3& setIdentity();

	HDXMatrix3x3 getInvert() const;
	HDXMatrix3x3& setInvert();

	HDXMatrix3x3 getTranspose() const;
	HDXMatrix3x3& setTranspose();

	HDXMatrix3x3& operator*=(const HDXMatrix3x3 &r);
	HDXMatrix3x3 operator*(const HDXMatrix3x3 &r) const;

	friend HDXVector2 operator*(const HDXVector2 &l,const HDXMatrix3x3 &r) { D3DXVECTOR4 tmp; D3DXVec2Transform(&tmp,l.getD3DX(),&r._d3dx); return HDXVector2(tmp.x,tmp.y); }
	friend HDXVector3 operator*(const HDXVector3 &l,const HDXMatrix3x3 &r) { D3DXVECTOR4 tmp; D3DXVec3Transform(&tmp,l.getD3DX(),&r._d3dx); return HDXVector3(tmp.x,tmp.y,tmp.z); }
	friend HDXVector4 operator*(const HDXVector4 &l,const HDXMatrix3x3 &r) { D3DXVECTOR4 tmp; D3DXVec4Transform(&tmp,l.getD3DX(),&r._d3dx); return HDXVector4(tmp.x,tmp.y,tmp.z,tmp.w); }

	unsigned static ij(unsigned i,unsigned j);

protected:
	D3DXMATRIX _d3dx;

	void _contrain();
};

class HDXMatrix4x4 {
	friend class HDXVector2;
	friend class HDXVector3;
	friend class HDXVector4;
	friend class HDXQuaternion;
	friend class HDXMatrix3x3;

public:
	class gen {
	public:
		static HDXMatrix4x4 identity();

		static HDXMatrix4x4 translation3d(const HDXVector3 &v);
		static HDXMatrix4x4 scale3d(const HDXVector3 &scale);

		static HDXMatrix4x4 transformation3d(const HDXVector3 &scale,const HDXVector3 &rotcenter,const HDXQuaternion &rot,const HDXVector3 &pos);
	};

	HDXMatrix4x4();
	HDXMatrix4x4(FLOAT _11,FLOAT _12,FLOAT _13,FLOAT _14,
				 FLOAT _21,FLOAT _22,FLOAT _23,FLOAT _24,
				 FLOAT _31,FLOAT _32,FLOAT _33,FLOAT _34,
				 FLOAT _41,FLOAT _42,FLOAT _43,FLOAT _44);
	HDXMatrix4x4(const HDXMatrix4x4 &r);
	HDXMatrix4x4(const HDXMatrix3x3 &r);
	HDXMatrix4x4(const D3DXMATRIX &r);
	~HDXMatrix4x4();

	HDXMatrix4x4& operator=(const HDXMatrix4x4 &r);

	BOOL operator==(const HDXMatrix4x4 &r) const;
	BOOL operator!=(const HDXMatrix4x4 &r) const;

	void clear();

	float& operator[](unsigned r);
	const float& operator[](unsigned r) const;

	HDXMatrix4x4& operator+=(const HDXMatrix4x4 &r);
	HDXMatrix4x4& operator-=(const HDXMatrix4x4 &r);

	HDXMatrix4x4& operator*=(float r);
	HDXMatrix4x4& operator/=(float r);

	HDXMatrix4x4 operator+(const HDXMatrix4x4 &r) const;
	HDXMatrix4x4 operator-(const HDXMatrix4x4 &r) const;

	friend HDXMatrix4x4 operator*(const HDXMatrix4x4 &l,float r) { HDXMatrix4x4 ret = l; ret *= r; return ret; }
	friend HDXMatrix4x4 operator/(const HDXMatrix4x4 &l,float r) { HDXMatrix4x4 ret = l; ret /= r; return ret; }
	friend HDXMatrix4x4 operator*(float l,const HDXMatrix4x4 &r) { HDXMatrix4x4 ret = r; ret *= l; return ret; }
	friend HDXMatrix4x4 operator/(float l,const HDXMatrix4x4 &r) { HDXMatrix4x4 ret = r; ret /= l; return ret; }

	HDXMatrix4x4 operator-() const;

	D3DXMATRIX* getD3DX();
	const D3DXMATRIX* getD3DX() const;
	float* data();
	const float* data() const;

	HDXMatrix3x3 demote() const;

	HDXMatrix3x3 getRotation() const;

	HDXVector4 getRow(unsigned i) const;
	void setRow(unsigned i,const HDXVector3 &vec);
	void setRow(unsigned i,const HDXVector4 &vec);
	HDXVector4 getColumn(unsigned i) const;
	void setColumn(unsigned i,const HDXVector3 &vec);
	void setColumn(unsigned i,const HDXVector4 &vec);

	HDXVector4 getScale() const;

	float determinant() const;

	HDXMatrix4x4& setIdentity();

	HDXMatrix4x4 getInvert() const;
	HDXMatrix4x4& setInvert();

	HDXMatrix4x4 getTranspose() const;
	HDXMatrix4x4& setTranspose();

	HDXMatrix4x4& operator*=(const HDXMatrix4x4 &r);
	HDXMatrix4x4 operator*(const HDXMatrix4x4 &r) const;

	friend HDXVector2 operator*(const HDXVector2 &l,const HDXMatrix4x4 &r) { D3DXVECTOR4 tmp; D3DXVec2Transform(&tmp,l.getD3DX(),&r._d3dx); return HDXVector2(tmp.x,tmp.y); }
	friend HDXVector3 operator*(const HDXVector3 &l,const HDXMatrix4x4 &r) { D3DXVECTOR4 tmp; D3DXVec3Transform(&tmp,l.getD3DX(),&r._d3dx); return HDXVector3(tmp.x,tmp.y,tmp.z); }
	friend HDXVector4 operator*(const HDXVector4 &l,const HDXMatrix4x4 &r) { D3DXVECTOR4 tmp; D3DXVec4Transform(&tmp,l.getD3DX(),&r._d3dx); return HDXVector4(tmp.x,tmp.y,tmp.z,tmp.w); }

	unsigned static ij(unsigned i,unsigned j);

protected:
	D3DXMATRIX _d3dx;
};
