#pragma once

#include "hdx/component.h"
#include "hdx/hdx9math.h"

class component_Transform : public component_Base {
public:
	component_Transform(Entity *entity);
	virtual ~component_Transform();

	void setPosition(const HDXVector3 &position);
	void setPosition(const float x,const float y,const float z);
	void addPosition(const HDXVector3 &position);
	void getPosition(HDXVector3 *position) const;
	const HDXVector3& getPosition() const;
	HDXVector3* getPositionPtr();


	void setScale(const HDXVector3 &scale);
	void setScale(const float x,const float y,const float z);
	void addScale(const HDXVector3 &scale);
	void getScale(HDXVector3 *scale) const;
	const HDXVector3& getScale() const;
	HDXVector3* getScalePtr();

	void setOrientation(const HDXQuaternion &orientation);
	void setOrientation(const float w,const float x,const float y,const float z);
	void getOrientation(HDXQuaternion *orientation) const;
	void addOrientation(const HDXVector3 &localeulardelta);
	const HDXQuaternion& getOrientation() const;
	HDXQuaternion* getOrientationPtr();

	const HDXMatrix4x4& getTransform() const;

	virtual const char* getName() const { return _getName(); }
	static const char* _getName() { return "transform"; }

private:
	HDXVector3 mpos;
	HDXVector3 mscale;
	HDXQuaternion mori;
	bool mchanged;
	HDXMatrix4x4 mtrans;
};