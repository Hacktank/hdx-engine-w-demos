#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <directxmath.h>

struct HDXVERTP {
	D3DXVECTOR3 p;

	static IDirect3DVertexDeclaration9 *decl;
	static bool init(IDirect3DDevice9 *device);

	HDXVERTP(const D3DXVECTOR3 &pp);
};

struct HDXVERTPTN {
	D3DXVECTOR3 p;
	D3DXVECTOR2 t;
	D3DXVECTOR3 n;

	static IDirect3DVertexDeclaration9 *decl;
	static bool init(IDirect3DDevice9 *device);

	HDXVERTPTN(const D3DXVECTOR3 &pp,
			   const D3DXVECTOR2 &pt,
			   const D3DXVECTOR3 &pn);
};

struct HDXVERTPN {
	D3DXVECTOR3 p;
	D3DXVECTOR3 n;

	static IDirect3DVertexDeclaration9 *decl;
	static bool init(IDirect3DDevice9 *device);

	HDXVERTPN(const D3DXVECTOR3 &pp,
			  const D3DXVECTOR3 &pn);
};

struct HDXVERTPCTN {
	D3DXVECTOR3 p;
	D3DCOLOR c;
	D3DXVECTOR2 t;
	D3DXVECTOR3 n;

	static IDirect3DVertexDeclaration9 *decl;
	static bool init(IDirect3DDevice9 *device);

	HDXVERTPCTN(const D3DXVECTOR3 &pp,
				const D3DCOLOR &pc,
				const D3DXVECTOR2 &pt,
				const D3DXVECTOR3 &pn);
};
