#pragma once

#include <string>

class component_Base {
	friend class Entity;

public:
	component_Base(Entity *entity) { mentity = entity; }
	virtual ~component_Base() {}

	Entity* getEntity() const { return mentity; }

	void* getData() const { return mdata; }
	void setData(void *data) { mdata = data; }

	virtual const char* getName() const { return _getName(); }
	static const char* _getName() { return "NULL"; }

private:
	Entity *mentity;
	void *mdata;
};
