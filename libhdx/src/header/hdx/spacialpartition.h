#pragma once

#include <iostream>
#include <string>
#include <unordered_map>

#include "hdx/basemanager.h"
#include "hdx/util.h"
#include "hdx/hdx9math.h"
#include "hdx/component.h"

#define HDX_SPMAN SpacialPartitionManager::instance()

#define POSMANDEFAULTGRIDSIZE 10

class SpacialPartitionManager;
namespace spdata {
	struct bucket;
};

#define UniverseBucketCont BaseManagerKey<spdata::bucket,spdata::position,hthash<spdata::position>>
#define MultiverseCont std::unordered_map<std::string,Universe>

#define FASTPOW_FI(base,exp) ([](){auto ret = base; for(int i = 1; i < exp; i++) ret*=base; return ret;})()

class component_SpacialPartition : public component_Base {
public:
	struct Registration {
		enum RegType {
			SPT_POINT = 0,
			SPT_AABB,
			SPT_OBB,

			SPT_NUM
		};

		RegType mtype;
		int mid;
		component_SpacialPartition *mcomp;
		std::string muniverse;
		std::vector<spdata::bucket*> mbuckets;
		HDXVector3 mbbext;
		void *muserdata;
	};

	component_SpacialPartition(Entity *entity);
	virtual ~component_SpacialPartition();

	int addRegistrationPoint(const std::string &universe,void *userdata = 0);
	int addRegistrationAABB(const std::string &universe,const HDXVector3 &ext,void *userdata = 0);
	int addRegistrationOBB(const std::string &universe,const HDXVector3 &ext,void *userdata = 0);
	void removeRegistration(int id);

	virtual const char* getName() const { return _getName(); }
	static const char* _getName() { return "spacialpartition"; }

protected:
	BaseManagerID<Registration*> mregistrations;
	int mcbid_onupdate_post0;
};

namespace spdata {
	typedef int pointdt;

	struct position {
		pointdt x,y,z;

		position() { set(0,0,0); }
		position(pointdt px,pointdt py,pointdt pz) { set(px,py,pz); }

		bool operator==(const position &other) const { return x==other.x && y==other.y && z==other.z; }
		bool operator!=(const position &other) const { return !(*this == other); }

		pointdt& operator[](int ind) { if(ind==0)return x; if(ind==1)return y; if(ind==2)return z; return x; }

		void set(pointdt px,pointdt py,pointdt pz) { x = px; y = py; z = pz; }
	};

	struct bucket {
		std::vector<component_SpacialPartition::Registration*> mregs;
		position mpos;

		bool operator==(const bucket &other) const { return mpos==other.mpos; }
	};
}

template<>
struct hthash<spdata::position> {
public:
	std::size_t operator()(const spdata::position &a) {
		std::size_t seed = 0;
		hash_combine(seed,a.x);
		hash_combine(seed,a.y);
		hash_combine(seed,a.z);
		return seed;
	}
};

class SpacialPartitionManager {
	friend class component_SpacialPartition;

	struct Universe {
		UniverseBucketCont mbuckets;
		double mcellgridsize;

		Universe();
	};

public:
	static SpacialPartitionManager* instance() { static SpacialPartitionManager *gnew = new SpacialPartitionManager(); return gnew; }

	~SpacialPartitionManager();

	void createUniverse(const std::string &universe,double gridsize);
	void destroyUniverse(const std::string &universe);

	double getUniverseGridsize(const std::string &universe);
	void getUniverseGridsize(const std::string &universe,double gridsize);

	void doIterateUniverseBuckets(std::function<bool(spdata::bucket &bucket)> func,const std::string &universe);

	void doIterateUniverseIntersectionAABBBuckets(std::function<bool(spdata::bucket &bucket)> func,const std::string &universe,const HDXVector3 &pos,const HDXVector3 &ext);
	void doIterateUniverseIntersectionOBBBuckets(std::function<bool(spdata::bucket &bucket)> func,const std::string &universe,const HDXVector3 &pos,const HDXVector3 &ext,const HDXQuaternion &ori);

	void doIterateUniverseCellElements(std::function<bool(component_SpacialPartition::Registration *reg)> func,const std::string &universe,const HDXVector3 &pos);
	void doIterateUniverseIntersectionAABBElements(std::function<bool(component_SpacialPartition::Registration *reg)> func,const std::string &universe,const HDXVector3 &pos,const HDXVector3 &ext);
	void doIterateUniverseIntersectionOBBElements(std::function<bool(component_SpacialPartition::Registration *reg)> func,const std::string &universe,const HDXVector3 &pos,const HDXVector3 &ext,const HDXQuaternion &ori);

private:
	MultiverseCont mmultiverse;

	SpacialPartitionManager();

	void p_reportLocationPoint(component_SpacialPartition::Registration *reg,const HDXVector3 &pos);
	void p_reportLocationAABB(component_SpacialPartition::Registration *reg,const HDXVector3 &pos,const HDXVector3 &ext);
	void p_reportLocationOBB(component_SpacialPartition::Registration *reg,const HDXVector3 &pos,const HDXVector3 &ext,const HDXQuaternion &ori);

	void p_doIterateIntersectionPoint(std::function<bool(const spdata::position &pos)> func,Universe &universe,const HDXVector3 &pos);
	void p_doIterateIntersectionAABB(std::function<bool(const spdata::position &pos)> func,Universe &universe,const HDXVector3 &pos,const HDXVector3 &ext);

	HDXVector3 p_calculateAABBExtentsFromOBB(const HDXVector3 &ext,const HDXQuaternion &ori);

	//returns the bucket the comp is placed in
	spdata::bucket& p_cellAddComp(Universe &universe,component_SpacialPartition::Registration *comp,const spdata::position &key);
	//returns the remaining comps in the bucket
	int p_cellRemoveComp(Universe &universe,component_SpacialPartition::Registration *comp,const spdata::position &key);

	static spdata::position p_calculateUniverseCell(const HDXVector3 &pos,double gridsize);
};
