#pragma once

#include <windows.h>
#include <functional>
#include <chrono>

#include "hdx/basemanager.h"

#define HDX_INPUT HDXInputManager::instance()

class HDXInputManager {
	friend class HDX9;

	struct Key {
		std::chrono::high_resolution_clock::time_point mtime_last;
		bool mdown;
	};
public:
	static HDXInputManager* instance() { static HDXInputManager *gnew = new HDXInputManager(); return gnew; }

	~HDXInputManager();

	bool getMouseFPSMode() const;
	void setMouseFPSMode(bool fpsmode);

	int registerCallbackOnKeyPress(std::function<void(WPARAM key)> &&func);
	void unregisterCallbackOnKeyPress(int id);

	int registerCallbackOnKeyRelease(std::function<void(WPARAM key,float timeheld)> &&func);
	void unregisterCallbackOnKeyRelease(int id);

	int registerCallbackOnMousePress(std::function<void(WPARAM key)> &&func);
	void unregisterCallbackOnMousePress(int id);

	int registerCallbackOnMouseRelease(std::function<void(WPARAM key,float timeheld)> &&func);
	void unregisterCallbackOnMouseRelease(int id);

	int registerCallbackOnMouseMove(std::function<void(const POINT &oldpos,const POINT &newpos)> &&func);
	void unregisterCallbackOnMouseMove(int id);

	int registerCallbackOnMouseWheel(std::function<void(short delta)> &&func);
	void unregisterCallbackOnMouseWheel(int id);

	bool getKeyDown(WPARAM key);
	float getKeyTimeSinceLastEvent(WPARAM key);

	POINT getMousePostion();
	void setMousePosition(POINT pos);

private:
	Key mkeys[256];
	POINT mmousepos_old;

	bool mmouse_fpsmode;
	POINT mmmousepos_fpstmp;

	int mcbid_onupdate;

	BaseManagerID<std::function<void(WPARAM key)>> mcallback_key_press;
	BaseManagerID<std::function<void(WPARAM key,float timeheld)>> mcallback_key_release;
	BaseManagerID<std::function<void(const POINT &oldpos,const POINT &newpos)>> mcallback_mouse_move;
	BaseManagerID<std::function<void(WPARAM key)>> mcallback_mouse_press;
	BaseManagerID<std::function<void(WPARAM key,float timeheld)>> mcallback_mouse_release;
	BaseManagerID<std::function<void(short delta)>> mcallback_mouse_wheel;

	HDXInputManager();

	void _procMessage(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam);
};