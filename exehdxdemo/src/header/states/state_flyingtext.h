#pragma once

#include "states/basedx9state.h"

class State_FlyingText : public BaseDX9State {
	class FlyingText {
	public:
		FlyingText(const std::string &text,float x,float y,float vx,float vy,const std::string &fontname,DWORD color);
		~FlyingText();

		void setText(const std::string &text);
		std::string getText() const { return mtext; }

		void update();
		void render();

		void onLostDevice();
		void onResetDevice();

	protected:
		float mx,my,mvx,mvy;
		std::string mtext;
		std::string mfontname;
		RECT mtextsizerect;
		DWORD mcolor;
	};

public:
	State_FlyingText() {}
	virtual ~State_FlyingText() {}

	virtual void stateEnter();
	virtual void stateExecute(float dt);
	virtual void stateDraw();
	virtual void stateExit();
	virtual void stateOnLostDevice();
	virtual void stateOnResetDevice();

private:
	BaseManagerID<FlyingText*> mflyingtext;
};
