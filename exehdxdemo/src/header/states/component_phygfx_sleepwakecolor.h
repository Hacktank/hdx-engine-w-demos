#pragma once

#include "hdx/hdx9.h"
#include "hdx/entity.h"
#include "hdx/physics/physics.h"
#include "hdx/component_draw_mesh.h"
#include "hdx/hdx9mesh.h"

class component_PhyGfx_SleepWakeColor : public component_Base {
public:
	component_PhyGfx_SleepWakeColor(Entity *entity) : component_Base(entity) {
		mcomp_phy = entity->getComponent<component_Physics>(true);
		mcomp_gfx = entity->getComponent<component_Draw_Mesh>(true);

		mcbid_onupdatep1 = HDX_MAIN->registerFuncUpdatePost1([&](float dt)->void {
			if(mcomp_phy && mcomp_gfx && mcomp_phy->getBody() && mcomp_gfx->getItems().size()>0) {
				D3DXCOLOR col_target;
				if(mcomp_phy->getBody()->isAwake() || !mcomp_phy->getBody()->hasFiniteMass())
					col_target = mwakecolor;
				else
					col_target = msleepcolor;
				for(auto &gfxitm : mcomp_gfx->getItems()) {
					for(auto &mat : gfxitm->getMesh()->mmaterials) {
						mat = (HDXMaterial)col_target;
					}
				}
			}
		});
	}
	virtual ~component_PhyGfx_SleepWakeColor() {
		HDX_MAIN->unregisterFuncUpdatePost1(mcbid_onupdatep1);
	}

	const D3DXCOLOR& getWakeColor() const { return mwakecolor; }
	void setWakeColor(const D3DXCOLOR &col) { mwakecolor = col; }

	const D3DXCOLOR& getSleepColor() const { return msleepcolor; }
	void setSleepColor(const D3DXCOLOR &col) { msleepcolor = col; }

	virtual const char* getName() const { return _getName(); }
	static const char* _getName() { return "physics:graphicssleepwake"; }

private:
	int mcbid_onupdatep1;
	component_Physics *mcomp_phy;
	component_Draw_Mesh *mcomp_gfx;
	D3DXCOLOR mwakecolor;
	D3DXCOLOR msleepcolor;
};