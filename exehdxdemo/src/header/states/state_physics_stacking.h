#pragma once

#include "states/basedx9state.h"

#include "hdx/hdx9mesh.h"
#include "hdx/shader/hdx9shader_phonglighting.h"
#include "hdx/hdx9light.h"

#include "hdx/camera/hdx9cameramouselook.h"

#include "hdx/entity.h"
#include "hdx/physics/physics.h"
#include "hdx/physics/force/force.h"
#include "hdx/component_draw_mesh.h"

class HDXTexture;

class State_Physics_Stacking : public BaseDX9State {
public:
	State_Physics_Stacking() {}
	virtual ~State_Physics_Stacking() {}

	virtual void stateEnter();
	virtual void stateExecute(float dt);
	virtual void stateDraw();
	virtual void stateExit();
	virtual void stateOnLostDevice();
	virtual void stateOnResetDevice();

private:
	HDXCamera_Mouselook mcamera;
	HDXShader_PhongLighting *mhdxfx;
	HDXLight *mlight_directional,*mlight_spot;
	ForceGravityAxis *mgravityforce;
	int mcbid_onkeypress;
};
