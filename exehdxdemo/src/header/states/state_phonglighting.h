#pragma once

#include "states/basedx9state.h"

#include "hdx/hdx9mesh.h"
#include "hdx/shader/hdx9shader_phonglighting.h"
#include "hdx/hdx9light.h"

#include "hdx/camera/hdx9cameramouselook.h"

class HDXTexture;

class State_PhongLighting : public BaseDX9State {
public:
	State_PhongLighting() {}
	virtual ~State_PhongLighting() {}

	virtual void stateEnter();
	virtual void stateExecute(float dt);
	virtual void stateDraw();
	virtual void stateExit();
	virtual void stateOnLostDevice();
	virtual void stateOnResetDevice();

private:
	HDXCamera_Mouselook mcamera;

	int cbid_onrendershader;

	HDXMesh *mmesh_tank;
	HDXShader_PhongLighting *mhdxfx;

	HDXLight *mlight_directional,*mlight_point,*mlight_spot;
	HDXMesh *mmesh_light_point;

	int mcbid_lighttoggle[3];

	float mcounter_time[3];
};
