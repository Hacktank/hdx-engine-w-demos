#pragma once

#include "hdx/hdx9app.h"

#include "hdx/ui/hdx9ui.h"

struct IDirect3DDevice9;
struct ID3DXSprite;
class BaseDX9State;

class HDX9AppDemo : public HDX9App {
public:
	HDX9AppDemo();
	virtual ~HDX9AppDemo();

	virtual void initializeScene();
	virtual void deinitializeScene();

	BaseDX9State* getCurrentState();
	void setState(BaseDX9State *state);

private:
	BaseDX9State *mcurstate;
	HDXUIMenuMap *mmenu_map;
};
