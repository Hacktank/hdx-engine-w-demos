#include "hdx9appdemo.h"

#include "hdx/hdx9.h"
#include "hdx/hdx9font.h"
#include "hdx/hdx9texture.h"
#include "hdx/hdx9inputmanager.h"
#include "hdx/sound/hdx9sound.h"
#include "hdx/sound/hdx9musicmanager.h"

#include "hdx/ui/hdx9ui.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_textdraw.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_texturedraw.h"

#include "hdx/ui/effect/hdx9ui_elementeffect_transition_lerp.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_var_oscillate.h"

#include "hdx/ui/element/hdx9ui_element_alignment.h"
#include "hdx/ui/element/hdx9ui_element_button.h"
#include "hdx/ui/element/hdx9ui_element_checkbox.h"
#include "hdx/ui/element/hdx9ui_element_slider.h"

#include "states/basedx9state.h"
#include "states/state_handprim.h"
#include "states/state_phonglighting.h"
#include "states/state_spacegame.h"
#include "states/state_physics_funnel.h"
#include "states/state_physics_stacking.h"
#include "states/state_physics_launcher.h"
#include "states/state_physics_bridge.h"

#include "hdx/hdx9video.h"

#include "hdx/hdx9mesh.h"

#include "hdx/messagemanager.h"

HDX9AppDemo::HDX9AppDemo() {
	mcurstate = 0;
}

HDX9AppDemo::~HDX9AppDemo() {
}

#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_utils.hpp>

void HDX9AppDemo::initializeScene() {
	HDX_MAIN->setWindowTitle("Jacob Tyndall");
	HDX_MAIN->setWindowSize(640,480);
	//HDX_MAIN->setWindowBorderless(true);
	//HDX_MAIN->setWindowVsync(false);
	//HDX_MAIN->setWindowFullscreen(true);

	{
		//put the window to the top right side of the screen, out of the way
		//if fullscreen is active this algorithm generates position 0,0 anyway
		int ww,wh,sw,sh;
		HDX_MAIN->getWindowSize(&ww,&wh);
		sw = GetSystemMetrics(SM_CXSCREEN);
		sh = GetSystemMetrics(SM_CYSCREEN);
		RECT wrect;
		SetRect(&wrect,0,0,ww,wh);
		AdjustWindowRect(&wrect,HDX_MAIN->getWindowCurrentStyle(),false);
		int tx,ty;
		tx = sw - ww + wrect.left;
		ty = -wrect.top;
		HDX_MAIN->setWindowPosition(tx,ty);
	}

	HDX9PlayVideoFile(L"data/video/intro.wmv",HDX_MAIN->getWindowHWND());

	HDX_FONTMAN->createFontDX("DX:Arial_10","Arial",10,FW_BOLD);
	HDX_FONTMAN->createFontDX("DX:Arial_15","Arial",15,FW_BOLD);
	HDX_FONTMAN->createFontDX("DX:Castellar","Castellar",20,FW_BOLD);

	HDX_SOUNDMAN->getChannelGroup("music")->setVolume(0.5f); //max volume is undesired for a default

	/*
	* Load sounds
	*
	**/

	HDX9Sound *snd_gui_mouse_enter = HDX_SOUNDMAN->loadSoundFAF("data/sound/gui_tick_01.wav",FMOD_DEFAULT);
	HDX9Sound *snd_gui_mouse_click = HDX_SOUNDMAN->loadSoundFAF("data/sound/gui_tick_02.wav",FMOD_DEFAULT);
	HDX9Sound *snd_gui_menu_transition_enter = HDX_SOUNDMAN->loadSoundFAF("data/sound/gui_swish_in.wav",FMOD_DEFAULT);
	HDX9Sound *snd_gui_menu_transition_exit = HDX_SOUNDMAN->loadSoundFAF("data/sound/gui_swish_out.wav",FMOD_DEFAULT);
	HDX9Sound *snd_music_menu = HDX_SOUNDMAN->loadSoundStream("data/sound/music_menu.mp3",FMOD_DEFAULT);

	/*
	* Set up menus
	*
	*/

	std::string tex_menubackground("data/texture/ui/menubackground.png");

	std::string tex_basebutton("data/texture/ui/basebutton.png");
	std::string tex_basebutton_hover("data/texture/ui/basebutton_hover.png");

	std::string tex_basecheckbox("data/texture/ui/basecheckbox.png");
	std::string tex_basecheckbox_hover("data/texture/ui/basecheckbox_hover.png");
	std::string tex_basecheckbox_check("data/texture/ui/basecheckbox_check.png");

	std::string tex_baseslider("data/texture/ui/baseslider.png");
	std::string tex_baseslider_handle("data/texture/ui/baseslider_handle.png");

	int ww,wh;
	HDX_MAIN->getWindowSize(&ww,&wh);

	mmenu_map = new HDXUIMenuMap();

	HDX_MAIN->registerFuncUpdate([&](float dt)->void {
		if(getCurrentState()) getCurrentState()->stateExecute(dt);

		auto lam_colorRandom = [](bool alpha)->unsigned {
			return ((alpha ? ((rand_getint())&0xff)<<24 : 0xff000000) | ((rand_getint())&0xff)<<16 | ((rand_getint())&0xff)<<8 | ((rand_getint())&0xff));
		};

		auto lam_colorFade = [](int col_start,int col_end,float progress) {
			if(progress>1.0f) progress = 1.0f;
			int ret = col_start;
			unsigned char *col[3];
			col[0] = (unsigned char*)&col_start;
			col[1] = (unsigned char*)&col_end;
			col[2] = (unsigned char*)&ret;
			for(int i = 0; i < 4; i++) {
				col[2][i] = (unsigned char)CLAMP(col[0][i] + (float)(col[1][i]-col[0][i]) * progress,0,255);
			}
			return ret;
		};

		static unsigned col_points[2] = {lam_colorRandom(false),lam_colorRandom(false)};
		static unsigned int col_curi = 0;
		unsigned int col_max = HDX_MAIN->getFPS()*2;

		//only cross fade random colors when in menu
		if(getCurrentState() == 0) {
			if(col_curi > col_max) {
				col_points[0] = col_points[1];
				col_points[1] = lam_colorRandom(false);
				col_curi = 0;
			}

			HDX_MAIN->setWindowClearColor(lam_colorFade(col_points[0],col_points[1],CLAMP(col_curi/(float)col_max,0.0f,1.0f)));
			col_curi++;
		}

		; {
			static int ww_prev = -1,wh_prev = -1;
			int ww_old,wh_old,ww_new,wh_new;
			ww_old = ww_prev;
			wh_old = wh_prev;
			RECT rect;
			GetClientRect(HDX_MAIN->getWindowHWND(),&rect);
			ww_new = RECTWIDTH(rect);
			wh_new = RECTHEIGHT(rect);

			if((ww_prev>=0&&wh_prev>=0) && (ww_prev!=ww_new || wh_prev!=wh_new)) {
				mmenu_map->doIterate([&](HDXUIMenu *item)->bool {
					HDXVector2 delta((ww_new-ww_old)/2.0f,(wh_new-wh_old)/2.0f);
					item->setPosition(&(item->getPosition()+delta));
					return false;
				});
			}

			ww_prev = ww_new;
			wh_prev = wh_new;
		}

		MSGMAN->update();

		mmenu_map->update(dt);
	});

	HDX_MAIN->registerFuncRenderSprite([&](ID3DXSprite *com)->void {
		mmenu_map->render();
	});

	HDX_MAIN->registerFuncRenderDevice([&](IDirect3DDevice9 *com) {
		if(getCurrentState()) getCurrentState()->stateDraw();

		HDXFontBase *font = HDX_FONTMAN->getFont("DX:Arial_10");
		if(font) {
			char buff[20];
			sprintf_s(buff,"FPS: %i",HDX_MAIN->getFPS());
			font->drawText(buff,HDX_RECT_POINT(5,5),DT_NOCLIP,0xffffffff);
		}
	});

	HDX_MAIN->registerFuncOnDeviceLost([&]()->void {
		if(getCurrentState()) getCurrentState()->stateOnLostDevice();
	});

	HDX_MAIN->registerFuncOnDeviceReset([&]()->void {
		if(getCurrentState()) getCurrentState()->stateOnResetDevice();
	});

	HDX_MAIN->registerFuncOnWndProc([&](HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam) {
		switch(msg) {
			case WM_KEYDOWN: {
				switch(wparam) {
					case VK_ESCAPE:	{
						setState(0);
						mmenu_map->navigateToMenu("menu_main",true);
						break;
					}
					case VK_F11: {
						HDX_MAIN->setWindowBorderless(!HDX_MAIN->getWindowBorderless());
						break;
					}
					case VK_F12: {
						HDX_MAIN->setWindowFullscreen(!HDX_MAIN->getWindowFullscreen());
						break;
					}
				}
				break;
			}
		}
	});

	// *************
	//
	// Main Menu
	//
	// *************

	{
		HDXUIMenu *menu_mainmenu = mmenu_map->createMenu("menu_main");

		menu_mainmenu->registerCallbackOnTransitionEnter([=](bool dotransition)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_enter,"gui");
			HDX_MUSICMAN->setTrackCrossfade(snd_music_menu,3.0f);
		});

		menu_mainmenu->registerCallbackOnTransitionExit([=](bool dotransition)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_exit,"gui");
		});

		auto menu_mainmenu_texturedraw = menu_mainmenu->createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																									 tex_menubackground);

		menu_mainmenu->setActionSize(512.0f,
									 384.0f);

		menu_mainmenu->setPosition(ww+menu_mainmenu->getActionWidth()*0.6f,
								   wh/2.0f);

		auto menu_mainmenu_transition_lerp_position = menu_mainmenu->createEffect<HDXUIElementEffect_Transition_LERP<HDXVector2>>("LERP_position",
																																   [=](void) {return menu_mainmenu->getPosition(); },
																																   [=](const HDXVector2 &var) {return menu_mainmenu->setPosition(var); },
																																   HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																																   0.75f,
																																   HDXVector2(ww/2.0f,wh/2.0f));

		auto menu_mainmenu_transition_lerp_rotation = menu_mainmenu->createEffect<HDXUIElementEffect_Transition_LERP<float>>("LERP_rotation",
																															 [=](void) {return menu_mainmenu->getRotation(); },
																															 [=](const float &var) {return menu_mainmenu->setRotation(var); },
																															 HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																															 0.75f,
																															 (float)M_PI*2.0f);

		auto menu_mainmenu_oscillation_rotation = menu_mainmenu->createEffect<HDXUIElementEffect_Var_Oscillate<float>>("oscillate_rotation",
																													   [=](void) {return menu_mainmenu->getRotation(); },
																													   [=](const float &var) {return menu_mainmenu->setRotation(var); },
																													   (float)M_PI/32.0f,
																													   5.0f);

		auto alignment_center = menu_mainmenu->createChildElement<HDXUIElement_Alignment>("alignment_center",
																						  HDXAL_CENTER,
																						  VEC2TOARG(YUNIT2),
																						  0.0f,
																						  0.0f,
																						  10.0f);

		alignment_center->setPosition(0,
									  -menu_mainmenu->getActionHeight()/4+alignment_center->getPadding()*2);

		auto button_demos = alignment_center->createChildElement<HDXUIElement_Button>("button_demos",
																					  tex_basebutton,
																					  tex_basebutton_hover,
																					  "DX:Arial_15",
																					  "Demos",
																					  DT_CENTER|DT_VCENTER,
																					  0xffffffff);
		button_demos->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demos->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("menu_demos",true);
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_options = alignment_center->createChildElement<HDXUIElement_Button>("button_options",
																						tex_basebutton,
																						tex_basebutton_hover,
																						"DX:Arial_15",
																						"Options",
																						DT_CENTER|DT_VCENTER,
																						0xffffffff);
		button_options->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_options->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("menu_options",true);
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_credits = alignment_center->createChildElement<HDXUIElement_Button>("button_credits",
																						tex_basebutton,
																						tex_basebutton_hover,
																						"DX:Arial_15",
																						"Credits",
																						DT_CENTER|DT_VCENTER,
																						0xffffffff);
		button_credits->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_credits->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("menu_credits",true);
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		alignment_center->jumpDistance(alignment_center->getPadding()*10.0f);

		auto button_quit = alignment_center->createChildElement<HDXUIElement_Button>("button_quit",
																					 tex_basebutton,
																					 tex_basebutton_hover,
																					 "DX:Arial_15",
																					 "Quit",
																					 DT_CENTER|DT_VCENTER,
																					 0xffff0000);
		button_quit->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_quit->registerCallbackOnMousePress([=](WPARAM key)->void {
			PostQuitMessage(0);
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_quit_oscillation_position_x = button_quit->createEffect<HDXUIElementEffect_Var_Oscillate<float>>("oscillate_position_x",
																													 [=](void) {return button_quit->getPosition().x; },
																													 [=](const float &var) {return button_quit->setPosition(var,button_quit->getPosition().y); },
																													 50.0f,
																													 5.0f,
																													 0.0f);

		auto button_quit_oscillation_position_y = button_quit->createEffect<HDXUIElementEffect_Var_Oscillate<float>>("oscillate_position_y",
																													 [=](void) {return button_quit->getPosition().y; },
																													 [=](const float &var) {return button_quit->setPosition(button_quit->getPosition().x,var); },
																													 10.0f,
																													 1.5f,
																													 0.5f);
	}

	// *************
	//
	// Credits Menu
	//
	// *************

	{
		HDXUIMenu *menu_credits = mmenu_map->createMenu("menu_credits");

		menu_credits->registerCallbackOnTransitionEnter([=](bool dotransition)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_enter,"gui");
		});

		menu_credits->registerCallbackOnTransitionExit([=](bool dotransition)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_exit,"gui");
		});

		auto menu_credits_texturedraw = menu_credits->createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																								   tex_menubackground);
		menu_credits->setActionSize(512.0f,
									384.0f);

		menu_credits->setPosition(ww/2.0f,
								  wh+menu_credits->getActionHeight()*0.6f);

		auto menu_credits_transition_lerp_position = menu_credits->createEffect<HDXUIElementEffect_Transition_LERP<HDXVector2>>("LERP_position",
																																 [=](void) {return menu_credits->getPosition(); },
																																 [=](const HDXVector2 &var) {return menu_credits->setPosition(var); },
																																 HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																																 0.75f,
																																 HDXVector2(ww/2.0f,wh/2.0f));

		menu_credits->setScale(1.0f,0.0f);

		auto menu_credits_transition_lerp_scale = menu_credits->createEffect<HDXUIElementEffect_Transition_LERP<HDXVector2>>("LERP_scale",
																															  [=](void) {return menu_credits->getScale(); },
																															  [=](const HDXVector2 &var) {return menu_credits->setScale(var); },
																															  HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																															  0.75f,
																															  HDXVector2(1.0f,1.0f));

		auto button_back = menu_credits->createChildElement<HDXUIElement_Button>("button_back",
																				 tex_basebutton,
																				 tex_basebutton_hover,
																				 "DX:Arial_15",
																				 "Back",
																				 DT_CENTER|DT_VCENTER,
																				 0xffffffff);

		button_back->setPosition(-menu_credits->getActionWidth()/2+20.0f+button_back->getActionWidth()/2,
								 menu_credits->getActionHeight()/2-50.0f-button_back->getActionHeight()/2);

		button_back->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_back->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateBackwards(true);
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto alignment_center = menu_credits->createChildElement<HDXUIElement_Alignment>("alignment_center",
																						 HDXAL_LEFT,
																						 VEC2TOARG(YUNIT2),
																						 0.0f,
																						 0.0f,
																						 5.0f);

		alignment_center->setPosition(-menu_credits->getActionWidth()/4+alignment_center->getPadding()+10,
									  -menu_credits->getActionHeight()/4+alignment_center->getPadding()+10);

		std::vector<std::string> items = {
			"Design, Programming, GUI Art:\n"
			"   Jacob Tyndall (Hacktank@Hacktank.NET)\n",
			"--------\n",
			"Music:\n"
			"   Kevin McLeod (kevin@incompetech.com)\n",
			"--------\n",
			"Sound Effects:\n"
			"   http://www.freesound.org/",
			"--------\n",
			"Models/Textures:\n"
			"   SolCommand: Asteroids (http://www.sharecg.com/solcommand)\n"
			"   3DRegenerator: Player's Ship (http://tf3dm.com/user/3dregenerator)\n"
			"   RazorBreak: Enemy's Ship (http://tf3dm.com/user/razorbreak)\n"
		};

		for(unsigned int i = 0; i < items.size(); i++) {
			char buff[20];
			sprintf_s(buff,"textelement_%i",i);
			auto newitem = alignment_center->createChildElement<HDXUIElementBase>(buff);

			RECT text_draw_rect;
			HDXFontBase *font = HDX_FONTMAN->getFont("DX:Arial_10");
			font->calculateFormattedTextDrawRect(items[i].c_str(),&text_draw_rect,DT_LEFT,0,0,0,0);

			newitem->setActionSize((float)RECTWIDTH(text_draw_rect),
								   (float)RECTHEIGHT(text_draw_rect));

			newitem->setPosition(newitem->getPosition()+HDXVector2(newitem->getActionWidth()/2.0f,0.0f));

			alignment_center->jumpDistance(alignment_center->getDirection()*newitem->getActionSize());

			auto text_effect = newitem->createEffect<HDXUIElementEffect_TextDraw>("text_main",
																				  "DX:Arial_10",
																				  items[i],
																				  DT_LEFT,
																				  0xffffffff);
		}

		auto button_demo0 = alignment_center->createChildElement<HDXUIElementBase>("button_demo0");
	}

	// *************
	//
	// Demo Menu
	//
	// *************

	{
		HDXUIMenu *menu_demos = mmenu_map->createMenu("menu_demos");

		menu_demos->registerCallbackOnTransitionEnter([=](bool dotransition)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_enter,"gui");
		});

		menu_demos->registerCallbackOnTransitionExit([=](bool dotransition)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_exit,"gui");
		});

		auto menu_demos_texturedraw = menu_demos->createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																							   tex_menubackground);
		menu_demos->setActionSize(512.0f,
								  384.0f);

		menu_demos->setPosition(ww/2.0f,
								wh+menu_demos->getActionHeight()*0.6f);

		auto menu_demos_transition_lerp_position = menu_demos->createEffect<HDXUIElementEffect_Transition_LERP<HDXVector2>>("LERP_position",
																															 [=](void) {return menu_demos->getPosition(); },
																															 [=](const HDXVector2 &var) {return menu_demos->setPosition(var); },
																															 HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																															 0.75f,
																															 HDXVector2(ww/2.0f,wh/2.0f));

		menu_demos->setScale(0.0f,1.0f);

		auto menu_demos_transition_lerp_scale = menu_demos->createEffect<HDXUIElementEffect_Transition_LERP<HDXVector2>>("LERP_scale",
																														  [=](void) {return menu_demos->getScale(); },
																														  [=](const HDXVector2 &var) {return menu_demos->setScale(var); },
																														  HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																														  0.75f,
																														  HDXVector2(1.0f,1.0f));

		auto menu_demos_oscillation_scale_y = menu_demos->createEffect<HDXUIElementEffect_Var_Oscillate<float>>("oscillate_scale_y",
																												[=](void) {return menu_demos->getScale().y; },
																												[=](const float &var) {return menu_demos->setScale(menu_demos->getScale().x,var); },
																												0.25f,
																												5.0f,
																												0.0f);

		auto button_back = menu_demos->createChildElement<HDXUIElement_Button>("button_back",
																			   tex_basebutton,
																			   tex_basebutton_hover,
																			   "DX:Arial_15",
																			   "Back",
																			   DT_CENTER|DT_VCENTER,
																			   0xffffffff);

		button_back->setPosition(-menu_demos->getActionWidth()/2+20.0f+button_back->getActionWidth()/2,
								 menu_demos->getActionHeight()/2-50.0f-button_back->getActionHeight()/2);

		button_back->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_back->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateBackwards(true);
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto alignment_center = menu_demos->createChildElement<HDXUIElement_Alignment>("alignment_center",
																					   HDXAL_CENTER,
																					   VEC2TOARG(YUNIT2),
																					   0.0f,
																					   0.0f,
																					   10.0f);

		alignment_center->setPosition(0,
									  -menu_demos->getActionHeight()/2.5f+alignment_center->getPadding()*2);
		/*
		auto button_demo0 = alignment_center->createChildElement<HDXUIElement_Button>("button_demo0",
		tex_basebutton,
		tex_basebutton_hover,
		"DX:Arial_15",
		"Window & Text",
		DT_CENTER|DT_VCENTER,
		0xffffffff);
		button_demo0->registerCallbackOnMouseEnter([=](void)->void {
		HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demo0->registerCallbackOnMousePress([=](WPARAM key)->void {
		setState(new State_FlyingText());
		mmenu_map->navigateToMenu("",true);
		HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_demo1 = alignment_center->createChildElement<HDXUIElement_Button>("button_demo1",
		tex_basebutton,
		tex_basebutton_hover,
		"DX:Arial_15",
		"Textured Pyramid",
		DT_CENTER|DT_VCENTER,
		0xffffffff);
		button_demo1->registerCallbackOnMouseEnter([=](void)->void {
		HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demo1->registerCallbackOnMousePress([=](WPARAM key)->void {
		setState(new State_HandPrim());
		mmenu_map->navigateToMenu("",true);
		HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});
		

		auto button_demo2 = alignment_center->createChildElement<HDXUIElement_Button>("button_demo2",
																					  tex_basebutton,
																					  tex_basebutton_hover,
																					  "DX:Arial_15",
																					  "Phong Lighting",
																					  DT_CENTER|DT_VCENTER,
																					  0xffffffff);
		button_demo2->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demo2->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("",true);
			setState(new State_PhongLighting());
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});
		*/

		auto button_demo3 = alignment_center->createChildElement<HDXUIElement_Button>("button_demo3",
																					  tex_basebutton,
																					  tex_basebutton_hover,
																					  "DX:Arial_15",
																					  "Space Chase Game",
																					  DT_CENTER|DT_VCENTER,
																					  0xffffffff);
		button_demo3->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demo3->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("",true);
			setState(new State_SpaceGame());
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_demo4 = alignment_center->createChildElement<HDXUIElement_Button>("button_demo4",
																					  tex_basebutton,
																					  tex_basebutton_hover,
																					  "DX:Arial_15",
																					  "XML Test Menu",
																					  DT_CENTER|DT_VCENTER,
																					  0xffffffff);
		button_demo4->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demo4->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("menu_xmltest",true);
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_demo5 = alignment_center->createChildElement<HDXUIElement_Button>("button_demo5",
																					  tex_basebutton,
																					  tex_basebutton_hover,
																					  "DX:Arial_15",
																					  "Physics: Funnel",
																					  DT_CENTER|DT_VCENTER,
																					  0xffffffff);
		button_demo5->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demo5->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("",true);
			setState(new State_Physics_Funnel());
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_demo6 = alignment_center->createChildElement<HDXUIElement_Button>("button_demo6",
																					  tex_basebutton,
																					  tex_basebutton_hover,
																					  "DX:Arial_15",
																					  "Physics: Stacking",
																					  DT_CENTER|DT_VCENTER,
																					  0xffffffff);
		button_demo6->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demo6->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("",true);
			setState(new State_Physics_Stacking());
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_demo7 = alignment_center->createChildElement<HDXUIElement_Button>("button_demo7",
																					  tex_basebutton,
																					  tex_basebutton_hover,
																					  "DX:Arial_15",
																					  "Physics: Launcher",
																					  DT_CENTER|DT_VCENTER,
																					  0xffffffff);
		button_demo7->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demo7->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("",true);
			setState(new State_Physics_Launcher());
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_demo8 = alignment_center->createChildElement<HDXUIElement_Button>("button_demo8",
																					  tex_basebutton,
																					  tex_basebutton_hover,
																					  "DX:Arial_15",
																					  "Physics: Bridge",
																					  DT_CENTER|DT_VCENTER,
																					  0xffffffff);
		button_demo8->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_demo8->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateToMenu("",true);
			setState(new State_Physics_Bridge());
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});
	}

	// *************
	//
	// Options Menu
	//
	// *************

	{
		HDXUIMenu *menu_options = mmenu_map->createMenu("menu_options");

		menu_options->registerCallbackOnTransitionEnter([=](bool dotransition)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_enter,"gui");
		});

		menu_options->registerCallbackOnTransitionExit([=](bool dotransition)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_menu_transition_exit,"gui");
		});

		auto menu_demos_texturedraw = menu_options->createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																								 tex_menubackground);
		menu_options->setActionSize(512.0f,
									384.0f);

		menu_options->setPosition(ww/2.0f,
								  wh/2.0f);

		menu_options->setScale(0.0f,0.0f);

		auto menu_options_transition_lerp_scale = menu_options->createEffect<HDXUIElementEffect_Transition_LERP<HDXVector2>>("LERP_scale",
																															  [=](void) {return menu_options->getScale(); },
																															  [=](const HDXVector2 &var) {return menu_options->setScale(var); },
																															  HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																															  1.0f,
																															  HDXVector2(1.0f,1.0f));

		auto button_back = menu_options->createChildElement<HDXUIElement_Button>("button_back",
																				 tex_basebutton,
																				 tex_basebutton_hover,
																				 "DX:Arial_15",
																				 "Back",
																				 DT_CENTER|DT_VCENTER,
																				 0xffffffff);

		button_back->setPosition(-menu_options->getActionWidth()/2+20.0f+button_back->getActionWidth()/2,
								 menu_options->getActionHeight()/2-50.0f-button_back->getActionHeight()/2);

		button_back->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		button_back->registerCallbackOnMousePress([=](WPARAM key)->void {
			mmenu_map->navigateBackwards(true);
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto button_back_oscillation_scale_x = button_back->createEffect<HDXUIElementEffect_Var_Oscillate<float>>("oscillate_scale_x",
																												  [=](void) {return button_back->getScale().x; },
																												  [=](const float &var) {return button_back->setScale(var,button_back->getScale().y); },
																												  0.15f,
																												  5.3f,
																												  0.0f);

		auto button_back_oscillation_scale_y = button_back->createEffect<HDXUIElementEffect_Var_Oscillate<float>>("oscillate_scale_y",
																												  [=](void) {return button_back->getScale().y; },
																												  [=](const float &var) {return button_back->setScale(button_back->getScale().x,var); },
																												  0.15f,
																												  4.0f,
																												  0.0f);

		auto alignment_center = menu_options->createChildElement<HDXUIElement_Alignment>("alignment_center",
																						 HDXAL_CENTER,
																						 VEC2TOARG(YUNIT2),
																						 0.0f,
																						 0.0f,
																						 10.0f);

		alignment_center->setScale(0.7f,0.7f);

		alignment_center->setPosition(0,
									  -menu_options->getActionHeight()/4+alignment_center->getPadding()*2);

		auto checkbox_vsync = alignment_center->createChildElement<HDXUIElement_Checkbox>("checkbox_vsync",
																						  tex_basecheckbox,
																						  tex_basecheckbox_hover,
																						  tex_basecheckbox_check,
																						  "DX:Arial_15",
																						  "VSync",
																						  DT_CENTER|DT_VCENTER,
																						  0xffffffff);

		checkbox_vsync->setChecked(HDX_MAIN->getWindowBorderless());

		checkbox_vsync->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		checkbox_vsync->registerCallbackOnMousePress([=](WPARAM key)->void {
			HDX_MAIN->setWindowVsync(!HDX_MAIN->getWindowVsync());
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto checkbox_borderless = alignment_center->createChildElement<HDXUIElement_Checkbox>("checkbox_borderless",
																							   tex_basecheckbox,
																							   tex_basecheckbox_hover,
																							   tex_basecheckbox_check,
																							   "DX:Arial_15",
																							   "Borderless (windowed)",
																							   DT_CENTER|DT_VCENTER,
																							   0xffffffff);

		checkbox_borderless->setChecked(HDX_MAIN->getWindowBorderless());

		checkbox_borderless->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		checkbox_borderless->registerCallbackOnMousePress([=](WPARAM key)->void {
			HDX_MAIN->setWindowBorderless(!HDX_MAIN->getWindowBorderless());
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		auto checkbox_fullscreen = alignment_center->createChildElement<HDXUIElement_Checkbox>("checkbox_fullscreen",
																							   tex_basecheckbox,
																							   tex_basecheckbox_hover,
																							   tex_basecheckbox_check,
																							   "DX:Arial_15",
																							   "Fullscreen",
																							   DT_CENTER|DT_VCENTER,
																							   0xffffffff);

		checkbox_fullscreen->setChecked(HDX_MAIN->getWindowFullscreen());

		checkbox_fullscreen->registerCallbackOnMouseEnter([=](void)->void {
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
		});
		checkbox_fullscreen->registerCallbackOnMousePress([=](WPARAM key)->void {
			checkbox_borderless->setEnabled(HDX_MAIN->getWindowFullscreen());
			checkbox_borderless->getMainTextureEffect()->setBlendColor(checkbox_borderless->isEnabled() ? 0xffffffff : 0xff555555);
			HDX_MAIN->setWindowFullscreen(!HDX_MAIN->getWindowFullscreen());
			HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_click,"gui");
		});

		alignment_center->jumpDistance(alignment_center->getPadding()*3);

		{
			float voltmp;

			auto slider_volume_master = alignment_center->createChildElement<HDXUIElement_Slider>("slider_volume_master",
																								  tex_baseslider,
																								  tex_baseslider_handle,
																								  "DX:Arial_15",
																								  "Master",
																								  DT_CENTER|DT_VCENTER,
																								  0xffffffff);
			HDX_SOUNDMAN->getChannelGroup("master")->getVolume(&voltmp);
			slider_volume_master->setValue(voltmp);
			slider_volume_master->getHandle()->registerCallbackOnMouseEnter([=](void)->void {
				HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
			});
			slider_volume_master->registerCallbackOnValueChange([=](float value)->void {
				HDX_SOUNDMAN->getChannelGroup("master")->setVolume(value);
			});

			auto slider_volume_gui = alignment_center->createChildElement<HDXUIElement_Slider>("slider_volume_gui",
																							   tex_baseslider,
																							   tex_baseslider_handle,
																							   "DX:Arial_15",
																							   "GUI",
																							   DT_CENTER|DT_VCENTER,
																							   0xffffffff);
			HDX_SOUNDMAN->getChannelGroup("gui")->getVolume(&voltmp);
			slider_volume_gui->setValue(voltmp);
			slider_volume_gui->getHandle()->registerCallbackOnMouseEnter([=](void)->void {
				HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
			});
			slider_volume_gui->registerCallbackOnValueChange([=](float value)->void {
				HDX_SOUNDMAN->getChannelGroup("gui")->setVolume(value);
			});

			auto slider_volume_music = alignment_center->createChildElement<HDXUIElement_Slider>("slider_volume_music",
																								 tex_baseslider,
																								 tex_baseslider_handle,
																								 "DX:Arial_15",
																								 "Music",
																								 DT_CENTER|DT_VCENTER,
																								 0xffffffff);
			HDX_SOUNDMAN->getChannelGroup("music")->getVolume(&voltmp);
			slider_volume_music->setValue(voltmp);
			slider_volume_music->getHandle()->registerCallbackOnMouseEnter([=](void)->void {
				HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
			});
			slider_volume_music->registerCallbackOnValueChange([=](float value)->void {
				HDX_SOUNDMAN->getChannelGroup("music")->setVolume(value);
			});

			auto slider_volume_fx = alignment_center->createChildElement<HDXUIElement_Slider>("slider_volume_fx",
																							  tex_baseslider,
																							  tex_baseslider_handle,
																							  "DX:Arial_15",
																							  "FX",
																							  DT_CENTER|DT_VCENTER,
																							  0xffffffff);
			HDX_SOUNDMAN->getChannelGroup("fx")->getVolume(&voltmp);
			slider_volume_fx->setValue(voltmp);
			slider_volume_fx->getHandle()->registerCallbackOnMouseEnter([=](void)->void {
				HDX_SOUNDMAN->playSoundChannelGroup(snd_gui_mouse_enter,"gui");
			});
			slider_volume_fx->registerCallbackOnValueChange([=](float value)->void {
				HDX_SOUNDMAN->getChannelGroup("fx")->setVolume(value);
			});
		}
	}

	mmenu_map->createFromXML("data/ui/menu_xmltest.xml");

	mmenu_map->navigateToMenu("menu_main",true);
}

void HDX9AppDemo::deinitializeScene() {
}

BaseDX9State* HDX9AppDemo::getCurrentState() {
	return (BaseDX9State*)mcurstate;
}

void HDX9AppDemo::setState(BaseDX9State *state) {
	if(mcurstate) {
		mcurstate->stateExit();
		delete mcurstate;
	}
	mcurstate = state;
	if(mcurstate) mcurstate->stateEnter();
}