#include <windows.h>
#pragma comment(lib, "Winmm.lib") //timeGetTime()

#include "hdx/hdx9.h"
#include "hdx/sound/hdx9sound.h"
#include "hdx9appdemo.h"

#ifdef _DEBUG
#pragma comment(lib, "hdxd.lib")
#else
#pragma comment(lib, "hdx.lib")
#endif

int WINAPI WinMain(HINSTANCE hinstance,HINSTANCE hprevinstance,LPSTR lpcmdline,int ncmdshow) {
	srand((unsigned int)timeGetTime());

	if(HDX_SOUNDMAN->initializeFMOD(500) != FMOD_OK) return 1;

	HDX9AppDemo app;

	HDX_MAIN->initializeNewWindow(hinstance,100,100,640,480);

	app.initializeScene();

	MSG msg;
	memset(&msg,0,sizeof(MSG));

	while(msg.message != WM_QUIT) {
		if(PeekMessage(&msg,0,0,0,PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
			HDX_SOUNDMAN->update();
			HDX_MAIN->step();
		}
	}

	app.deinitializeScene();

	HDX_MAIN->deinitialize();

	return 0;
}