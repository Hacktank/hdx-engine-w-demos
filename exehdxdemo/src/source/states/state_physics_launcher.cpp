#include "states/state_physics_launcher.h"
#include "hdx/spacialpartition.h"
#include "hdx/component_draw_mesh.h"
#include "hdx/hdx9inputmanager.h"
#include "hdx/hdx9font.h"

#include "hdx/util.h"
#include "hdx/hdx9math.h"
#include "hdx/hdx9geometry.h"

#include "states/component_phygfx_sleepwakecolor.h"

void State_Physics_Launcher::stateEnter() {
	HDX_MAIN->setWindowClearColor(0xff050522);

	mcamera.initialize(0.001f,
					   0.001f,
					   75.0f,
					   'W',
					   'A',
					   'S',
					   'D',
					   VK_SPACE,
					   VK_MENU,
					   VK_CONTROL,
					   VK_SHIFT,
					   VK_RBUTTON);
	HDX_MAIN->setCamera(&mcamera);

	mcamera.setPosition(HDXVector3(0,100,-100));
	mcamera.setLookAt(HDXVector3(0,0,0));

	mhdxfx = new HDXShader_PhongLighting(HDX_MAIN->getD3DDevice(),"data/shader/lighting.fx","phong_tech");
	HDX_MAIN->setShader(mhdxfx);

	mlight_directional = mhdxfx->createLight();
	mlight_directional->setLightDirectional(HDXCOL_WHITE*0.1f,
											HDXCOL_WHITE*0.8f,
											HDXCOL_BLUE*0.15f,
											HDXVector3(0,-1,3));
	mlight_directional->setEnabled(true);

	mlight_spot = mhdxfx->createLight();
	mlight_spot->setLightSpot(HDXCOL_BLACK,
							  HDXCOL_WHITE*0.1f,
							  HDXCOL_WHITE*0.5f,
							  HDXVector3(0,0,0),
							  HDXVector3(0.5f,0.03f,0),
							  ZUNIT3,
							  10.0f,
							  (float)M_PI*0.3f*0.6f,
							  (float)M_PI*0.3f);
	mlight_spot->setEnabled(true);

	mgravityforce = new ForceGravityAxis(YUNIT3*-40.0f);
	mgravityforce->setAwakensBody(false);

	const HDXVector3 _PEDESTAL_EXTENTS(20,15,20);
	const HDXVector3 _PEDESTAL_POSITION(0,_PEDESTAL_EXTENTS.y,0);
	const float _PEDESTAL_MASS = 0.0f;
	const HDXVector3 _LAUNCHBOARD_EXTENTS(25,0.5f,5);
	const float _LAUNCHBOARD_MASS = 30.0f;
	const HDXVector3 _WEIGHT_EXTENTS(8,8,8);
	const float _WEIGHT_MASS = 250.0f;
	const float _WEIGHT_FALLDST = 50.0f;
	const unsigned _STACK_NUM = 5;
	const HDXVector3 _STACK_EXTENTS(3,3,3);
	const float _STACK_MASS = 10.0f;
	const float _STACK_SPACING = 0.5f;

	auto lam_makePhysGfxBox = [=](float mass,const HDXVector3 &halfsize,const HDXVector3 &pos,const HDXQuaternion &ori,unsigned color)->Entity* {
		Entity *ent = HDX_ENTMAN->createEntity();
		auto comp_phygfxcol = ent->getComponent<component_PhyGfx_SleepWakeColor>(true);
		comp_phygfxcol->setWakeColor(color);
		comp_phygfxcol->setSleepColor(HDXCOL_WHITE*0.1f);
		auto comp_phy = ent->getComponent<component_Physics>(true);
		auto comp_trans = ent->getComponent<component_Transform>(true);
		auto comp_gfx = ent->getComponent<component_Draw_Mesh>(true);
		comp_phy->initializeBox(halfsize,mass);
		comp_trans->setPosition(pos);
		comp_trans->setOrientation(ori);
		int gfxid = comp_gfx->addBox(HDXMatrix4x4::gen::identity(),HDXMaterial(color),halfsize);
		comp_gfx->getItem(gfxid)->setWireframe(!mhdxfx->isLightingEnabled());
		return ent;
	};

	auto lam_makePhysGfxSphere = [=](float mass,float r,const HDXVector3 &pos,const HDXQuaternion &ori,unsigned color)->Entity* {
		Entity *ent = HDX_ENTMAN->createEntity();
		auto comp_phygfxcol = ent->getComponent<component_PhyGfx_SleepWakeColor>(true);
		comp_phygfxcol->setWakeColor(color);
		comp_phygfxcol->setSleepColor(HDXCOL_WHITE*0.1f);
		auto comp_phy = ent->getComponent<component_Physics>(true);
		auto comp_trans = ent->getComponent<component_Transform>(true);
		auto comp_gfx = ent->getComponent<component_Draw_Mesh>(true);
		comp_phy->initializeSphere(r,mass);
		comp_trans->setPosition(pos);
		comp_trans->setOrientation(ori);
		int gfxid = comp_gfx->addSphere(HDXMatrix4x4::gen::identity(),HDXMaterial(color),r);
		comp_gfx->getItem(gfxid)->setWireframe(!mhdxfx->isLightingEnabled());
		return ent;
	};

	auto lam_makePhysGfxHalfspace = [=](const HDXVector3 &pos,const HDXVector3 &normal,unsigned color)->Entity* {
		Entity *ent = HDX_ENTMAN->createEntity();
		auto comp_phy = ent->getComponent<component_Physics>(true);
		auto comp_trans = ent->getComponent<component_Transform>(true);
		auto comp_gfx = ent->getComponent<component_Draw_Mesh>(true);
		comp_phy->initializeHalfspace(pos,normal);
		comp_gfx->addBox(HDXMatrix4x4::gen::identity(),HDXMaterial(color),HDXVector3(0.1f,500,500));
		comp_phy->getBody()->setFriction(2.5f);
		return ent;
	};

	auto lam_makeStack = [=](const HDXVector3 &basepos,float spacing,unsigned num,const HDXVector3 &hsmin,const HDXVector3 &hsmax,float rotmin,float rotmax,float mass)->void {
		HDXVector3 curpos = basepos;
		for(unsigned i = 0; i < num; i++) {
			HDXVector3 ex(FRANDRANGE(hsmin.x,hsmax.x),
						  FRANDRANGE(hsmin.y,hsmax.y),
						  FRANDRANGE(hsmin.z,hsmax.z));
			HDXVector3 pos = curpos;
			pos.y += ex.y;
			curpos.y += ex.y*2 + spacing;

			HDXQuaternion ori(0,FRANDRANGE(rotmin,rotmax),0);

			auto ent = lam_makePhysGfxBox(mass,ex,pos,ori,colorRandom());
			HDX_FORCEMAN->add(ent->getComponent<component_Physics>()->getBody(),mgravityforce);
		}
	};

	lam_makePhysGfxHalfspace(HDXVector3(0,0,0),YUNIT3,colorRandom());
	//lam_makeStack(HDXVector3(0,0,0),5,HDXVector3(2,2,2),HDXVector3(3,3,3),0,0);

	lam_makePhysGfxBox(_PEDESTAL_MASS,_PEDESTAL_EXTENTS,_PEDESTAL_POSITION,HDXQuaternion::gen::identity(),colorRandom());

	const HDXVector3 _LAUNCHBOARD_POSITION =
		_PEDESTAL_POSITION +
		(_PEDESTAL_EXTENTS*YUNIT3*YUNIT3) +
		(_LAUNCHBOARD_EXTENTS*YUNIT3*YUNIT3) +
		-(_PEDESTAL_EXTENTS*XUNIT3*XUNIT3);
	Entity *ent_pedestal = lam_makePhysGfxBox(_LAUNCHBOARD_MASS,_LAUNCHBOARD_EXTENTS,_LAUNCHBOARD_POSITION,HDXQuaternion::gen::identity(),colorRandom());
	HDX_FORCEMAN->add(ent_pedestal->getComponent<component_Physics>()->getBody(),mgravityforce);

	const HDXVector3 _STACK_POSITION =
		_LAUNCHBOARD_POSITION +
		(_LAUNCHBOARD_EXTENTS*YUNIT3*YUNIT3) +
		(_LAUNCHBOARD_EXTENTS*XUNIT3*XUNIT3) +
		-(_STACK_EXTENTS*XUNIT3*XUNIT3);
	lam_makeStack(_STACK_POSITION,_STACK_SPACING,_STACK_NUM,_STACK_EXTENTS,_STACK_EXTENTS,0,0,_STACK_MASS);

	const HDXVector3 _WEIGHT_POSITION =
		_LAUNCHBOARD_POSITION +
		(_LAUNCHBOARD_EXTENTS*YUNIT3*YUNIT3) +
		(_WEIGHT_FALLDST*YUNIT3) +
		-(_LAUNCHBOARD_EXTENTS*XUNIT3*XUNIT3) +
		(_WEIGHT_EXTENTS*XUNIT3*XUNIT3);

	mcbid_onkeypress = HDX_INPUT->registerCallbackOnKeyPress([=](WPARAM key)->void {
		switch(key) {
			case '1': { //toggle wireframe (and lighting)
				mhdxfx->setLightingEnabled(!mhdxfx->isLightingEnabled());
				auto comp_gfx_lst = HDX_ENTMAN->getComponentList<component_Draw_Mesh>();
				for(auto cmp : comp_gfx_lst) {
					auto comp_phy = cmp->getEntity()->getComponent<component_Physics>();
					if(comp_phy == 0 || comp_phy->getBody()->getBV()->type == BaseBV::BV_HALFSPACE) continue;
					for(auto itm : cmp->getItems()) {
						itm->setWireframe(!mhdxfx->isLightingEnabled());
					}
				}
				break;
			}

			case '2': { //drop weight
				Entity *ent_weight = lam_makePhysGfxBox(_WEIGHT_MASS,_WEIGHT_EXTENTS,_WEIGHT_POSITION,HDXQuaternion::gen::identity(),colorRandom());
				HDX_FORCEMAN->add(ent_weight->getComponent<component_Physics>()->getBody(),mgravityforce);
				break;
			}

			case '9': {
				auto comp_gfx_lst = HDX_ENTMAN->getComponentList<component_Physics>();
				for(auto cmp : comp_gfx_lst) {
					if(cmp->getBody()->hasFiniteMass()) cmp->getEntity()->destroy();
				}
				break;
			}

			case 'R': { //fire bullet: sphere
				auto bullet = lam_makePhysGfxSphere(5,3,HDXVector3(0,0,0),HDXQuaternion::gen::identity(),colorRandom());
				auto comp_trans = bullet->getComponent<component_Transform>(true);
				comp_trans->setPosition(mcamera.getPosition());
				comp_trans->setOrientation(HDXMatrix3x3::gen::view(mcamera.getRight(),mcamera.getUp(),mcamera.getForward()));

				HDXVector3 vel = 200 * mcamera.getForward();

				HDXVector3 avel(FRANDRANGE(-(float)M_PI/2,(float)M_PI/2),
								0,
								0);

				float resitution = 0.15f;
				float friction = 1.0f;

				RigidBody *newbody = bullet->getComponent<component_Physics>()->getBody();
				newbody->setVelocity(vel);
				newbody->setAngularVelocity(avel);
				newbody->setFriction(friction);
				newbody->setRestitution(resitution);
				HDX_FORCEMAN->add(newbody,mgravityforce);
				break;
			}
		}
	});
}

void State_Physics_Launcher::stateExecute(float dt) {
	const float _TIME_PERCENTCHANGEPERSECOND = 0.75f;
	if(HDX_INPUT->getKeyDown('Z')) {
		HDX_MAIN->setTimeScale(HDX_MAIN->getTimeScale()*(1.0f-(_TIME_PERCENTCHANGEPERSECOND*(dt/HDX_MAIN->getTimeScale()))));
	}

	if(HDX_INPUT->getKeyDown('X')) {
		HDX_MAIN->setTimeScale(HDX_MAIN->getTimeScale()*(1.0f+(_TIME_PERCENTCHANGEPERSECOND*(dt/HDX_MAIN->getTimeScale()))));
	}

	if(mlight_spot->isEnabled()) {
		mlight_spot->setPosition(mcamera.getPosition());
		mlight_spot->setDirection(mcamera.getForward());
	}
}

void State_Physics_Launcher::stateDraw() {
	HDXFontBase *testfont = HDX_FONTMAN->getFont("DX:Arial_10");
	if(testfont) {
		char buf[512];
		sprintf_s(buf,
				  "Time Scale: %f.2\n"
				  "Contacts: %i\n"
				  "\n"
				  "Controls:\n"
				  "WASD: move camera\n"
				  "SPACE: move camera up the y axis\n"
				  "RCLICK+MOUSE MOVE: mouselook camera mode\n"
				  "\n"
				  "R: shoot a sphere\n"
				  "\n"
				  "1: toggle wireframe\n"
				  "2: drop a heavy block on the launch board\n"
				  "\n"
				  "9: clear all dynamic bodies",
				  HDX_MAIN->getTimeScale(),
				  HDX_PHYMAN->getContacts().size());
		testfont->drawText(buf,HDX_RECT_POINT(5,30),DT_NOCLIP,0xffffffff);
	}
}

void State_Physics_Launcher::stateExit() {
	HDX_INPUT->unregisterCallbackOnKeyPress(mcbid_onkeypress);
	HDX_MAIN->setCameraDefault();
	HDX_MAIN->setShader(0);
	HDX_ENTMAN->clear();
	delete mgravityforce;
	delete mhdxfx;
}

void State_Physics_Launcher::stateOnLostDevice() {
}

void State_Physics_Launcher::stateOnResetDevice() {
}