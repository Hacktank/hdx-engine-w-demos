#include "states/state_flyingtext.h"

#include "hdx/hdx9.h"
#include "hdx/hdx9font.h"

State_FlyingText::FlyingText::FlyingText(const std::string &text,float x,float y,float vx,float vy,const std::string &fontname,DWORD color) {
	mx = x;
	my = y;
	mvx = vx;
	mvy = vy;
	mfontname = fontname;
	mcolor = color;
	setText(text);
}

State_FlyingText::FlyingText::~FlyingText() {
}

void State_FlyingText::FlyingText::setText(const std::string &text) {
	mtext = text;
	HDXFontBase *testfont = HDX_FONTMAN->getFont(mfontname);
	if(testfont) {
		testfont->calculateTextRect(mtext.c_str(),&mtextsizerect,0);
	}
}

void State_FlyingText::FlyingText::update() {
	mx += mvx;
	my += mvy;

	int sc_w,sc_h;
	HDX_MAIN->getWindowSize(&sc_w,&sc_h);

	float overlap[2][2] = {
		{
			-mx, //-x
			(mx+mtextsizerect.right)-sc_w, //+x
		},
		{
			-my, //-y
			(my+mtextsizerect.bottom)-sc_h, //+y
		}
	};

	for(int i = 0; i < 2; i++) {
		for(int j = 0; j < 2; j++) {
			if(overlap[i][j]>0) {
				float *cur_p,*cur_v;
				if(i==0) {
					cur_p = &mx;
					cur_v = &mvx;
				} else {
					cur_p = &my;
					cur_v = &mvy;
				}

				int norm;
				if(j==0)
					norm = 1;
				else
					norm = -1;

				*cur_p += overlap[i][j] * norm;
				*cur_v *= -1;
			}
		}
	}
}

void State_FlyingText::FlyingText::render() {
	HDXFontBase *testfont = HDX_FONTMAN->getFont(mfontname);
	if(testfont) {
		RECT drawrect = mtextsizerect;
		drawrect.left += (int)mx;
		drawrect.top += (int)my;
		drawrect.right += (int)mx;
		drawrect.bottom += (int)my;
		testfont->drawText(mtext.c_str(),drawrect,0,mcolor);
	}
}

void State_FlyingText::FlyingText::onLostDevice() {
}

void State_FlyingText::FlyingText::onResetDevice() {
}

void State_FlyingText::stateEnter() {
	int sc_w,sc_h;
	HDX_MAIN->getWindowSize(&sc_w,&sc_h);

	for(int i = 0; i < 15; i++) {
		FlyingText *newitem = new FlyingText(rand_getint()%2==0 ? "Jacob" : "Tyndall",
											 (float)RANDRANGE(0,sc_w-20),
											 (float)RANDRANGE(0,sc_h-20),
											 (rand_getfloat()-0.5f)*2.0f,
											 (rand_getfloat()-0.5f)*2.0f,
											 rand_getint()%2==0 ? "DX:Arial_15" : "DX:Castellar",
											 D3DCOLOR_XRGB(rand_getint()%255,rand_getint()%255,rand_getint()%255));
		mflyingtext.addItem(newitem);
	}
}

void State_FlyingText::stateExecute(float dt) {
	mflyingtext.doIterate([&](FlyingText *item)->bool {
		item->update();
		return false;
	});
}

void State_FlyingText::stateDraw() {
	mflyingtext.doIterate([&](FlyingText *item)->bool {
		item->render();
		return false;
	});
}

void State_FlyingText::stateExit() {
	mflyingtext.doIterate([&](FlyingText *item)->bool {
		delete item;
		return false;
	});
	mflyingtext.clear();
}

void State_FlyingText::stateOnLostDevice() {
	mflyingtext.doIterate([&](FlyingText *item)->bool {
		item->onLostDevice();
		return false;
	});
}

void State_FlyingText::stateOnResetDevice() {
	mflyingtext.doIterate([&](FlyingText *item)->bool {
		item->onResetDevice();
		return false;
	});
}