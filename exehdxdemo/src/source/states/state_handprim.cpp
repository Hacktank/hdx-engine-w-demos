#include "states/state_handprim.h"

#include "hdx/hdx9.h"
#include "hdx/hdx9font.h"
#include "hdx/hdx9texture.h"
#include "hdx/hdx9inputmanager.h"

#include "hdx/hdx9color.h"

#include "hdx/hdx9vertex.h"

#include "hdx/hdx9mesh.h"

void State_HandPrim::stateEnter() {
	HDX_MAIN->getD3DDevice()->CreateVertexBuffer(16*sizeof(HDXVERTPCTN),
												 D3DUSAGE_POINTS,
												 0,
												 D3DPOOL_MANAGED,
												 &mvb,
												 0);

	if(mvb==0) return;

	HDX_MAIN->getD3DDevice()->CreateIndexBuffer(18*sizeof(WORD),
												D3DUSAGE_POINTS,
												D3DFMT_INDEX16,
												D3DPOOL_MANAGED,
												&mib,
												0);

	if(mib==0) return;

	const float s_x = 10.0f;
	const float s_y = 10.0f;
	const float s_z = 10.0f;

	const float s_x2 = s_x/2.0f;
	const float s_y2 = s_y/2.0f;
	const float s_z2 = s_z/2.0f;

	HDXVERTPCTN *verts;
	mvb->Lock(0,16*sizeof(HDXVERTPCTN),(void**)&verts,0);

	//square bottom pyramid

	enum {
		PT_BOT_0 = 0,
		PT_BOT_1,
		PT_BOT_2,
		PT_BOT_3,

		PT_SID_01_0,
		PT_SID_01_1,
		PT_SID_01_2,

		PT_SID_12_0,
		PT_SID_12_1,
		PT_SID_12_2,

		PT_SID_23_0,
		PT_SID_23_1,
		PT_SID_23_2,

		PT_SID_31_0,
		PT_SID_31_1,
		PT_SID_31_2,
	};

	//bottom
	{
		int cursec = PT_BOT_0,curind = 0;

		//quadrant 2
		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(-s_x2,0.0f,s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.0f,0.0f),
											   D3DXVECTOR3(0,1,0));

		//quadrant 3
		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(-s_x2,0.0f,-s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.0f,1.0f),
											   D3DXVECTOR3(0,1,0));

		//quadrant 4
		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(s_x2,0.0f,-s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(1.0f,1.0f),
											   D3DXVECTOR3(0,1,0));

		//quadrant 1
		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(s_x2,0.0f,s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(1.0f,0.0f),
											   D3DXVECTOR3(0,1,0));
	}

	//face 0-1
	{
		int cursec = PT_SID_01_0,curind = 0;

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(-s_x2,0.0f,s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.0f,1.0f),
											   D3DXVECTOR3(0,1,0));

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(0.0f,s_y,0.0f),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.5f,0.0f),
											   D3DXVECTOR3(0,1,0));

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(-s_x2,0.0f,-s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(1.0f,1.0f),
											   D3DXVECTOR3(0,1,0));
	}

	//face 1-2
	{
		int cursec = PT_SID_12_0,curind = 0;

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(-s_x2,0.0f,-s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.0f,1.0f),
											   D3DXVECTOR3(0,1,0));

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(0.0f,s_y,0.0f),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.5f,0.0f),
											   D3DXVECTOR3(0,1,0));

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(s_x2,0.0f,-s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(1.0f,1.0f),
											   D3DXVECTOR3(0,1,0));
	}

	//face 2-3
	{
		int cursec = PT_SID_23_0,curind = 0;

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(s_x2,0.0f,-s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.0f,1.0f),
											   D3DXVECTOR3(0,1,0));

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(0.0f,s_y,0.0f),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.5f,0.0f),
											   D3DXVECTOR3(0,1,0));

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(s_x2,0.0f,s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(1.0f,1.0f),
											   D3DXVECTOR3(0,1,0));
	}

	//face 3-1
	{
		int cursec = PT_SID_31_0,curind = 0;

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(s_x2,0.0f,s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.0f,1.0f),
											   D3DXVECTOR3(0,1,0));

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(0.0f,s_y,0.0f),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(0.5f,0.0f),
											   D3DXVECTOR3(0,1,0));

		verts[cursec + curind++] = HDXVERTPCTN(D3DXVECTOR3(-s_x2,0.0f,s_z2),
											   D3DCOLOR(0xffffffff),
											   D3DXVECTOR2(1.0f,1.0f),
											   D3DXVECTOR3(0,1,0));
	}

	//center the pyramid around its origin
	for(int i = 0; i < 16; i++) {
		verts[i].p.y -= s_y2;
	}

	mvb->Unlock();

	WORD *inds;
	mib->Lock(0,18*sizeof(WORD),(void**)&inds,0);

	{
		int curind = 0;

		//bottom
		{
			inds[curind++] = PT_BOT_0;
			inds[curind++] = PT_BOT_1;
			inds[curind++] = PT_BOT_2;

			inds[curind++] = PT_BOT_0;
			inds[curind++] = PT_BOT_2;
			inds[curind++] = PT_BOT_3;
		}

		//side faces are only triangles, and are already defined in the proper order
		for(int i = 0; i < 12; i++) {
			inds[curind++] = PT_SID_01_0 + i;
		}
	}

	mib->Unlock();

	mtexture = HDXTextureCreateFromFile("data/texture/level/pyramid/tex_pyramid.png",true);

	mrot.x = mrot.y = mrot.z = 0.0f;

	mrotating = false;

	mcbid_input_mousebuttonpress = HDX_INPUT->registerCallbackOnMousePress([&](WPARAM key)->void {
		if(key == VK_LBUTTON) mrotating = true;
	});

	mcbid_input_mousebuttonrelease = HDX_INPUT->registerCallbackOnMouseRelease([&](WPARAM key,float timeheld)->void {
		if(key == VK_LBUTTON) mrotating = false;
	});

	mcbid_input_mousemove = HDX_INPUT->registerCallbackOnMouseMove([&](const POINT &oldpos,const POINT &newpos)->void {
		if(mrotating) {
			int dx = newpos.x - oldpos.x;
			int dy = newpos.y - oldpos.y;
			mrot.y -= (float)dx*0.004f;
			mrot.x -= (float)dy*0.004f;
		}
	});

	HDXCamera *cam = HDX_MAIN->getCameraCurrent();

	cam->setPosition(HDXVector3(0,10,-10));
	cam->setLookAt(HDXVector3(0,0,0));
}

void State_HandPrim::stateExecute(float dt) {
	auto lam_colorRandom = [](bool alpha)->unsigned {
		return ((alpha ? ((rand_getint())&0xff)<<24 : 0xff000000) | ((rand_getint())&0xff)<<16 | ((rand_getint())&0xff)<<8 | ((rand_getint())&0xff));
	};

	auto lam_colorFade = [](int col_start,int col_end,float progress) {
		if(progress>1.0f) progress = 1.0f;
		int ret = col_start;
		unsigned char *col[3];
		col[0] = (unsigned char*)&col_start;
		col[1] = (unsigned char*)&col_end;
		col[2] = (unsigned char*)&ret;
		for(int i = 0; i < 4; i++) {
			col[2][i] = (unsigned char)CLAMP(col[0][i] + (float)(col[1][i]-col[0][i]) * progress,0,255);
		}
		return ret;
	};

	static unsigned int col_curi = 0;
	unsigned int col_max = HDX_MAIN->getFPS();

	static DWORD startcolor[16] = {
		lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),
		lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),
		lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),
		lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false)
	};

	static DWORD targetcolor[16] = {
		lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),
		lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),
		lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),
		lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false),lam_colorRandom(false)
	};

	HDXVERTPCTN *verts;
	mvb->Lock(0,16*sizeof(HDXVERTPCTN),(void**)&verts,0);

	for(int i = 0; i < 16; i++) {
		if(col_curi > col_max) {
			startcolor[i] = targetcolor[i];
			targetcolor[i] = lam_colorRandom(false);
		} else {
			verts[i].c = lam_colorFade(startcolor[i],targetcolor[i],CLAMP(col_curi/(float)col_max,0.0f,1.0f));
		}
	}

	if(col_curi > col_max) {
		col_curi = 0;
	} else {
		col_curi++;
	}

	mvb->Unlock();
}

void State_HandPrim::stateDraw() {
	IDirect3DDevice9 *device = HDX_MAIN->getD3DDevice();

	if(device && mvb && mib && mtexture) {
		device->SetVertexDeclaration(HDXVERTPCTN::decl);
		device->SetStreamSource(0,mvb,0,sizeof(HDXVERTPCTN));
		device->SetIndices(mib);
		device->SetTexture(0,mtexture->getD3DTexture());

		device->SetRenderState(D3DRS_LIGHTING,FALSE);
		//device->SetRenderState(D3DRS_FILLMODE,D3DFILL_WIREFRAME);

		{
			HDXMatrix4x4 world = HDXMatrix3x3::gen::rotationAxisZ(mrot.z)*HDXMatrix3x3::gen::rotationAxisY(mrot.y)*HDXMatrix3x3::gen::rotationAxisX(mrot.x);

			device->SetTransform(D3DTS_WORLD,world.getD3DX());
		}

		device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST,0,0,16,0,6);
	}

	HDXFontBase *testfont = HDX_FONTMAN->getFont("DX:Arial_15");
	if(testfont) {
		testfont->drawText("Controls:\n   Main Menu: Escape\n\n   Rotate: Click & Drag",HDX_RECT_POINT(5,30),DT_NOCLIP,0xff000000);
	}
}

void State_HandPrim::stateExit() {
	HDX_INPUT->unregisterCallbackOnMouseMove(mcbid_input_mousemove);
	HDX_INPUT->unregisterCallbackOnMousePress(mcbid_input_mousebuttonpress);
	HDX_INPUT->unregisterCallbackOnMouseRelease(mcbid_input_mousebuttonrelease);
	if(mvb) mvb->Release();
	if(mib) mib->Release();
	if(mtexture) mtexture->release();
}

void State_HandPrim::stateOnLostDevice() {
}

void State_HandPrim::stateOnResetDevice() {
}