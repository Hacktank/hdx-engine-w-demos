#include "states/state_phonglighting.h"

#include "hdx/hdx9.h"
#include "hdx/hdx9font.h"
#include "hdx/hdx9texture.h"
#include "hdx/hdx9inputmanager.h"

#include "hdx/hdx9color.h"

#include "hdx/hdx9vertex.h"

#include "hdx/hdx9mesh.h"

void State_PhongLighting::stateEnter() {
	HDX_MAIN->setWindowClearColor(0xff050522);

	mcamera.initialize(0.001f,
					   0.001f,
					   5.0f,
					   'W',
					   'A',
					   'S',
					   'D',
					   VK_SPACE,
					   VK_MENU,
					   VK_CONTROL,
					   VK_SHIFT,
					   VK_RBUTTON);
	HDX_MAIN->setCamera(&mcamera);

	mcamera.setPosition(HDXVector3(0,5,-5));
	mcamera.setLookAt(HDXVector3(0,0,0));

	mcounter_time[0] = mcounter_time[1] = mcounter_time[2] = 0.0f;

	mhdxfx = new HDXShader_PhongLighting(HDX_MAIN->getD3DDevice(),"data/shader/lighting.fx","phong_tech");
	HDX_MAIN->setShader(mhdxfx);

	{
		mmesh_tank = HDXMeshLoadFromX("data/mesh/tank.x","data/texture/",true);

		//enable specular highlights on the tank's materials, for demonstration

		for(auto it = mmesh_tank->mmaterials.begin(); it != mmesh_tank->mmaterials.end(); it++) {
			(*it).specular = 0xffffffff;
		}
	}

	mmesh_light_point = HDXMeshCreateSphere(0.25f,HDXMAT_WHITE);
	mmesh_light_point->mmaterials[0].emisive = HDXCOL_WHITE;

	mlight_directional = mhdxfx->createLight();
	mlight_directional->setLightDirectional(HDXCOL_WHITE*0.8f,
											HDXCOL_WHITE*0.8f,
											HDXCOL_BLUE*0.15f,
											HDXVector3(1,-1,0));
	mlight_directional->setEnabled(true);

	mlight_point = mhdxfx->createLight();
	mlight_point->setLightPoint(HDXCOL_WHITE*0.25f,
								HDXCOL_WHITE*10.0f,
								HDXCOL_RED*0.75f,
								HDXVector3(0,5,0),
								HDXVector3(0,0,0.2f));
	mlight_point->setEnabled(true);

	mlight_spot = mhdxfx->createLight();
	mlight_spot->setLightSpot(HDXCOL_BLACK,
							  HDXCOL_WHITE*5.0f,
							  HDXCOL_GREEN*0.5f,
							  HDXVector3(0,5,0),
							  HDXVector3(0,0,0.05f),
							  HDXVector3(0,-1,0),
							  5.0f,
							  0.25f,
							  0.5f);
	mlight_spot->setEnabled(true);

	mcbid_lighttoggle[0] = HDX_INPUT->registerCallbackOnKeyPress([&](WPARAM key) {
		if(key == '1') {
			mlight_directional->setEnabled(!mlight_directional->isEnabled());
		}
	});

	mcbid_lighttoggle[1] = HDX_INPUT->registerCallbackOnKeyPress([&](WPARAM key) {
		if(key == '2') {
			mlight_point->setEnabled(!mlight_point->isEnabled());
		}
	});

	mcbid_lighttoggle[2] = HDX_INPUT->registerCallbackOnKeyPress([&](WPARAM key) {
		if(key == '3') {
			mlight_spot->setEnabled(!mlight_spot->isEnabled());
		}
	});

	cbid_onrendershader = HDX_MAIN->registerFuncRenderShader([&](HDXShader_Base *hdxfx)->void {
		auto lam_drawSubset = [&](HDXMesh *mesh,int sub) {
			hdxfx->setMaterial(mesh->mmaterials[sub]);
			hdxfx->setTexture(mesh->mtextures[sub]);
			mesh->md3dmesh->DrawSubset(sub);
		};

		lam_drawSubset(mmesh_tank,0); //body part 1
		lam_drawSubset(mmesh_tank,1); //body part 2

		{
			hdxfx->pushMatrixWorld();

			HDXMatrix4x4 matrot = HDXMatrix3x3::gen::rotationAxisY(cos(mcounter_time[2])*0.5f);

			hdxfx->multMatrixWorld(matrot);

			lam_drawSubset(mmesh_tank,2); //turret part 1
			lam_drawSubset(mmesh_tank,3); //turret part 2

			hdxfx->popMatrixWorld();
		}

		if(mlight_point->isEnabled()) {
			hdxfx->pushMatrixWorld();

			HDXMatrix4x4 mattrans = HDXMatrix4x4::gen::translation3d(mlight_point->getPosition());

			hdxfx->multMatrixWorld(mattrans);

			lam_drawSubset(mmesh_light_point,0);

			hdxfx->popMatrixWorld();
		}
	});
}

void State_PhongLighting::stateExecute(float dt) {
	if(mlight_point->isEnabled()) {
		float s0 = sin(mcounter_time[0]);
		float c0 = cos(mcounter_time[0]);
		float s1 = sin(mcounter_time[1]);
		mlight_point->setPosition(HDXVector3(6*c0,4+4*s1,6*s0));
	}

	if(mlight_spot->isEnabled()) {
		mlight_spot->setPosition(mcamera.getPosition());
		mlight_spot->setDirection(mcamera.getForward());
	}

	mcounter_time[0] += dt*2.5f;
	mcounter_time[1] += dt*2.5f*0.66f;
	mcounter_time[2] += dt*2.0f;
}

void State_PhongLighting::stateDraw() {
	IDirect3DDevice9 *device = HDX_MAIN->getD3DDevice();

	HDXFontBase *testfont = HDX_FONTMAN->getFont("DX:Arial_10");
	if(testfont) {
		testfont->drawText("Controls:\n"
						   "   Main Menu: Escape\n\n"
						   "   Mouse Look: Right Click + Move Mouse\n"
						   "   Camera:\n"
						   "      WASD: standard movement\n"
						   "      U: up\n"
						   "      SHIFT: fast mode\n"
						   "      CTRL: slow mode\n"
						   "   Lights:\n"
						   "      Toggle Lights: 1-3\n",HDX_RECT_POINT(5,30),DT_NOCLIP,0xffffffff);
	}
}

void State_PhongLighting::stateExit() {
	HDX_MAIN->setCameraDefault();
	HDX_INPUT->unregisterCallbackOnKeyPress(mcbid_lighttoggle[0]);
	HDX_INPUT->unregisterCallbackOnKeyPress(mcbid_lighttoggle[1]);
	HDX_INPUT->unregisterCallbackOnKeyPress(mcbid_lighttoggle[2]);
	HDX_MAIN->unregisterFuncRenderShader(cbid_onrendershader);
	HDX_MAIN->setShader(0);
	delete mhdxfx;
}

void State_PhongLighting::stateOnLostDevice() {
}

void State_PhongLighting::stateOnResetDevice() {
}