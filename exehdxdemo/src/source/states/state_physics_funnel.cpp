#include "states/state_physics_funnel.h"
#include "hdx/spacialpartition.h"
#include "hdx/component_draw_mesh.h"
#include "hdx/hdx9inputmanager.h"
#include "hdx/hdx9font.h"

#include "hdx/physics/collision/volume/bvconvexhull.h"

#include "hdx/util.h"
#include "hdx/hdx9math.h"
#include "hdx/hdx9geometry.h"

#include "states/component_phygfx_sleepwakecolor.h"

#include "testmesh/BunnyMesh.inl"

void State_Physics_Funnel::stateEnter() {
	HDX_MAIN->setWindowClearColor(0xff050522);

	mcamera.initialize(0.001f,
					   0.001f,
					   75.0f,
					   'W',
					   'A',
					   'S',
					   'D',
					   VK_SPACE,
					   VK_MENU,
					   VK_CONTROL,
					   VK_SHIFT,
					   VK_RBUTTON);
	HDX_MAIN->setCamera(&mcamera);

	mcamera.setPosition(HDXVector3(-200,200,-200));
	mcamera.setLookAt(HDXVector3(0,0,0));

	mhdxfx = new HDXShader_PhongLighting(HDX_MAIN->getD3DDevice(),"data/shader/lighting.fx","phong_tech");
	HDX_MAIN->setShader(mhdxfx);

	mlight_directional = mhdxfx->createLight();
	mlight_directional->setLightDirectional(HDXCOL_WHITE*0.1f,
											HDXCOL_WHITE*0.8f,
											HDXCOL_BLUE*0.15f,
											HDXVector3(1,-1,0));
	mlight_directional->setEnabled(true);

	mlight_spot = mhdxfx->createLight();
	mlight_spot->setLightSpot(HDXCOL_BLACK,
							  HDXCOL_WHITE*0.1f,
							  HDXCOL_WHITE*0.5f,
							  HDXVector3(0,0,0),
							  HDXVector3(0.5f,0.03f,0),
							  ZUNIT3,
							  10.0f,
							  (float)M_PI*0.3f*0.6f,
							  (float)M_PI*0.3f);
	mlight_spot->setEnabled(true);

	mgravityforce = new ForceGravityAxis(YUNIT3*-40.0f);
	mgravityforce->setAwakensBody(false);

	const float _FUNNEL_HEIGHT = 160.0f;
	const float _FUNNEL_THICKNESS = 2.0f;
	const float _FUNNEL_RAD_BOTTOM = 30.0f;
	const float _FUNNEL_RAD_TOP = 160.0f;
	const int _FUNNEL_RESOLUTION = 15;
	const HDXVector3 _FUNNEL_POSITION(0,-50,0);

	const float _FODDER_DENSITY = 0.05f;
	const int _FODDER_NUMPERWAVE = 20;

// 	; {
// 		std::vector<HDXVector3> bunny_verts(BUNNY_NUM_VERTICES);
// 		std::vector<unsigned> bunny_inds(BUNNY_NUM_INDICES);
// 
// 		for(unsigned i = 0; i < BUNNY_NUM_VERTICES; i++) {
// 			for(unsigned j = 0; j < 3; j++) bunny_verts[i][j] = gVerticesBunny[i+j];
// 		}
// 
// 		for(unsigned i = 0; i < BUNNY_NUM_TRIANGLES; i++) {
// 			for(unsigned j = 0; j < 3; j++) bunny_inds[i*3+j] = gIndicesBunny[i][j];
// 		}
// 
// 		auto bunnyhulls = HDXMeshDecomposeToConvexSet(bunny_verts.data(),BUNNY_NUM_VERTICES,bunny_inds.data(),BUNNY_NUM_TRIANGLES,0.05f);
// 
// 		int _i = 0;
// 		for(auto &subhull : bunnyhulls) {
// 			subhull.offset(HDXVector3(0,3*_i++,0));
// 			Entity *ent = HDX_ENTMAN->createEntity();
// 			auto comp_trans = ent->getComponent<component_Transform>(true);
// 			auto comp_gfx = ent->getComponent<component_Draw_Mesh>(true);
// 			comp_trans->setPosition(0,0,0);
// 			comp_gfx->addMesh(HDXMatrix4x4::gen::scale3d(HDXVector3(20,20,20)),HDXMeshCreateFromConvexHull(&subhull,HDXMaterial(colorRandom()),true),true);
// 		}
// 	}

	auto lam_makePhysGfxConvexHull = [=](float mass,const HDXConvexHull &hull,const HDXVector3 &pos,const HDXQuaternion &ori,unsigned color)->Entity* {
		Entity *ent = HDX_ENTMAN->createEntity();
		auto comp_phygfxcol = ent->getComponent<component_PhyGfx_SleepWakeColor>(true);
		comp_phygfxcol->setWakeColor(color);
		comp_phygfxcol->setSleepColor(HDXCOL_WHITE*0.1f);
		auto comp_phy = ent->getComponent<component_Physics>(true);
		auto comp_trans = ent->getComponent<component_Transform>(true);
		auto comp_gfx = ent->getComponent<component_Draw_Mesh>(true);
		comp_phy->initializeConvexHull(hull,mass);
		comp_trans->setPosition(pos);
		comp_trans->setOrientation(ori);
		int gfxid = comp_gfx->addMesh(HDXMatrix4x4::gen::identity(),HDXMeshCreateFromConvexHull(&((BVConvexHull*)comp_phy->getBody()->getBV())->getHull(),HDXMaterial(color),true),true);
		comp_gfx->getItem(gfxid)->setWireframe(!mhdxfx->isLightingEnabled());
		return ent;
	};

	auto lam_makePhysGfxBox = [=](float mass,const HDXVector3 &halfsize,const HDXVector3 &pos,const HDXQuaternion &ori,unsigned color)->Entity* {
		Entity *ent = HDX_ENTMAN->createEntity();
		auto comp_phygfxcol = ent->getComponent<component_PhyGfx_SleepWakeColor>(true);
		comp_phygfxcol->setWakeColor(color);
		comp_phygfxcol->setSleepColor(HDXCOL_WHITE*0.1f);
		auto comp_phy = ent->getComponent<component_Physics>(true);
		auto comp_trans = ent->getComponent<component_Transform>(true);
		auto comp_gfx = ent->getComponent<component_Draw_Mesh>(true);
		comp_phy->initializeBox(halfsize,mass);
		comp_trans->setPosition(pos);
		comp_trans->setOrientation(ori);
		int gfxid = comp_gfx->addBox(HDXMatrix4x4::gen::identity(),HDXMaterial(color),halfsize);
		comp_gfx->getItem(gfxid)->setWireframe(!mhdxfx->isLightingEnabled());
		return ent;
	};

	auto lam_makePhysGfxSphere = [=](float mass,float r,const HDXVector3 &pos,const HDXQuaternion &ori,unsigned color)->Entity* {
		Entity *ent = HDX_ENTMAN->createEntity();
		auto comp_phygfxcol = ent->getComponent<component_PhyGfx_SleepWakeColor>(true);
		comp_phygfxcol->setWakeColor(color);
		comp_phygfxcol->setSleepColor(HDXCOL_WHITE*0.1f);
		auto comp_phy = ent->getComponent<component_Physics>(true);
		auto comp_trans = ent->getComponent<component_Transform>(true);
		auto comp_gfx = ent->getComponent<component_Draw_Mesh>(true);
		comp_phy->initializeSphere(r,mass);
		comp_trans->setPosition(pos);
		comp_trans->setOrientation(ori);
		int gfxid = comp_gfx->addSphere(HDXMatrix4x4::gen::identity(),HDXMaterial(color),r);
		comp_gfx->getItem(gfxid)->setWireframe(!mhdxfx->isLightingEnabled());
		return ent;
	};

	; {
//  		auto s1_ent = lam_makePhysGfxSphere(10,10,HDXVector3(5,80,5),HDXQuaternion(0,0,0,1),colorRandom());
//  		auto s1_comp_phy = s1_ent->getComponent<component_Physics>(true);
//  		s1_comp_phy->getBody()->addVelocity(HDXVector3(0,-10,0));
//  		auto s2_ent = lam_makePhysGfxSphere(10,10,HDXVector3(0,20,0),HDXQuaternion(0,0,0,1),colorRandom());
//  		auto s2_comp_phy = s2_ent->getComponent<component_Physics>(true);
//  		s2_comp_phy->getBody()->addVelocity(HDXVector3(0,10,0));
// 		auto s3_ent = lam_makePhysGfxSphere(10,10,HDXVector3(0,50,0),HDXQuaternion(0,0,0,1),colorRandom());
// 		auto s3_comp_phy = s3_ent->getComponent<component_Physics>(true);
// 		s3_comp_phy->getBody()->addVelocity(HDXVector3(0,0,0));


//  		auto s2_ent = lam_makePhysGfxSphere(10,10,HDXVector3(0,-339,0),HDXQuaternion(0,0,0,1),colorRandom());
//  		auto s2_comp_phy = s2_ent->getComponent<component_Physics>(true);
//  		//s2_comp_phy->getBody()->addVelocity(HDXVector3(0,10,0));
//  		s2_comp_phy->getBody()->setAcceleration(HDXVector3(0,-30,0));
//  		//s2_comp_phy->getBody()->setAngularVelocity(HDXVector3(4,0,0));
// 
// 		auto b1_ent = lam_makePhysGfxSphere(10,10,HDXVector3(0,-200,0),HDXQuaternion(0,0,0,1),colorRandom());
// 		auto b1_comp_phy = b1_ent->getComponent<component_Physics>(true);
// 		b1_comp_phy->getBody()->setAcceleration(HDXVector3(0,-30,0));
	}

	auto lam_makePhysGfxHalfspace = [=](const HDXVector3 &pos,const HDXVector3 &normal,unsigned color)->Entity* {
		Entity *ent = HDX_ENTMAN->createEntity();
		auto comp_phy = ent->getComponent<component_Physics>(true);
		auto comp_trans = ent->getComponent<component_Transform>(true);
		auto comp_gfx = ent->getComponent<component_Draw_Mesh>(true);
		comp_phy->initializeHalfspace(pos,normal);
		comp_gfx->addBox(HDXMatrix4x4::gen::identity(),HDXMaterial(color),HDXVector3(0.1f,500,500));
		comp_phy->getBody()->setFriction(2.5f);
		return ent;
	};

	auto lam_initalizeAsFunnelFodder = [=](Entity *ent,const HDXVector3 &posoffset)->void {
		HDXVector3 pos;
		pos = _FUNNEL_POSITION + posoffset;
		pos.y += _FUNNEL_HEIGHT / 2.0f + FRANDRANGE(0,50);
		; {
			//uniform point-in-circle algorithm from: http://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly
			const float R = _FUNNEL_RAD_TOP*0.8f;
			const float t = 2 * (float)M_PI * rand_getfloat();
			const float u = rand_getfloat() + rand_getfloat();
			const float r = u > 1.0f ? 2.0f-u : u;
			pos.x = r*cos(t)*R;
			pos.z = r*sin(t)*R;
		}
		HDXQuaternion ori(FRANDRANGE(0,(float)M_PI*2),FRANDRANGE(0,(float)M_PI*2),FRANDRANGE(0,(float)M_PI*2));

		auto comp_trans = ent->getComponent<component_Transform>(true);
		comp_trans->setPosition(pos);
		comp_trans->setOrientation(ori);

		float angularvelx = FRANDRANGE(-(float)M_PI/4,(float)M_PI/4);
		float angularvely = FRANDRANGE(-(float)M_PI/4,(float)M_PI/4);
		float angularvelz = FRANDRANGE(-(float)M_PI/4,(float)M_PI/4);

		float resitution = FRANDRANGE(0.2f,0.7f);
		float friction = FRANDRANGE(0.4f,1.2f);
		RigidBody *newbody = ent->getComponent<component_Physics>()->getBody();
		newbody->setAngularVelocity(angularvelx,angularvely,angularvelx);
		newbody->setFriction(friction);
		newbody->setRestitution(resitution);
		HDX_FORCEMAN->add(newbody,mgravityforce);
	};

	auto lam_generateFodder_Box = [=]()->Entity* {
		const HDXVector3 hs(FRANDRANGE(1.5f,7),
							FRANDRANGE(1.5f,7),
							FRANDRANGE(1.5f,7));
		const float mass = _FODDER_DENSITY * (hs.x*hs.y*hs.z);
		const unsigned color = colorLighten(colorRandom(),0.25f);
		return lam_makePhysGfxBox(mass,hs,HDXVector3(0,0,0),HDXQuaternion::gen::identity(),color);
	};

	auto lam_generateFodder_Sphere = [=]()->Entity* {
		const float r(FRANDRANGE(2.0f,8));
		const float mass = _FODDER_DENSITY * (4.0f/3.0f*(float)M_PI*r*r*r);
		const unsigned color = colorLighten(colorRandom(),0.25f);
		return lam_makePhysGfxSphere(mass,r,HDXVector3(0,0,0),HDXQuaternion::gen::identity(),color);
	};

	auto lam_generateFodder_ConvexHull = [=]()->Entity* {
		std::vector<HDXVector3> points(RANDRANGE(5,25));
		for(unsigned i = 0; i < points.size(); i++) {
			for(int j = 0; j < 3; j++) {
				float frnd = FRANDRANGE(1.5f,3.5f);
				points[i][j] = SQ(frnd) * (rand_getint()%2==0 ? 1 : -1);
			}
			if(rand_getint()%50==0) points[i] *= 3.0f;
		}
		HDXConvexHull hull = HDXQuickHull(points.data(),points.size());
		const float mass = _FODDER_DENSITY * (hull.maabbhs.x*hull.maabbhs.y*hull.maabbhs.z);
		const unsigned color = colorLighten(colorRandom(),0.25f);
		return lam_makePhysGfxConvexHull(mass,std::move(hull),HDXVector3(0,0,0),HDXQuaternion::gen::identity(),color);
	};

	auto lam_generateFodder_Random = [=]()->Entity* {
		switch(rand_getint()%3) {
			case 0: return lam_generateFodder_Box();
			case 1: return lam_generateFodder_Sphere();
			case 2: return lam_generateFodder_ConvexHull();
		}
		return 0;
	};

	auto lam_makeFunnel = [=](float height,float thickness,float radius_bottom,float radius_top,int resolution,const HDXVector3 &pos,const HDXQuaternion &ori)->void {
		float inc = 2.0f * (float)M_PI / resolution;
		float theta = 0.0f;
		float boxwidth = 2*tan((float)M_PI/resolution)*max(radius_bottom,radius_top);
		float boxheight = sqrt(SQ(abs(radius_bottom-radius_top))+SQ(height));
		for(int i = 0; i < resolution; i++) {
			HDXVector3 point[2];
			HDXVector3 rotoff = HDXVector3::gen::transformQuaternion(HDXVector3(cos(theta),0.0f,sin(theta)),ori);
			point[0] = pos + radius_bottom * rotoff;
			point[1] = pos + radius_top * rotoff;
			point[0].y -= height/2;
			point[1].y += height/2;

			float rotonaxis[2] = {
				acos(height/boxheight),
				(float)M_PI/2.0f-theta
			};
			HDXQuaternion rot = ori;
			{
				HDXQuaternion qtmp(YUNIT3,rotonaxis[1]);//rotate to face center
				rot = qtmp * rot;
				qtmp.setAxisAngle(XUNIT3,rotonaxis[0]);//rotate to form slope
				rot = qtmp * rot;
			}

			HDXVector3 newpos = point[0] + (rot * (HDXVector3(0,boxheight/2,thickness/2)));

			lam_makePhysGfxBox(0,HDXVector3(boxwidth/2,boxheight/2,thickness),newpos,rot,colorRandom());

			theta += inc;
		}
	};

	lam_makeFunnel(_FUNNEL_HEIGHT,_FUNNEL_THICKNESS,_FUNNEL_RAD_BOTTOM,_FUNNEL_RAD_TOP,_FUNNEL_RESOLUTION,_FUNNEL_POSITION,HDXQuaternion(0,0,0,1));
	lam_makePhysGfxHalfspace(_FUNNEL_POSITION + HDXVector3(0,-300,0),YUNIT3,colorRandom());
	//lam_makePhysGfxBox(0,HDXVector3(300,10,300),HDXVector3(0,-300,0),HDXQuaternion::gen::identity(),colorRandom());

//  	; {
//  		const float r = 10;
//  		const float mass = 10;
//  		const unsigned color = colorRandom();
//  		Entity *ent = HDX_ENTMAN->createEntity();
//  		auto comp_phy = ent->getComponent<component_Physics>(true);
//  		auto comp_trans = ent->getComponent<component_Transform>(true);
//  		auto comp_gfx = ent->getComponent<component_Draw_Mesh>(true);
//  		comp_phy->initializeRod(r,mass);
//  		comp_gfx->addRod(HDXMatrix4x4::gen::identity(),HDXMaterial(color),r);
//  		lam_initalizeAsFunnelFodder(ent,HDXVector3(0,0,0));
//  	}

	mcbid_onkeypress = HDX_INPUT->registerCallbackOnKeyPress([=](WPARAM key)->void {
		switch(key) {
			case '1': { //toggle wireframe (and lighting)
				mhdxfx->setLightingEnabled(!mhdxfx->isLightingEnabled());
				auto comp_gfx_lst = HDX_ENTMAN->getComponentList<component_Draw_Mesh>();
				for(auto cmp : comp_gfx_lst) {
					auto comp_phy = cmp->getEntity()->getComponent<component_Physics>();
					if(comp_phy == 0 || comp_phy->getBody()->getBV()->type == BaseBV::BV_HALFSPACE) continue;
					for(auto itm : cmp->getItems()) {
						itm->setWireframe(!mhdxfx->isLightingEnabled());
					}
				}
				break;
			}

			case '2': { //generate fodder: spheres
				HDXVector3 posoff(0,0,0);
				for(int i = 0; i < _FODDER_NUMPERWAVE; i++) {
					lam_initalizeAsFunnelFodder(lam_generateFodder_Sphere(),posoff);
					posoff.y += 5;
				}
				break;
			}

			case '3': { //generate fodder: boxes
				HDXVector3 posoff(0,0,0);
				for(int i = 0; i < _FODDER_NUMPERWAVE; i++) {
					lam_initalizeAsFunnelFodder(lam_generateFodder_Box(),posoff);
					posoff.y += 5;
				}
				break;
			}

			case '4': { //generate fodder: hulls
				HDXVector3 posoff(0,0,0);
				for(int i = 0; i < _FODDER_NUMPERWAVE; i++) {
					lam_initalizeAsFunnelFodder(lam_generateFodder_ConvexHull(),posoff);
					posoff.y += 5;
				}
				break;
			}

			case '5': { //generate fodder: random
				HDXVector3 posoff(0,0,0);
				for(int i = 0; i < _FODDER_NUMPERWAVE; i++) {
					lam_initalizeAsFunnelFodder(lam_generateFodder_Random(),posoff);
					posoff.y += 5;
				}
				break;
			}

			case '9': {
				auto comp_gfx_lst = HDX_ENTMAN->getComponentList<component_Physics>();
				for(auto cmp : comp_gfx_lst) {
					if(cmp->getBody()->hasFiniteMass()) cmp->getEntity()->destroy();
				}
				break;
			}

			case 'R': { //fire bullet: hull
				auto bullet = lam_generateFodder_ConvexHull();
				auto comp_trans = bullet->getComponent<component_Transform>(true);
				comp_trans->setPosition(mcamera.getPosition());
				comp_trans->setOrientation(HDXMatrix3x3::gen::view(mcamera.getRight(),mcamera.getUp(),mcamera.getForward()));

				HDXVector3 vel = 200 * mcamera.getForward();

				HDXVector3 avel(FRANDRANGE(-(float)M_PI/2,(float)M_PI/2),
							   0,
							   0);

				float resitution = 0.15f;
				float friction = 1.0f;

				RigidBody *newbody = bullet->getComponent<component_Physics>()->getBody();
				newbody->setVelocity(vel);
				newbody->setAngularVelocity(avel);
				newbody->setFriction(friction);
				newbody->setRestitution(resitution);
				HDX_FORCEMAN->add(newbody,mgravityforce);
				break;
			}
		}
	});
}

void State_Physics_Funnel::stateExecute(float dt) {
	const float _TIME_PERCENTCHANGEPERSECOND = 0.75f;
	if(HDX_INPUT->getKeyDown('Z')) {
		HDX_MAIN->setTimeScale(HDX_MAIN->getTimeScale()*(1.0f-(_TIME_PERCENTCHANGEPERSECOND*(dt/HDX_MAIN->getTimeScale()))));
	}

	if(HDX_INPUT->getKeyDown('X')) {
		HDX_MAIN->setTimeScale(HDX_MAIN->getTimeScale()*(1.0f+(_TIME_PERCENTCHANGEPERSECOND*(dt/HDX_MAIN->getTimeScale()))));
	}

	if(mlight_spot->isEnabled()) {
		mlight_spot->setPosition(mcamera.getPosition());
		mlight_spot->setDirection(mcamera.getForward());
	}
}

void State_Physics_Funnel::stateDraw() {
	HDXFontBase *testfont = HDX_FONTMAN->getFont("DX:Arial_10");
	if(testfont) {
		char buf[512];
		sprintf_s(buf,
				  "Time Scale: %f.2\n"
				  "Contacts: %i\n"
				  "\n"
				  "Controls:\n"
				  "WASD: move camera\n"
				  "SPACE: move camera up the y axis\n"
				  "RCLICK+MOUSE MOVE: mouselook camera mode\n"
				  "\n"
				  "R: shoot a convex hull\n"
				  "\n"
				  "1: toggle wireframe\n"
				  "2-5: spawn physics objects\n"
				  "2: spheres\n"
				  "3: boxes\n"
				  "4: convex hulls\n"
				  "5: random assortment\n"
				  "\n"
				  "9: clear all dynamic bodies",
				  HDX_MAIN->getTimeScale(),
				  HDX_PHYMAN->getContacts().size());
		testfont->drawText(buf,HDX_RECT_POINT(5,30),DT_NOCLIP,0xffffffff);
	}
}

void State_Physics_Funnel::stateExit() {
	HDX_INPUT->unregisterCallbackOnKeyPress(mcbid_onkeypress);
	HDX_MAIN->setCameraDefault();
	HDX_MAIN->setShader(0);
	HDX_ENTMAN->clear();
	delete mgravityforce;
	delete mhdxfx;
}

void State_Physics_Funnel::stateOnLostDevice() {
}

void State_Physics_Funnel::stateOnResetDevice() {
}