//base
uniform extern float4x4 g_wvp;
uniform extern texture g_tex;
uniform extern bool g_tex_enabled;

//phong
uniform extern bool g_flag_lightingenabled;

uniform extern float3 g_eye_pos;
uniform extern float4x4 g_w;
uniform extern float4x4 g_w_inv_trans;

uniform extern float4 g_mat_diffuse;
uniform extern float4 g_mat_ambient;
uniform extern float4 g_mat_specular;
uniform extern float4 g_mat_emissive;
uniform extern float g_mat_power;

static const uint g_light_max = 20;

uniform extern uint g_light_num;
uniform extern uint g_light_type[g_light_max];
uniform extern float4 g_light_ambient[g_light_max];
uniform extern float4 g_light_diffuse[g_light_max];
uniform extern float4 g_light_specular[g_light_max];
uniform extern float3 g_light_pos_w[g_light_max];
uniform extern float3 g_light_attenuation_012[g_light_max];
uniform extern float3 g_light_direction[g_light_max];
uniform extern float g_light_falloff[g_light_max];
uniform extern float g_light_theta[g_light_max];
uniform extern float g_light_phi[g_light_max];

sampler sampler_g_tex = sampler_state {
	Texture = <g_tex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};

struct phong_vs_out {
	float4 pos_h : POSITION0;
	float2 texcoord : TEXCOORD0;
	float3 normal_w : TEXCOORD1;
	float3 pos_w : TEXCOORD2;
};

phong_vs_out phong_vs(float3 pos_l : POSITION0,
					  float2 texcoord : TEXCOORD0,
					  float3 normal_l : NORMAL0) {
	phong_vs_out ret = (phong_vs_out)0;
	ret.pos_h = mul(float4(pos_l,1.0f),g_wvp);
	ret.texcoord = texcoord;

	if(g_flag_lightingenabled) {
		//calculate world-space normal
		ret.normal_w = mul(float4(normal_l,0.0f),g_w_inv_trans).xyz;
		ret.normal_w = normalize(ret.normal_w);
		ret.pos_w = mul(float4(pos_l,1.0f),g_w).xyz;
	}

	return ret;
}

float4 phong_ps(float2 texcoord : TEXCOORD0,
				float3 normal_w : TEXCOORD1,
				float3 pos_w : TEXCOORD2) : COLOR{
	float4 ret = (float4)0;

	float4 col_texel = g_tex_enabled ? tex2D(sampler_g_tex,texcoord) : float4(1,1,1,1);

	float4 mat_ambient = col_texel * g_mat_ambient;
	float4 mat_diffuse = col_texel * g_mat_diffuse;
	float4 mat_specular = g_mat_specular;

	if(g_flag_lightingenabled) {
		normal_w = normalize(normal_w);

		float3 toeye_w = normalize(g_eye_pos - pos_w);
		float3 lightvec_w;
		float3 reflect_w;
		float3 col_ambient;
		float3 col_diffuse;
		float3 col_specular;

		float tmpf[3];

		[loop] for(uint i = 0; i < g_light_num; i++) {
			lightvec_w = normalize(g_light_pos_w[i] - pos_w);
			reflect_w = reflect(-lightvec_w,normal_w);
			switch(g_light_type[i]) {
				case 0: { //directional
					lightvec_w = -g_light_direction[i];
					reflect_w = reflect(-lightvec_w,normal_w);

					//ambient
					col_ambient = (mat_ambient * g_light_ambient[i]).rgb;

					tmpf[2] = dot(normal_w,lightvec_w);

					//diffuse
					{
						col_diffuse = (max(tmpf[2],0.0f)) *(mat_diffuse*g_light_diffuse[i]).rgb;
					}

					//specular
					if(tmpf[2] > 0) {
						col_specular = ((pow(max(dot(reflect_w,toeye_w),0.0f),g_mat_power))*(mat_specular*g_light_specular[i])).rgb;
					}

					ret.xyz += col_ambient + (col_diffuse+col_specular);
					break;
				}
				case 1: { //point
					//ambient
					col_ambient = (mat_ambient * g_light_ambient[i]).rgb;

					tmpf[2] = dot(normal_w,lightvec_w);

					//diffuse
					{
						col_diffuse = (max(tmpf[2],0.0f)) *(mat_diffuse*g_light_diffuse[i]).rgb;
					}

					//specular
					if(tmpf[2] > 0) {
						col_specular = ((pow(max(dot(reflect_w,toeye_w),0.0f),g_mat_power))*(mat_specular*g_light_specular[i])).rgb;
					}

					//attenuation
					tmpf[0] = distance(g_light_pos_w[i],pos_w);
					tmpf[0] = g_light_attenuation_012[i].x + g_light_attenuation_012[i].y*tmpf[0] + g_light_attenuation_012[i].z*tmpf[0]*tmpf[0];

					ret.xyz += col_ambient + ((col_diffuse+col_specular)/tmpf[0]);
					break;
				}
				case 2: { //spot
					//ambient
					col_ambient = (mat_ambient * g_light_ambient[i]).rgb;

					tmpf[2] = dot(normal_w,lightvec_w);

					//diffuse
					{
						col_diffuse = (max(tmpf[2],0.0f)) *(mat_diffuse*g_light_diffuse[i]).rgb;
					}

					//specular
					if(tmpf[2] > 0) {
						col_specular = ((pow(max(dot(reflect_w,toeye_w),0.0f),g_mat_power))*(mat_specular*g_light_specular[i])).rgb;
					}

					//attenuation
					tmpf[0] = distance(g_light_pos_w[i],pos_w);
					tmpf[0] = g_light_attenuation_012[i].x + g_light_attenuation_012[i].y*tmpf[0] + g_light_attenuation_012[i].z*tmpf[0]*tmpf[0];

					tmpf[1] = acos(dot(-lightvec_w,g_light_direction[i])/(length(-lightvec_w)*length(g_light_direction[i])));
					tmpf[1] = pow(max((cos(tmpf[1])-cos(g_light_phi[i]/2))/(cos(g_light_theta[i]/2)-cos(g_light_phi[i]/2)),0.0f),g_light_falloff[i]);

					ret.xyz += tmpf[1] * (col_ambient + ((col_diffuse+col_specular)/tmpf[0]));
					break;
				}
			}
		}
	} else {
		ret.xyz = mat_diffuse.xyz;
	}

	ret.xyz += g_mat_emissive.xyz;

	ret.a = mat_diffuse.a;

	return ret;
}

technique phong_tech {
	pass P0 {
		vertexShader = compile vs_3_0 phong_vs();
		pixelShader = compile ps_3_0 phong_ps();

		//NormalizeNormals = true;
		Lighting = false;
	}
}