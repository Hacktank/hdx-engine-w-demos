Process:
Make new, empty Win32 project.

Adjust project settings:
General>Use of MFC>Use MFC in a Shared DLL
General>Character Set>Not Set

Create MFC app, example code: http://www.codeproject.com/Articles/1672/MFC-under-the-hood

Create new resource (.rc).

Add new dialog resource to the .rc file.

Right-click on the dialog and click create class.

Add a Picture Control for the Property Grid

!!!!!!!!!!!!!!!!!!!!!!
In the properties of the Picture Control set Notify to true.
!!!!!!!!!!!!!!!!!!!!!!

in the DoDataExchange function of the dialog class add:
DDX_Control(pDX,ID_OF_THE_PICTURE_CONTROL,mpropgrid);