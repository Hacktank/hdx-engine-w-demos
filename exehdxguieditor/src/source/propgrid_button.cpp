
#include "propgrid_button.h"

propgrid_button::propgrid_button(void) {
	mfunc_onclick = nullptr;
	mcolor = RGB(255,0,0);
}

propgrid_button::~propgrid_button(void) {

}

CPropertyGrid::EEditMode propgrid_button::GetEditMode() {
	return CPropertyGrid::EM_CUSTOM;
}

void propgrid_button::DrawItem(CDC& dc,CRect rc,bool focused) {
	rc.DeflateRect(2,2);
	dc.FillSolidRect(&rc,mcolor);
}

bool propgrid_button::OnLButtonDown(CRect rc,CPoint pt) {
	return rc.PtInRect(pt);
}

void propgrid_button::OnLButtonUp(CRect rc,CPoint pt) {
	if(rc.PtInRect(pt)) {
		if(mfunc_onclick != nullptr) mfunc_onclick();
	}
}
