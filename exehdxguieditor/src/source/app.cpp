#include "app.h"

MainApp g_app;

BOOL MainApp::InitInstance() {
	InitCommonControls();
	CWinApp::InitInstance();

	m_pMainWnd = &mdlg;
	m_pActiveWnd = m_pMainWnd;

	mdlg.DoModal();
	return TRUE;
}

BOOL MainApp::ExitInstance() {
	return CWinApp::ExitInstance();
}