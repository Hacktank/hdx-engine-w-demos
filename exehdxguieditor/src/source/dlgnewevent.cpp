// F:\PROGRAMMING\C++\Projects\RealTime_Gui_Physics\exehdxguieditor\src\source\dlgnewevent.cpp : implementation file
//

#include "mfc.h"
#include "dlgnewevent.h"

// dlgnewevent dialog

IMPLEMENT_DYNAMIC(dlgnewevent,CDialog)

dlgnewevent::dlgnewevent(CWnd* pParent /*=NULL*/)
: CDialog(dlgnewevent::IDD,pParent) {
}

dlgnewevent::~dlgnewevent() {
}

BOOL dlgnewevent::OnInitDialog() {
	; {
		CTreeCtrl *tree = (CTreeCtrl*)GetDlgItem(IDC_TREE_EVENT);
		mchoice_event = "MOUSE_CLICK";
		tree->SetItemData(tree->InsertItem("MOUSE_CLICK"),(DWORD_PTR)"MOUSE_CLICK");
		tree->SetItemData(tree->InsertItem("MOUSE_HOVER_ENTER"),(DWORD_PTR)"MOUSE_HOVER_ENTER");
		tree->SetItemData(tree->InsertItem("MOUSE_HOVER_EXIT"),(DWORD_PTR)"MOUSE_HOVER_EXIT");
		tree->SetItemData(tree->InsertItem("TRANSITION_ENTER"),(DWORD_PTR)"TRANSITION_ENTER");
		tree->SetItemData(tree->InsertItem("TRANSITION_EXIT"),(DWORD_PTR)"TRANSITION_EXIT");
	}

	; {
		CTreeCtrl *tree = (CTreeCtrl*)GetDlgItem(IDC_TREE_ACTION);
		mchoice_action = "MENU_NAVIGATE_MENU";
		tree->SetItemData(tree->InsertItem("MENU_NAVIGATE_MENU"),(DWORD_PTR)"MENU_NAVIGATE_MENU");
		tree->SetItemData(tree->InsertItem("MENU_NAVIGATE_BACK"),(DWORD_PTR)"MENU_NAVIGATE_BACK");
		tree->SetItemData(tree->InsertItem("SOUND_PLAY"),(DWORD_PTR)"SOUND_PLAY");
	}

	return TRUE;
}

void dlgnewevent::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(dlgnewevent,CDialog)
	ON_NOTIFY(TVN_SELCHANGED,IDC_TREE_EVENT,&dlgnewevent::OnTvnSelchangedTreeEvent)
	ON_NOTIFY(TVN_SELCHANGED,IDC_TREE_ACTION,&dlgnewevent::OnTvnSelchangedTreeAction)
END_MESSAGE_MAP()

// dlgnewevent message handlers

void dlgnewevent::OnTvnSelchangedTreeEvent(NMHDR *pNMHDR,LRESULT *pResult) {
	CTreeCtrl *tree = (CTreeCtrl*)GetDlgItem(IDC_TREE_EVENT);
	HTREEITEM sel = tree->GetSelectedItem();
	if(sel != NULL) {
		mchoice_event = (const char*)tree->GetItemData(sel);
	}
}

void dlgnewevent::OnTvnSelchangedTreeAction(NMHDR *pNMHDR,LRESULT *pResult) {
	CTreeCtrl *tree = (CTreeCtrl*)GetDlgItem(IDC_TREE_ACTION);
	HTREEITEM sel = tree->GetSelectedItem();
	if(sel != NULL) {
		mchoice_action = (const char*)tree->GetItemData(sel);
	}
}