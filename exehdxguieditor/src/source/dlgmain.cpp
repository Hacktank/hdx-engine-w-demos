// dlgmain.cpp : implementation file
//

#include "stdafx.h"
#include "dlgmain.h"
#include "afxdialogex.h"

#include "app.h"
#include "dlgnewevent.h"

// DLGMain dialog

#include "hdx/hdx9.h"
#include "hdx/messagemanager.h"
#include "hdx/sound/hdx9sound.h"
#include "hdx/hdx9font.h"

#ifdef _DEBUG
#pragma comment(lib, "hdxd.lib")
#else
#pragma comment(lib, "hdx.lib")
#endif

#include <fstream>
#include <sstream>

#include <rapidxml/rapidxml_print.hpp>

IMPLEMENT_DYNAMIC(DLGMain,CDialog)

DLGMain::DLGMain(CWnd* pParent /*=NULL*/)
: CDialog(DLGMain::IDD,pParent) {
}

DLGMain::~DLGMain() {
	HDX_MAIN->deinitialize();
}

void DLGMain::DoDataExchange(CDataExchange* pDX) {
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX,IDC_PB_PROPERTIES,mpropgrid);
}

BEGIN_MESSAGE_MAP(DLGMain,CDialog)
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK,IDC_TREE_ELEMENTS,&DLGMain::OnNMClickTreeElements)
	ON_BN_CLICKED(IDC_BUTTON_LOAD,&DLGMain::OnBnClickedButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_SAVE,&DLGMain::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_ADDNEWELEMENT,&DLGMain::OnBnClickedButtonAddnewelement)
	ON_BN_CLICKED(IDC_BUTTON_ADDNEWEFFECT,&DLGMain::OnBnClickedButtonAddneweffect)
	ON_BN_CLICKED(IDC_BUTTON_APPLYCHANGES,&DLGMain::OnBnClickedButtonApplychanges)
	ON_BN_CLICKED(IDC_BUTTON_DELETESELECTEDITEM,&DLGMain::OnBnClickedButtonDeleteSelectedElement)
END_MESSAGE_MAP()

BOOL DLGMain::OnInitDialog() {
	CDialog::OnInitDialog();
	SetTimer(TIMER_UPDATE,2,0);

	srand((unsigned int)(std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()).time_since_epoch().count()));

	HDX_SOUNDMAN->initializeFMOD(500);

	HDX_MAIN->initializeEmbeddedWindow(GetDlgItem(IDC_PB_HDX)->GetSafeHwnd());

	HDX_FONTMAN->createFontDX("DX:Arial_15","Arial",15,FW_BOLD);

	HDX_MAIN->registerFuncUpdate([&](float dt)->void {
		auto lam_colorRandom = [](bool alpha)->unsigned {
			return ((alpha ? ((rand_getint())&0xff)<<24 : 0xff000000) | ((rand_getint())&0xff)<<16 | ((rand_getint())&0xff)<<8 | ((rand_getint())&0xff));
		};

		auto lam_colorFade = [](int col_start,int col_end,float progress) {
			if(progress>1.0f) progress = 1.0f;
			int ret = col_start;
			unsigned char *col[3];
			col[0] = (unsigned char*)&col_start;
			col[1] = (unsigned char*)&col_end;
			col[2] = (unsigned char*)&ret;
			for(int i = 0; i < 4; i++) {
				col[2][i] = (unsigned char)CLAMP(col[0][i] + (float)(col[1][i]-col[0][i]) * progress,0,255);
			}
			return ret;
		};

		static unsigned col_points[2] = {lam_colorRandom(false),lam_colorRandom(false)};
		static unsigned int col_curi = 0;
		unsigned int col_max = HDX_MAIN->getFPS()*2;

		if(col_curi > col_max) {
			col_points[0] = col_points[1];
			col_points[1] = lam_colorRandom(false);
			col_curi = 0;
		}

		HDX_MAIN->setWindowClearColor(lam_colorFade(col_points[0],col_points[1],CLAMP(col_curi/(float)col_max,0.0f,1.0f)));
		col_curi++;

		MSGMAN->update();

		mmenu_map.update(dt);
	});

	HDX_MAIN->registerFuncRenderSprite([&](ID3DXSprite *com)->void {
		mmenu_map.render();
	});

	mcurselection = 0;

	{
		CTreeCtrl *tree = (CTreeCtrl*)GetDlgItem(IDC_TREE_NEWELEMENT);
		HTREEITEM itm_objects = tree->InsertItem("Objects");
		tree->SetItemData(itm_objects,0);
		tree->SetItemState(itm_objects,TVIS_BOLD|TVIS_EXPANDED,TVIS_BOLD|TVIS_EXPANDED);
		tree->SetItemData(tree->InsertItem("Menu",itm_objects),(DWORD_PTR)"menu");
		tree->SetItemData(tree->InsertItem("Alignmnet",itm_objects),(DWORD_PTR)"alignment");
		tree->SetItemData(tree->InsertItem("Button",itm_objects),(DWORD_PTR)"button");
		tree->SetItemData(tree->InsertItem("Checkbox",itm_objects),(DWORD_PTR)"checkbox");
		tree->SetItemData(tree->InsertItem("Slider",itm_objects),(DWORD_PTR)"slider");
	}

	{
		CTreeCtrl *tree = (CTreeCtrl*)GetDlgItem(IDC_TREE_NEWEFFECT);
		HTREEITEM itm_standard = tree->InsertItem("Standard");
		tree->SetItemState(itm_standard,TVIS_BOLD|TVIS_EXPANDED,TVIS_BOLD|TVIS_EXPANDED);
		tree->SetItemData(tree->InsertItem("Draw Text",itm_standard),(DWORD_PTR)"draw_text");
		tree->SetItemData(tree->InsertItem("Draw Texture",itm_standard),(DWORD_PTR)"draw_texture");
		tree->SetItemData(tree->InsertItem("Element Event",itm_standard),(DWORD_PTR)"event");

		HTREEITEM itm_oscillate = tree->InsertItem("Variable Oscillate");
		tree->SetItemState(itm_oscillate,TVIS_BOLD|TVIS_EXPANDED,TVIS_BOLD|TVIS_EXPANDED);
		tree->SetItemData(tree->InsertItem("Variable: position",itm_oscillate),(DWORD_PTR)"var_oscillate,position");
		tree->SetItemData(tree->InsertItem("Variable: scale",itm_oscillate),(DWORD_PTR)"var_oscillate,scale");
		tree->SetItemData(tree->InsertItem("Variable: rotation",itm_oscillate),(DWORD_PTR)"var_oscillate,rotation");

		HTREEITEM itm_lerp = tree->InsertItem("Variable LERP (transition)");
		tree->SetItemState(itm_lerp,TVIS_BOLD|TVIS_EXPANDED,TVIS_BOLD|TVIS_EXPANDED);
		tree->SetItemData(tree->InsertItem("Variable: position",itm_lerp),(DWORD_PTR)"transition_lerp,position");
		tree->SetItemData(tree->InsertItem("Variable: scale",itm_lerp),(DWORD_PTR)"transition_lerp,scale");
		tree->SetItemData(tree->InsertItem("Variable: rotation",itm_lerp),(DWORD_PTR)"transition_lerp,rotation");
	}

	return TRUE;
}

void DLGMain::OnTimer(UINT evtid) {
	switch(evtid) {
		case TIMER_UPDATE: {
			HDX_SOUNDMAN->update();
			HDX_MAIN->step();
			break;
		}
		case TIMER_ELEMENTTREECLICK: {
			KillTimer(evtid);

			CTreeCtrl *tree = (CTreeCtrl*)g_app.mdlg.GetDlgItem(IDC_TREE_ELEMENTS);
			HTREEITEM sel = tree->GetSelectedItem();
			if(sel != NULL) {
				HDXUIElementBase *element = (HDXUIElementBase*)tree->GetItemData(sel);
				selectElement(element);
			}
			break;
		}
	}
}

void DLGMain::OnNMClickTreeElements(NMHDR *pNMHDR,LRESULT *pResult) {
	*pResult = 0;

	SetTimer(TIMER_ELEMENTTREECLICK,1,0);
}

void DLGMain::OnBnClickedButtonLoad() {
	CFileDialog dlg(TRUE,
					_T("xml"),
					NULL,
					OFN_FILEMUSTEXIST,
					_T("XML Files (.xml)|*.xml|"),
					this);
	char buff[256];
	GetFullPathName("data\\ui",sizeof(buff),buff,0);
	dlg.m_ofn.lpstrInitialDir = buff;
	if(dlg.DoModal() == IDOK) {
		CString fname = dlg.GetPathName();
		loadXML((const char*)fname,"",true);
	}
}

void DLGMain::OnBnClickedButtonSave() {
	CFileDialog dlg(FALSE,
					_T("xml"),
					NULL,
					OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,
					_T("XML Files (.xml)|*.xml|"),
					this);
	char buff[256];
	GetFullPathName("data\\ui",sizeof(buff),buff,0);
	dlg.m_ofn.lpstrInitialDir = buff;
	if(dlg.DoModal() == IDOK) {
		CString fname = dlg.GetPathName();
		saveXML((const char*)fname);
	}
}

void DLGMain::OnBnClickedButtonAddnewelement() {
	CTreeCtrl *tree = (CTreeCtrl*)g_app.mdlg.GetDlgItem(IDC_TREE_NEWELEMENT);

	HTREEITEM sel = tree->GetSelectedItem();
	if(sel != NULL) {
		const char *etype = (const char*)tree->GetItemData(sel);
		HDXUIElementBase *parent = mcurselection;
		HDXUIElementBase *newelement = 0;

		std::string newname = uiGenerateValidName(etype,[&](const std::string &test)->bool {
			if(parent) {
				return parent->getChildElement(test)==0;
			} else {
				return mmenu_map.getMenu(test)==0;
			}
		});

		if(strcmp(etype,"menu")==0) {
			if(parent==0) {
				newelement = mmenu_map.createMenu(newname);
			} else {
				newelement = parent->createChildElement<HDXUIMenu>(newname);
			}
			newelement->createEffect<HDXUIElementEffect_TextureDraw>("texture_main",
																	 "data/texture/ui/menubackground.png");
		} else if(strcmp(etype,"alignment")==0) {
			if(parent) {
				newelement = parent->createChildElement<HDXUIElement_Alignment>(newname,
																				HDXAL_CENTER,
																				VEC2TOARG(YUNIT2),
																				0.0f,
																				0.0f,
																				10.0f);
			}
		} else if(strcmp(etype,"button")==0) {
			if(parent) {
				newelement = parent->createChildElement<HDXUIElement_Button>(newname,
																			 "data/texture/ui/basebutton.png",
																			 "data/texture/ui/basebutton_hover.png",
																			 "[ui.font_head]",
																			 "Text",
																			 DT_CENTER|DT_VCENTER,
																			 0xffffffff);
			}
		} else if(strcmp(etype,"checkbox")==0) {
			if(parent) {
				newelement = parent->createChildElement<HDXUIElement_Checkbox>(newname,
																			   "data/texture/ui/basecheckbox.png",
																			   "data/texture/ui/basecheckbox_hover.png",
																			   "data/texture/ui/basecheckbox_check.png",
																			   "[ui.font_head]",
																			   "Text",
																			   DT_CENTER|DT_VCENTER,
																			   0xffffffff);
			}
		} else if(strcmp(etype,"slider")==0) {
			if(parent) {
				newelement = parent->createChildElement<HDXUIElement_Slider>(newname,
																			 "data/texture/ui/baseslider.png",
																			 "data/texture/ui/baseslider_handle.png",
																			 "[ui.font_head]",
																			 "Text",
																			 DT_CENTER|DT_VCENTER,
																			 0xffffffff);
			}
		}

		if(parent) {
			if(parent->xml_getTypeName() == "alignment") {
				// the alignment object sets the position of the new object, thereby contaminating its editable values
				// so sero it out and when the changes are applied it will reload and properly reset the position
				newelement->setPosition(0,0);
			}
		}

		if(newelement) {
			uiActorOnCreate(newelement);
			newelement->getEffects().doIterate([&](HDXUIElementEffect_Base *effect)->bool {
				uiActorOnCreate(effect);
				return false;
			});

			OnBnClickedButtonApplychanges();
			//			selectElement(newelement);
		}
	}
}

void DLGMain::OnBnClickedButtonAddneweffect() {
	CTreeCtrl *tree = (CTreeCtrl*)g_app.mdlg.GetDlgItem(IDC_TREE_NEWEFFECT);

	HTREEITEM sel = tree->GetSelectedItem();
	if(sel != NULL) {
		std::string etype,arg;
		etype = (const char*)tree->GetItemData(sel);
		{
			unsigned loc_comma = etype.find_first_of(',');
			if(loc_comma!=std::string::npos) {
				arg = etype.substr(loc_comma+1,etype.length()-loc_comma-1);
				etype = etype.substr(0,loc_comma);
			} else {
				arg = "";
			}
		}
		HDXUIElementBase *parent = mcurselection;
		HDXUIElementEffect_Base *neweffect = 0;

		std::string newname = uiGenerateValidName(etype,[&](const std::string &test)->bool {
			return parent->getEffect(test)==0;
		});

		if(etype == "draw_text") {
			neweffect = parent->createEffect<HDXUIElementEffect_TextDraw>(newname,
																		  "[ui.font_head]",
																		  "Text",
																		  DT_CENTER|DT_VCENTER,
																		  0xffffffff);
		} else if(etype == "draw_texture") {
			neweffect = parent->createEffect<HDXUIElementEffect_TextureDraw>(newname,
																			 "");
		} else if(etype == "var_oscillate") {
			if(arg == "position") {
				neweffect = parent->createEffect<HDXUIElementEffect_Var_Oscillate<HDXVector2>>(newname,
																								[=](void) {return parent->getPosition(); },
																								[=](const HDXVector2 &var) {return parent->setPosition(var); },
																								HDXVector2(10,10),
																								2.0f,
																								0.0f);
			} else if(arg == "scale") {
				neweffect = parent->createEffect<HDXUIElementEffect_Var_Oscillate<HDXVector2>>(newname,
																								[=](void) {return parent->getScale(); },
																								[=](const HDXVector2 &var) {return parent->setScale(var); },
																								HDXVector2(2,2),
																								2.0f,
																								0.0f);
			} else if(arg == "rotation") {
				neweffect = parent->createEffect<HDXUIElementEffect_Var_Oscillate<float>>(newname,
																						  [=](void) {return parent->getRotation(); },
																						  [=](const float &var) {return parent->setRotation(var); },
																						  1.0f,
																						  2.0f,
																						  0.0f);
			}
		} else if(etype == "transition_lerp") {
			if(arg == "position") {
				neweffect = parent->createEffect<HDXUIElementEffect_Transition_LERP<HDXVector2>>(newname,
																								  [=](void) {return parent->getPosition(); },
																								  [=](const HDXVector2 &var) {return parent->setPosition(var); },
																								  HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																								  2.0f,
																								  HDXVector2(10,10));
			} else if(arg == "scale") {
				neweffect = parent->createEffect<HDXUIElementEffect_Transition_LERP<HDXVector2>>(newname,
																								  [=](void) {return parent->getScale(); },
																								  [=](const HDXVector2 &var) {return parent->setScale(var); },
																								  HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																								  2.0f,
																								  HDXVector2(1,1));
			} else if(arg == "rotation") {
				neweffect = parent->createEffect<HDXUIElementEffect_Transition_LERP<float>>(newname,
																							[=](void) {return parent->getRotation(); },
																							[=](const float &var) {return parent->setRotation(var); },
																							HDX_TRANS_ENTER|HDX_TRANS_EXIT,
																							2.0f,
																							6.28f);
			}
		} else if(etype == "event") {
			dlgnewevent dlg;
			if(dlg.DoModal() == IDOK) {
				uiUserData_element::uiEvent::ArgCont args;
				if(dlg.mchoice_action == "MENU_NAVIGATE_MENU") {
					args.emplace_back("target","");
					args.emplace_back("dotransition","true");
				} else if(dlg.mchoice_action == "MENU_NAVIGATE_BACK") {
					args.emplace_back("dotransition","true");
				} else if(dlg.mchoice_action == "SOUND_PLAY") {
					args.emplace_back("file","");
					args.emplace_back("volume","1.0");
				}

				uiUserData_element *ud = (uiUserData_element*)parent->getUserData();
				ud->eventAdd(dlg.mchoice_event,dlg.mchoice_action,std::move(args));
				OnBnClickedButtonApplychanges();
			}
		}

		if(neweffect) {
			uiActorOnCreate(neweffect,arg);
			OnBnClickedButtonApplychanges();
		}
	}
}

void DLGMain::OnBnClickedButtonApplychanges() {
	std::vector<std::string> curselectpath;
	{
		HDXUIElementBase *cur = mcurselection;
		while(cur) {
			curselectpath.push_back(cur->getName());
			cur = cur->getParent();
		}
	}

	selectElement(0);

	saveXML("editortmp.xml");
	loadXML("editortmp.xml","",false);
	std::remove("editortmp.xml");

	if(curselectpath.size()>0) {
		HDXUIElementBase *cur = mmenu_map.getMenu(curselectpath.back());
		for(auto it = curselectpath.rbegin()+1; it != curselectpath.rend(); it++) {
			if(cur) cur = cur->getChildElement(*it);
		}
		if(cur) {
			selectElement(cur,false);
		}
	}
}

void DLGMain::loadXML(const std::string &fname,const std::string &startmenu,bool dotransition) {
	mmenu_map.clear();
	mcurselection = 0;

	mmenu_map.createFromXML(fname,[&](IHDXUIIActor *actor,rapidxml::xml_node<> *node)->void {
		std::string data = "";
		if(strcmp(node->first_attribute("type")->value(),"transition_lerp")==0 ||
		   strcmp(node->first_attribute("type")->value(),"var_oscillate")==0) {
			data = node->first_attribute("variable")->value();
		}
		uiActorOnCreate(actor,data);
		uiUserData_base *ud = (uiUserData_base*)actor->getUserData();
		ud->xml_onLoad(node);
	});

	if(startmenu.length()>0) mmenu_map.navigateToMenu(startmenu,dotransition);

	updateElementTree();
}

void DLGMain::saveXML(const std::string &fname) {
	rapidxml::xml_document<> doc;
	rapidxml::xml_node<> *root = doc.allocate_node(rapidxml::node_element,"ui");
	doc.append_node(root);

	mmenu_map.doIterate([&](HDXUIMenu *menu)->bool {
		rapidxml::xml_node<> *n = doc.allocate_node(rapidxml::node_element,"uielement");
		root->append_node(n);
		((uiUserData_base*)menu->getUserData())->xml_save(&doc,n);
		return false;
	});

	std::ofstream os(fname,std::ios::trunc);
	os << doc;
	os.close();
}

void DLGMain::updateElementTree() {
	CTreeCtrl *tree = (CTreeCtrl*)g_app.mdlg.GetDlgItem(IDC_TREE_ELEMENTS);
	mtreeelements.clear();
	tree->DeleteAllItems();
	{
		HTREEITEM itm = tree->InsertItem("nothing",TVI_ROOT);
		mtreeelements[0] = itm;
		tree->SetItemState(itm,TVIS_BOLD,TVIS_BOLD);
		tree->SetItemData(itm,0);
	}
	mmenu_map.doIterate([&](HDXUIMenu *menu)->bool {
		std::function<void(HDXUIElementBase *element,HTREEITEM parent)> lam_processelement = [&](HDXUIElementBase *element,HTREEITEM parent)->void {
			// if userdata is null the element (and its children) are derived objects that arent editable
			if(element->getUserData()) {
				HTREEITEM itm = tree->InsertItem(element->getName().c_str(),parent);
				mtreeelements[element] = itm;
				tree->SetItemState(itm,TVIS_EXPANDED,TVIS_EXPANDED);
				tree->SetItemData(itm,(DWORD_PTR)element);
				element->getChilderen().doIterate([&](HDXUIElementBase *child)->bool {
					lam_processelement(child,itm);
					return false;
				});
			}
		};

		lam_processelement(menu,TVI_ROOT);
		return false;
	});
}

void DLGMain::selectElement(HDXUIElementBase *element,bool dotransition) {
	CTreeCtrl *tree = (CTreeCtrl*)g_app.mdlg.GetDlgItem(IDC_TREE_ELEMENTS);
	HTREEITEM itm = mtreeelements.count(element)>0 ? mtreeelements[element] : 0;
	tree->SelectItem(itm);

	if(mcurselection != element) {
		if(mcurselection) {
			uiUserData_base *ud = (uiUserData_base*)mcurselection->getUserData();
			ud->editor_onDeselection();
			mcurselection->getEffects().doIterate([&](HDXUIElementEffect_Base *effect)->bool {
				if(effect->getUserData()) {
					uiUserData_base *ud = (uiUserData_base*)effect->getUserData();
					ud->editor_onDeselection();
				}
				return false;
			});
		}

		HDXUIMenu *tmenu = 0;
		{
			HDXUIElementBase *cur = element;
			while(cur) {
				if(cur->xml_getTypeName() == "menu") {
					tmenu = (HDXUIMenu*)cur;
					break;
				}
				cur = cur->getParent();
			}
		}

		if(mmenu_map.getCurrentMenu() != tmenu) {
			mmenu_map.navigateToMenu(tmenu ? tmenu->getName() : "",dotransition);
		}

		propgridClear();
		if(element) {
			uiUserData_base *ud = (uiUserData_base*)element->getUserData();
			ud->editor_onSelection();
			element->getEffects().doIterate([&](HDXUIElementEffect_Base *effect)->bool {
				if(effect->getUserData()) {
					uiUserData_base *ud = (uiUserData_base*)effect->getUserData();
					ud->editor_onSelection();
				}
				return false;
			});
		}
		mcurselection = element;
	}
}

void DLGMain::OnBnClickedButtonDeleteSelectedElement() {
	CTreeCtrl *tree = (CTreeCtrl*)g_app.mdlg.GetDlgItem(IDC_TREE_ELEMENTS);
	HTREEITEM sel = tree->GetSelectedItem();
	if(sel != NULL) {
		HDXUIElementBase *element = (HDXUIElementBase*)tree->GetItemData(sel);
		if(element) {
			HDXUIElementBase *parent = element->getParent();
			selectElement(parent);
			uiUserData_base *ud = (uiUserData_base*)element->getUserData();
			delete ud;
			if(parent) {
				parent->destroyChildElement(element->getName());
			} else {
				mmenu_map.destroyMenu(element->getName());
			}
			OnBnClickedButtonApplychanges();
		}
	}
}

void DLGMain::propgridClear() {
	mpropgrid.ResetContents();
	mpropgrid_itemlist_string.clear();
	mpropgrid_itemlist_bool.clear();
	for(auto it = mpropgrid_itemlist_button.begin(); it != mpropgrid_itemlist_button.end(); it++) {
		delete it->first;
	}
	mpropgrid_itemlist_button.clear();
}

void DLGMain::propgridRemoveItem(HITEM itm) {
	for(auto it = mpropgrid_itemlist_string.begin(); it != mpropgrid_itemlist_string.end(); it++) {
		if(it->second == itm) {
			mpropgrid_itemlist_string.erase(it);
			return;
		}
	}

	for(auto it = mpropgrid_itemlist_bool.begin(); it != mpropgrid_itemlist_bool.end(); it++) {
		if(it->second == itm) {
			mpropgrid_itemlist_bool.erase(it);
			return;
		}
	}

	for(auto it = mpropgrid_itemlist_button.begin(); it != mpropgrid_itemlist_button.end(); it++) {
		if(it->second == itm) {
			delete it->first;
			mpropgrid_itemlist_button.erase(it);
			return;
		}
	}
}

HSECTION DLGMain::propgridAddSection(const std::string &name) {
	return mpropgrid.AddSection(name);
}

HITEM DLGMain::propgridAddTextItem(std::string *var,HSECTION section,const std::string &item,const std::string &defvalue,bool editable) {
	if(mpropgrid_itemlist_string.count(var)==0) return mpropgrid_itemlist_string[var] = mpropgrid.AddTextItem(section,item,defvalue,editable);
	return 0;
}

HITEM DLGMain::propgridAddStringItem(std::string *var,HSECTION section,const std::string &item,const std::string &defvalue,bool editable) {
	if(mpropgrid_itemlist_string.count(var)==0) return mpropgrid_itemlist_string[var] = mpropgrid.AddStringItem(section,item,defvalue,editable);
	return 0;
}

HITEM DLGMain::propgridAddBoolItem(bool *var,HSECTION section,const std::string &item,bool defvalue,bool editable) {
	if(mpropgrid_itemlist_bool.count(var)==0) return mpropgrid_itemlist_bool[var] = mpropgrid.AddBoolItem(section,item,defvalue,editable);
	return 0;
}

HITEM DLGMain::propgridAddButtonItem(HSECTION section,const std::string &item,COLORREF color,std::function<void(void)> func) {
	propgrid_button *newbutton = new propgrid_button();
	newbutton->mcolor = color;
	newbutton->mfunc_onclick = func;
	return mpropgrid_itemlist_button[newbutton] = mpropgrid.AddCustomItem(section,item,newbutton);
}

void DLGMain::propgridRetrieveValue(std::string *ptr) {
	if(mpropgrid_itemlist_string.count(ptr)>0) {
		mpropgrid.GetItemValue(mpropgrid_itemlist_string[ptr],*ptr);
	}
}

void DLGMain::propgridRetrieveValue(bool *ptr) {
	if(mpropgrid_itemlist_bool.count(ptr)>0) {
		mpropgrid.GetItemValue(mpropgrid_itemlist_bool[ptr],*ptr);
	}
}

// DLGMain message handlers

uiUserData_base::uiUserData_base(IHDXUIIActor *owner) {
	mowner = owner;

	mvar_name = getOwner()->getName();
	mvar_type = getOwner()->xml_getTypeName();
}

void uiUserData_base::editor_onSelection() {
}

void uiUserData_base::editor_onDeselection() {
}

void uiUserData_base::xml_onLoad(rapidxml::xml_node<> *node) {
}

void uiUserData_base::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	node->append_attribute(doc->allocate_attribute("name",mvar_name.c_str()));
	node->append_attribute(doc->allocate_attribute("type",mvar_type.c_str()));
}

uiUserData_element::uiUserData_element(IHDXUIIActor *owner) : uiUserData_base(owner) {
	XMLToString(&mvar_position_x,getOwner()->getPosition().x);
	XMLToString(&mvar_position_y,getOwner()->getPosition().y);
	XMLToString(&mvar_scale_x,getOwner()->getScale().x);
	XMLToString(&mvar_scale_y,getOwner()->getScale().y);
	XMLToString(&mvar_rotation,getOwner()->getRotation());
	XMLToString(&mvar_blendcolor,getOwner()->getBlendColor());
	XMLToString(&mvar_enabled,getOwner()->isEnabled());
	XMLToString(&mvar_active,getOwner()->isActive());
	XMLToString(&mvar_actionsize_x,getOwner()->getActionSize().x);
	XMLToString(&mvar_actionsize_y,getOwner()->getActionSize().y);
}

void uiUserData_element::editor_onSelection() {
	uiUserData_base::editor_onSelection();
	HSECTION sec = g_app.mdlg.propgridAddSection("Element");
	g_app.mdlg.propgridAddButtonItem(sec,"Delete Element",RGB(255,0,0),[&]() {
		g_app.mdlg.OnBnClickedButtonDeleteSelectedElement();
	});
	g_app.mdlg.propgridAddStringItem(&mvar_name,sec,"Name",mvar_name);
	g_app.mdlg.propgridAddStringItem(&mvar_type,sec,"Type",mvar_type,false);
	g_app.mdlg.propgridAddStringItem(&mvar_position_x,sec,"Position.x",mvar_position_x);
	g_app.mdlg.propgridAddStringItem(&mvar_position_y,sec,"Position.y",mvar_position_y);
	g_app.mdlg.propgridAddStringItem(&mvar_scale_x,sec,"Scale.x",mvar_scale_x);
	g_app.mdlg.propgridAddStringItem(&mvar_scale_y,sec,"Scale.y",mvar_scale_y);
	g_app.mdlg.propgridAddStringItem(&mvar_rotation,sec,"Rotation",mvar_rotation);
	g_app.mdlg.propgridAddStringItem(&mvar_blendcolor,sec,"Blend Color",mvar_blendcolor);
	g_app.mdlg.propgridAddStringItem(&mvar_enabled,sec,"enabled",mvar_enabled);
	g_app.mdlg.propgridAddStringItem(&mvar_active,sec,"active",mvar_active);
	g_app.mdlg.propgridAddStringItem(&mvar_actionsize_x,sec,"ActionSize.x",mvar_actionsize_x);
	g_app.mdlg.propgridAddStringItem(&mvar_actionsize_y,sec,"ActionSize.y",mvar_actionsize_y);

	for(auto it = mevents.begin(); it != mevents.end(); it++) {
		HSECTION evtsec = g_app.mdlg.propgridAddSection("Event");
		g_app.mdlg.propgridAddStringItem(&(*it)->mvar_event,evtsec,"Event",(*it)->mvar_event,false);
		g_app.mdlg.propgridAddStringItem(&(*it)->mvar_action,evtsec,"Action",(*it)->mvar_action,false);
		uiEvent *curevt = *it;
		g_app.mdlg.propgridAddButtonItem(evtsec,"Delete Event",RGB(128,128,0),[=]() {
			eventRemove(curevt);
			g_app.mdlg.OnBnClickedButtonApplychanges();
		});
		for(auto argit = (*it)->marguments.begin(); argit != (*it)->marguments.end(); argit++) {
			g_app.mdlg.propgridAddStringItem(&argit->second,evtsec,argit->first,argit->second);
		}
	}
}

void uiUserData_element::editor_onDeselection() {
	uiUserData_base::editor_onDeselection();

	g_app.mdlg.propgridRetrieveValue(&mvar_name);
	g_app.mdlg.propgridRetrieveValue(&mvar_type);
	g_app.mdlg.propgridRetrieveValue(&mvar_position_x);
	g_app.mdlg.propgridRetrieveValue(&mvar_position_y);
	g_app.mdlg.propgridRetrieveValue(&mvar_scale_x);
	g_app.mdlg.propgridRetrieveValue(&mvar_scale_y);
	g_app.mdlg.propgridRetrieveValue(&mvar_rotation);
	g_app.mdlg.propgridRetrieveValue(&mvar_blendcolor);
	g_app.mdlg.propgridRetrieveValue(&mvar_enabled);
	g_app.mdlg.propgridRetrieveValue(&mvar_active);
	g_app.mdlg.propgridRetrieveValue(&mvar_actionsize_x);
	g_app.mdlg.propgridRetrieveValue(&mvar_actionsize_y);

	for(auto it = mevents.begin(); it != mevents.end(); it++) {
		for(auto argit = (*it)->marguments.begin(); argit != (*it)->marguments.end(); argit++) {
			g_app.mdlg.propgridRetrieveValue(&argit->second);
		}
	}
}

void uiUserData_element::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_base::xml_onLoad(node);

	mevents.clear();

	for(rapidxml::xml_node<> *node_i = node->first_node(); node_i; node_i = node_i->next_sibling()) {
		if(strcmp(node_i->name(),"position")==0) {
			mvar_position_x = XMLGetNodeValueRemoveScript(node_i->first_node("x"));
			mvar_position_y = XMLGetNodeValueRemoveScript(node_i->first_node("y"));
		} else if(strcmp(node_i->name(),"scale")==0) {
			mvar_scale_x = XMLGetNodeValueRemoveScript(node_i->first_node("x"));
			mvar_scale_y = XMLGetNodeValueRemoveScript(node_i->first_node("y"));
		} else if(strcmp(node_i->name(),"rotation")==0) {
			mvar_rotation = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"blendcolor")==0) {
			mvar_blendcolor = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"enabled")==0) {
			mvar_enabled = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"active")==0) {
			mvar_active = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"actionsize")==0) {
			mvar_actionsize_x = XMLGetNodeValueRemoveScript(node_i->first_node("x"));
			mvar_actionsize_y = XMLGetNodeValueRemoveScript(node_i->first_node("y"));
		} else if(strcmp(node_i->name(),"uievent")==0) {
			uiEvent::ArgCont args;
			for(rapidxml::xml_node<> *node_j = node_i->first_node(); node_j; node_j = node_j->next_sibling()) {
				args.emplace_back(node_j->name(),XMLGetNodeValueRemoveScript(node_j));
			}
			eventAdd(node_i->first_attribute("event")->value(),
					 node_i->first_attribute("action")->value(),
					 std::move(args));
		}
	}
}

void uiUserData_element::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
		{
			auto n = doc->allocate_node(rapidxml::node_element,"position",0);
			node->append_node(n);
			XMLAppendScriptNode(doc,n,"x",mvar_position_x);
			XMLAppendScriptNode(doc,n,"y",mvar_position_y);
		}
	{
		auto n = doc->allocate_node(rapidxml::node_element,"scale",0);
		node->append_node(n);
		XMLAppendScriptNode(doc,n,"x",mvar_scale_x);
		XMLAppendScriptNode(doc,n,"y",mvar_scale_y);
	}
	XMLAppendScriptNode(doc,node,"rotation",mvar_rotation);
	XMLAppendScriptNode(doc,node,"blendcolor",mvar_blendcolor);
	XMLAppendScriptNode(doc,node,"enabled",mvar_enabled);
	XMLAppendScriptNode(doc,node,"active",mvar_active);
	{
		auto n = doc->allocate_node(rapidxml::node_element,"actionsize",0);
		node->append_node(n);
		XMLAppendScriptNode(doc,n,"x",mvar_actionsize_x);
		XMLAppendScriptNode(doc,n,"y",mvar_actionsize_y);
	}

	for(auto it = mevents.begin(); it != mevents.end(); it++) {
		auto n = doc->allocate_node(rapidxml::node_element,"uievent",0);
		node->append_node(n);
		n->append_attribute(doc->allocate_attribute("event",doc->allocate_string((*it)->mvar_event.c_str())));
		n->append_attribute(doc->allocate_attribute("action",doc->allocate_string((*it)->mvar_action.c_str())));
		for(auto argit = (*it)->marguments.begin(); argit != (*it)->marguments.end(); argit++) {
			XMLAppendScriptNode(doc,n,argit->first,argit->second);
		}
	}

	{
		getOwner()->getChilderen().doIterate([&](HDXUIElementBase *element)->bool {
			if(element->getUserData()) {
				rapidxml::xml_node<> *n = doc->allocate_node(rapidxml::node_element,"uielement");
				node->append_node(n);
				((uiUserData_base*)element->getUserData())->xml_save(doc,n);
			}
			return false;
		});

		getOwner()->getEffects().doIterate([&](HDXUIElementEffect_Base *effect)->bool {
			if(effect->getUserData()) {
				rapidxml::xml_node<> *n = doc->allocate_node(rapidxml::node_element,"uieffect");
				node->append_node(n);
				((uiUserData_base*)effect->getUserData())->xml_save(doc,n);
			}
			return false;
		});
	}
	uiUserData_base::xml_save(doc,node);
}

uiUserData_element::uiEvent* uiUserData_element::eventAdd(const std::string &evt,const std::string &action,const uiEvent::ArgCont &args) {
	uiEvent *newevt = new uiEvent;
	mevents.insert(newevt);

	newevt->mvar_action = action;
	newevt->mvar_event = evt;

	newevt->marguments = args;

	return newevt;
}

void uiUserData_element::eventRemove(uiUserData_element::uiEvent *evt) {
	mevents.erase(evt);
	delete evt;
}

void uiUserData_element::eventRemoveAll() {
	for(auto it = mevents.begin(); it != mevents.end(); it++) {
		delete *it;
	}
	mevents.clear();
}

uiUserData_element_menu::uiUserData_element_menu(IHDXUIIActor *owner) : uiUserData_element(owner) {
}

void uiUserData_element_menu::editor_onSelection() {
	uiUserData_element::editor_onSelection();
}

void uiUserData_element_menu::editor_onDeselection() {
	uiUserData_element::editor_onDeselection();
}

void uiUserData_element_menu::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_element::xml_onLoad(node);
}

void uiUserData_element_menu::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	uiUserData_element::xml_save(doc,node);
}

uiUserData_element_alignment::uiUserData_element_alignment(IHDXUIIActor *owner) : uiUserData_element(owner) {
	XMLToString(&mvar_posflags,getOwner()->getFlags());
	XMLToString(&mvar_padding,getOwner()->getPadding());
	XMLToString(&mvar_offset_x,getOwner()->getOffset().x);
	XMLToString(&mvar_offset_y,getOwner()->getOffset().y);
	XMLToString(&mvar_direction_x,getOwner()->getDirection().x);
	XMLToString(&mvar_direction_y,getOwner()->getDirection().y);
}

void uiUserData_element_alignment::editor_onSelection() {
	uiUserData_element::editor_onSelection();
	HSECTION sec = g_app.mdlg.propgridAddSection("Alignment");
	g_app.mdlg.propgridAddStringItem(&mvar_posflags,sec,"Pos Flags",mvar_posflags);
	g_app.mdlg.propgridAddStringItem(&mvar_padding,sec,"Padding",mvar_padding);
	g_app.mdlg.propgridAddStringItem(&mvar_offset_x,sec,"Offset.x",mvar_offset_x);
	g_app.mdlg.propgridAddStringItem(&mvar_offset_y,sec,"Offset.y",mvar_offset_y);
	g_app.mdlg.propgridAddStringItem(&mvar_direction_x,sec,"Direction.x",mvar_direction_x);
	g_app.mdlg.propgridAddStringItem(&mvar_direction_y,sec,"Direction.y",mvar_direction_y);
}

void uiUserData_element_alignment::editor_onDeselection() {
	uiUserData_element::editor_onDeselection();

	g_app.mdlg.propgridRetrieveValue(&mvar_posflags);
	g_app.mdlg.propgridRetrieveValue(&mvar_padding);
	g_app.mdlg.propgridRetrieveValue(&mvar_offset_x);
	g_app.mdlg.propgridRetrieveValue(&mvar_offset_y);
	g_app.mdlg.propgridRetrieveValue(&mvar_direction_x);
	g_app.mdlg.propgridRetrieveValue(&mvar_direction_y);
}

void uiUserData_element_alignment::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_element::xml_onLoad(node);

	for(rapidxml::xml_node<> *node_i = node->first_node(); node_i; node_i = node_i->next_sibling()) {
		if(strcmp(node_i->name(),"posflags")==0) {
			mvar_posflags = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"padding")==0) {
			mvar_padding = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"offset")==0) {
			mvar_offset_x = XMLGetNodeValueRemoveScript(node_i->first_node("x"));
			mvar_offset_y = XMLGetNodeValueRemoveScript(node_i->first_node("y"));
		} else if(strcmp(node_i->name(),"direction")==0) {
			mvar_direction_x = XMLGetNodeValueRemoveScript(node_i->first_node("x"));
			mvar_direction_y = XMLGetNodeValueRemoveScript(node_i->first_node("y"));
		}
	}
}

void uiUserData_element_alignment::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	XMLAppendScriptNode(doc,node,"posflags",mvar_posflags);
	XMLAppendScriptNode(doc,node,"padding",mvar_padding);
	{
		auto n = doc->allocate_node(rapidxml::node_element,"offset",0);
		node->append_node(n);
		XMLAppendScriptNode(doc,n,"x",mvar_offset_x);
		XMLAppendScriptNode(doc,n,"y",mvar_offset_y);
	}
	{
		auto n = doc->allocate_node(rapidxml::node_element,"direction",0);
		node->append_node(n);
		XMLAppendScriptNode(doc,n,"x",mvar_direction_x);
		XMLAppendScriptNode(doc,n,"y",mvar_direction_y);
	}
	uiUserData_element::xml_save(doc,node);
}

uiUserData_element_button::uiUserData_element_button(IHDXUIIActor *owner) : uiUserData_element(owner) {
}

void uiUserData_element_button::editor_onSelection() {
	uiUserData_element::editor_onSelection();
}

void uiUserData_element_button::editor_onDeselection() {
	uiUserData_element::editor_onDeselection();
}

void uiUserData_element_button::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_element::xml_onLoad(node);
}

void uiUserData_element_button::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	uiUserData_element::xml_save(doc,node);
}

uiUserData_element_checkbox::uiUserData_element_checkbox(IHDXUIIActor *owner) : uiUserData_element(owner) {
	XMLToString(&mvar_checked,getOwner()->getChecked());
}

void uiUserData_element_checkbox::editor_onSelection() {
	uiUserData_element::editor_onSelection();

	HSECTION sec = g_app.mdlg.propgridAddSection("Checkbox");
	g_app.mdlg.propgridAddStringItem(&mvar_checked,sec,"Checked",mvar_checked);
}

void uiUserData_element_checkbox::editor_onDeselection() {
	uiUserData_element::editor_onDeselection();

	g_app.mdlg.propgridRetrieveValue(&mvar_checked);
}

void uiUserData_element_checkbox::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_element::xml_onLoad(node);

	for(rapidxml::xml_node<> *node_i = node->first_node(); node_i; node_i = node_i->next_sibling()) {
		if(strcmp(node_i->name(),"checked")==0) {
			mvar_checked = XMLGetNodeValueRemoveScript(node_i);
		}
	}
}

void uiUserData_element_checkbox::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	uiUserData_element::xml_save(doc,node);
	doc->allocate_node(rapidxml::node_element,"checked",mvar_checked.c_str());
}

uiUserData_element_slider::uiUserData_element_slider(IHDXUIIActor *owner) : uiUserData_element(owner) {
		{
			auto texeff = getOwner()->getHandleTextureEffect();
			XMLToString(&mvar_texture_handle,texeff ? texeff->getTexture() : "");
		}
	XMLToString(&mvar_value,getOwner()->getValue());
}

void uiUserData_element_slider::editor_onSelection() {
	uiUserData_element::editor_onSelection();
	HSECTION sec = g_app.mdlg.propgridAddSection("Slider");
	g_app.mdlg.propgridAddStringItem(&mvar_texture_handle,sec,"Handle Texture",mvar_texture_handle);
	g_app.mdlg.propgridAddStringItem(&mvar_value,sec,"Value",mvar_value);
}

void uiUserData_element_slider::editor_onDeselection() {
	uiUserData_element::editor_onDeselection();

	g_app.mdlg.propgridRetrieveValue(&mvar_texture_handle);
	g_app.mdlg.propgridRetrieveValue(&mvar_value);
}

void uiUserData_element_slider::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_element::xml_onLoad(node);

	for(rapidxml::xml_node<> *node_i = node->first_node(); node_i; node_i = node_i->next_sibling()) {
		if(strcmp(node_i->name(),"texture_handle")==0) {
			mvar_texture_handle = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"value")==0) {
			mvar_value = XMLGetNodeValueRemoveScript(node_i);
		}
	}
}

void uiUserData_element_slider::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	uiUserData_element::xml_save(doc,node);

	XMLAppendScriptNode(doc,node,"texture_handle",mvar_texture_handle,true);
	XMLAppendScriptNode(doc,node,"value",mvar_value);
}

uiUserData_effect::uiUserData_effect(IHDXUIIActor *owner) : uiUserData_base(owner) {
	meditorsection = 0;
}

void uiUserData_effect::editor_onSelection() {
	uiUserData_base::editor_onSelection();
	if(meditorsection == 0) {
		meditorsection = g_app.mdlg.propgridAddSection("Effect: "+mvar_name);
		g_app.mdlg.propgridAddButtonItem(meditorsection,"Delete Effect",RGB(255,128,0),[&]() {
			uiUserData_effect *self = this;
			auto owner = getOwner();
			HDXUIElementBase *parent = owner->getParent();
			parent->destroyEffect(owner->getName());
			g_app.mdlg.OnBnClickedButtonApplychanges();
			delete self;
		});
		g_app.mdlg.propgridAddStringItem(&mvar_name,meditorsection,"Name",mvar_name);
		g_app.mdlg.propgridAddStringItem(&mvar_type,meditorsection,"Type",mvar_type,false);
	}
}

void uiUserData_effect::editor_onDeselection() {
	uiUserData_base::editor_onDeselection();
	meditorsection = 0;

	g_app.mdlg.propgridRetrieveValue(&mvar_name);
	g_app.mdlg.propgridRetrieveValue(&mvar_type);
}

void uiUserData_effect::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_base::xml_onLoad(node);
}

void uiUserData_effect::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	uiUserData_base::xml_save(doc,node);
}

uiUserData_effect_textdraw::uiUserData_effect_textdraw(IHDXUIIActor *owner) : uiUserData_effect(owner) {
	XMLToString(&mvar_font,getOwner()->getFont());
	XMLToString(&mvar_color,getOwner()->getFontDrawColor());
	XMLToString(&mvar_drawflags,getOwner()->getFontDrawFlags());

	{
		mvar_drawrect_actionsize = true;
		RECT drawrect;
		getOwner()->getFontDrawRect(&drawrect);
		XMLToString(&mvar_drawrect_rect_t,drawrect.top);
		XMLToString(&mvar_drawrect_rect_l,drawrect.left);
		XMLToString(&mvar_drawrect_rect_b,drawrect.bottom);
		XMLToString(&mvar_drawrect_rect_r,drawrect.right);
	}

	XMLToString(&mvar_text,getOwner()->getText());
}

void uiUserData_effect_textdraw::editor_onSelection() {
	uiUserData_effect::editor_onSelection();
	g_app.mdlg.propgridAddStringItem(&mvar_font,meditorsection,"Font",mvar_font);
	g_app.mdlg.propgridAddStringItem(&mvar_color,meditorsection,"Color",mvar_color);
	g_app.mdlg.propgridAddStringItem(&mvar_drawflags,meditorsection,"Draw Flags",mvar_drawflags);
	g_app.mdlg.propgridAddBoolItem(&mvar_drawrect_actionsize,meditorsection,"Base Draw Rect on Element Size",mvar_drawrect_actionsize);
	g_app.mdlg.propgridAddStringItem(&mvar_drawrect_rect_t,meditorsection,"Draw Rect.top",mvar_drawrect_rect_t);
	g_app.mdlg.propgridAddStringItem(&mvar_drawrect_rect_l,meditorsection,"Draw Rect.left",mvar_drawrect_rect_l);
	g_app.mdlg.propgridAddStringItem(&mvar_drawrect_rect_b,meditorsection,"Draw Rect.bottom",mvar_drawrect_rect_b);
	g_app.mdlg.propgridAddStringItem(&mvar_drawrect_rect_r,meditorsection,"Draw Rect.right",mvar_drawrect_rect_r);
	g_app.mdlg.propgridAddTextItem(&mvar_text,meditorsection,"Text",mvar_text);
}

void uiUserData_effect_textdraw::editor_onDeselection() {
	uiUserData_effect::editor_onDeselection();

	g_app.mdlg.propgridRetrieveValue(&mvar_font);
	g_app.mdlg.propgridRetrieveValue(&mvar_color);
	g_app.mdlg.propgridRetrieveValue(&mvar_drawflags);
	g_app.mdlg.propgridRetrieveValue(&mvar_drawrect_actionsize);
	g_app.mdlg.propgridRetrieveValue(&mvar_drawrect_rect_t);
	g_app.mdlg.propgridRetrieveValue(&mvar_drawrect_rect_l);
	g_app.mdlg.propgridRetrieveValue(&mvar_drawrect_rect_b);
	g_app.mdlg.propgridRetrieveValue(&mvar_drawrect_rect_r);
	g_app.mdlg.propgridRetrieveValue(&mvar_text);
}

void uiUserData_effect_textdraw::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_effect::xml_onLoad(node);

	for(rapidxml::xml_node<> *node_i = node->first_node(); node_i; node_i = node_i->next_sibling()) {
		if(strcmp(node_i->name(),"font")==0) {
			mvar_font = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"color")==0) {
			mvar_color = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"drawflags")==0) {
			mvar_drawflags = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"text")==0) {
			mvar_text = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"drawrect")==0) {
			if(strcmp(node_i->first_attribute("mode")->value(),"actionsize")==0) {
				mvar_drawrect_actionsize = true;
			} else if(strcmp(node_i->first_attribute("mode")->value(),"rect")==0) {
				mvar_drawrect_actionsize = false;
				mvar_drawrect_rect_t = XMLGetNodeValueRemoveScript(node_i->first_node("t"));
				mvar_drawrect_rect_l = XMLGetNodeValueRemoveScript(node_i->first_node("l"));
				mvar_drawrect_rect_b = XMLGetNodeValueRemoveScript(node_i->first_node("b"));
				mvar_drawrect_rect_r = XMLGetNodeValueRemoveScript(node_i->first_node("r"));
			}
		}
	}
}

void uiUserData_effect_textdraw::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	XMLAppendScriptNode(doc,node,"font",mvar_font,true);
	XMLAppendScriptNode(doc,node,"color",mvar_color);
	XMLAppendScriptNode(doc,node,"drawflags",mvar_drawflags);
	XMLAppendScriptNode(doc,node,"text",mvar_text,true);
	{
		auto n = doc->allocate_node(rapidxml::node_element,"drawrect",0);
		node->append_node(n);
		n->append_attribute(doc->allocate_attribute("mode",mvar_drawrect_actionsize ? "actionsize" : "rect"));
		if(!mvar_drawrect_actionsize) {
			XMLAppendScriptNode(doc,n,"t",mvar_drawrect_rect_t);
			XMLAppendScriptNode(doc,n,"l",mvar_drawrect_rect_l);
			XMLAppendScriptNode(doc,n,"b",mvar_drawrect_rect_b);
			XMLAppendScriptNode(doc,n,"r",mvar_drawrect_rect_r);
		}
	}
	uiUserData_effect::xml_save(doc,node);
}

uiUserData_effect_texturedraw::uiUserData_effect_texturedraw(IHDXUIIActor *owner) : uiUserData_effect(owner) {
	XMLToString(&mvar_file,getOwner()->getTexture());
	XMLToString(&mvar_blendcolor,getOwner()->getBlendColor());
	{
		mvar_sourcerect_texture = true;
		RECT sourcerect;
		getOwner()->getSourceRect(&sourcerect);
		XMLToString(&mvar_sourcerect_rect_t,sourcerect.top);
		XMLToString(&mvar_sourcerect_rect_l,sourcerect.left);
		XMLToString(&mvar_sourcerect_rect_b,sourcerect.bottom);
		XMLToString(&mvar_sourcerect_rect_r,sourcerect.right);
	}
	mvar_sourcecenter_sourcerect = true;
	XMLToString(&mvar_sourcecenter_point_x,getOwner()->getSourceCenter().x);
	XMLToString(&mvar_sourcecenter_point_y,getOwner()->getSourceCenter().y);

	mvar_setparentactionrect = mvar_name=="texture_main";
	XMLToString(&mvar_setparentactionrect_scale_x,1.0f);
	XMLToString(&mvar_setparentactionrect_scale_y,1.0f);
}

void uiUserData_effect_texturedraw::editor_onSelection() {
	uiUserData_effect::editor_onSelection();
	g_app.mdlg.propgridAddStringItem(&mvar_file,meditorsection,"File",mvar_file);
	g_app.mdlg.propgridAddStringItem(&mvar_blendcolor,meditorsection,"Blend Color",mvar_blendcolor);
	g_app.mdlg.propgridAddBoolItem(&mvar_sourcerect_texture,meditorsection,"Base Source Rect on Texture Size",mvar_sourcerect_texture);
	g_app.mdlg.propgridAddStringItem(&mvar_sourcerect_rect_t,meditorsection,"Source Rect.top",mvar_sourcerect_rect_t);
	g_app.mdlg.propgridAddStringItem(&mvar_sourcerect_rect_l,meditorsection,"Source Rect.left",mvar_sourcerect_rect_l);
	g_app.mdlg.propgridAddStringItem(&mvar_sourcerect_rect_b,meditorsection,"Source Rect.bottom",mvar_sourcerect_rect_b);
	g_app.mdlg.propgridAddStringItem(&mvar_sourcerect_rect_r,meditorsection,"Source Rect.right",mvar_sourcerect_rect_r);
	g_app.mdlg.propgridAddBoolItem(&mvar_sourcecenter_sourcerect,meditorsection,"Base Source Center on Source Rect",mvar_sourcecenter_sourcerect);
	g_app.mdlg.propgridAddStringItem(&mvar_sourcecenter_point_x,meditorsection,"Source Center.x",mvar_sourcecenter_point_x);
	g_app.mdlg.propgridAddStringItem(&mvar_sourcecenter_point_y,meditorsection,"Source Center.y",mvar_sourcecenter_point_y);
	g_app.mdlg.propgridAddBoolItem(&mvar_setparentactionrect,meditorsection,"Set Parent Action Size to Source Rect",mvar_setparentactionrect);
	g_app.mdlg.propgridAddStringItem(&mvar_setparentactionrect_scale_x,meditorsection,"Set Scale.x",mvar_setparentactionrect_scale_x);
	g_app.mdlg.propgridAddStringItem(&mvar_setparentactionrect_scale_y,meditorsection,"Set Scale.y",mvar_setparentactionrect_scale_y);
}

void uiUserData_effect_texturedraw::editor_onDeselection() {
	uiUserData_effect::editor_onDeselection();

	g_app.mdlg.propgridRetrieveValue(&mvar_file);
	g_app.mdlg.propgridRetrieveValue(&mvar_blendcolor);
	g_app.mdlg.propgridRetrieveValue(&mvar_sourcerect_texture);
	g_app.mdlg.propgridRetrieveValue(&mvar_sourcerect_rect_t);
	g_app.mdlg.propgridRetrieveValue(&mvar_sourcerect_rect_l);
	g_app.mdlg.propgridRetrieveValue(&mvar_sourcerect_rect_b);
	g_app.mdlg.propgridRetrieveValue(&mvar_sourcerect_rect_r);
	g_app.mdlg.propgridRetrieveValue(&mvar_sourcecenter_sourcerect);
	g_app.mdlg.propgridRetrieveValue(&mvar_sourcecenter_point_x);
	g_app.mdlg.propgridRetrieveValue(&mvar_sourcecenter_point_y);
	g_app.mdlg.propgridRetrieveValue(&mvar_setparentactionrect);
	g_app.mdlg.propgridRetrieveValue(&mvar_setparentactionrect_scale_x);
	g_app.mdlg.propgridRetrieveValue(&mvar_setparentactionrect_scale_y);
}

void uiUserData_effect_texturedraw::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_effect::xml_onLoad(node);

	mvar_setparentactionrect = false;

	for(rapidxml::xml_node<> *node_i = node->first_node(); node_i; node_i = node_i->next_sibling()) {
		if(strcmp(node_i->name(),"file")==0) {
			mvar_file = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"blendcolor")==0) {
			mvar_blendcolor = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"sourcerect")==0) {
			if(strcmp(node_i->first_attribute("mode")->value(),"texture")==0) {
				mvar_sourcerect_texture = true;
			} else if(strcmp(node_i->first_attribute("mode")->value(),"rect")==0) {
				mvar_sourcerect_texture = false;
				mvar_sourcerect_rect_t = XMLGetNodeValueRemoveScript(node_i->first_node("t"));
				mvar_sourcerect_rect_l = XMLGetNodeValueRemoveScript(node_i->first_node("l"));
				mvar_sourcerect_rect_b = XMLGetNodeValueRemoveScript(node_i->first_node("b"));
				mvar_sourcerect_rect_r = XMLGetNodeValueRemoveScript(node_i->first_node("r"));
			}
		} else if(strcmp(node_i->name(),"sourcecenter")==0) {
			if(strcmp(node_i->first_attribute("mode")->value(),"sourcerect")==0) {
				mvar_sourcecenter_sourcerect = true;
			} else if(strcmp(node_i->first_attribute("mode")->value(),"pos")==0) {
				mvar_sourcecenter_sourcerect = false;
				mvar_sourcecenter_point_x = XMLGetNodeValueRemoveScript(node_i->first_node("x"));
				mvar_sourcecenter_point_y = XMLGetNodeValueRemoveScript(node_i->first_node("y"));
			}
		} else if(strcmp(node_i->name(),"setparentactionrect")==0) {
			mvar_setparentactionrect = true;
			mvar_setparentactionrect_scale_x = XMLGetNodeValueRemoveScript(node_i->first_node("x"));
			mvar_setparentactionrect_scale_y = XMLGetNodeValueRemoveScript(node_i->first_node("y"));
		}
	}
}

void uiUserData_effect_texturedraw::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	XMLAppendScriptNode(doc,node,"file",mvar_file,true);
	XMLAppendScriptNode(doc,node,"blendcolor",mvar_blendcolor);
	{
		auto n = doc->allocate_node(rapidxml::node_element,"sourcerect",0);
		node->append_node(n);
		n->append_attribute(doc->allocate_attribute("mode",mvar_sourcerect_texture ? "texture" : "rect"));
		if(!mvar_sourcerect_texture) {
			XMLAppendScriptNode(doc,n,"t",mvar_sourcerect_rect_t);
			XMLAppendScriptNode(doc,n,"l",mvar_sourcerect_rect_l);
			XMLAppendScriptNode(doc,n,"b",mvar_sourcerect_rect_b);
			XMLAppendScriptNode(doc,n,"r",mvar_sourcerect_rect_r);
		}
	}
	{
		auto n = doc->allocate_node(rapidxml::node_element,"sourcecenter",0);
		node->append_node(n);
		n->append_attribute(doc->allocate_attribute("mode",mvar_sourcecenter_sourcerect ? "sourcerect" : "pos"));
		if(!mvar_sourcecenter_sourcerect) {
			XMLAppendScriptNode(doc,n,"x",mvar_sourcecenter_point_x);
			XMLAppendScriptNode(doc,n,"y",mvar_sourcecenter_point_y);
		}
	}
	if(mvar_setparentactionrect) {
		auto n = doc->allocate_node(rapidxml::node_element,"setparentactionrect",0);
		node->append_node(n);
		XMLAppendScriptNode(doc,n,"x",mvar_setparentactionrect_scale_x);
		XMLAppendScriptNode(doc,n,"y",mvar_setparentactionrect_scale_y);
	}
	uiUserData_effect::xml_save(doc,node);
}

uiUserData_effect_transition_base::uiUserData_effect_transition_base(IHDXUIIActor *owner) : uiUserData_effect(owner) {
	XMLToString(&mvar_runflags,getOwner()->getRunFlags());
	XMLToString(&mvar_duration,getOwner()->getDesiredTransitionTime());
}

void uiUserData_effect_transition_base::editor_onSelection() {
	uiUserData_effect::editor_onSelection();
	g_app.mdlg.propgridAddStringItem(&mvar_runflags,meditorsection,"Run Flags",mvar_runflags);
	g_app.mdlg.propgridAddStringItem(&mvar_duration,meditorsection,"Duration",mvar_duration);
}

void uiUserData_effect_transition_base::editor_onDeselection() {
	uiUserData_effect::editor_onDeselection();

	g_app.mdlg.propgridRetrieveValue(&mvar_runflags);
	g_app.mdlg.propgridRetrieveValue(&mvar_duration);
}

void uiUserData_effect_transition_base::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_effect::xml_onLoad(node);

	for(rapidxml::xml_node<> *node_i = node->first_node(); node_i; node_i = node_i->next_sibling()) {
		if(strcmp(node_i->name(),"runflags")==0) {
			mvar_runflags = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"duration")==0) {
			mvar_duration = XMLGetNodeValueRemoveScript(node_i);
		}
	}
}

void uiUserData_effect_transition_base::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	XMLAppendScriptNode(doc,node,"runflags",mvar_runflags);
	XMLAppendScriptNode(doc,node,"duration",mvar_duration);
	uiUserData_effect::xml_save(doc,node);
}

uiUserData_effect_varcontrol_base::uiUserData_effect_varcontrol_base(IHDXUIIActor *owner,const std::string &variable) : uiUserData_effect(owner) {
	mvar_variable = variable;
}

void uiUserData_effect_varcontrol_base::editor_onSelection() {
	uiUserData_effect::editor_onSelection();
}

void uiUserData_effect_varcontrol_base::editor_onDeselection() {
	uiUserData_effect::editor_onDeselection();
}

void uiUserData_effect_varcontrol_base::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_effect::xml_onLoad(node);
}

void uiUserData_effect_varcontrol_base::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	node->append_attribute(doc->allocate_attribute("variable",mvar_variable.c_str()));
	uiUserData_effect::xml_save(doc,node);
}

uiUserData_effect_transition_lerp::uiUserData_effect_transition_lerp(IHDXUIIActor *owner,const std::string &variable) : uiUserData_effect_varcontrol_base(owner,variable),uiUserData_effect_transition_base(owner),uiUserData_effect(owner) {
	if(mvar_variable == "scale" || mvar_variable == "position") {
		HDXUIElementEffect_Transition_LERP<HDXVector2> *cowner = (HDXUIElementEffect_Transition_LERP<HDXVector2>*)getOwner();
		XMLToString(&mvar_target_vector_x,cowner->getTarget().x);
		XMLToString(&mvar_target_vector_y,cowner->getTarget().y);
	} else if(mvar_variable == "rotation") {
		HDXUIElementEffect_Transition_LERP<float> *cowner = (HDXUIElementEffect_Transition_LERP<float>*)getOwner();
		XMLToString(&mvar_target_scalar,cowner->getTarget());
	}
}

void uiUserData_effect_transition_lerp::editor_onSelection() {
	uiUserData_effect_transition_base::editor_onSelection();
	uiUserData_effect_varcontrol_base::editor_onSelection();
	if(mvar_variable == "scale" || mvar_variable == "position") {
		g_app.mdlg.propgridAddStringItem(&mvar_target_vector_x,meditorsection,"Target.x",mvar_target_vector_x);
		g_app.mdlg.propgridAddStringItem(&mvar_target_vector_y,meditorsection,"Target.y",mvar_target_vector_y);
	} else if(mvar_variable == "rotation") {
		g_app.mdlg.propgridAddStringItem(&mvar_target_scalar,meditorsection,"Target",mvar_target_scalar);
	}
}

void uiUserData_effect_transition_lerp::editor_onDeselection() {
	uiUserData_effect_transition_base::editor_onDeselection();
	uiUserData_effect_varcontrol_base::editor_onDeselection();

	if(mvar_variable == "scale" || mvar_variable == "position") {
		g_app.mdlg.propgridRetrieveValue(&mvar_target_vector_x);
		g_app.mdlg.propgridRetrieveValue(&mvar_target_vector_y);
	} else if(mvar_variable == "rotation") {
		g_app.mdlg.propgridRetrieveValue(&mvar_target_scalar);
	}
}

void uiUserData_effect_transition_lerp::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_effect_transition_base::xml_onLoad(node);
	uiUserData_effect_varcontrol_base::xml_onLoad(node);

	for(rapidxml::xml_node<> *node_i = node->first_node(); node_i; node_i = node_i->next_sibling()) {
		if(strcmp(node_i->name(),"target")==0) {
			if(mvar_variable == "scale" || mvar_variable == "position") {
				mvar_target_vector_x = XMLGetNodeValueRemoveScript(node_i->first_node("x"));
				mvar_target_vector_y = XMLGetNodeValueRemoveScript(node_i->first_node("y"));
			} else if(mvar_variable == "rotation") {
				mvar_target_scalar = XMLGetNodeValueRemoveScript(node_i);
			}
		}
	}
}

void uiUserData_effect_transition_lerp::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
	//from uiUserData_effect_varcontrol_base, actually calling varcontrol_base::xml_save() would cause the base classes xml_save() to be called twice
	node->append_attribute(doc->allocate_attribute("variable",mvar_variable.c_str()));

	{
		if(mvar_variable == "scale" || mvar_variable == "position") {
			auto n = doc->allocate_node(rapidxml::node_element,"target",0);
			node->append_node(n);
			XMLAppendScriptNode(doc,n,"x",mvar_target_vector_x);
			XMLAppendScriptNode(doc,n,"y",mvar_target_vector_y);
		} else if(mvar_variable == "rotation") {
			XMLAppendScriptNode(doc,node,"target",mvar_target_scalar);
		}
	}
	uiUserData_effect_transition_base::xml_save(doc,node);
}

uiUserData_effect_varcontrol_oscillate::uiUserData_effect_varcontrol_oscillate(IHDXUIIActor *owner,const std::string &variable) : uiUserData_effect_varcontrol_base(owner,variable),uiUserData_effect(owner) {
	if(mvar_variable == "scale" || mvar_variable == "position") {
		HDXUIElementEffect_Var_Oscillate<HDXVector2> *cowner = dynamic_cast<HDXUIElementEffect_Var_Oscillate<HDXVector2>*>(getOwner());
		XMLToString(&mvar_wavelength_vector_x,cowner->getWavelength().x);
		XMLToString(&mvar_wavelength_vector_y,cowner->getWavelength().y);

		XMLToString(&mvar_frequency,cowner->getFrequency());
		XMLToString(&mvar_phaseshift,cowner->getFrquencyCounter()/cowner->getFrequency());
	} else if(mvar_variable == "rotation") {
		HDXUIElementEffect_Var_Oscillate<float> *cowner = dynamic_cast<HDXUIElementEffect_Var_Oscillate<float>*>(getOwner());
		XMLToString(&mvar_wavelength_scalar,cowner->getWavelength());

		XMLToString(&mvar_frequency,cowner->getFrequency());
		XMLToString(&mvar_phaseshift,cowner->getFrquencyCounter()/cowner->getFrequency());
	}
}

void uiUserData_effect_varcontrol_oscillate::editor_onSelection() {
	uiUserData_effect_varcontrol_base::editor_onSelection();
	if(mvar_variable == "scale" || mvar_variable == "position") {
		g_app.mdlg.propgridAddStringItem(&mvar_wavelength_vector_x,meditorsection,"Wavelength.x",mvar_wavelength_vector_x);
		g_app.mdlg.propgridAddStringItem(&mvar_wavelength_vector_y,meditorsection,"Wavelength.y",mvar_wavelength_vector_y);
	} else if(mvar_variable == "rotation") {
		g_app.mdlg.propgridAddStringItem(&mvar_wavelength_scalar,meditorsection,"Wavelength",mvar_wavelength_scalar);
	}
	g_app.mdlg.propgridAddStringItem(&mvar_frequency,meditorsection,"Frequency",mvar_frequency);
	g_app.mdlg.propgridAddStringItem(&mvar_phaseshift,meditorsection,"Phase Shift",mvar_phaseshift);
}

void uiUserData_effect_varcontrol_oscillate::editor_onDeselection() {
	uiUserData_effect_varcontrol_base::editor_onDeselection();

	if(mvar_variable == "scale" || mvar_variable == "position") {
		g_app.mdlg.propgridRetrieveValue(&mvar_wavelength_vector_x);
		g_app.mdlg.propgridRetrieveValue(&mvar_wavelength_vector_y);
	} else if(mvar_variable == "rotation") {
		g_app.mdlg.propgridRetrieveValue(&mvar_wavelength_scalar);
	}
	g_app.mdlg.propgridRetrieveValue(&mvar_frequency);
	g_app.mdlg.propgridRetrieveValue(&mvar_phaseshift);
}

void uiUserData_effect_varcontrol_oscillate::xml_onLoad(rapidxml::xml_node<> *node) {
	uiUserData_effect_varcontrol_base::xml_onLoad(node);

	for(rapidxml::xml_node<> *node_i = node->first_node(); node_i; node_i = node_i->next_sibling()) {
		if(strcmp(node_i->name(),"wavelength")==0) {
			if(mvar_variable == "scale" || mvar_variable == "position") {
				mvar_wavelength_vector_x = XMLGetNodeValueRemoveScript(node_i->first_node("x"));
				mvar_wavelength_vector_y = XMLGetNodeValueRemoveScript(node_i->first_node("y"));
			} else if(mvar_variable == "rotation") {
				mvar_wavelength_scalar = XMLGetNodeValueRemoveScript(node_i);
			}
		} else if(strcmp(node_i->name(),"frequency")==0) {
			mvar_frequency = XMLGetNodeValueRemoveScript(node_i);
		} else if(strcmp(node_i->name(),"phaseshift")==0) {
			mvar_phaseshift = XMLGetNodeValueRemoveScript(node_i);
		}
	}
}

void uiUserData_effect_varcontrol_oscillate::xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node) {
		{
			if(mvar_variable == "scale" || mvar_variable == "position") {
				auto n = doc->allocate_node(rapidxml::node_element,"wavelength",0);
				node->append_node(n);
				XMLAppendScriptNode(doc,n,"x",mvar_wavelength_vector_x);
				XMLAppendScriptNode(doc,n,"y",mvar_wavelength_vector_y);
			} else if(mvar_variable == "rotation") {
				XMLAppendScriptNode(doc,node,"wavelength",mvar_wavelength_scalar);
			}
		}

	XMLAppendScriptNode(doc,node,"frequency",mvar_frequency);
	XMLAppendScriptNode(doc,node,"phaseshift",mvar_phaseshift);
	uiUserData_effect_varcontrol_base::xml_save(doc,node);
}

void uiActorOnCreate(IHDXUIIActor *actor,const std::string &data) {
	std::string atype = actor->xml_getTypeName();

	uiUserData_base *ud = 0;

	if(atype=="menu") {
		ud = dynamic_cast<uiUserData_base*>(new uiUserData_element_menu(actor));
	} else if(atype=="alignment") {
		ud = dynamic_cast<uiUserData_base*>(new uiUserData_element_alignment(actor));
	} else if(atype=="button") {
		ud = dynamic_cast<uiUserData_base*>(new uiUserData_element_button(actor));
	} else if(atype=="checkbox") {
		ud = dynamic_cast<uiUserData_base*>(new uiUserData_element_checkbox(actor));
	} else if(atype=="slider") {
		ud = dynamic_cast<uiUserData_base*>(new uiUserData_element_slider(actor));
	} else if(atype=="draw_text") {
		ud = dynamic_cast<uiUserData_base*>(new uiUserData_effect_textdraw(actor));
	} else if(atype=="draw_texture") {
		ud = dynamic_cast<uiUserData_base*>(new uiUserData_effect_texturedraw(actor));
	} else if(atype=="transition_lerp") {
		ud = dynamic_cast<uiUserData_base*>(new uiUserData_effect_transition_lerp(actor,data));
	} else if(atype=="var_oscillate") {
		ud = dynamic_cast<uiUserData_base*>(new uiUserData_effect_varcontrol_oscillate(actor,data));
	}

	if(ud) {
		actor->setUserData(ud);
	}
}

std::string uiGenerateValidName(const std::string &base,std::function<bool(const std::string &test)> func_isvalid) {
	for(unsigned int i = 0; i < 1000; i++) {
		std::string test = base+std::to_string(i);
		if(func_isvalid(test)) return test;
	}
	return "";
}

std::string XMLGetNodeValueRemoveScript(rapidxml::xml_node<> *xmlnode) {
	if(strcmp(xmlnode->first_node()->name(),"script")==0) return xmlnode->first_node()->value();
	return xmlnode->value();
}

void XMLAppendScriptNode(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *root,const std::string &name,const std::string &script,bool string) {
	rapidxml::xml_node<> *n = doc->allocate_node(rapidxml::node_element,doc->allocate_string(name.c_str()));
	root->append_node(n);
	rapidxml::xml_node<> *ns = doc->allocate_node(rapidxml::node_element,"script",doc->allocate_string(script.c_str()));
	n->append_node(ns);
	if(string) ns->append_attribute(doc->allocate_attribute("type","string"));
}