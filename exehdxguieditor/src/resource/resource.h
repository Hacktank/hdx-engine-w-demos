//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by wnd.rc
//
#define IDD_DIALOG1                     101
#define IDD_WND_MAIN                    101
#define IDD_WND_NEWEVENT                102
#define IDC_PB_HDX                      1001
#define IDC_TREE_NEWELEMENT             1002
#define IDC_TREE_NEWEFFECT              1003
#define IDC_TREE_ELEMENTS               1004
#define IDC_BUTTON_LOAD                 1005
#define IDC_BUTTON_SAVE                 1006
#define IDC_PB_PROPERTIES               1007
#define IDC_BUTTON_ADDNEWELEMENT        1008
#define IDC_BUTTON_ADDNEWEFFECT         1009
#define IDC_BUTTON_APPLYCHANGES         1010
#define IDC_BUTTON_DELETESELECTEDITEM   1011
#define IDC_TREE_EVENT                  1012
#define IDC_TREE_ACTION                 1013
#define IDC_BUTTON1                     1013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
