#pragma once

#include "resource.h"

#include <PropertyGrid/PropertyGrid.h>
#ifndef _DEBUG
#pragma comment(lib,"PropertyGrid.lib")
#else
#pragma comment(lib,"PropertyGridd.lib")
#endif

#include "propgrid_button.h"

#include "hdx/ui/hdx9ui.h"
#include "hdx/ui/element/hdx9ui_element_alignment.h"
#include "hdx/ui/element/hdx9ui_element_button.h"
#include "hdx/ui/element/hdx9ui_element_checkbox.h"
#include "hdx/ui/element/hdx9ui_element_slider.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_textdraw.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_texturedraw.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_transition_lerp.h"
#include "hdx/ui/effect/hdx9ui_elementeffect_var_oscillate.h"

#include <unordered_map>
#include <set>

struct uiUserData_base {
public:
	uiUserData_base(IHDXUIIActor *owner);
	virtual ~uiUserData_base() {};

	IHDXUIIActor* getOwner() { return mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	IHDXUIIActor *mowner;
	std::string mvar_name;
	std::string mvar_type;
};

struct uiUserData_element : public uiUserData_base {
public:
	struct uiEvent {
	public:
		typedef std::vector<std::pair<std::string,std::string>> ArgCont;

		std::string mvar_event;
		std::string mvar_action;
		ArgCont marguments;
	};

	uiUserData_element(IHDXUIIActor *owner);
	virtual ~uiUserData_element() {};

	HDXUIElementBase* getOwner() { return (HDXUIElementBase*)mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

	uiEvent* eventAdd(const std::string &evt,const std::string &action,const uiEvent::ArgCont &args);
	void eventRemove(uiEvent *evt);
	void eventRemoveAll();

protected:
	std::string mvar_position_x;
	std::string mvar_position_y;
	std::string mvar_scale_x;
	std::string mvar_scale_y;
	std::string mvar_rotation;
	std::string mvar_blendcolor;
	std::string mvar_enabled;
	std::string mvar_active;
	std::string mvar_actionsize_x;
	std::string mvar_actionsize_y;
	std::set<uiEvent*> mevents;
};

struct uiUserData_element_menu : public uiUserData_element {
public:
	uiUserData_element_menu(IHDXUIIActor *owner);
	virtual ~uiUserData_element_menu() {};

	HDXUIMenu* getOwner() { return (HDXUIMenu*)mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);
};

struct uiUserData_element_alignment : public uiUserData_element {
public:
	uiUserData_element_alignment(IHDXUIIActor *owner);
	virtual ~uiUserData_element_alignment() {};

	HDXUIElement_Alignment* getOwner() { return (HDXUIElement_Alignment*)mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	std::string mvar_posflags;
	std::string mvar_padding;
	std::string mvar_offset_x;
	std::string mvar_offset_y;
	std::string mvar_direction_x;
	std::string mvar_direction_y;
};

struct uiUserData_element_button : public uiUserData_element {
public:
	uiUserData_element_button(IHDXUIIActor *owner);
	virtual ~uiUserData_element_button() {};

	HDXUIElement_Button* getOwner() { return (HDXUIElement_Button*)mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);
};

struct uiUserData_element_checkbox : public uiUserData_element {
public:
	uiUserData_element_checkbox(IHDXUIIActor *owner);
	virtual ~uiUserData_element_checkbox() {};

	HDXUIElement_Checkbox* getOwner() { return (HDXUIElement_Checkbox*)mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	std::string mvar_checked;
};

struct uiUserData_element_slider : public uiUserData_element {
public:
	uiUserData_element_slider(IHDXUIIActor *owner);
	virtual ~uiUserData_element_slider() {};

	HDXUIElement_Slider* getOwner() { return (HDXUIElement_Slider*)mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	std::string mvar_texture_handle;
	std::string mvar_value;
};

struct uiUserData_effect : public uiUserData_base {
public:
	uiUserData_effect(IHDXUIIActor *owner);
	virtual ~uiUserData_effect() {};

	HDXUIElementEffect_Base* getOwner() { return (HDXUIElementEffect_Base*)mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	HSECTION meditorsection;
};

struct uiUserData_effect_textdraw : public uiUserData_effect {
public:
	uiUserData_effect_textdraw(IHDXUIIActor *owner);
	virtual ~uiUserData_effect_textdraw() {};

	HDXUIElementEffect_TextDraw* getOwner() { return (HDXUIElementEffect_TextDraw*)mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	std::string mvar_font;
	std::string mvar_color;
	std::string mvar_drawflags;
	bool mvar_drawrect_actionsize;
	std::string mvar_drawrect_rect_t;
	std::string mvar_drawrect_rect_l;
	std::string mvar_drawrect_rect_b;
	std::string mvar_drawrect_rect_r;
	std::string mvar_text;
};

struct uiUserData_effect_texturedraw : public uiUserData_effect {
public:
	uiUserData_effect_texturedraw(IHDXUIIActor *owner);
	virtual ~uiUserData_effect_texturedraw() {};

	HDXUIElementEffect_TextureDraw* getOwner() { return (HDXUIElementEffect_TextureDraw*)mowner; }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	std::string mvar_file;
	std::string mvar_blendcolor;
	bool mvar_sourcerect_texture;
	std::string mvar_sourcerect_rect_t;
	std::string mvar_sourcerect_rect_l;
	std::string mvar_sourcerect_rect_b;
	std::string mvar_sourcerect_rect_r;
	bool mvar_sourcecenter_sourcerect;
	std::string mvar_sourcecenter_point_x;
	std::string mvar_sourcecenter_point_y;
	bool mvar_setparentactionrect;
	std::string mvar_setparentactionrect_scale_x;
	std::string mvar_setparentactionrect_scale_y;
};

struct uiUserData_effect_transition_base : virtual public uiUserData_effect {
public:
	uiUserData_effect_transition_base(IHDXUIIActor *owner);
	virtual ~uiUserData_effect_transition_base() {};

	HDXUIElementEffect_Transition_Base* getOwner() { return dynamic_cast<HDXUIElementEffect_Transition_Base*>(mowner); }

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	std::string mvar_runflags;
	std::string mvar_duration;
};

struct uiUserData_effect_varcontrol_base : virtual public uiUserData_effect {
public:
	uiUserData_effect_varcontrol_base(IHDXUIIActor *owner,const std::string &variable);
	virtual ~uiUserData_effect_varcontrol_base() {};

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	std::string mvar_variable;
};

struct uiUserData_effect_transition_lerp : public uiUserData_effect_transition_base,public uiUserData_effect_varcontrol_base {
public:
	uiUserData_effect_transition_lerp(IHDXUIIActor *owner,const std::string &variable);
	virtual ~uiUserData_effect_transition_lerp() {};

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	std::string mvar_target_vector_x;
	std::string mvar_target_vector_y;
	std::string mvar_target_scalar;
};

struct uiUserData_effect_varcontrol_oscillate : public uiUserData_effect_varcontrol_base {
public:
	uiUserData_effect_varcontrol_oscillate(IHDXUIIActor *owner,const std::string &variable);
	virtual ~uiUserData_effect_varcontrol_oscillate() {};

	virtual void editor_onSelection();
	virtual void editor_onDeselection();

	virtual void xml_onLoad(rapidxml::xml_node<> *node);
	virtual void xml_save(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *node);

protected:
	std::string mvar_wavelength_vector_x;
	std::string mvar_wavelength_vector_y;
	std::string mvar_wavelength_scalar;
	std::string mvar_frequency;
	std::string mvar_phaseshift;
};

std::string XMLGetNodeValueRemoveScript(rapidxml::xml_node<> *xmlnode);
void XMLAppendScriptNode(rapidxml::xml_document<> *doc,rapidxml::xml_node<> *root,const std::string &name,const std::string &script,bool string = false);

void uiActorOnCreate(IHDXUIIActor *actor,const std::string &data = "");

std::string uiGenerateValidName(const std::string &base,std::function<bool(const std::string &test)> func_isvalid);

// DLGMain dialog

class DLGMain : public CDialog {
	DECLARE_DYNAMIC(DLGMain)

	enum {
		TIMER_UPDATE,
		TIMER_ELEMENTTREECLICK,
	};

public:
	CPropertyGrid mpropgrid;
	std::unordered_map<std::string*,HITEM> mpropgrid_itemlist_string;
	std::unordered_map<bool*,HITEM> mpropgrid_itemlist_bool;
	std::unordered_map<propgrid_button*,HITEM> mpropgrid_itemlist_button;

	std::unordered_map<HDXUIElementBase*,HTREEITEM> mtreeelements;

	HDXUIMenuMap mmenu_map;
	HDXUIElementBase *mcurselection;

	DLGMain(CWnd* pParent = NULL);   // standard constructor
	virtual ~DLGMain();

	// Dialog Data
	enum { IDD = IDD_WND_MAIN };

	void loadXML(const std::string &fname,const std::string &startmenu,bool dotransition);
	void saveXML(const std::string &fname);

	void selectElement(HDXUIElementBase *element,bool dotransition = true);

	void updateElementTree();

	void propgridClear();
	HSECTION propgridAddSection(const std::string &name);
	void propgridRemoveItem(HITEM itm);
	HITEM propgridAddTextItem(std::string *var,HSECTION section,const std::string &item,const std::string &defvalue = "",bool editable = true);
	HITEM propgridAddStringItem(std::string *var,HSECTION section,const std::string &item,const std::string &defvalue = "",bool editable = true);
	HITEM propgridAddBoolItem(bool *var,HSECTION section,const std::string &item,bool defvalue = "",bool editable = true);
	HITEM propgridAddButtonItem(HSECTION section,const std::string &item,COLORREF color,std::function<void(void)> func);
	void propgridRetrieveValue(std::string *ptr);
	void propgridRetrieveValue(bool *ptr);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT evtid);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMClickTreeElements(NMHDR *pNMHDR,LRESULT *pResult);
	afx_msg void OnBnClickedButtonLoad();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonAddnewelement();
	afx_msg void OnBnClickedButtonAddneweffect();
	afx_msg void OnBnClickedButtonApplychanges();
	afx_msg void OnBnClickedButtonDeleteSelectedElement();
};
