#pragma once

#include "mfc.h"

#include "dlgmain.h"

class MainApp : public CWinApp {
public:
	DLGMain mdlg;

	virtual BOOL InitInstance();
	BOOL ExitInstance();
};

extern MainApp g_app;
