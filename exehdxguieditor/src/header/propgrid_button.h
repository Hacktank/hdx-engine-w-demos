#pragma once

#include <PropertyGrid/PropertyGrid.h>
#include <PropertyGrid/CustomItem.h>

#include <functional>

class propgrid_button : public ICustomItem {
public:
	std::function<void(void)> mfunc_onclick;
	COLORREF mcolor;

	propgrid_button(void);
	~propgrid_button(void);

	virtual CPropertyGrid::EEditMode GetEditMode();
	virtual void DrawItem(CDC& dc,CRect rc,bool focused);
	virtual bool OnLButtonDown(CRect rc,CPoint pt);
	virtual void OnLButtonUp(CRect rc,CPoint pt);
};