#pragma once

#include "resource.h"

#include <string>

// dlgnewevent dialog

class dlgnewevent : public CDialog {
	DECLARE_DYNAMIC(dlgnewevent)

public:
	dlgnewevent(CWnd* pParent = NULL);   // standard constructor
	virtual ~dlgnewevent();

	// Dialog Data
	enum { IDD = IDD_WND_NEWEVENT };

	std::string mchoice_event;
	std::string mchoice_action;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTvnSelchangedTreeEvent(NMHDR *pNMHDR,LRESULT *pResult);
	afx_msg void OnTvnSelchangedTreeAction(NMHDR *pNMHDR,LRESULT *pResult);
};
